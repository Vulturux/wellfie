<?php

use App\Controllers\BaseController;
use App\Services\UserScanService;
use App\Services\CompanyScanService;
use App\Services\InviteService;
use Illuminate\Support\Facades\Validator;

class DashboardController extends BaseController
{

    protected $layout = 'layoutsWerkgever.master';
    protected $userScanService;
    protected $inviteService;
    protected $companyScanService;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function __construct(UserScanService $userScanService, InviteService $inviteService, CompanyScanService $companyScanService)
    {
        $this->userScanService = $userScanService;
        $this->inviteService = $inviteService;
        $this->companyScanService = $companyScanService;
    }

    public function index()
    {
        $user = Auth::user();

        $userScan = $this->userScanService->getLatestUserScan($user->id);
        $companyScans = $this->companyScanService->getAllScansByCompany($user->company_id);
        $currentCompanyScan = $this->companyScanService->getLatestByCompanyId($user->company_id);
        $invitedEmployees = $this->inviteService->getInvitedEmployeesByCompanyId($user->company_id, $userScan->scan_id);
        $completedScans = $this->userScanService->getCompletedUserScansForCurrentScan($user->company_id, $userScan->scan_id);

        if($userScan->questionnaire_position === 5) $this->userScanService->updateUserScan();
        $data = [
            'navPos' => $userScan->questionnaire_position,
            'title' => 'Dashboard',
            'companyScans' => $companyScans,
            'current_scan' => $currentCompanyScan,
            'invitedEmployees' => $invitedEmployees,
            'completed_scans' => $completedScans
        ];

        $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
        $this->layout->content = View::make('dashboard.index', $data);
    }

    public function werknemersuitnodigen()
    {
        $userScan = $this->userScanService->getLatestUserScan(Auth::user()->id);
        $invitedEmployees = $this->inviteService->getInvitedEmployeesByCompanyId(\Auth::user()->company_id, $userScan->scan_id);

        $data = [
            'title' => 'Werknemers Uitnodigen',
            'navPos' => $userScan->questionnaire_position,
            'scan_id' => $userScan->scan_id,
            'current_scan_id' => $userScan->scan_id,
            'position' => $userScan->questionnaire_position,
            'werknemers' => $invitedEmployees,
            'defaultMailMessage' => \Lang::get('messages.defaultInviteMailMessage'),
        ];

        // Render layouts.
        $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
        $this->layout->content = View::make('werkgevers.werknemersuitnodigen', $data);
    }

    public function postinvite()
    {

        $input = Input::except('_token');
        $subject = $input['onderwerp'];
        $message = $input['bericht'];

        foreach ($input['email'] as $key => $email) {
            $validation = Validator::make(
                ['email' => $email, 'subject' => $subject, 'message' => $message],
                [
                    'email' => 'required|email',
                    'subject' => 'required',
                    'message' => 'required'
                ]
            );

            $error = null;
            if ($validation->passes()) {
                $this->inviteService->createandSendInvite($email, $subject, $message, 'werknemer');
            } else {
                $error = "Validatiefout, is de data die u ingaf correct?";
            }
        }

        $success = isset($errors) ?: "De uitnodigingen werden succesvol verstuurd";
        return Redirect::to('werkgevers/uitgenodigdewerknemers')->withError($error)->withSucces($success);
    }

    public function uitgenodigdewerknemers()
    {
        $queryScanId = Request::get('scan');

        $latestScan = $this->userScanService->getLatestUserScan(Auth::user()->id);
        $userScan =
            isset($queryScanId)
                ? $this->userScanService->getUserScanByUserId(Auth::user()->id, $queryScanId)
                : $latestScan;

        $invitedEmployees = $this->inviteService->getInvitedEmployeesByCompanyId(\Auth::user()->company_id, $userScan->scan_id);


        $data = [
            'title' => 'Uitgenodigde werknemers',
            'navPos' => $userScan->questionnaire_position,
            'scan_id' => $latestScan->scan_id,
            'current_scan_id' => $userScan->scan_id,
            'position' => $userScan->questionnaire_position,
            'werknemers' => $invitedEmployees,
            'defaultMailMessage' => \Lang::get('messages.defaultInviteMailMessage'),
        ];

        $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
        $this->layout->content = View::make('dashboard.uitgenodigdegebruikers', $data);
    }

    public function reinvite($inviteId)
    {
        $invitation = $this->inviteService->getInviteById($inviteId);

        $subject = Input::get('subject');
        $message = Input::get('message');

        if (isset($invitation)) {
            $validation = Validator::make(
                ['email' => $invitation->email, 'subject' => $subject, 'message' => $message],
                [
                    'email' => 'required|email',
                    'subject' => 'required',
                    'message' => 'required'
                ]
            );

            $this->inviteService->sendInvite($invitation->email, $subject, $message, $invitation->invitecode);
            $this->layout = '';
            echo( json_encode(['success' => 'gebruiker werd succesvol opnieuw uitgenodigd.']));
        } else {
            echo 'empty';
        }
    }

    public function postWerknemersuitnodigen()
    {
        $postedData = Input::all();
        $file = Input::file('excelfile');
        $mime = $file->getMimeType();
        $rules = array(
            'excelfile' => 'required|max:10000|mimes:xls,csv,xlsx,txt'
        );
        $validator = Validator::make($postedData, $rules);

        if ($validator->passes()) {
            // VALIDATION IS OK

            $upload_success = 0;
            $destinationPath = "";
            $emailList = array();
            $errorsList = array();

            if (!empty($file)) {

                // upload
//                $publicPath         = public_path();
                $destinationPath = storage_path() . '/uploads/contacts/';
                $extension = $file->getClientOriginalExtension();
                $rand = str_random(12);
                $filename = 'usr_' . Auth::user()->id . '_str=' . $rand . '.' . $extension;
                $upload_success = $file->move($destinationPath, $filename);

                if ($upload_success) {
                    // excel processing
                    /** @var \Maatwebsite\Excel\Readers\LaravelExcelReader $excelReader */
                    $excelReader = Excel::load($destinationPath . '/' . $filename, function ($reader) {
                    });
                    $collection = $excelReader->get();

                    //only the first tab
                    if ($collection instanceof \Maatwebsite\Excel\Collections\SheetCollection) {
                        $results = $collection->first()->toArray();
                    } elseif ($collection instanceof \Maatwebsite\Excel\Collections\RowCollection) {
                        $results = $collection->toArray();
                    }

                    foreach ($results as $key => $row) {

                        $data = ['email' => $row[0]];
                        /** @var \Illuminate\Validation\Validator $emailValidator */
                        $emailValidator = Validator::make($data, [
                            'email' => 'required|email'
                        ]);

                        if ($emailValidator->passes()) {
                            $emailList[] = $row[0];
                        } else {
                            $errorsList[] = [
                                'error' => $emailValidator->errors(),
                                'data' => $row,
                                'line' => $key + 2,
                            ];
                        }

                    }


                    return Response::json([
                        'errors' => $errorsList,
                        'emails' => $emailList,
                    ], 200);

                } else {
                    return Response::json('error: uploading file went wrong', 200);
                }

            } else {
                return Response::json('error: empty file', 200);

            }

        } else {
            // VALIDATION ERROR
            return Response::json('error: ongeldige file. Mime ' . $mime . ' is ongeldig', 200);
        }

    }

    public function startnewscan()
    {
        $currentCompanyScan = $this->companyScanService->getLatestByCompanyId(Auth::user()->company_id);

        //check if current companyscan is finished
        if($currentCompanyScan->scan_finished){
            return Redirect::to('werkgevers/intro');
        }else {
            return Redirect::to('werkgevers/dashboard');
        }
    }
}
