<?php

use App\Controllers\BaseController;
use App\Models\Question;
use App\Models\QuestionsTitle;

class QuestionsController extends BaseController  {


    protected $layout = 'layoutsSuperadmin.master';

    public function __construct()
    {
    }

    public function index()
    {

        $questions = Question::orderBy('id')->get();
        $questions = Question::paginate(5);

        $this->layout->content = View::make('questions.index', compact('questions'));

    }


    public function create()
    {

        $this->layout->content = View::make('questions.create');

    }


    public function store()
    {

        $input = Input::all();

        $validation = Validator::make($input, Question::$rules);

        if ($validation->passes()) {

            Question::create($input);
            return Redirect::route('questions.index');
        }

        return Redirect::route('questions.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }


    public function show($id)
    {
        $question = Question::find($id);
        if (is_null($question))
        {
            return Redirect::route('questions.index');
        }
        return View::make('questions.show', compact('question'));

    }

    public function edit($id)
    {
        $question = Question::find($id);
       // $questionTitles =  DB::table('questions_titles')->lists('title');
        $questionTitles = QuestionsTitle::lists('title', 'id');

        if (is_null($question))
        {
            return Redirect::route('questions.index');
        }
        return View::make('questions.edit', compact('question', 'questionTitles'));

    }

    public function update($id)
    {
        $input = Input::all();
        $validation = Validator::make($input, Question::$rules);
        if ($validation->passes())
        {
            $question = Question::find($id);
            $question->update($input);
            return Redirect::route('questions.show', $id);
        }
        return Redirect::route('questions.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');

    }

    public function destroy($id)
    {
        Question::find($id)->delete();
        return Redirect::route('questions.index');

    }


}
