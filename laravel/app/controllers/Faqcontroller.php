<?php

use App\Controllers\BaseController;
use App\Models\Faq;

class FaqController extends BaseController  {


    protected $layout = 'layoutsSuperadmin.master';

    public function __construct()
    {
    }

    public function index()
    {
        $data = array();
        $faq = Faq::orderBy('id')->get();
        $faq = Faq::paginate(50);

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('faq.index', compact('faq'));

    }


    public function create()
    {
        $data = array();

        $data['categories'] = Faq::getCategories();

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('faq.create', $data);

    }


    public function store()
    {
        $data = array();
        $input = Input::all();

        $validation = Validator::make($input, Faq::$rules);

        if ($validation->passes()) {

            Faq::create($input);
            return Redirect::route('superadmin.faq.index');
        }

        return Redirect::route('superadmin.faq.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }


    public function show($id)
    {
        $data = array();
        $faq = Faq::find($id);

        if (is_null($faq))
        {
            return Redirect::route('superadmin.faq.index');
        }
        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('faq.show', compact('faq'));

    }

    public function edit($id)
    {
        $data = array();
        $data['faq'] = Faq::find($id);

        $data['categories'] = Faq::getCategories();


        if (is_null($data['faq']))
        {
            return Redirect::route('superadmin.faq.index');
        }
        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('faq.edit', $data);

    }


    public function update($id)
    {
        $data = array();
        $input = Input::all();
        $validation = Validator::make($input, Faq::$rules);
        if ($validation->passes())
        {
            $question = Faq::find($id);
            $question->update($input);
            return Redirect::route('superadmin.faq.show', $id);
        }
        return Redirect::route('superadmin.faq.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');

    }

    public function destroy($id)
    {
        $data = array();
        Faq::find($id)->delete();
        return Redirect::route('superadmin.faq.index');

    }


}
