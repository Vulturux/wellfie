<?php
use App\Models\Company;
use App\Models\CompanyRapport;
use App\Models\Invite;
use App\Models\User;
use App\Services\WerknemersService;
use App\Services\InviteService;
use App\Services\UserService;
use App\Services\UserScanService;
use App\Services\CompanyScanService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;


/**
 * UsersController Class
 *
 * Implements actions regarding user management
 */
class UsersController extends Controller
{
    protected $werknemersService;
    protected $inviteService;
    protected $companyScanService;
    protected $userScanService;

    public function __construct(WerknemersService $werknemersService, InviteService $inviteService, UserService $userService, UserScanService $userScanService, CompanyScanService $companyScanService)
    {
        $this->werknemersService = $werknemersService;
        $this->inviteService = $inviteService;
        $this->userService = $userService;
        $this->companyScanService = $companyScanService;
        $this->userScanService = $userScanService;
    }

    /**
     * Displays the form for account creation
     *
     * @return  Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('website.signup');
    }

    public function createfrominvite($invitecode = false)
    {
        if (Auth::check()) {
            Auth::logout();
            Session::flush();
        }

        if ($invitecode == false) {
            $data['navPos'] = 1;
            $data['title'] = "Account valideren";
            $data['sentence'] = "Je hebt een geldige invitatiecode nodig.";
            return View::make('website.signupfrominvitenotvalid', $data);
        }

        $invites = $this->inviteService->checkIfValidInvite($invitecode);

        $user = $this->userService->getUserByInviteCode($invitecode);
        if (isset($user)) {
            if ($user->isEmployer()) {
                $data['navPos'] = 1;
                $data['title'] = "Account valideren";
                $data['sentence'] = "U lijkt reeds een werkgeversaccount te hebben. Gelieve een verantwoordelijke van Wellfie 
             te contacteren alvorens u kan deelnemen aan deze Wellfie.";
                return View::make('website.signupfrominvitenotvalid', $data);
            }else{
                $companyScan = $this->companyScanService->getLatestByCompanyId($user->company_id);

                if($invites
                    && $invites->company_id === $companyScan->company_id
                    && $invites->scan_id === $companyScan->scan_id
                ){
                    $userScan = $this->userScanService->getUserScanByUserId($user->id, $companyScan->scan_id);

                    $notice = 'Uw account was reeds geactiveerd voor de laatste Wellfie van uw bedrijf. Gelieve in te loggen om door te gaan.';

                    if(!isset($userScan)){
                        $this->werknemersService->initializeNewScan($user->company_id, $user->id);
                        $notice = 'Uw account werd geactiveerd voor de laatste Wellfie van uw bedrijf. Gelieve in te loggen om door te gaan.';
                    }
                    return Redirect::action('UsersController@login')
                        ->with('notice', $notice);
                }
            }
        }

        if ($invites) {
            $data['invitecodeId'] = $invites->id;
            $data['email'] = $invites->email;
            $data['invitecode'] = $invites->invitecode;
            $data['company_id'] = $invites->company_id;

            return View::make('website.signupfrominvite', $data);
        } else {
            $data['navPos'] = 1;
            $data['title'] = "Account valideren";
            $data['sentence'] = 'Je hebt een geldige invitatiecode nodig.<br><br>De Wellfie code vervalt zodra je een login hebt aangemaakt, of je werkgever de Wellfie heeft afgesloten.';

            return View::make('website.signupfrominvitenotvalid', $data);

        }
    }

    /**
     * Stores new COMPANY account
     *
     * @return  Illuminate\Http\Response
     */
    public function store()
    {
        $repo = App::make('App\Repositories\UserRepository');
        $storeddata = Input::all();

        $user = $repo->signup($storeddata);

        if ($user->id) {

            // adds a company
            $company = new Company();
            $arr = array(
                'company_name' => $storeddata['companyname'],
                'ondernemingsnummer' => $storeddata['ondernemingsnummer']
            );
            $company->fill($arr);
            $company->save();
            $companyId = $company->id;

            $companyRapport = new CompanyRapport();
            $companyRapport->company_id = $companyId;
            $companyRapport->scan_id = 1;
            $companyRapport->finalized = false;
            $companyRapport->save();

            // nu ook company id en permission level toevoegen bij user
            $userUpd = new User();
            // 6 wow verantwoordelijke, 5 ceo

            $userid = User::where('id', '=', $user->id)->update($arr = array(
                'company_id' => $companyId,
                'permission_level_id' => 6
            ));


            if (!App::environment('local')) {
                // The environment is not local

                // commentaar uitzetten van bevestigingmail

                /* if (Config::get('confide::signup_email')) {

                    Mail::queueOn(
                        Config::get('confide::email_queue'),
                        Config::get('confide::email_account_confirmation'),
                        compact('user'),
                        function ($message) use ($user) {
                            $message
                                ->to($user->email, $user->username)
                                ->subject(Lang::get('confide::confide.email.account_confirmation.subject'));
                        }
                    );
                } */
            }

            // do login
            $arraylogin = array('email' => $user->email, 'password' => $storeddata['password']);
            $repo = App::make('App\Repositories\UserRepository');
            if ($repo->login($arraylogin)) {
                return Redirect::intended('werkgevers/');
            } else {
                return Redirect::action('UsersController@login')
                    ->with('notice', Lang::get('confide::confide.alerts.account_created'));
            }

        } else {
            $error = $user->errors()->all(':message');

            return Redirect::action('UsersController@create')
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }


    /**
     * Stores new user account from invite
     *
     * @return  Illuminate\Http\Response
     */
    public function storefrominvite()
    {
        $storeddata = Input::all();
        $invitecode = array_get(Input::all(), 'invitecode');
        $invitecodeId = array_get(Input::all(), 'invitecodeId');

        // get invite ID
        $invites = Invite::where('invitecode', '=', $invitecode)->take(1)->get();
        $invitesID = $invites[0]->id;

        // create new user
        $repo = App::make('App\Repositories\UserRepository');
        $userarray = Input::all();
        $userarray['company_id'] = $invites[0]->company_id;
        $userarray['function'] = 'werknemer';

        $user = $repo->signup($userarray);
        //initialize new userscan for the user
        if (!isset($user->errors)) {
            // via invite model invite deactiveren
            $inviteUpd = Invite::find($invitesID);
            $arr = array(
                'user_id' => $user->id,
            );
            $inviteUpd->fill($arr);
            $inviteUpd->save();

            $this->werknemersService->initializeNewScan($user->company_id, $user->id);

            // do login
            $arraylogin = array('email' => $user->email, 'password' => $storeddata['password']);
            $repo = App::make('App\Repositories\UserRepository');

            return Redirect::action('UsersController@login')
                ->with('notice','Uw account werd geactiveerd voor de laatste Wellfie van uw bedrijf. Gelieve in te loggen om door te gaan.');
//            if ($repo->login($arraylogin)) {
//                return Redirect::intended('werknemers/');
//            } else {
//                return Redirect::action('UsersController@login')
//                    ->with('notice', Lang::get('confide::confide.alerts.account_created'));
//            }
        } else {
            $error = $user->errors()->all(':message');

            return Redirect::action('UsersController@createfrominvite', array(
                    'invitecode' => $invitecode)
            )
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }

    /**
     * Displays the login form
     *
     * @return  Illuminate\Http\Response
     */
    public function login()
    {
        if (\Confide::user()) {
            return Redirect::to('/');
        } else {
            return View::make('website.login');

        }
    }

    /**
     * Attempt to do login
     *
     * @return  Illuminate\Http\Response
     */
    public function doLogin()
    {
        $repo = App::make('App\Repositories\UserRepository');
        $input = Input::all();


        if ($repo->login($input)) {
            if (Auth::user()->permission_level_id == 0 || Auth::user()->permission_level_id == NULL) {
                return Redirect::intended('werknemers/');
            }
            if (Auth::user()->permission_level_id > 3 && Auth::user()->permission_level_id < 8) {
                return Redirect::intended('werkgevers/');
            }
            if (Auth::user()->permission_level_id == 9) {
                return Redirect::intended('superadmin/');
            }

        } else {
            if ($repo->isThrottled($input)) {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ($repo->existsButNotConfirmed($input)) {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }

            return Redirect::action('UsersController@login')
                ->withInput(Input::except('password'))
                ->with('error', $err_msg);
        }
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     *
     * @return  Illuminate\Http\Response
     */
    public function confirm($code)
    {
        if (\Confide::confirm($code)) {
            $notice_msg = Lang::get('confide::confide.alerts.confirmation');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
            return Redirect::action('UsersController@login')
                ->with('error', $error_msg);
        }
    }

    /**
     * Displays the forgot password form
     *
     * @return  Illuminate\Http\Response
     */
    public function forgotPassword()
    {
        return View::make('website.forgotpassword');
    }

    /**
     * Attempt to send change password link to the given email
     *
     * @return  Illuminate\Http\Response
     */
    public function doForgotPassword()
    {
        if (\Confide::forgotPassword(Input::get('email'))) {
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');
            return Redirect::action('UsersController@doForgotPassword')
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /**
     * Shows the change password form with the given token
     *
     * @param  string $token
     *
     * @return  Illuminate\Http\Response
     */
    public function resetPassword($token)
    {
        return View::make('website.resetpassword')
            ->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     * @return  Illuminate\Http\Response
     */
    public function doResetPassword()
    {
        $repo = App::make('App\Repositories\UserRepository');
        $input = array(
            'token' => Input::get('token'),
            'password' => Input::get('password'),
            'password_confirmation' => Input::get('password_confirmation'),
        );

        // By passing an array with the token, password and confirmation
        if ($repo->resetPassword($input)) {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
            return Redirect::action('UsersController@login')
                ->with('notice', $notice_msg);
        } else {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
            return Redirect::action('UsersController@resetPassword', array('token' => $input['token']))
                ->withInput()
                ->with('error', $error_msg);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @return  Illuminate\Http\Response
     */
    public function logout()
    {
        $adminId = Session::get('impersonate');

        if (isset($adminId)) {
            $id = Crypt::decrypt($adminId);
            Session::forget('impersonate');

            Auth::loginUsingId($id);

            return Redirect::to('/superadmin/werknemers');
        }

        \Confide::logout();
        return Redirect::to('/');
    }
}
