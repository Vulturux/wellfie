<?php

use App\Controllers\BaseController;
use App\Models\Documentation;

class DocumentationController extends BaseController
{

    protected $layout = 'layoutsSuperadmin.master';

    /**
     * Display a listing of the resource.
     * GET /documentation
     *
     * @return Response
     */
    public function index()
    {
        $data = array();
        $documentation = Documentation::orderBy('id')->paginate(50);

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('documentation.index', compact('documentation'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /documentation/create
     *
     * @return Response
     */
    public function create()
    {
        $data = array();

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('documentation.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /documentation
     *
     * @return Response
     */
    public function store()
    {
        $data = array();
        $input = Input::all();

        $validation = Validator::make($input, Documentation::$rules);

        if ($validation->passes()) {

            $documentation = new Documentation();
            $documentation->link = $this->processLink($documentation);
            $documentation->title = $input['title'];
            $documentation->description = $input['description'];
            $documentation->type = $input['type'];

            $documentation->save();
            return Redirect::route('superadmin.documentation.index');
        }

        return Redirect::route('superadmin.documentation.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     * GET /documentation/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $data = array();
        $documentation = Documentation::find($id);

        if (is_null($documentation)) {
            return Redirect::route('superadmin.documentation.index');
        }
        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('documentation.edit', compact('documentation'));
    }
    
    /**
     * Update the specified resource in storage.
     * PUT /documentation/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $data = array();
        $input = Input::all();
        $validation = Validator::make($input, Documentation::$rules);
        if ($validation->passes()) {
            $documentation = Documentation::find($id);

            $documentation->link = $this->processLink($documentation);
            $documentation->title = $input['title'];
            $documentation->description = $input['description'];
            $documentation->type = $input['type'];

            $documentation->save();

            return Redirect::route('superadmin.documentation.show', $id);
        }
        return Redirect::route('superadmin.documentation.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    private function processLink(Documentation $documentation = null){
        if (Input::hasFile('link')) {
            $file = Input::file('link');
            $pathinfo = pathinfo($file->getClientOriginalName());
            $filename = $pathinfo['filename'] . '-' . uniqid() . '.' . $pathinfo['extension'];
            $path = storage_path('documentation');
            $file->move($path, $filename);

            File::delete(storage_path('documentation/'.$documentation->link));

            return $filename;
        }elseif (Input::has('link')) {
            File::delete(storage_path('documentation/'.$documentation->link));
            return Input::get('link');
        }


        return null;

    }

    /**
     * Remove the specified resource from storage.
     * DELETE /documentation/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $data = array();
        Documentation::find($id)->delete();
        return Redirect::route('superadmin.documentation.index');
    }

}
