<?php

use App\Controllers\BaseController;

class AuthController extends BaseController  {

    /*
    |-NOT USED ANYMORE
    |
    */


    public function __construct()
    {

        $this->beforeFilter(function()
        {
            if (Auth::guest()) {
                echo "not logged in";
                // Redirect::guest('users/login');
            } else {
                echo "logged in id";
                print_r(Auth::user()->id);
                echo " -- ";
            }
        });

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
 /*   public function index()
    {
        //
        echo "index auth";

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }




}
