<?php

use App\Controllers\BaseController;
use App\Models\UserResponse;
use App\Services\ValidationService;
use App\Services\WerknemersService;
use App\Services\UserScanService;
use Illuminate\Support\Facades\View;
use Knp\Snappy\Pdf;

class WerknemersController extends BaseController
{


    protected $layout = 'layoutsWerknemer.master';
    protected $werknemersService;
    protected $userScanService;

    public function __construct(WerknemersService $werknemersService, UserScanService $userScanService)
    {
        $this->werknemersService = $werknemersService;
        $this->userScanService = $userScanService;
    }

    public function index()
    {
        echo "Position unknown";
    }

    /**
     * Stage param if given, needs to be one of the paths in the "positions" table
     *
     * @param string $stage
     */
    public function stage($stage = false)
    {
        // BASE POSITION  ----
        $user = Auth::user();
        $data = $this->getPosition();

        // Get questions with user responses if any.
        $questions = $this->werknemersService->getCatQuestionsAnswers($user->id, $data['cat'], $data['scan_id']);

        $data['questions'] = $questions['questions'];
        $data['submitted'] = $questions['submitted'];

        $this->layout->navbar = View::make('layoutsWerknemer.navbar', $data);
        $this->layout->content = View::make('werknemers.vragenlijst', $data);
    }

    public function postanswers()
    {
        // BASE POSITION
        $pos = $this->getPosition();
        if ($pos) {
            if ($pos['navPos'] > 5) {
                return Redirect::to('werknemers/resultaten');
            }
            $data['title'] = "Tussentijdse resultaten van " . $pos['title'];
            $data['navPos'] = $pos['navPos'];
            $data['cat'] = $pos['cat'];
            $data['scanId'] = $pos['scan_id'];
        } else {
            $data['title'] = "Something went wrong";
            $data['navPos'] = 1;
            $data['cat'] = 1;
            $data['scanId'] = $pos['scan_id'];
        }

        return $this->werknemersService->validateAnswers($data);

    }


    public function tussenresultaat($pos = 1)
    {

        $pos = $this->getPosition($pos - 1);

        if ($pos) {
            if ($pos['navPos'] > 5) {
                return Redirect::to('werknemers/resultaten');
            }
            $data['title'] = $pos['title'];
            $data['navPos'] = $pos['navPos'];
            $data['cat'] = $pos['cat'];
        } else {
            $data['title'] = "Something went wrong";
            $data['navPos'] = 1;
            $data['cat'] = 1;
        }
        $data['level'] = $data['navPos'] - 1;

        $scoreObj = new UserResponse();

        // scan id
        $scores = $scoreObj->totalCatscore($data['navPos'], Auth::user()->id, $pos['scan_id']);
        $data['scores'] = $scores;
        $data['catevaluation'] = $data['scores']['catevaluation'];
        $data['catpoints'] = $data['scores']['sumpoints'];
        unset($data['scores']['id']);
        unset($data['scores']['sumpoints']);
        unset($data['scores']['catevaluation']);
        $data['levelClass'] = 'level-' . $data['level'];

        $this->layout->navbar = View::make('layoutsWerknemer.navbar', $data);
        $this->layout->content = View::make('werknemers.tussenresultaat', $data);


    }

    public function eindresultaat()
    {
        // get scan id
        $positionData = $this->getPosition();
        $data = [
            'navPos' => $positionData['navPos'],
            'scan_id' => $positionData['scan_id'],
            'title' => 'Eindresultaat'
        ];

        $scoreObj = new UserResponse();
        $data['scoresAll'] = $scoreObj->allScores(Auth::user()->id, $data['scan_id']);

        $data['scoresLevelOne'] = $scoreObj->totalCatscore(2, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelOneEvaluation'] = $data['scoresLevelOne']['catevaluation'];
        $data['scoresLevelOnePoints'] = $data['scoresLevelOne']['sumpoints'];
        unset($data['scoresLevelOne']['id']);
        unset($data['scoresLevelOne']['sumpoints']);
        unset($data['scoresLevelOne']['catevaluation']);

        $data['scoresLevelTwo'] = $scoreObj->totalCatscore(3, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelTwoEvaluation'] = $data['scoresLevelTwo']['catevaluation'];
        $data['scoresLevelTwoPoints'] = $data['scoresLevelTwo']['sumpoints'];
        unset($data['scoresLevelTwo']['id']);
        unset($data['scoresLevelTwo']['sumpoints']);
        unset($data['scoresLevelTwo']['catevaluation']);

        $data['scoresLevelThree'] = $scoreObj->totalCatscore(4, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelThreeEvaluation'] = $data['scoresLevelThree']['catevaluation'];
        $data['scoresLevelThreePoints'] = $data['scoresLevelThree']['sumpoints'];
        unset($data['scoresLevelThree']['id']);
        unset($data['scoresLevelThree']['sumpoints']);
        unset($data['scoresLevelThree']['catevaluation']);

        $data['scoresLevelFour'] = $scoreObj->totalCatscore(5, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelFourEvaluation'] = $data['scoresLevelFour']['catevaluation'];
        $data['scoresLevelFourPoints'] = $data['scoresLevelFour']['sumpoints'];
        unset($data['scoresLevelFour']['id']);
        unset($data['scoresLevelFour']['sumpoints']);
        unset($data['scoresLevelFour']['catevaluation']);

        $data['scoresLevelFive'] = $scoreObj->totalCatscore(6, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelFiveEvaluation'] = $data['scoresLevelFive']['catevaluation'];
        $data['scoresLevelFivePoints'] = $data['scoresLevelFive']['sumpoints'];
        unset($data['scoresLevelFive']['id']);
        unset($data['scoresLevelFive']['sumpoints']);
        unset($data['scoresLevelOne']['catevaluation']);

        $this->layout->navbar = View::make('layoutsWerknemer.navbar', $data);
        $this->layout->content = View::make('werknemers.eindresultaat', $data);

    }

    public function pdfexport()
    {
        $positionData = $this->getPosition();
        $data = [
            'navPos' => $positionData['navPos'],
            'scan_id' => $positionData['scan_id'],
            'title' => 'Eindresultaat'
        ];

        $scoreObj = new UserResponse();
        $data['scoresAll'] = $scoreObj->allScores(Auth::user()->id, $data['scan_id']);

        $data['scoresLevelOne'] = $scoreObj->totalCatscore(2, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelOneEvaluation'] = $data['scoresLevelOne']['catevaluation'];
        $data['scoresLevelOnePoints'] = $data['scoresLevelOne']['sumpoints'];
        unset($data['scoresLevelOne']['id']);
        unset($data['scoresLevelOne']['sumpoints']);
        unset($data['scoresLevelOne']['catevaluation']);

        $data['scoresLevelTwo'] = $scoreObj->totalCatscore(3, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelTwoEvaluation'] = $data['scoresLevelTwo']['catevaluation'];
        $data['scoresLevelTwoPoints'] = $data['scoresLevelTwo']['sumpoints'];
        unset($data['scoresLevelTwo']['id']);
        unset($data['scoresLevelTwo']['sumpoints']);
        unset($data['scoresLevelTwo']['catevaluation']);

        $data['scoresLevelThree'] = $scoreObj->totalCatscore(4, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelThreeEvaluation'] = $data['scoresLevelThree']['catevaluation'];
        $data['scoresLevelThreePoints'] = $data['scoresLevelThree']['sumpoints'];
        unset($data['scoresLevelThree']['id']);
        unset($data['scoresLevelThree']['sumpoints']);
        unset($data['scoresLevelThree']['catevaluation']);

        $data['scoresLevelFour'] = $scoreObj->totalCatscore(5, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelFourEvaluation'] = $data['scoresLevelFour']['catevaluation'];
        $data['scoresLevelFourPoints'] = $data['scoresLevelFour']['sumpoints'];
        unset($data['scoresLevelFour']['id']);
        unset($data['scoresLevelFour']['sumpoints']);
        unset($data['scoresLevelFour']['catevaluation']);

        $data['scoresLevelFive'] = $scoreObj->totalCatscore(6, Auth::user()->id, $data['scan_id']);
        $data['scoresLevelFiveEvaluation'] = $data['scoresLevelFive']['catevaluation'];
        $data['scoresLevelFivePoints'] = $data['scoresLevelFive']['sumpoints'];
        unset($data['scoresLevelFive']['id']);
        unset($data['scoresLevelFive']['sumpoints']);
        unset($data['scoresLevelOne']['catevaluation']);

        $htmldata = View::make('werknemers.eindresultaat-pdf', $data)->render();

        if (App::environment() == "production") {
            $htmldata = str_replace(Config::get('app.url'), 'http://localhost', $htmldata);
        }

        // CREATE THE PDF
        $filename = 'Wellfie-WN-rapport-' . $this->keygen(10) . '.pdf';

//        // These are options for a different vendor: mikehaertl/phpwkhtmltopdf
//        $snappy = new Pdf(array(
//                'path' => sys_get_temp_dir(),
//                'xvfb' => TRUE,
//                'ignoreWarnings' => true,
//            )
//        );

        // knplabs/knp-snappy
        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf');
        $snappy->generateFromHtml($htmldata, '/tmp/' . $filename);

        //    echo $snappy->getOutputFromHtml($htmldata);

        // DOWNLOAD THE PDF
        header("Content-Type: application/octet-stream");
        $file = '/tmp/' . $filename;
        header("Content-Disposition: attachment; filename=" . urlencode($filename));
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        header("Content-Length: " . filesize($file));
        flush(); // this doesn't really matter.
        $fp = fopen($file, "r");
        while (!feof($fp)) {
            echo fread($fp, 65536);
            flush(); // this is essential for large downloads
        }
        fclose($fp);

    }

    public function geenActieveScan()
    {
        $data = [
            'navPos' => -1,
            'title' => 'Geen actieve Wellfie'
        ];
        $this->layout->navbar = View::make('layoutsWerknemer.navbar', $data);
        $this->layout->content = View::make('werknemers.geenActieveScan', $data);
    }


    /* test */
    public function score()
    {
        $scoreObj = new UserResponse();
        $score = $scoreObj->totalscore(2, Auth::user()->id, 1);
    }

    private function getPosition()
    {

        $userScan = $this->userScanService->getUserScanByUserId(Auth::user()->id, Auth::user()->company->getCurrentScan()->scan_id);
        $positionObject = $userScan->position();

        /*
        you do cat_id + 5 because for some reason the devs before me decided that the only way you could identify an employee and
        an employer is by the id of the question_categories. Apparently this was the best design pattern at the time. Thanks for wasting
        my time when I was looking for this 'logic'.
        */

        $data = [
            'navPos' => $userScan->questionnaire_position,
            'cat' => $positionObject->cat_id + 5,
            'title' => $positionObject->title,
            'scan_id' => $userScan->scan_id
        ];
        return $data;
    }

    public function keygen($length = 10)
    {
        $key = '';
        list($usec, $sec) = explode(' ', microtime());
        mt_srand((float)$sec + ((float)$usec * 100000));

        $inputs = array_merge(range('z', 'a'), range(0, 9), range('A', 'Z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $inputs{mt_rand(0, 61)};
        }
        return $key;
    }


}
