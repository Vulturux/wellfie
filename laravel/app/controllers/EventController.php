<?php

use App\Controllers\BaseController;
use App\Models\EventModel;

class EventController extends BaseController   {

    protected $layout = 'layoutsSuperadmin.master';

    /**
	 * Display a listing of the resource.
	 * GET /event
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = array();
		$events = EventModel::groupBy('id')->orderBy('datetime','DESC');

        if($events->count() > 0) $events = $events->paginate(50);

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
		$this->layout->content = View::make('events.index', compact('events'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /event/create
	 *
	 * @return Response
	 */
	public function create()
	{
        $data = array();

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('events.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /event
	 *
	 * @return Response
	 */
	public function store()
	{
        $data = array();
        $input = Input::all();

        $validation = Validator::make($input, EventModel::$rules);

        if ($validation->passes()) {

            $events = new EventModel();
            $events->title = $input['title'];
            $events->description = $input['description'];
            $events->datetime = $input['datetime'];

            $events->save();
            return Redirect::route('superadmin.events.index');
        }

        return Redirect::route('superadmin.events.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 * GET /event/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = array();
        $event = EventModel::find($id);

        if (is_null($event)) {
            return Redirect::route('superadmin.events.index');
        }
        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('events.edit', compact('event'));
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /event/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $data = array();
        $input = Input::all();
        $validation = Validator::make($input, EventModel::$rules);
        if ($validation->passes()) {
            $events = EventModel::find($id);

            $events->title = $input['title'];
            $events->description = $input['description'];
            $events->datetime = $input['datetime'];

            $events->save();

            return Redirect::route('superadmin.events.show', $id);
        }
        return Redirect::route('superadmin.events.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /event/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $data = array();
        EventModel::find($id)->delete();
        return Redirect::route('superadmin.events.index');
	}

}
