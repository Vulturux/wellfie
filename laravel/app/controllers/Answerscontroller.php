<?php

use App\Controllers\BaseController;
use App\Models\Answer;
use App\Models\Question;

class AnswersController extends BaseController  {


    protected $layout = 'layoutsSuperadmin.master';

    public function __construct()
    {
    }

    public function index()
    {

        $answers = Answer::orderBy('id')->get();
        $answers = Answer::paginate(100);

        $this->layout->content = View::make('answers.index', compact('answers'));

    }


    public function create()
    {

        $questionslist= Question::lists('question', 'id');
        $answer = new Answer;

        $this->layout->content = View::make('answers.create', compact('answer', 'questionslist'));

    }


    public function store()
    {

        $input = Input::all();

        $validation = Validator::make($input, Answer::$rules);

        if ($validation->passes()) {

            Answer::create($input);
            return Redirect::route('answers.index');
        }

        return Redirect::route('answers.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }


    public function show($id)
    {
        $answer = Answer::find($id);

        if (is_null($answer))
        {
            return Redirect::route('answers.index');
        }
        return View::make('answers.show', compact('answer'));

    }

    public function edit($id)
    {

        $answer = Answer::find($id);
       // $answerTitles =  DB::table('answers_titles')->lists('title');
        $questionslist= Question::lists('question', 'id');

        if (is_null($answer))
        {
            return Redirect::route('answers.index');
        }
        return View::make('answers.edit', compact('answer', 'questionslist'));

    }


    public function update($id)
    {
        $input = Input::all();
        $validation = Validator::make($input, Answer::$rules);
        if ($validation->passes())
        {
            $answer = Answer::find($id);
            $answer->update($input);
            return Redirect::route('answers.show', $id);
        }
        return Redirect::route('answers.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');

    }

    public function destroy($id)
    {
        Answer::find($id)->delete();
        return Redirect::route('answers.index');

    }


}
