<?php

use App\Controllers\BaseController;
use App\Models\Company;
use App\Models\CompanyRapport;
use App\Models\Invite;
use App\Models\User;
use App\Models\UserPosition;
use App\Models\UserResponse;
use App\Services\WerkgeversService;
use App\Services\UserScanService;
use App\Services\CompanyScanService;
use App\Services\RapportService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

/**
 * Class WergeversController
 *
 * There is a new UserPosition attribute "position_max" operating as the previously
 * known "position" identifier. The "position" indicating the user's current position
 * in the form. (MaxPos)
 *
 * The old attribute "position"'s purpose now shifts to the position of the
 * impersonating position by admins. (NavPos)
 */
class WerkgeversController extends BaseController
{

    protected $layout = 'layoutsWerkgever.master';
    protected $werkgeversService;
    protected $userScanService;
    protected $companyScanService;
    protected $rapportService;

    public function __construct(WerkgeversService $werkgeversService, UserScanService $userScanService, CompanyScanService $companyScanService, RapportService $rapportService)
    {
        $this->werkgeversService = $werkgeversService;
        $this->userScanService = $userScanService;
        $this->companyScanService = $companyScanService;
        $this->rapportService = $rapportService;
    }

    public function wellfieprocess()
    {
        $userScan = $this->userScanService->getLatestUserScan(Auth::user()->id);

        if (isset($userScan)) {
            if($userScan->questionnaire_position === -1){
                return Redirect::to('werkgevers/intro');
            }
            else if ($userScan->questionnaire_position >= 0 && $userScan->questionnaire_position < 5) {
                $this->stage();
            }
            else if ($userScan->questionnaire_position >= 5) {
                return Redirect::to('werkgevers/dashboard');
            }
        } else {
            return Redirect::to('werkgevers/intro');
        }

    }

    /* ------------------ (-1) Static : intro video ------------------ */
    public function intro()
    {
        //todo need this?
        $data = $this->werkgeversService->getPositionForRoute('Introductie');

        $this->werkgeversService->initializeNewScan(Auth::user()->company_id);
        $this->userScanService->updateUserScan();


        // Render layouts.
        $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
        $this->layout->content = View::make('werkgevers.intro', $data);

    }

    /* ------------------ vragenlijst ------------------ */
    /**
     * Stage param if given, needs to be one of the paths in the "positions" table
     *
     * @param string $stage
     */
    public function stage()
    {
        // vars.
        $userScan = $this->userScanService->getLatestUserScan(Auth::user()->id);

        $positionObject = $userScan->position();

        // Get questions with user responses if any.
        $questions = $this->werkgeversService->getCatQuestionsAnswers($userScan->user_id, $positionObject->cat_id, $userScan->scan_id);

        $data = [
            'title' => $positionObject->title,
            'navPos' => $positionObject->pos_id,
            'cat' => $positionObject->cat_id,
            'questions' => $questions['questions'],
            'submitted' => $questions['submitted']
        ];

        // Render layouts.
        $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
        $this->layout->content = View::make('werkgevers.vragenlijst', $data);
    }

    public function postanswers()
    {
        $userScan = $this->userScanService->getLatestUserScan(Auth::user()->id);
        $positionObject = $userScan->position();

        $data = [
            'title' => $positionObject->title,
            'navPos' => $positionObject->pos_id,
            'cat' => $positionObject->cat_id,
            'scanId' => $userScan->scan_id
        ];

        return $this->werkgeversService->validateAnswers($data);
    }

    public function eindresultaat($scanId)
    {
        $userScan = $this->userScanService->getLatestUserScan(Auth::user()->id);
        $scoreObj = new UserResponse();

        $data = [
            'navPos' => $userScan->questionnaire_position,
            'title' => 'Eindresultaat',
            'scan_id' => $scanId,
            'scoresBeleidFeedback' => $scoreObj->allScores(Auth::user()->id, $scanId, 2)
        ];

        $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
        $this->layout->content = View::make('werkgevers.eindresultaat', $data);
    }

    // ajax
    public function reinvite($userid)
    {
        $user = Invite::find($userid);
        $this->layout = '';

        $subject = Input::get('subject');
        $message = Input::get('message');


        if (count($user) && isset($subject, $message)) {

            // Check company id
            if ($user->company_id == Auth::user()->company_id) {

                /** @var Company $company */
                $company = Auth::user()->Company()->first();
                $currentScanId = $company->getHighestFinalizedScanId() + 1;

                // inserts invite into database and sends email
                $invite = new Invite();
                $data['invite'] = $invite->resendInviteMail($user->email, $subject, $message, $currentScanId);

                $this->layout = '';
            } else {
                echo "No employee from your company - company id is ";
                echo $user->company_id;
            }
        } else {
            echo "empty";
        }
    }

    public function finalizeScan()
    {
        $companyScan = $this->companyScanService->getLatestByCompanyId(Auth::user()->company_id);
        if(!$companyScan->scan_finished){
            $companyScan->scan_finished = true;
            $companyScan->save();
        }

        $userScan = $this->userScanService->getLatestUserScan(Auth::user()->id);
        $canRapportbeGenerated = $this->userScanService->canRapportBeGenerated(Auth::user()->company_id, $userScan->scan_id);

        if($canRapportbeGenerated && !$companyScan->rapport_generated){
            Event::fire('questionnaire.closed', array([Auth::user()->company_id, $userScan->scan_id, $userScan->user_id]));

            $data = [
                'title' => 'Hoe resultaten kennen',
                'navPos' => $userScan->questionnaire_position,
                'scan_id' => $userScan->scan_id,
                'companyScan' => $companyScan
            ];

            // Render layouts.
            $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
            $this->layout->content = View::make('werkgevers.hoeresultatenkennen', $data);
        }else {
            return Redirect::to('werkgevers/eindresultaat/'.$userScan->scan_id);
        }
    }

    //only accessible after scan is closed, shows the 'PDF'
    public function WellfieRapport ($scanId)
    {
        $userScan = $this->userScanService->getUserScanByUserId(Auth::user()->id, $scanId);
        $canRapportBeGenerated = $this->userScanService->canRapportBeGenerated(Auth::user()->company_id, $scanId);
        if(!$canRapportBeGenerated) return Redirect::to('werkgevers/eindresultaat/'.$scanId);

        $diskData = $this->rapportService->readFromDisk(Auth::user()->company_id, $scanId);

        $data = [
            'navPos' => $userScan->questionnaire_position,
            'title' => 'TotaalResultaat',
            'scan_id' => $scanId
        ];

        if ($diskData) {
            $scoreObj = new UserResponse();
            $data = array_merge($data, $diskData);

            // Render view required params fallback.
            if (!isset($data['scoresWG']['117']['order'])) $data['scoresWG']['117']['order'] = null;
            if (!isset($data['scoresWG']['118']['order'])) $data['scoresWG']['118']['order'] = null;
            if (!isset($data['scoresWG']['119']['order'])) $data['scoresWG']['119']['order'] = null;

            $data['$werknemersCompletedScan'] = $this->userScanService->getCompletedUserScansForCurrentScan(Auth::user()->company_id, $scanId);
            $data['scoresBeleidFeedback'] = $scoreObj->allScores(Auth::user()->id, $scanId, 2);
            $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
            $this->layout->content = View::make('werkgevers.totaalresultaat', $data);
        } else {
            $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
            $this->layout->content = View::make('werkgevers.totaalresultaat-preparation', $data);
        }
    }

    //PDF
    public function pdfexport($scanId = 1)
    {
        $rapportService = new \App\Services\RapportService($this->userScanService);

        $filePath = $rapportService->readFromDisk(Auth::user()->company_id, $scanId, 'pdf');

        if (!$filePath) return Redirect::action('WerkgeversController@stage', $stage = false);

        // DOWNLOAD THE PDF
        $headers = array(
            "Content-Type: application/octet-stream",
            "Content-Disposition: attachment; filename=" . urlencode($filePath),
            "Content-Type: application/octet-stream",
            "Content-Type: application/download",
            "Content-Description: File Transfer",
            "Content-Length: " . filesize($filePath)
        );

        $date = new DateTime();
        $companyName = preg_replace("/[^A-Za-z0-9 ]/", '', Auth::user()->company->company_name);
        $companyName = $date->format('Ymd') . '_' . str_replace(" ", "-", strtolower($companyName)) . '.pdf';

        return Response::download($filePath, $companyName, $headers);
    }

    public function wellfiePdfRapport($scanId = 1, $userId = 0, $companyId = 0)
    {

        // same as below
        $data['navPos'] = 10;
        $data['title'] = "Totaalresultaat";
        $amount = new UserResponse();

        $rapportService = new \App\Services\RapportService($this->userScanService);
        $diskData = $rapportService->readFromDisk(Auth::user()->company_id, 'pdf');

        if ($diskData) {
            return $diskData;
        } else {
            $this->layout->navbar = View::make('layoutsWerkgever.navbar', $data);
            $this->layout->content = View::make('werkgevers.totaalresultaat-preparation', $data);
        }
    }

    public function checkRapport($scanId)
    {
        $companyId = Auth::user()->company_id;
        $companyScan = $this->companyScanService->getByScanId($companyId, $scanId);

        if (isset($companyScan) && $companyScan->rapport_generated && !empty($companyScan->rapport_filename))
        {
            return Response::json(array('rapport' => true, 'scan_id' => (int)$scanId));
        }

        return Response::json(array('rapport' => false, 'scan_id' => (int)$scanId ));
    }

    public function getPreviousScanInviteEmails(){
        $user =Auth::user();
        $userScan = $this->userScanService->getLatestUserScan($user->id);

        $emails = Invite::where('scan_id', $userScan->scan_id-1)->where('company_id', $user->company_id)->select('email')->get();

        return Response::json([
            'emails' => array_pluck($emails, 'email'),
        ]);
    }
}
