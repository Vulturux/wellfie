<?php

// REMOVE ME ONLY FOR TESTING
use App\Controllers\BaseController;
use App\Models\Documentation;
use App\Models\EventModel;
use App\Models\Faq;
use Knp\Snappy\Pdf;

class WebsiteController extends BaseController  {


    protected $layout = 'layoutsWebsite.master';

    public function index()
    {
        $data['navPos'] = 0;
        $data['title'] = "Homepage Wellfie";

        setlocale(LC_TIME, 'nl_NL.UTF-8');

        $data['events'] = EventModel::where('datetime', '>', \Carbon\Carbon::now()
          ->subDay())
          ->orderBy('datetime')
          ->take(1)
          ->get();

        $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
        $this->layout->content = View::make('website.index', $data);

    }


    public function werkvermogen()
    {
        $data['navPos'] = 1;
        $data['title'] = "Wellfie: Werkvermogen";

        $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
        $this->layout->content = View::make('website.werkvermogen', $data);

    }

    public function hoebouwenaanwerkvermogen()
    {
        $data['navPos'] = 2;
        $data['title'] = "Wellfie: Hoe bouwen aan werkvermogen?";

        $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
        $this->layout->content = View::make('website.hoebouwenaanwerkvermogen', $data);

    }


    public function links()
    {
        $data['navPos'] = 3;
        $data['title'] = "Wellfie: Homepage Wellfie";

        $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
        $this->layout->content = View::make('website.links', $data);

    }


    public function overons()
    {
        $data['navPos'] = 4;
        $data['title'] = "Wellfie: Over Ons";

        $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
        $this->layout->content = View::make('website.overons', $data);

    }

    public function faq()
    {
        $data['navPos'] = 5;
        $data['title'] = "FAQ";
        $data['faq'] = Faq::all();
        $data['categories'] = Faq::getCategories();

        $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
        $this->layout->content = View::make('website.faq', $data);

    }

    public function documentationFile($file)
    {

        $isDownload = Input::get('download', false);
        $path = storage_path('documentation/' . $file);

        if(File::exists($path)){
            if ($isDownload) return Response::download($path);

            $file = File::get($path);
            $mimetype = mime_content_type($path);
            $size = filesize($path);
            $response = Response::make($file, 200);
            $response->header('Content-Type', $mimetype);
            $response->header('Content-Length', $size);


            return $response;
        }

        return Redirect::back();



    }

    public function documentation($type = null)
    {
        $data['navPos'] = 6;
        $data['title'] = "Documentatie";
        $data['afbeelding'] = Documentation::where('type', 'afbeelding')->get();
        $data['brochure'] = Documentation::where('type', 'brochure')->get();
        $data['video'] = Documentation::where('type', 'video')->get();

        $data['numCols'] = 0;
        if($data['afbeelding']->count() > 0) $data['numCols']++;
        if($data['brochure']->count() > 0) $data['numCols']++;
        if($data['video']->count() > 0) $data['numCols']++;

        $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
        $this->layout->content = View::make('website.documentation', $data);


    }

    public function events($id = null){
        setlocale(LC_TIME, 'nl_NL.UTF-8');


        $data['navPos'] = 7;
        $data['title'] = "Evenementen";

        if(!isset($id)){
            $futureEvents = EventModel::where('datetime', '>', \Carbon\Carbon::now()->subDay())
                ->orderBy('datetime')
                ->get();

            $pastEvents = EventModel::where('datetime', '<=', \Carbon\Carbon::now()->subDay())
                ->orderBy('datetime', 'DESC')
                ->get();


            $data['events'] = $futureEvents->merge($pastEvents);

            $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
            $this->layout->content = View::make('website.events', $data);
        }else{
            $data['event'] = EventModel::find($id);

            if(! $data['event']) return Redirect::to('/events');

            $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
            $this->layout->content = View::make('website.event', $data);
        }

    }

    public function falseinvitation()
    {
        $data['navPos'] = 1;
        $data['title'] = "FalseInvitation";
        $data['sentence'] = 'Je hebt ongeldige invitatiecode.<br><br>De Wellfie code vervalt na 14 dagen.<br><br><br><br><br><br>';

        $this->layout->navbar = View::make('layoutsWebsite.navbar', $data);
        $this->layout->content = View::make('website.signupfrominvitenotvalid', $data);

    }


    /* TESTS TO BE REMOVED */

    public function pdftest()
    {

        $snappy = new Pdf();
        $snappy->setBinary('/usr/local/bin/wkhtmltopdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="wellfietestfile.pdf"');
        echo $snappy->getOutput('http://www.github.com');

    }

    public function pdftestwithassetsview()
    {

        $data['navPos'] = 5;
        $data['title'] = "FAQ";
        $data['faq'] = Faq::all();

        return View::make('website.pdftestassets', $data);

    }

    public function pdftestwithoutassetsview()
    {

        $data['navPos'] = 5;
        $data['title'] = "FAQ";
        $data['faq'] = Faq::all();
        return View::make('website.pdftestwithoutassets', $data);

    }

    public function pdftestwithassets()
    {
        $data['navPos'] = 5;
        $data['title'] = "FAQ";
        $data['faq'] = Faq::all();

        $htmldata = View::make('website.pdftestassets', $data)->render();

        // CREATE THE PDF
        $filename = 'TESTWellfie-WN-rapport-' . $this->keygen(10) . '.pdf';

        $snappy = new Pdf(array(
                'path' => sys_get_temp_dir(),
                'xvfb' => TRUE,
            )
        );
        // LOCALHOST
        //$snappy->setBinary('/usr/bin/wkhtmltopdf.sh');
        // live
        $snappy->setBinary('/usr/local/bin/wkhtmltopdf');

        echo $snappy->generateFromHtml($htmldata, 'tmp/' . $filename);

        // DOWNLOAD THE PDF
        header("Content-Type: application/octet-stream");
        $file = 'tmp/' . $filename;
        header("Content-Disposition: attachment; filename=" . urlencode($filename));
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        header("Content-Length: " . filesize($file));
        flush(); // this doesn't really matter.
        $fp = fopen($file, "r");
        while (!feof($fp)) {
            echo fread($fp, 65536);
            flush(); // this is essential for large downloads
        }
        fclose($fp);

    }

    public function pdftestwithoutassets()
    {
        $data['navPos'] = 5;
        $data['title'] = "FAQ";
        $data['faq'] = Faq::all();

        $htmldata = View::make('website.pdftestwithoutassets', $data)->render();

        // CREATE THE PDF
        $filename = 'TESTWellfie-WN-rapport-' . $this->keygen(10) . '.pdf';

        $snappy = new Pdf(array(
                'path' => sys_get_temp_dir(),
                'xvfb' => TRUE,
            )
        );
        // LOCALHOST
        //$snappy->setBinary('/usr/bin/wkhtmltopdf.sh');
        // live
        $snappy->setBinary('/usr/local/bin/wkhtmltopdf');

        echo $snappy->generateFromHtml($htmldata, 'tmp/' . $filename);

        // DOWNLOAD THE PDF
        header("Content-Type: application/octet-stream");
        $file = 'tmp/' . $filename;
        header("Content-Disposition: attachment; filename=" . urlencode($filename));
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        header("Content-Length: " . filesize($file));
        flush(); // this doesn't really matter.
        $fp = fopen($file, "r");
        while (!feof($fp)) {
            echo fread($fp, 65536);
            flush(); // this is essential for large downloads
        }
        fclose($fp);

    }

    public function keygen($length = 10)
    {
        $key = '';
        list($usec, $sec) = explode(' ', microtime());
        mt_srand((float)$sec + ((float)$usec * 100000));

        $inputs = array_merge(range('z', 'a'), range(0, 9), range('A', 'Z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $inputs{mt_rand(0, 61)};
        }
        return $key;
    }


}
