<?php

use App\Controllers\BaseController;
use App\Models\Company;
use App\Models\Invite;
use App\Models\User;
use App\Models\UserPosition;
use App\Models\UserResponse;
use App\Models\CompanyScan;
use App\Models\CompanyRapport;
use App\Models\Position;

use App\Services\WerkgeversService;
use App\Services\WerknemersService;
use App\Services\UserService;
use App\Services\CompanyService;

use App\Services\UserScanService;
use App\Services\ExportService;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;

class SuperadminController extends BaseController
{
    protected $layout = 'layoutsSuperadmin.master';
    protected $userScanService;
    protected $exportService;
    /**
     * @var \App\Services\WerkgeversService
     */
    private $werkgeversService;
    /**
     * @var \App\Services\WerknemersService
     */
    private $werknemersService;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var CompanyService
     */
    private $companyService;

    public function __construct(WerkgeversService $werkgeversService, WerknemersService $werknemersService, UserService $userService, CompanyService $companyService, UserScanService $userScanService, ExportService $exportService)
    {


        $this->beforeFilter(function () {

            if (Auth::guest()) {
                return Redirect::to('/');
            } else {
                if (Auth::user()->permission_level_id < 9) {
                    return Redirect::to('/');
                }
            }
        });

        $this->werkgeversService = $werkgeversService;
        $this->werknemersService = $werknemersService;
        $this->userService = $userService;
        $this->companyService = $companyService;
        $this->userScanService = $userScanService;
        $this->exportService = $exportService;
    }

    public function index()
    {
        $year = Input::get("year");
        $week = Input::get("week");

        $dto = new DateTime();
        $data['year'] = is_null($year) ? $dto->format('Y') : $year;
        $data['week'] = is_null($week) ? $dto->format('W') : $week;

        $companies = new Company();
        $users = new User();
        $invites = new Invite();
        $positions = new UserPosition();

        $data['companies_per_week'] = $companies->getCompaniesPerWeek();

        $data['this_week'] = [
            'user_count' => $users->getUserCount($data['year'], $data['week']),
            'company_count' => $companies->getCompanyCount($data['year'], $data['week']),
            'company_invitation_count' => $invites->getInviteCount($data['year'], $data['week'], 'company_id'),
            'company_active_count' => $this->userScanService->getActiveScanCount($data['year'], $data['week']),
//          'company_active_count' => $positions->getActiveCount($data['year'], $data['week']),
//          'company_completed_count' => $positions->getCompletedCount($data['year'], $data['week']),
            'company_completed_count' => $this->userScanService->getCompletedCount($data['year'], $data['week'])
        ];

        $data['overal'] = [
            'user_count' => $users->getUserCount(),
            'company_count' => $companies->getCompanyCount(NULL, NULL),
            'invited_count' => $invites->getInviteCount(),
            'completed_wn_count' => $positions->getCompletedCount(NULL, NULL, 0),
            'completed_wg_count' => $positions->getCompletedCount(NULL, NULL, 6),
        ];

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('superadmin.dashboard', $data);
    }


    public function bedrijven()
    {
        $data = array();

        $companies = new Company();
        $companiesArray = $companies->getStatusOverview();

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('superadmin.bedrijven', compact('companiesArray'));

    }


    public function bedrijvenDetail($companyId = 0)
    {
        $data = array();

        // company info
        /** @var Company $company */
        $company = Company::find($companyId);

        $highestScanId = $company->getHighestFinalizedScanId();

        $employer = $company->getEmployer();

        $responseObj = new UserResponse();
        // scan id
        $response = $responseObj->allResponses($companyId, $highestScanId);

        // scan id
//        $userObj = new UserPosition();
//        $users = $userObj->fullCompanyoverview($companyId);

        $invites = Invite::where('company_id', $companyId)->with(['user', 'user.UserScans' => function ($q) {
            $q->orderBy('scan_id', 'DESC');
        },])->get();

        foreach ($invites as $key => &$invite) {

            if (!empty($invite->user_id)) {
                $invite->status = "logged in";
                $invite->invitecode = "(used)";

            } else {
                $invite->status = "invited";
                $invite->invitecode = \Config::get('app.url') . "/users/createfrominvite/" . $invite->invitecode;

            }
        }

//        \Kint::$maxLevels = 10;
//        ddd($invites);

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('superadmin.bedrijvenDetail', compact('company', 'employer', 'response', 'invites'));

    }

    public function bedrijvenEdit($companyId)
    {

        $data = array();

        // company info
        /** @var Company $company */
        $company = Company::find($companyId);

        $highestScanId = $company->getHighestFinalizedScanId();

        $employer = $company->getEmployer();

        $responseObj = new UserResponse();
        // scan id
        $response = $responseObj->allResponses($companyId, $highestScanId);

        // scan id
//        $userObj = new UserPosition();
//        $users = $userObj->fullCompanyoverview($companyId);

        $invites = Invite::where('company_id', $companyId)->with(['user', 'user.UserScans' => function ($q) {
            $q->orderBy('scan_id', 'DESC');
        },])->get();

        foreach ($invites as $key => &$invite) {

            if (!empty($invite->user_id)) {
                $invite->status = "logged in";
                $invite->invitecode = "(used)";

            } else {
                $invite->status = "invited";
                $invite->invitecode = \Config::get('app.url') . "/users/createfrominvite/" . $invite->invitecode;

            }
        }

//        \Kint::$maxLevels = 10;
//        ddd($invites);

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('superadmin.bedrijvenEdit', compact('company', 'employer', 'response', 'invites'));

    }

    public function bedrijvenDelete($companyId)
    {

        return $this->companyService->delete($companyId) ?
            Redirect::to('/superadmin/bedrijven/')->with('message', 'Bedrijf verwijderd') :
            Redirect::to('/superadmin/bedrijven/' . $companyId)->with('message', 'Bedrijf verwijderen mislukt');

    }

    public function bedrijvenUpdate($companyId)
    {
        // company info
        /** @var Company $company */
        $company = Company::find($companyId);
        $companyDetail = $company->getWGPosition($company->id);

        $company->fill(Input::all());
        if ($company->isDirty()) {
            $company->save();
        }

        if ($companyDetail->position !== (int)Input::get('companyPosition')) {
            $this->werkgeversService->updateUserPosition($companyDetail->user_id, (int)Input::get('companyPosition'));
        }

        return Redirect::to('/superadmin/bedrijven/' . $companyId . '/edit')->with('message', 'Bedrijf opgeslagen');
    }

    public function werknemers()
    {
        $data = array();
        $users = User::orderBy('id', 'desc')->paginate(100);

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('superadmin.werknemers', compact('users'));

    }

    public function werknemersData()
    {
        $users = User::with(['company', 'latestUserScan'])
            ->select(['users.created_at', 'users.id', 'users.username', 'users.email', 'users.permission_level_id', 'users.company_id']);

        $dt = Datatables::of($users)->editColumn('email', '<a href="mailto:{{$email}}">{{$email}}</a>')
            ->editColumn('id', '{{$id}}')
            ->editColumn('company', '@if(isset($company))<a href="/superadmin/bedrijven/{{$company->id}}">{{$company->company_name}}</a>@else - @endif')
            ->editColumn('latest_user_scan', '
                @if(count($latest_user_scan) > 0)
                    Scan {{$latest_user_scan[0]->scan_id}}:
                    {{ $latest_user_scan[0]->questionnaire_position }} 
                        @if($permission_level_id==0) /5 @elseif($permission_level_id==6) /6 @endif
                @endif')
            ->editColumn('permission_level_id', '@if($permission_level_id==0)
                                <span class="label secondary">WN</span>
                            @elseif($permission_level_id==6)
                                <span class="label info">WG</span>
                            @elseif($permission_level_id==9)
                                <span class="label info">SA</span>
                            @endif')
            ->addColumn('extra', '<a href="/superadmin/impersonate/{{$id}}">Impersonate</a><br />
									<a href="/superadmin/werknemers/{{$id}}/responses">Antwoorden</a><br />
									<a href="/superadmin/werknemers/{{$id}}/edit" style="color:#CCC;font-size:16px;"><i class="fi-pencil"></i></a>');

        return $dt->make(true);

    }


    public function werknemerResponses($userid = 0)
    {

        $data = array();
        $user = User::find($userid);

        $responseObj = new UserResponse();
        $response = $responseObj->allResponses($userid, 1);

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('superadmin.werknemerDetail', compact('user', 'response'));

    }

    public function werknemerEdit($userId)
    {

        $data = array();

        /** @var User $user */
        $user = User::find($userId);

        $userPosition = $user->UserPosition()->first();

        $allPositions = Position::all()->splice(0, 7);
        $positions = $allPositions->map(function ($position) {
            return $position->title . ' (' . $position->pos_id . ')';
        })->splice(0, 7);
        $positions = array_combine(array_pluck($allPositions, 'pos_id'), $positions->toArray());

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('superadmin.werknemerEdit', compact('user', 'userPosition', 'positions'));

    }

    public function werknemerUpdate($userId)
    {
        UserPosition::where('user_id', $userId)->update(array('position' => Input::get('userPosition')));

        return Redirect::to('/superadmin/werknemers/' . $userId . '/edit')->with('message', 'Werknemer opgeslagen');
    }

    public function werknemerDelete($userId)
    {

        return $this->userService->delete($userId) ?
            Redirect::to('/superadmin/werknemers/')->with('message', 'Werknemer verwijderd') :
            Redirect::to('/superadmin/werknemers/' . $userId)->with('message', 'Werknemer verwijderderen mislukt');
    }


    public function export($companyid = 0)
    {

        $data = array();
        $data['companyId'] = $companyid;
        $data['companyList'] = Company::orderBy('company_name')->lists('company_name', 'id');

        $data['regioList'] = array(
            447 => 'West-Vlaanderen',
            448 => 'Oost-Vlaanderen',
            449 => 'Vlaams-Brabant',
            450 => 'Antwerpen',
            451 => 'Limburg',
            452 => 'Brussel',
        );

        $data['sectorList'] = array(
            454 => 'bouw',
            455 => 'diensten',
            456 => 'gezondheidszorg',
            457 => 'handel',
            458 => 'horeca',
            459 => 'industrie',
            460 => 'onderwijs',
            461 => 'overheid',
            462 => 'transport',
            463 => 'overige sectoren',
        );

        $data['positionList'] = array(
            'CompWG' => 'Afgewerkte Vragenlijsten (Werkgevers)',
            'CompAWN' => 'Afgewerkte Vragenlijsten (Werknemers)',
            'CWWG' => 'Alle Vragenlijsten in Afgesloten Wellfie (Werkgever)',
            'CWWN' => 'Alle Vragenlijsten in Afgesloten Wellfie (Werknemer)',
            'CWCompWG' => 'Afgewerkte Vragenlijsten in Afgesloten Wellfie (Werkgever)',
            'CWCompWN' => 'Afgewerkte Vragenlijsten in Afgesloten Wellfie (Werknemer)'
        );

        $maxScanId = CompanyScan::max('scan_id');
        $scanIds = [];
        for ($i = 1; $i <= $maxScanId; $i++) {
            $scanIds[$i] = $i;
        }
        $data['scanIds'] = $scanIds;

        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', $data);
        $this->layout->content = View::make('superadmin.exportparam', $data);

    }

    public function exportaction()
    {
        $filterData = Input::all();

        // Validate filterdata:
        $rules = array(
            'scanId' => 'required',
            'companyId' => 'required_without_all:regionId,sectorId,positionId,permissionLevelId,employeeCountMin,employeeCountMax,periodFrom,periodTo',
            'regionId' => 'required_without_all:companyId,sectorId,positionId,permissionLevelId,employeeCountMin,employeeCountMax,periodFrom,periodTo',
            'sectorId' => 'required_without_all:companyId,regionId,positionId,permissionLevelId,employeeCountMin,employeeCountMax,periodFrom,periodTo',
            'employeeCountMin' => 'required_without_all:companyId,regionId,sectorId,positionId,permissionLevelId,employeeCountMax,periodFrom,periodTo|min:0',
            'employeeCountMax' => 'required_without_all:companyId,regionId,sectorId,positionId,permissionLevelId,employeeCountMin,periodFrom,periodTo|min:0',
            'periodFrom' => 'required_without_all:companyId,regionId,sectorId,positionId,permissionLevelId,employeeCountMin,employeeCountMax,periodTo|date_format:"Y-m-d"',
            'periodTo' => 'required_without_all:companyId,regionId,sectorId,positionId,permissionLevelId,employeeCountMin,employeeCountMax,periodFrom|date_format:"Y-m-d"',
            'positionId' => 'required_without_all:regionId,sectorId,companyId,permissionLevelId,employeeCountMin,employeeCountMax,periodFrom,periodTo',
            'permissionLevelId' => 'required_without_all:regionId,sectorId,companyId,positionId,employeeCountMin,employeeCountMax,periodFrom,periodTo',
        );

        $messages = array(
            'min' => 'Geef minstens een getal boven 0 in.',
            'periodFrom.regex' => 'Gelieve een correcte periode op te geven (YYYY-MM-DD)',
            'periodTo.regex' => 'Gelieve een correcte periode op te geven (YYYY-MM-DD)',
            'required_without_all' => 'Vul minstens 1 filter in.'
        );

        // do the validation ----------------------------------
        // validate against the inputs from our form
        $validator = Validator::make($filterData, $rules, $messages);

        $filename = 'wellfie-export' . date("d-M-Y ") . time();
        $filterData['filename'] = $filename;

        if ($validator->fails()) {
            // redirect our user back to the form with the errors from the validator
            return Redirect::to('superadmin/export')
                ->withErrors($validator)
                ->withInput();
        } else {
//            $this->exportService->getWellfieDataWithFilters($filterData);
            Event::fire('generate.export', [$filterData]);
        }

//        return Redirect::to('superadmin/exportlist');
        $this->exportList($filename);
    }

    public function exportList($filename = false)
    {
        $files = File::allFiles(storage_path('excel/exports'));

        usort($files, function ($a, $b) use ($files) {
            return File::lastModified($b) - File::lastModified($a);
        });


        $sortedFileObjects = [];
        foreach ($files as $file) {
            $sortedFileObjects[] = [
                'creationDate' => gmdate("Y-m-d H:i:s", (File::lastModified($file))),
                'path' => $file->getRelativePathName()
            ];
        }
        $data = ['files' => array_slice($sortedFileObjects, 0, 10), 'filename' => $filename];
        $this->layout->navbar = View::make('layoutsSuperadmin.navbar', []);
        $this->layout->content = View::make('superadmin.exportList', $data);
    }

    public function checkForExport($filename = null)
    {
        $export = \File::exists(storage_path('excel/exports/' . urldecode($filename) . '.xls'));
        return Response::json(['export' => $export]);
    }

    public function exportDownload($path)
    {
        \Excel::load(storage_path('excel/exports/' . $path))->export('xls');
        return Redirect::to('superadmin/exportlist');
    }

    public function validateDate($date)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }

    public function deleteCompanyRapport($companyId)
    {
        Company::where('id', $companyId)->update(array('finalized' => 0, 'rapport' => null));
        return Redirect::to('/superadmin/bedrijven/detail/' . $companyId);
    }

    public function viewCompanyRapport($companyId)
    {
        // same as below
        $data['navPos'] = 10;
        $data['title'] = "Totaalresultaat";
        $data['scan_id'] = 1;

        /** @var Company $company */
        $company = Company::where('id', $companyId)->first();

        $rapportService = new \App\Services\RapportService($this->userScanService);
        $diskData = $rapportService->readFromDisk($companyId, $company->getHighestFinalizedScanId());

        if ($diskData) {
            $data = array_merge($data, $diskData);
            $data['companyName'] = $company->company_name;

            $this->layout->content = View::make('superadmin.totaalresultaat', $data);
        }
    }

    public function downloadCompanyRapport($companyId)
    {
        /** @var Company $company */
        $company = Company::where('id', $companyId)->first();

        $rapportService = new \App\Services\RapportService($this->userScanService);
        $filePath = $rapportService->readFromDisk($companyId, $company->getHighestFinalizedScanId(), 'pdf');

        // DOWNLOAD THE PDF
        $headers = array(
            "Content-Type: application/octet-stream",
            "Content-Disposition: attachment; filename=" . urlencode($filePath),
            "Content-Type: application/octet-stream",
            "Content-Type: application/download",
            "Content-Description: File Transfer",
            "Content-Length: " . filesize($filePath)
        );


        $date = new DateTime();
        $companyName = preg_replace("/[^A-Za-z0-9 ]/", '', $company->company_name);
        $companyName = $date->format('Ymd') . '_' . str_replace(" ", "-", strtolower($companyName)) . '.pdf';

        return Response::download($filePath, $companyName, $headers);
    }

    public function impersonate($id)
    {
        $adminId = Auth::user()->id;

        Session::put('impersonate', Crypt::encrypt($adminId));
        Auth::loginUsingId($id);

        if (Auth::user()->permission_level_id > 5) {
            return Redirect::to('/werkgevers');
        }

        return Redirect::to('/werknemers');

    }

}
