<?php namespace App\Providers;

use App\Models\UserResponse;
use Illuminate\Support\ServiceProvider;


class UserResponseServiceProvider extends ServiceProvider
{
    public function register() {
        // TODO: Implement register() method.
        $this->app->singleton(UserResponse::class, function ($app) {
            return new UserResponse();
        });

    }
}