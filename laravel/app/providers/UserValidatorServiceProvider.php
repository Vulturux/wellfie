<?php namespace App\Providers;

use App\Models\UserValidator;
use Illuminate\Support\ServiceProvider;

class UserValidatorServiceProvider extends ServiceProvider
{
    public function register() {
        // Override default confide user validator.
        $this->app->bind('confide.user_validator', UserValidator::class);
    }
}