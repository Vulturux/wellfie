<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Question extends \Eloquent {

	protected $fillable = ['question', 'question_nr', 'order', 'more_info', 'type', 'layout_type_class','question_connection_id', 'question_titles_id', 'totalsection_id', 'status'];

    public static $rules = array(
        'question' => 'required|min:5',
    );

    public $with = ['questionTitle'];

    /**
     * Get the owning question title of this question.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionTitle()
    {
       return $this->belongsTo('App\Models\QuestionsTitle', 'question_titles_id');
    }

    // returns array with survey for view
    public function getfullsurvey($catid = 0)
    {

        $words = DB::table('questions')
            ->select('questions.id', 'questions.question_nr', 'questions.question',
                     'questions.more_info', 'questions.type', 'questions.layout_type_class',
                     'questions.question_titles_id',
                      'titles.id as titlesID', 'titles.title',
                      'answers.questions_id', 'answers.id as answer_id', 'answers.answer', 'answers.status'

            )
            ->leftJoin('questions_titles as titles', 'questions.question_titles_id', '=', 'titles.id')
            ->leftJoin('answers as answers', 'questions.id', '=', 'answers.questions_id')
            ->where('answers.status', '!=', 1)

            ->orderBy('questions.question_titles_id', 'DESC')
            ->orderBy('questions.question_nr', 'DESC')
            ->orderBy('questions.id', 'ASC')
            ->get();

        $wordSort = array();
        foreach ($words as $key => $word) {
            $word  = (array) $word;

            $wordSort [$word['titlesID']]['title'] = $word['title'];
            $wordSort [$word['titlesID']][$word['id']]['question'] = array(
                    'question_id' => $word['id'],
                    'question' => $word['question'],
                    'question_nr' => $word['question_nr'],
                    'more_info' => $word['more_info'],
                    'type' => $word['type'],
                    'layout_type_class' => $word['layout_type_class'],
            );

            // answers
            $answer = array(
                "id" => $word['answer_id'],
                "answer" => $word['answer']
            );
            if (!array_key_exists('answer', $wordSort[$word['titlesID']][$word['id']])) {
                $wordSort[$word['titlesID']][$word['id']]['answer']=array();
            }
            array_push($wordSort[$word['titlesID']][$word['id']]['answer'], $answer);

        }

        return $wordSort;


    }

    // returns array with survey for view

    public function getCatQuestionsAnswers($catid = 0)
    {

        $words = DB::table('questions')
            ->select('questions.id', 'questions.question_nr', 'questions.question',
                'questions.more_info', 'questions.type', 'questions.layout_type_class',
                'questions.question_titles_id',
                'titles.id as titlesID', 'titles.title',  'titles.question_cat_id', 'titles.type as titletype',
                'answers.questions_id', 'answers.id as answer_id', 'answers.answer', 'answers.points', 'answers.more_info as answerinfo',  'answers.status as status'

            )
            ->leftJoin('questions_titles as titles', 'questions.question_titles_id', '=', 'titles.id')
            ->leftJoin('answers as answers', 'questions.id', '=', 'answers.questions_id')
            ->leftJoin('questions_categories as categories', 'categories.id', '=', 'titles.question_cat_id')
            ->where('answers.status', '!=', 1)
            ->orderBy('questions.question_titles_id', 'ASC')
            ->orderBy('questions.id', 'ASC')
            ->orderBy('answers.order', 'ASC')
            ->orderBy('answers.id', 'ASC')
            ->where('titles.question_cat_id', $catid)
            ->get();

        $wordSort = array();
        foreach ($words as $key => $word) {
            $word  = (array) $word;

            $wordSort [$word['titlesID']]['title'] = $word['title'];
            $wordSort [$word['titlesID']]['titletype'] = $word['titletype'];

            $wordSort [$word['titlesID']][$word['id']]['question'] = array(
                'question_id' => $word['id'],
                'question' => $word['question'],
                'question_nr' => $word['question_nr'],
                'more_info' => $word['more_info'],
                'type' => $word['type'],
                'layout_type_class' => $word['layout_type_class'],
            );

            // answers
            $answer = array(
                "id" => $word['answer_id'],
                "answer" => $word['answer'],
                "points" => $word['points'],
                "answerinfo" => $word['answerinfo'],
            );
            if (!array_key_exists('answer', $wordSort[$word['titlesID']][$word['id']])) {
                $wordSort[$word['titlesID']][$word['id']]['answer']=array();
            }
            array_push($wordSort[$word['titlesID']][$word['id']]['answer'], $answer);

        }
        return $wordSort;


    }

    public function getCatRequiredQuestions($catid) {
        $words = DB::table('questions')
            ->select('questions.id', 'questions.question_nr', 'questions.question',
                'questions.more_info', 'questions.type', 'questions.layout_type_class',
                'questions.question_titles_id',
                'titles.id as titlesID', 'titles.title',  'titles.question_cat_id', 'titles.type as titletype'
            )
            ->leftJoin('questions_titles as titles', 'questions.question_titles_id', '=', 'titles.id')
            ->leftJoin('questions_categories as categories', 'categories.id', '=', 'titles.question_cat_id')
            ->where('questions.layout_type_class', 'not like', '%notrequired%')
            ->orderBy('questions.question_titles_id', 'ASC')
            ->orderBy('questions.id', 'ASC')
            ->where('titles.question_cat_id', $catid)
            ->get();

        $wordSort = array();
        foreach ($words as $key => $word) {
            $word  = (array) $word;

           $wordSort[$word['id']] = array(
                'question_id' => $word['id'],
                'question_nr' => $word['question_nr'],
                'type' => $word['type'],
                'layout_type_class' => $word['layout_type_class'],
            );

        }
        return $wordSort;
    }


}