<?php namespace App\Models;

class Documentation extends \Eloquent
{
    protected $fillable = ['title', 'description', 'type', 'link'];
    protected $table = 'documentations';
    public static $rules = array(
        'title' => 'required|min:5',
        'type' => 'required|in:brochure,afbeelding,video',
        'link' => 'required',
    );
}
