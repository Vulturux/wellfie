<?php namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Position;

//todo remove
class UserPosition extends \Eloquent {

    protected $fillable = ['position', 'position_max', 'scan_id', 'user_id'];

    /**
     * Scope a query to only include positions for given company id.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $companyId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCompanyUserPositions($query, $companyId)
    {
        return $query->join('users as u', 'user_positions.user_id', '=', 'u.id')
            ->select('u.id', 'u.username', 'user_positions.scan_id', 'u.company_id', 'user_positions.id', 'u.permission_level_id', 'user_positions.position', 'user_positions.position_max')
            ->where('u.company_id', $companyId)
            ->where('u.permission_level_id', '>', 0)
            ->orderBy('user_positions.position', 'desc');
    }

    /**
     * Get the owning user of this user position.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @todo Move this to service or repo. Does not belong here.
     */
    public function getUserPosition($userId = 0)
    {
        $positionData = array();

        // laatst toegevoegde positie eerst
        $userPosition = UserPosition::whereUserId($userId)
            ->orderBy('id', 'desc')
            ->first();

        //  company position for scan id
        $userHighestCompanyPos = UserPosition::companyUserPositions(Auth::user()->company_id)
            ->first();

        if (count($userPosition) == 0) {
            $positionData['pos']=0;
            $positionData['id']='';
            $positionData['user_scan_id']=0;
            $positionData['scan_id']=$userHighestCompanyPos->scan_id;
        } else {
            $positionData['pos']=$userPosition->position;
            $positionData['id']=$userPosition->id;
            $positionData['user_scan_id']=$userPosition->scan_id;
            $positionData['scan_id']=$userHighestCompanyPos->scan_id;
            if ($positionData['scan_id'] > $positionData['user_scan_id']) {
                $positionData['pos']=0;
            }
        }

        return $positionData;
    }

    /**
     * Get company position for authenticated user.
     *
     * @todo Move this to service or repo. Does not belong here.
     * @todo => I feel you bro, working on it
     * @return array $pos.
     */
    public static function getCompanyPosition()
    {
        // Get user for company's highest pos for company id from authenticated user.
        $userHighestCompanyPos = UserPosition::companyUserPositions(Auth::user()->company_id)
            ->first();

        if (!$userHighestCompanyPos) {

            // Create new user pos.
            UserPosition::create(array(
                'user_id' => Auth::user()->id,
                'scan_id' => 1,
                'position' => -1,
                'position_max' => -1,
            ));

            return array(
                'navPos' => -1, 
                'maxPos' => -1, 
                'cat' => 0, 
                'title' => "Introductie"
            );

        } else {

            // Check for "Vragenlijst" position values.
            $pos = Position::wherePosId($userHighestCompanyPos->position)->first();

            $title = $pos->title;
            $cat = $pos->cat_id ?: 0;

            return array(
                'navPos' => $userHighestCompanyPos->position,
                'maxPos' => $userHighestCompanyPos->position_max,
                'cat' => $cat,
                'scan_id' => $userHighestCompanyPos->scan_id, 
                'title' => $title
            );
        }
    }

    public function updateUserPosition()
    {
        //this is pure shit

        // current position
        $userId = Auth::user()->id;
        $position = $this->getUserPosition($userId);
        $posRow = UserPosition::where('user_id', $userId)->first();

        if ($position['pos'] == 0) {

            // NEW USER -> CREATE POS AFTER FIRST BLOCK OF ANSWERS
            // note to previous developer: this makes double records for a user in the user_positions table
            if(!$posRow) $posRow = new UserPosition();
            $posRow->position = 1;
            $posRow->user_id = $userId;
            $posRow->scan_id = $position['scan_id'];

            $newPos = 1;

        } else {

            // EXISTING USER -> UPDATE POS
            if(!$posRow) $posRow = new UserPosition();
            $CurPos = $position['pos'];
            $posRow->position = $CurPos+1;
            $posRow->scan_id = $position['scan_id'];

            $newPos = $CurPos+1;
        }

        $posRow->save();


        return $newPos;

    }

    public function fullCompanyoverview($companyId = 0) {

        $users = array();

        $invites = DB::table('invites')
            ->select('id', 'created_at', 'email', 'invitecode','company_id', 'user_id', 'scan_id')
            ->where('company_id', $companyId)
            ->groupBy('invites.id')
            ->orderBy('scan_id', 'DESC')
            ->orderBy('user_id', 'DESC')
            ->get();



        return $users;

    }

    public function getActiveCount($year = NULL, $week = NULL, $permission_level_id = NULL) {
        $query = DB::table('user_positions')
          ->where('user_positions.position', '>=', 1)
          ->leftJoin('users as users', 'user_positions.user_id', '=', 'users.id')
          ->groupBy('user_positions.id','users.id','users.company_id')
        ;

        //exit(var_dump($query));
        if (!is_null($year) && !is_null($week)) {
            //$yearweek = sprintf('%d%02d', $year, $week);
            //$query->where(DB::raw('YEARWEEK(user_positions.updated_at, 1)'), '=', $yearweek);
            $query->where(DB::raw('extract(year from user_positions.created_at::date)'), '=', $year);
            $query->where(DB::raw('extract(week from user_positions.created_at::date)'), '=', $week);

        }
        else if (!is_null($year)) {
            //$query->where(DB::raw('YEAR(user_positions.updated_at)'), '=', $year);
           $query->where(DB::raw('extract(year from user_positions.created_at::date)'), '=', $year);
        }
        
        if (!is_null($permission_level_id)) {
            $query->where('users.permission_level_id', '=', $permission_level_id);
        }

        $result =  count($query->get());

        return $result;
    }

    public function getCompletedCount($year = NULL, $week = NULL, $permission_level_id = NULL) {

        $query = DB::table('user_positions')
          ->where('user_positions.position', '>=', 5)
          ->leftJoin('users as users', 'user_positions.user_id', '=', 'users.id');

        if (!is_null($year) && !is_null($week)) {
            //$yearweek = sprintf('%d%02d', $year, $week);
            //$query->where(DB::raw('YEARWEEK(user_positions.updated_at, 1)'), '=', $yearweek);
            $query->where(DB::raw('extract(year from user_positions.created_at::date)'), '=', $year);
            $query->where(DB::raw('extract(week from user_positions.created_at::date)'), '=', $week);
        }
        else if (!is_null($year)) {
            //$query->where(DB::raw('YEAR(user_positions.updated_at)'), '=', $year);
            $query->where(DB::raw('extract(year from user_positions.created_at::date)'), '=', $year);
        }

        if (!is_null($permission_level_id)) {
            $query->where('users.permission_level_id', '=', $permission_level_id);
        }
        else {
            $query->groupBy('user_positions.id', 'users.id','users.company_id');
        }

        $result = count($query->get());

        return $result;
    }

}
