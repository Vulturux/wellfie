<?php namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Webpatser\Uuid\Uuid;

class Invite extends \Eloquent {
	protected $fillable = ['email', 'invitecode', 'company_id', 'user_id', 'scan_id'];

    // check if code exists

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function userScan(){
        return UserScan::where('user_id', '=', $this->user_id)
            ->where('scan_id', '=', $this->scan_id)
            ->first();
    }

     public function checkUserExists($invitecode) {

        $email = DB::table('invites')
            ->select('*')
            ->where('invitecode', $invitecode)
            ->take(1)
            ->get();

         if ($email) {
             $userExists = DB::table('users')
                 ->select('*')
                 ->where('email', $email[0]->email)
                 ->take(1)
                 ->get();

             return $userExists;
         }

         return false;
    }

//    public function checkInvite($invitecode)
//    {
//
//        $invite = DB::table('invites')
//            ->select('*')
//            ->where('invitecode', $invitecode)
//            ->first();
//
//
//        if ($invite) {
//        	/** @var \App\Models\Company $company */
//        	$company = Company::find($invite->company_id);
//            ddd( $company->getHighestFinalizedScanId() + 1, $invite->scan_id);
//        	if($invite->scan_id === $company->getHighestFinalizedScanId() + 1){
//		        return $invite;
//
//	        }else{
//        		return false;
//	        }
//
//        } else {
//            return false;
//        }
//
//    }

    /* public function checkEmailExists($invitecode)
    {

        return $invite;
    } */


    // first time
    public function createandSendInvite($email = false, $subject = false, $message = false, $scan_id = 1)
    {
        // TO DO wanneer user nog geen invite
        $invite = $this->createInvite($email, $scan_id);

        // Only send emails when new invite is created.

        if ($invite['failed'] !== true) {
            $this->sendMessageMail($email, $subject, $message, $invite['invitecode']);
            //$this->sendInviteMail($email, $invite['invitecode']);
        }
    }

    // second time resend
    public function resendInviteMail($email = false, $subject = false, $mailcontent = false, $scan_id = 1) {
        $invite = $this->createInvite($email);

        if ($invite['failed'] !== true) {
            $this->sendMessageMail($email, $subject, $mailcontent, $invite['invitecode']);
        }
    }

    #1
    public function sendMessageMail($email = false, $subject = false, $mailcontent = false, $inviteCode = null)
    {
        $data['email'] = $email;
        $data['subject'] = $subject;
        $data['mailcontent'] = $mailcontent;
	    $data['invitecode']= $inviteCode;
        // send email TO DO
        Mail::later(3, 'werkgevers.mailinvite', $data, function($message) use ($data)
        {
            $message->to($data['email'])->subject($data['subject']);

        });

        // FOR TESTING ONLY
        /*Mail::send('werkgevers.mailinvite', $data, function($message) use ($data)
        {
            $message->to('davedriesmans@gmail.com')->subject('LOG -- wellfie: new invitation for '.$data['email'] );
        });*/
    }
    #2
    public function createInvite($email = false, $scan_id = 1)
    {

        // Add email validation
        $validator = Validator::make(
            array('email' => $email),
            array('email' => 'required|email')
        );

        if ($validator->fails()) {
            return ['invitecode' => '', 'exists' => false, 'failed' => true];
        }

        // Return if invite is already in the DB
        if ($invite = Invite::where('email', $email)
                            ->where('scan_id', $scan_id)
                            ->first()) {
            $invite->invitecode = str_replace('-', '', Uuid::generate(4));
            $invite->save();
            return ['invitecode' => $invite->invitecode, 'exists' => true, 'failed' => false];
        }

        // Create new invite
        $invite = new Invite();
        $invite->invitecode = str_replace('-', '', Uuid::generate(4));
        $invite->email = $email;
        $invite->company_id = Auth::user()->company_id;
        $invite->scan_id = $scan_id;
        $invite->save();

        return ['invitecode' => $invite->invitecode, 'exists' => false, 'failed' => false];

    }

    public function keygen($length=10)
    {
        $key = '';
        list($usec, $sec) = explode(' ', microtime());
        mt_srand((float) $sec + ((float) $usec * 100000));

        $inputs = array_merge(range('z','a'),range(0,9),range('A','Z'));

        for($i=0; $i<$length; $i++)
        {
            $key .= $inputs{mt_rand(0,61)};
        }
        return $key;
    }

    public function getInviteCount($year = NULL, $week = NULL, $group = NULL, $companyId = NULL) {

        $query = DB::table('invites');

        if (!is_null($year) && !is_null($week)) {
            //$yearweek = sprintf('%d%02d', $year, $week);
            //$query->where(DB::raw('YEARWEEK(created_at, 1)'), '=', $yearweek);
            $query->where(DB::raw('extract(year from created_at::date)'), '=', $year);
            $query->where(DB::raw('extract(week from created_at::date)'), '=', $week);
        }
        else if (!is_null($year)) {
            //$query->where(DB::raw('YEAR(created_at)'), '=', $year);
            $query->where(DB::raw('extract(year from created_at::date)'), '=', $year);
        }

        if (!is_null($group)) {
            $query->groupBy('invites.id','invites.' . $group);
            $result = count($query->get());
        }
        
        else {
            $result = $query->count();
        }

        if (!is_null($companyId)) {
            $query->where('company_id', '=', $companyId);
        }

        return $result;
    }

}
