<?php 

namespace App\Models;

class Position extends \Eloquent
{
	protected $fillable = ['title', 'path', 'pos_id', 'cat_id'];

	public $timestamps = false;

	/**
     * Scope a query to only include stage categories.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStageCategories($query)
    {
        return $query->whereNotNull('cat_id');
    }

}