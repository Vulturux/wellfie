<?php namespace App\Models;

class QuestionsCategory extends \Eloquent {
	protected $fillable = ['category', 'werkgever_werknemer'];

    protected $table = 'questions_categories';

}