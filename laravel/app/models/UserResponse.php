<?php namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserResponse extends \Eloquent {

    protected $fillable = ['questions_id', 'answer_id', 'open_answer', 'user_id', 'scan_id', 'type'];
    protected $table = 'responses';

    /**
     * Get the owning question of user response.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
       return $this->belongsTo('App\Models\Question', 'questions_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function allCompanyWerknemers($scanid = 1, $companyId = 0)
    {
        $werknemers = DB::table('users')
            ->select('users.id', 'users.username', 'users.company_id', 'users.permission_level_id',
                     'userpositions.user_id', 'userpositions.position', 'userpositions.scan_id'
            )
            ->where('users.company_id', $companyId)
            ->where('userpositions.position', '>', 4)
            ->where('userpositions.scan_id', $scanid)
            ->where(function ($query) {
                $query->where('users.permission_level_id', '=', 0)
                    ->orWhere('users.permission_level_id', '=', NULL);
            })
            ->join('user_positions as userpositions', 'userpositions.user_id', '=', 'users.id')
            ->get();

        $werknemers = (array) $werknemers;
        return ($werknemers);
    }

//    public function getAllCompanyEmployeesId($scanid = 1, $companyId = 0)
//    {
//        $employees = DB::table('users')
//            ->select('users.id', 'users.username', 'users.company_id', 'users.permission_level_id',
//                'userpositions.user_id', 'userpositions.position', 'userpositions.scan_id'
//            )
//            ->where('users.company_id', $companyId)
//            ->where('userpositions.position', '>', 4)
//            ->where('userpositions.scan_id', $scanid)
//            ->where(function ($query) {
//                $query->where('users.permission_level_id', '=', 0)
//                    ->orWhere('users.permission_level_id', '=', NULL);
//            })
//            ->join('user_positions as userpositions', 'userpositions.user_id', '=', 'users.id')
//            ->get();
//
//        //$employees = (array) $werknemers;
//
//        $arr = [];
//        foreach($employees as $employee) {
//            $arr[] = $employee->id;
//        }
//        return ($arr);
//    }

    public function allCompanyUsers($scanid = 1, $companyId = 0)
    {
        $werknemers = DB::table('users')
            ->select('users.id', 'users.username', 'users.company_id', 'users.permission_level_id',
                'userpositions.user_id', 'userpositions.position', 'userpositions.scan_id'
            )
            ->where('users.company_id', $companyId)
            ->where('userpositions.scan_id', $scanid)
            ->join('user_positions as userpositions', 'userpositions.user_id', '=', 'users.id')
            ->distinct()
            ->get();

        $werknemers = (array) $werknemers;

        return ($werknemers);
    }

    public static function allResponses($userid = 0, $scanid = 1, $catId = false)
    {
        $query = DB::table('responses')
            ->select('responses.id', 'responses.questions_id', 'responses.answer_id', 'responses.open_answer', 'responses.user_id',  'responses.scan_id',
                 'questions.question', 'questions.question_nr', 'questions.type', 'questions.totalsection_id',
                'answers.id as answerID', 'answers.answer', 'answers.points'
            )
            ->join('questions as questions', 'responses.questions_id', '=', 'questions.id', 'left outer')
            ->join('answers as answers', 'responses.answer_id', '=', 'answers.id', 'left outer')
            ->orderBy('questions.id', 'ASC')
            ->where('responses.scan_id', $scanid)
            ->where('responses.user_id', $userid);

        if ($catId) {
            $query->leftJoin('questions_titles as titles', 'questions.question_titles_id', '=', 'titles.id')
                ->where('titles.question_cat_id', $catId);
        }

        return $query->get();
    }

    public function scoreFixed($userId = 0, $scanId = 0)
    {
        $scoreObject = UserResponse::select('responses.id', 'responses.questions_id', 'responses.answer_id', 'responses.open_answer', 'responses.user_id',  'responses.scan_id',
                'questions.id as QuestionID',  'questions.question', 'questions.question_nr', 'questions.type', 'questions.totalsection_id',
                'answers.id as answerID', 'answers.answer', 'answers.questions_id', 'answers.points',
                'sectiontotals.id', 'sectiontotals.section_title', 'sectiontotals.questions_cat_id', 'sectiontotals.total',  'sectiontotals.maxgood', 'sectiontotals.maxmodarate'
            )
            ->Join('answers as answers', 'responses.answer_id', '=', 'answers.id')
            ->Join('questions as questions', 'answers.questions_id', '=', 'questions.id')
            ->Join('sectiontotals as sectiontotals', 'sectiontotals.id', '=', 'questions.totalsection_id')
            ->orderBy('questions.id', 'ASC')
            ->where('responses.scan_id', $scanId)
            ->where('responses.user_id', $userId)
            ->groupBy('questions.id', 'responses.id','answers.id','sectiontotals.id')
            ->get();

        return $scoreObject;
    }

    public function scores($section =0, $userid = 0, $scanid = 1)
    {

        $scoreObject = DB::table('responses')
            ->select('responses.id', 'responses.questions_id', 'responses.answer_id', 'responses.open_answer', 'responses.user_id',  'responses.scan_id',
                      'questions.id as QuestionID',  'questions.question', 'questions.question_nr', 'questions.type', 'questions.totalsection_id',
                      'answers.id as answerID', 'answers.answer', 'answers.questions_id', 'answers.points',
                      'sectiontotals.id', 'sectiontotals.section_title', 'sectiontotals.questions_cat_id', 'sectiontotals.total',  'sectiontotals.maxgood', 'sectiontotals.maxmodarate'
            )
            ->Join('answers as answers', 'responses.answer_id', '=', 'answers.id')
            ->Join('questions as questions', 'answers.questions_id', '=', 'questions.id')
            ->Join('sectiontotals as sectiontotals', 'sectiontotals.id', '=', 'questions.totalsection_id')
            ->orderBy('questions.id', 'ASC')
            ->where('responses.scan_id', $scanid)
            ->where('responses.user_id', $userid)
            ->where('questions.totalsection_id', $section)

            ->groupBy('questions.id', 'responses.id','answers.id','sectiontotals.id')
            ->get();

        return ($scoreObject);
    }

    public function totalSectionScoreResult($scores, $section = 0, $userId = 0, $scanId = 1)
    {
        // Scores get all for user, then filter by section

        $questions = $scores[$userId];

        $totalsection = array();
        $totalsection['points'] = 0;

        $scores = [];
        foreach ($questions as $q) {
            if ($q->totalsection_id == $section)
            {
                $scores[] = $q;
                $totalsection['points'] = $totalsection['points'] + $q->points;
            }
        }

        if (!empty($scores)) {
            $totalsection['title'] = $scores[0]->section_title;
            $totalsection['total'] = $scores[0]->total;
            $totalsection['maxgood'] = $scores[0]->maxgood;
            $totalsection['maxmoderate'] = $scores[0]->maxmodarate;
            if ($totalsection['points'] <= $totalsection['maxgood']) {
                $totalsection['evaluation'] = "+2";
            } elseif ($totalsection['points'] > $totalsection['maxmoderate']) {
                $totalsection['evaluation'] = "-2";
            } else {
                $totalsection['evaluation'] = "00";
            }
        } else {
            $totalsection['title'] = 'no answers given for section id '.$section;
            $totalsection['total'] = 0;
            $totalsection['maxgood'] = 0;
            $totalsection['maxmoderate'] = 0;
            $totalsection['evaluation'] = "nodata";
        }


        return ($totalsection);
    }

    public function totalsectionscore($section = 0, $userid = 0, $scanid = 1)
    {

        $scores = $this->scores($section, $userid, $scanid);

        $totalsection = array();
        $totalsection['points'] = 0;

        foreach ($scores as $key => $score) {
            $totalsection['points'] = $totalsection['points'] + $score->points;
        }

        if (!empty($scores)) {
            $totalsection['title'] = $scores[0]->section_title;
            $totalsection['total'] = $scores[0]->total;
            $totalsection['maxgood'] = $scores[0]->maxgood;
            $totalsection['maxmoderate'] = $scores[0]->maxmodarate;
            if ($totalsection['points'] <= $totalsection['maxgood']) {
                $totalsection['evaluation'] = "+2";
            } elseif ($totalsection['points'] > $totalsection['maxmoderate']) {
                $totalsection['evaluation'] = "-2";
            } else {
                $totalsection['evaluation'] = "00";
            }
        } else {
            $totalsection['title'] = 'no answers given for section id '.$section;
            $totalsection['total'] = 0;
            $totalsection['maxgood'] = 0;
            $totalsection['maxmoderate'] = 0;
            $totalsection['evaluation'] = "nodata";
        }


        return ($totalsection);

    }

    public function totalCatscore($catid =0, $userid = 0, $scanid = 1)
    {
        $sections = Sectiontotal::where('questions_cat_id', '=', $catid)->get();

        $totalcat = array();
        $totalcat['id']=$catid;
        $totalcat['sumpoints'] = 0;
        $totalcat['catevaluation'] = "nocatdata";

        foreach ($sections as $key => $section) {
               $totalcat[$key] = $this->totalsectionscore($section->id, $userid, $scanid);
               $totalcat['sumpoints'] = $totalcat['sumpoints'] + $totalcat[$key]['points'];
        }

        // manual coded as too many rules!
        if ($catid == 3 || $catid == 4 || $catid == 5 || $catid == 6) {
            if ($catid == 3) {
                // competenties
                $maxmoderate = 11;
                $maxgood = 5;
            }
            if ($catid == 4) {
                // waarden
                $maxmoderate = 6;
                $maxgood = 2;
            }
            if ($catid == 5) {
                // werk
                $maxmoderate = 8;
                $maxgood = 3;
            }
            if ($catid == 6) {
                // werkvermogen (zit bij gezondheid)
                $maxmoderate = 15;
                $maxgood = 9;
            }

            if ($totalcat['sumpoints'] <= $maxgood) {
                $totalcat['catevaluation']  = "+2";
            } elseif ($totalcat['sumpoints'] > $maxmoderate) {
                $totalcat['catevaluation'] = "-2";
            } else {
                $totalcat['catevaluation'] = "00";
            }

        }
        if ($catid == 2) {
            // Medische vragen 0, levenstijl 1, burnout 2
            // totalcat $totalcat['sumpoints'] <= 4 used to be <= 2
             if ($totalcat['sumpoints'] <= 4) {
                $totalcat['catevaluation']  = "+2";
            } elseif (($totalcat[0]['points'] > 4 && $totalcat[2]['points'] > 4) || ($totalcat[3]['points'] > 12)) {
                $totalcat['catevaluation'] = "-2";
            } else {
                $totalcat['catevaluation'] = "00";
            }
        }

        return $totalcat;


    }

    // almost the same as totalCatscore
    public function totalMedianCatscore($employees, $scores, $catid = 0, $scanid = 1)
    {
        $sections = Sectiontotal::where('questions_cat_id', '=', $catid)->get();


        $totalcat = array();
        $totalcat['id']=$catid;
        $totalcat['sumpoints'] = 0;
        $totalcat['catevaluation'] = "nocatdata";

        // alle werknemers
        $SumPointsArray = array();
        $pointsSectionArray = array();

        $i=0;
        // lopen over alle werknemers
        foreach ($employees as $employee) {

            if (isset($scores[$employee])) {
                $sumpointsWN = 0;
                foreach ($sections as $key => $section) {
                    $totalcat[$key] = $this->totalSectionScoreResult($scores, $section->id, $employee, $scanid);
                    // voor median
                    $sumpointsWN = $sumpointsWN+$totalcat[$key]['points'];
                    $totalcat['sumpoints'] = $totalcat['sumpoints']+$totalcat[$key]['points'];
                    // percentages per sectie
                    $pointsSectionArray[$key][] = $totalcat[$key]['evaluation'];
                }

                $SumPointsArray[$i++] = $sumpointsWN;
            }
        }

        // median calls
        foreach ($sections as $key => $section) {
            $totalcat[$key]['medianevaluation'] =  $this->calculate_median($pointsSectionArray[$key]);
            $totalcat[$key]['pointssectionArray'] =  $this->calculate_percentages($pointsSectionArray[$key]);
        }
        $totalcat['sumpointsMedian'] = $this->calculate_median($SumPointsArray);
        $totalcat['sumpointsArray'] = $SumPointsArray;

        // manual coded as too many rules! same in other function, not DRY :)
        if ($catid == 3 || $catid == 4 || $catid == 5 || $catid == 6) {
            if ($catid == 3) {
                // competenties
                $maxmoderate = 11;
                $maxgood = 5;
            }
            if ($catid == 4) {
                // waarden
                $maxmoderate = 6;
                $maxgood = 2;
            }
            if ($catid == 5) {
                // werk
                $maxmoderate = 8;
                $maxgood = 3;
            }
            if ($catid == 6) {
                // werkvermogen (zit bij gezondheid)
                $maxmoderate = 15;
                $maxgood = 9;
            }

            if ($totalcat['sumpointsMedian'] <= $maxgood) {
                $totalcat['catevaluation']  = "+2";
            } elseif ($totalcat['sumpointsMedian'] > $maxmoderate) {
                $totalcat['catevaluation'] = "-2";
            } else {
                $totalcat['catevaluation'] = "00";
            }

        }


        if ($catid == 2) {
            // Medische vragen 0, levenstijl 1, burnout 2

            if ($totalcat['sumpointsMedian'] < 4) {
                $totalcat['catevaluation']  = "+2";
            } elseif (  ($totalcat[0]['points'] > 4 && $totalcat[2]['points'] > 4)  || ($totalcat[3]['points'] > 12)) {
                $totalcat['catevaluation'] = "-2";
            } else {
                $totalcat['catevaluation'] = "00";
            }
        }

        return $totalcat;


    }

    public function oneQuestionForEmployees($employees, $scanId = 1, $questionId = 0)
    {
        // werknemer = 1 werkgever = 2
        $scoreObject = \App\Models\Question::select('responses.id', 'responses.questions_id', 'responses.answer_id', 'responses.user_id')
            ->Join('responses as responses', 'responses.questions_id', '=', 'questions.id')
            ->where('responses.scan_id', $scanId)
            ->whereIn('responses.user_id', $employees)
            ->where('responses.questions_id', $questionId)
            ->get();

        return $scoreObject;
    }

    public function oneQuestion($userid = 0, $scanid = 1, $questionid = 0)
    {

        // werknemer = 1 werkgever = 2
        $scoreObject = DB::table('questions')
            ->select('responses.id', 'responses.questions_id', 'responses.answer_id', 'responses.open_answer', 'responses.user_id',  'responses.scan_id',
                'questions.id as QuestionID',  'questions.question', 'questions.question_nr', 'questions.type', 'questions.totalsection_id',
                'answers.id as answerID', 'answers.answer', 'answers.questions_id', 'answers.points', 'answers.order'
            )
            ->Join('responses as responses', 'responses.questions_id', '=', 'questions.id')
            ->Join('answers as answers', 'responses.answer_id', '=', 'answers.id')
            ->orderBy('questions.id', 'ASC')
            ->where('responses.scan_id', $scanid)
            ->where('responses.user_id', $userid)
            ->where('responses.questions_id', $questionid)

            ->get();

        $questions= array();

        foreach ($scoreObject as $key => $question) {
            $questions[] = array(
                'question' => $question->question,
                'points' => $question->points,
                'answer' => $question->answer,
                'answer_id' => $question->answerID,
                'order' => $question->order,
                'question_nr' => $question->question_nr);
        }

        return $questions;
    }

    public function oneQuestionCheckboxesWerknemers($employees, $scanid = 1, $questionid = 0) {

        $aantalWN = count($employees);

        // array declarations
        $responsesArray = array();
        $responses = array();
        $answersPossible = Answer::where('questions_id', '=', $questionid)->get();
        foreach ($answersPossible as $key => $answer) {
            $responsesArray[$answer->id]=0;
        }

        // ask all data
        $responseQuestionWNs = $this->oneQuestionForEmployees($employees, $scanid, $questionid);
        foreach ($responseQuestionWNs as $responseQuestionWN)
        {
           $responsesArray[$responseQuestionWN->answer_id]++;
        }

        // create percentages
        foreach ($answersPossible as $key => $answer) {
            $responses[$answer->id]['countpercentage'] = round(($responsesArray[$answer->id]/$aantalWN)*100);
        }

        return ($responses);

    }


    public function allScoresFixed($companyId, $scanId = 1)
    {
//        $employees = $this->getAllCompanyEmployeesId($scanId, $companyId);

        $scoreObject = \App\Models\Question::select('responses.id', 'responses.questions_id', 'responses.answer_id', 'responses.open_answer', 'responses.user_id',  'responses.scan_id',
                'questions.id as questionID',  'questions.question', 'questions.question_nr', 'questions.type', 'questions.totalsection_id',
                'answers.id as answerID', 'answers.answer', 'answers.questions_id', 'answers.points', 'answers.order'
            )
            ->Join('responses as responses', 'responses.questions_id', '=', 'questions.id')
            ->Join('answers as answers', 'responses.answer_id', '=', 'answers.id')
            ->join('users', 'responses.user_id', '=', 'users.id')
            ->join('user_scans', 'users.id', '=', 'user_scans.user_id')
            ->where('responses.scan_id', $scanId)
            ->where('questions.id', '>', 0)
            ->where('questions.id', '<', 102)
            ->where('user_scans.questionnaire_position', '>', 4)
            ->where('user_scans.scan_id', $scanId)
            ->where('users.company_id', $companyId)
            ->where(function ($query) {
                $query->where('users.permission_level_id', '=', 0)
                    ->orWhere('users.permission_level_id', '=', NULL);
            })
            ->get()->toArray();

        return $scoreObject;
    }

    public function allScores($userid = 0, $scanid = 1, $werknemerwerkgever = 1)
    {

        // werknemer = 1 werkgever = 2
        $scoreObject = DB::table('questions')
            ->select('responses.id', 'responses.questions_id', 'responses.answer_id', 'responses.open_answer', 'responses.user_id',  'responses.scan_id',
                'questions.id as QuestionID',  'questions.question', 'questions.question_nr', 'questions.type', 'questions.totalsection_id',
                'answers.id as answerID', 'answers.answer', 'answers.questions_id', 'answers.points', 'answers.order'
            )
            ->Join('responses as responses', 'responses.questions_id', '=', 'questions.id')
            ->Join('answers as answers', 'responses.answer_id', '=', 'answers.id')
            ->orderBy('questions.id', 'ASC')
            ->where('responses.scan_id', $scanid)
            ->where('responses.user_id', $userid)
            ->get();

        $questions= array();

        foreach ($scoreObject as $key => $question) {
            $questions[$question->questions_id] = array(
                            'question' => $question->question,
                            'points' => $question->points,
                            'answer' => $question->answer,
                            'answer_id' => $question->answerID,
                            'order' => $question->order,
                            'question_nr' => $question->question_nr);
        }

        // cleanArray array
        $start=1;
        $max=102;
        if ($werknemerwerkgever == 2) {
            $start=102;
            $max=191;
        }

        for($i = $start; $i < $max; $i++)  {
            if(!isset($questions[$i]))
                {
                    $questions[$i] = array(
                        'question' => 'not answered',
                        'points' => 0,
                        'answer' => '',
                        'answer_id' => 0,
                        'question_nr' => 0);
            }
        }

        return ($questions);


    }

    // used in allMedianScores
    // returns
    //      1/ per question the median answer
    //      2/ the % per answer
    //      3/ total of answers/questions for that scan

    public function answersForQuestions($start = 1, $stop = 101)
    {
        $answersObject = DB::table('questions')
            ->select(
                'questions.id as questionID',  'questions.question', 'questions.question_nr', 'questions.type',
                'answers.id as answerID', 'answers.answer', 'answers.questions_id', 'answers.points'
            )
            ->join('answers as answers', 'answers.questions_id', '=', 'questions.id')
            ->where('questions.id', '>=', $start)
            ->where('questions.id', '<=', $stop)
            ->get();

        foreach ($answersObject as $key => $answerObject) {
            $answersArray[$answerObject->questionID][$answerObject->answerID] = (array) $answerObject;
            $answersArray[$answerObject->questionID][$answerObject->answerID]['countanswered'] = 0;
            $answersArray[$answerObject->questionID][$answerObject->answerID]['countpercentage'] = 0;
        }

        $answerObject = null;
        return $answersArray;

    }

    public function allMedianScores($companyId, $scanid = 1)
    {
        $responseData = $this->allScoresFixed($companyId, $scanid);

        $score = array();
        $question = array();

        // Questions employee
        $start=1;
        $stop=101;

        $answers = $this->answersForQuestions($start, $stop);

        for ($questionID = $start; $questionID <= $stop; $questionID++) {

                $question[$questionID]['answers'] = $answers[$questionID];
                $answers[$questionID] = null;
                $questionExists = true;

                // vraag array opzetten en mogelijke antwoorden in array
                $question[$questionID]['total']=0;
                $question[$questionID]['avarageAnswer']=0;
                $question[$questionID]['avaragePoints']=0;
                $question[$questionID]['avarageAnswerInteger']=0;
                $question[$questionID]['avarageAnswerFloat']=0;
                $question[$questionID]['medianAnswer']=0;
                $question[$questionID]['medianPoints']=0;

            if ($questionExists)
            {

                $first = 1;
                $firstAnswerId = 0;
                $answersArray = array();
                $pointsArray = array();
                // fill array


                foreach($responseData as $k => $v)
                {
                    if ($v['questionID'] == $questionID)
                    {
                        $answerId = $responseData[$k]['answer_id'];
                        $points = $responseData[$k]['points'];

                        $question[$questionID]['total']++;

                        $question[$questionID]['answers'][$answerId]['countanswered']++;

                        // for to be proccessed array for avarage and median
                        if ($first == 1) {
                            $firstAnswerId = $answerId;
                            $first=0;
                        }
                        $answersArray[] = $answerId;
                        $pointsArray[] = $points;
                    }
                }


                //  loop over answers for percentages
                foreach ($question[$questionID]['answers'] as $key => $questionAnswers) {
                    if ($question[$questionID]['total'] > 0) {
                        $question[$questionID]['answers'][$key]['countpercentage'] = round(($questionAnswers['countanswered'] / $question[$questionID]['total']) * 100);
                    } else {
                        $question[$questionID]['answers'][$key]['countpercentage'] = 0;
                    }
                }
                // calculate median and avarages

                if (count($answersArray) > 0) {
                    $median = $this->calculate_median($answersArray);
                    $medianPoints = $this->calculate_median($pointsArray);

                    $average = $this->calculate_average($answersArray);
                    $averagePoints = $this->calculate_median($pointsArray);
                    $averagePointsPure =  (float) $this->calculate_average_pure($answersArray);

                    $question[$questionID]['medianPoints']= $medianPoints;
                    $question[$questionID]['avaragePoints']= $averagePoints;

                    $question[$questionID]['medianAnswer'] = $question[$questionID]['answers'][$median]['answer'];

                    $question[$questionID]['avarageAnswer']= (int) $question[$questionID]['answers'][$average]['answer'];

                    //$question[$questionnr]['avarageAnswerFloat'] = number_format( $question[$questionnr]['avarageAnswer'] - 1 + $averagePointsPure,2);

                    // Average
                    $arr = [];
                    foreach ($answersArray as $answer) {
                            $arr[] = $question[$questionID]['answers'][$answer]['answer'];
                    }

                    $average = 0;
                    if (count($arr) > 0)
                    {
                        $average = array_sum($arr) / count($arr);
                    }

                    $question[$questionID]['avarageAnswerFloat'] = number_format($average, 2);

                    // key($question[$questionnr]['answers']) = first answer
                    $question[$questionID]['avarageAnswerInteger']= $average-(key($question[$questionID]['answers'])-2);
                }
            }
        }
        $responseData = null;
        return $question;
    }

    /* MEDIAN FUNCTIONS */

    public function calculate_median($arr) {

        sort($arr);
        $count = count($arr); //total numbers in array
        $middleval = floor(($count-1)/2); // find the middle value, or the lowest middle value
        if($count % 2) { // odd number, middle is the median
            $median = $arr[$middleval];
        } else { // even number, calculate avg of 2 medians
            $low = $arr[$middleval];
            $high = $arr[$middleval+1];
            $median = (($low+$high)/2);
        }
        $median = $this->closest($arr, $median);
        return $median;

    }

    public function calculate_average($arr) {

        $count = count($arr); //total numbers in array
        $total = 0;
        foreach ($arr as $value) {
            $total = $total + $value; // total value of array numbers
        }
        $average = ($total/$count); // get average value

        $average = $this->closest($arr, $average);
        return $average;

    }


    public function calculate_average_pure($arr) {

        $count = count($arr); //total numbers in array
        $total = 0;
        foreach ($arr as $value) {
            $total = $total + $value; // total value of array numbers
        }
        $avarage = ($total/$count); // get average value
        $avarageRounded = round($avarage, 2);
        $averageKommaOnly =  $avarageRounded - round($avarage);
        return  number_format($averageKommaOnly, 2);

    }

    public function closest($array, $number) {

        sort($array);
        foreach ($array as $a) {
            if ($a >= $number) return $a;
        }
        return end($array);
    }

    public function calculate_percentages($arr) {

        $scores =  array(
            "slecht" => 0,
            "neutraal" => 0,
            "goed" => 0,
        );
        $count = count($arr); //total numbers in array

        foreach ($arr as $value) {
            if($value == '+2') {
                $scores['goed']++;
            } elseif ($value == '-2') {
                $scores['slecht']++;
            } else {
                $scores['neutraal']++;
            }
        }
        $scorespercentage['goed'] =  round(($scores['goed']/$count)*100);
        $scorespercentage['slecht'] = round(($scores['slecht']/$count)*100);
        $scorespercentage['neutraal'] = round(($scores['neutraal']/$count)*100);

        return $scorespercentage;


    }

    /* EXPORT */

//    public function export($filterData = [])
//    {
//        $filterData = array_merge([
//          'scanId' => 1,
//          'companyId' => NULL,
//          'positionId' => NULL,
//          'permissionLevelId' => NULL,
//          'employeeCountMin' => NULL,
//          'employeeCountMax' => NULL,
//          'regionId' => NULL,
//          'sectorId' => NULL,
//          'periodFrom' => NULL,
//          'periodTo' => NULL,
//          'exportType' => NULL
//        ], $filterData);
//
//        $header = [
//            0 => 'company_id',
//            1 => 'user_id',
//            2 => 'level',
//        ];
//
//        // Check type of export.
//        if ($filterData['exportType']) {
//            $header += [
//                3 => 'datum aangemaakt',
//                4 => 'naam bedrijf',
//                5 => 'datum voltooid',
//                6 => 'naam werkgever',
//                7 => 'functietitel',
//                8 => 'e-mail',
//                9 => 'status',
//                10 => 'ondernemingsnummer'
//            ];
//        }
//
//        $colOffset = count($header) - 1;
//
//        if (!is_null($filterData['companyId'])) {
//            $filterData['companyId'] = [$filterData['companyId']];
//        }
//
//        // header row QUESTIONS
//
//        // NULL = Antwoorden | 1 = Gegevens
//        if ($filterData['exportType'] === null) {
//
//            if (!is_null($filterData['permissionLevelId'])) {
//                // question_cat_id < 6 == WG   => permission level id = 6
//                // question_cat_id >= 6 == WN  => permission level id = 0
//                $allquestions = DB::table('questions')
//                    ->select('questions.*')
//                    ->leftJoin('questions_titles as titles', 'questions.question_titles_id', '=', 'titles.id')
//                    ->orderBy('questions.id', 'ASC')
//                    ->where('titles.question_cat_id', $filterData['permissionLevelId'] ? '<' : '>=', 6)
//                    ->get();
//            } else {
//                $allquestions = \App\Models\Question::all();
//            }
//
//            foreach ($allquestions as $key => $question) {
//                $header[$question->id + $colOffset] = substr(strip_tags($question->question), 0, 50);
//            }
//
//        }
//
//        $export = [
//            $header
//        ];
//
//        $companiesIds = $this->getCompanyIds($filterData['companyId'], $filterData['regionId'], $filterData['sectorId'], $filterData['employeeCountMin'], $filterData['employeeCountMax'], $filterData['positionId']);
//
//        foreach($companiesIds as $companyId) {
//            $employees = $this->getUsers($filterData['scanId'], $companyId, $filterData['periodFrom'], $filterData['periodTo'], $filterData['permissionLevelId'], $filterData['positionId']);
//            $rows = $this->addRowsFromWerknemers($employees, $filterData['scanId'], $header, $filterData['exportType'], $colOffset);
//            $export = array_merge($export, $rows);
//        }
//
//        return ($export);
//    }
//
//    public function addRowsFromWerknemers($werknemers, $scanid, $header, $exportType = null, $colOffset) {
//
//        $rows = [];
//
//        if ($exportType && isset($werknemers[0])) {
//            $companyWG = DB::table('users')
//                ->join('companies', 'users.company_id', '=', 'companies.id')
//                ->where('users.company_id', '=', $werknemers[0]->company_id)
//                ->where('users.permission_level_id', '>', 0)
//                ->first();
//        }
//
//        foreach ($werknemers as $key => $werknemer) {
//
//            $row = array_combine(array_keys($header), array_fill(0, count($header), '-'));
//            $row[0] = (int) $werknemer->company_id;
//            $row[1] = (int) $werknemer->id;
//             if ($werknemer->permission_level_id > 0) {
//                 $row[2] = "WG";
//             }
//             else {
//                 $row[2] = "WN";
//             };
//
//            if ($exportType) {
//
//                if (isset($companyWG)) {
//                    $row[3] = $werknemer->created_at;
//                    $row[4] = $companyWG->company_name;
//                    $row[5] = $companyWG->finalized ? $companyWG->updated_at : 'niet voltooid';
//                    $row[6] = $companyWG->username;
//                    $row[7] = $companyWG->function;
//                    $row[8] = $companyWG->email;
//                    $row[9] = $companyWG->confirmed ? 1 : 0;
//                    $row[10] = $companyWG->ondernemingsnummer;
//                } else {
//                    $row[3] = '/';
//                    $row[4] = '/';
//                    $row[5] = '/';
//                    $row[6] = '/';
//                    $row[7] = '/';
//                    $row[8] = '/';
//                    $row[9] = '/';
//                    $row[10] ='/';
//                }
//
//            }
//
//            else {
//                $antwoordvraag = (array) $this->allResponses($werknemer->id, $scanid);
//
//                foreach ($antwoordvraag as $key => $vraag) {
//                    if (!is_null($vraag->questions_id) && isset($row[$vraag->questions_id + 2])) {
//                        if ($vraag->answer_id !== null) {
//                            if (($row[$vraag->questions_id + $colOffset] !== '-') && !empty($vraag->answer)) {
//                                $row[$vraag->questions_id + $colOffset] = $row[$vraag->questions_id + $colOffset] . ' / ' . $vraag->answer;
//                            }
//                            else {
//                                $row[$vraag->questions_id + $colOffset] = $vraag->answer;
//                            }
//                        }
//                        else {
//                            $row[$vraag->questions_id + $colOffset] = $vraag->open_answer;
//                        }
//                    }
//                }
//            }
//            $rows[] = $row;
//        }
//
//        return $rows;
//    }
//
//    // USED in export
//    public function getUsers($scan_id = 1, $company_id = NULL, $periodFrom = NULL, $periodTo = NULL, $permission_level_id = NULL, $position_id = NULL) {
//
//        $query = DB::table('users')->select(
//            'users.id',
//            'users.username',
//            'users.company_id',
//            'users.permission_level_id',
//            'users.created_at',
//            'userpositions.position',
//            'userpositions.scan_id'
//        )->join('user_positions as userpositions', 'userpositions.user_id', '=', 'users.id');
//
//        if (!is_null($permission_level_id)) {
//            $query->where('users.permission_level_id', $permission_level_id ? '>=' : '<', 6);
//            $query->where('users.permission_level_id', '>=', 0);
//        }
//
//        if (!is_null($position_id) && $position_id != 10) {
//            $query->where('userpositions.position', '>=', $position_id);
//        }
//
//        if (!is_null($scan_id)) {
//            $query->where('userpositions.scan_id', $scan_id);
//        }
//
//        if (!is_null($company_id)) {
//            $query->where('users.company_id', $company_id);
//        }
//
//        if (!is_null($periodFrom)) {
//            $query->where('users.created_at', '>=', $periodFrom);
//        }
//
//        if (!is_null($periodFrom)) {
//            $query->where('users.created_at', '<', $periodTo);
//        }
//
//        $werknemers = $query->distinct()->get();
//
//        $werknemers = (array) $werknemers;
//
//        return ($werknemers);
//    }
//
//    public function getCompanyIds($companyId, $regionId, $sectorId, $employeeCountMin, $employeeCountMax, $positionId) {
//
//        $companies = array();
//
//        $query = DB::table('users')->select(
//          'users.id',
//          'users.username',
//          'users.company_id',
//          'users.permission_level_id'
//        )->distinct();
//
//        $query->where('users.permission_level_id', '>', 5);
//
//        if (!is_null($companyId)) {
//            $query->where('users.company_id', '=', $companyId);
//        }
//
//        if (!is_null($regionId)) {
//            $query->join('responses as region', 'region.user_id', '=', 'users.id');
//            $query->where('region.questions_id', '=', 102);
//            $query->where('region.answer_id', '=', $regionId);
//        }
//
//        if (!is_null($employeeCountMax) || !is_null($employeeCountMin)) {
//            $query->join('responses as employeecount', 'employeecount.user_id', '=', 'users.id');
//
//            $query->where('sector.questions_id', '=', 103);
//
//            if (!is_null($employeeCountMin)) {
//                $query->where('employeecount.open_answer', '>', $employeeCountMin);
//            }
//
//            if (!is_null($employeeCountMin)) {
//                $query->where('employeecount.open_answer', '<', $employeeCountMax);
//            }
//        }
//
//        if (!is_null($sectorId)) {
//            $query->join('responses as sector', 'sector.user_id', '=', 'users.id');
//            $query->where('sector.questions_id', '=', 104);
//            $query->where('sector.answer_id', '=', $sectorId);
//        }
//
//        if (!is_null($positionId) && $positionId == 10) {
//            $query->join('user_positions as userpositions', 'userpositions.user_id', '=', 'users.id');
//            $query->where('userpositions.position', '>=', $positionId);
//        }
//
//        $werkgevers = (array) $query->get();
//
//        foreach ($werkgevers as $werkgever) {
//            $companies[] = $werkgever->company_id;
//        }
//
//        return $companies;
//  }

}
