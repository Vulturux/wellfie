<?php namespace App\Models;

use Illuminate\Support\Facades\DB;

class Faq extends \Eloquent {
	protected $fillable = ['id','vraag', 'category_naam', 'image', 'antwoord', 'order'];
    protected $table = 'faq';
    public static $rules = array(
        'vraag' => 'required|min:5',
    );

    public static function getCategories(){

        $categories = DB::table('faq')->select('category_naam')->distinct()->orderBy('category_naam')->get();
        return array_pluck($categories, 'category_naam');
    }

}
