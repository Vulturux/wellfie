<?php namespace App\Models;

class Answer extends \Eloquent {

	protected $fillable = ['answer', 'questions_id', 'points', 'order', 'more_info', 'status'];

    public static $rules = array(
        'questions_id' => 'required',
    );

    // relationship
    public $with = ['question'];
    public function question() {
        return $this->belongsTo('App\Models\Question', 'questions_id');
    }


}