<?php namespace App\Models;

class EventModel extends \Eloquent
{

    protected $fillable = ['title', 'description', 'datetime'];
    protected $table = 'events';
    public static $rules = array(
        'title' => 'required|min:5|max:255',
        'datetime' => 'required',
    );
    protected $dates = ['datetime'];

    public function setDatetimeAttribute($value)
    {
        $this->attributes['datetime'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value);
    }


}
