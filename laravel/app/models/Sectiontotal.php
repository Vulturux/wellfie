<?php namespace App\Models;

class Sectiontotal extends \Eloquent {

	protected $fillable = ['sectiontitle' ,'questions_cat_id' ,'total'];

    protected $table= 'sectiontotals';

}