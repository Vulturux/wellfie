<?php namespace App\Models;

use Illuminate\Auth\UserInterface;
use Illuminate\Support\Facades\DB;
use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;

class User extends \Eloquent implements ConfideUserInterface
{

    protected $fillable = ['username', 'email', 'password', 'company_id', 'permission_level_id', 'confirmed', 'function', 'remember_token'];

    use ConfideUser;


    // relationship
//    public $with = ['Company', 'UserPosition'];
    public function company()
    {
        return $this->hasOne('App\Models\Company', 'id', 'company_id');
    }


    public function UserPosition()
    {
        return $this->hasOne('App\Models\UserPosition');
    }

    public function UserScans()
    {
        return $this->hasMany('App\Models\UserScan');
    }

    public function latestUserScan(){
        return $this->UserScans()->orderBy('scan_id', 'DESC')->take(1);
    }

    public function invites(){
        return $this->hasMany(Invite::class, 'email', 'email');
    }

    public function userExists($email = false)
    {
        if ($user = User::where('email', $email)->first()) {
            return true;
        }

        return false;
    }


    public function getUserCount($year = NULL, $week = NULL, $permission_level = NULL) {

        $query = DB::table('users');
        if (!is_null($year) && !is_null($week)) {
            //$yearweek = sprintf('%d%02d', $year, $week);
            $query->where(DB::raw('extract(year from created_at::date)'), '=', $year);
            $query->where(DB::raw('extract(week from created_at::date)'), '=', $week);
        }
        else if (!is_null($year)) {
            $query->where(DB::raw('extract(year from created_at::date)'), '=', $year);
        }

        if (!is_null($permission_level)) {
            $query->where('users.permission_level_id', '=', $permission_level);
        }

        return $query->count();
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->permission_level_id >= 9;
    }

    /**
     * @return bool
     */
    public function isEmployer(){
        return $this->permission_level_id === 6;
    }

    /**
     * @return bool
     */
    public function isImpersonator()
    {
        if (Session::has('impersonate')) {
            $userId = Crypt::decrypt(Session::get('impersonate'));
            $user = User::find($userId);
            if ($user) {
                return $user->permission_level_id >= 9;
            }            
        }
        return false;
    }

}
