<?php namespace App\Models;

use App\Constants\PermissionLevels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Company extends \Eloquent
{
    protected $fillable = ['id', 'company_name', 'ondernemingsnummer', 'association_type', 'region', 'remark_question', 'sector'];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function werkgever(){
        return $this->users()->where('permission_level_id', PermissionLevels::EMPLOYER);
    }

    public function rapports()
    {
        return $this->hasMany('App\Models\CompanyRapport');
    }

    public function companyScans(){
        return $this->hasMany('App\Models\CompanyScan');
    }

    public function latestCompanyScan(){
        return $this->companyScans()
            ->orderBy('scan_id', 'desc')->take(1);
    }

    public function getCurrentScan(){
        return $this->latestCompanyScan()->first();
    }

    public function employeeCount() {
        return $this->hasMany('users')->selectRaw('count(company_id) as numberOfEmployees')->groupBy('company_id');
    }


    //todo change or remove
    public function scan($scanId)
    {
        return CompanyRapport::where('company_id', '=', $this->id)
            ->where('scan_id', '=', $scanId)
            ->first();
    }


    public function getInvitedEmployees($scanId )
    {
        $invQuery = Invite::query();
        //todo changed this, check if still okay
        $invitesArray =
            $invQuery
                ->where('company_id', '=', \Auth::user()->company_id)
                ->where('scan_id', '=', $scanId)
                ->with('user')
                ->get();

//        $usersArray = DB::table('invites')
//            ->select('invites.created_at as created_at',  'invites.id as invitesId', 'users.id as userId', 'invites.email as invitesemail', 'users.email as usersemail',  'invites.username', 'invites.company_id', 'users.username', 'users.permission_level_id')
//            ->where('invites.company_id', \Auth::user()->company_id)
//	        ->where('invites.scan_id', $scanId)
//	        ->leftJoin('users as users', 'invites.email', '=',   'users.email')
//            ->groupBy('invites.id', 'users.id')
//            ->orderBy('invites.created_at', 'DESC')
//            ->get();

        if (count($invitesArray) == 0) {
            return array();
        } else {

            $users = array();
            foreach ($invitesArray as $invite) {
                $invited = 'uitgenodigd';
                if (isset($invite->user_id)) {
                    $invited = 'gebruiker';
                } else {
                    $invited = 'uitgenodigd';
                }

                array_push($users,
                    array(
                        'inviteId' => $invite->id,
                        'invited' => $invited,
                        'email' => $invite->email,
                    )
                );
            }
            return $users;
        }

    }


    public function getEmployer()
    {
        return User::where('company_id', $this->id)
            ->where('permission_level_id', PermissionLevels::EMPLOYER)
            ->first();
    }

    public function getCompaniesPerWeek()
    {
        $companiesArray = DB::table('companies')
            ->select(
                DB::raw("to_char(created_at, 'IYYYIW') as week"),
                DB::raw('COUNT(*) as count'),
                DB::raw("to_timestamp(to_char(created_at, 'IYYYIW'), 'IYYYIW')::date as weekstart")
            )
            ->where(DB::raw('created_at'), '>', DB::raw("NOW() - INTERVAL '6 MONTHS'"))
            ->groupBy('week')
            ->orderBy('week')
            ->get();

        return $companiesArray;
    }

    public function getCompanyCount($year = NULL, $week = NULL)
    {
        $query = DB::table('companies');

        if (!is_null($year) && !is_null($week)) {
            $query->where(DB::raw('extract(year from created_at::date)'), '=', $year);
            $query->where(DB::raw('extract(week from created_at::date)'), '=', $week);
        } else if (!is_null($year)) {
            $query->where(DB::raw('extract(year from created_at::date)'), '=', $year);
        }

        return $query->count();
    }

    public function getStatusOverview($limit = 250)
    {
        $companies = Company::with(['werkgever', 'latestCompanyScan'])
            ->select('companies.id', 'companies.created_at', 'companies.company_name')->get();
        /*$companies = (array)DB::table('companies')->select(
            'companies.id',
            DB::raw('DATE(companies.created_at) as created_at'),
            'companies.company_name',
            'users.email as WGemail',
            DB::raw('CASE WHEN userposition.position>=5 THEN DATE(userposition.updated_at) ELSE null END as WGstatus'),
            'users.id as userId'
        )->join('users as users', 'users.company_id', '=', 'companies.id')
            ->join('user_positions as userposition', 'userposition.user_id', '=', 'users.id')
            ->where('users.permission_level_id', '>', 5)
            ->groupBy('users.id', 'companies.id', 'users.email', 'userposition.position', 'userposition.updated_at')
            ->orderBy('companies.id', 'desc')
            ->get();*/


        $sectors = (array)DB::table('responses')->select(
            'user.id as userId',
            'answer.answer as sector'
        )->join('users as user', 'responses.user_id', '=', 'user.id')
            ->join('answers as answer', 'responses.answer_id', '=', 'answer.id')
            ->where('responses.questions_id', '=', 104)
            ->lists('sector', 'userId');

        $invites = (array)DB::table('invites')->select(
            'company_id as companyId',
            DB::raw('COUNT(company_id) as count')
        )
            ->groupBy('company_id')
            ->lists('count', 'companyId');

        $registered = (array)DB::table('invites')->select(
            'company_id as companyId',
            DB::raw('COUNT(company_id) as count')
        )->groupBy('company_id')
            ->where('user_id', '>', 0)
            ->lists('count', 'companyId');

        $completed = (array)DB::table('users')->select(
            'users.company_id as companyId',
            DB::raw('COUNT(company_id) as count')
        )->join('user_scans as user_scans', 'user_scans.user_id', '=', 'users.id')
            ->where('users.permission_level_id', '=', 0)
            ->where('user_scans.questionnaire_position', '>=', 5)
            ->groupBy('users.company_id')
            ->orderBy('users.company_id', 'desc')
            ->lists('count', 'companyId');

        foreach ($companies as $company) {

            $inviteCount = isset($invites[$company->id]) ? $invites[$company->id] : '0';
            $registeredCount = isset($registered[$company->id]) ? $registered[$company->id] : '0';
            $completedCount = isset($completed[$company->id]) ? $completed[$company->id] : '0';

            $company->WNstatus = "$inviteCount/$registeredCount/$completedCount";
            $company->sector = isset($sectors[$company->userId]) ? $sectors[$company->userId] : '-';
        }

        return $companies;
    }

    public function averagePosition($companyId = 0, $scanid = 1)
    {

        $userArray = DB::table('users')->select('users.id', 'users.company_id', 'users.created_at',
            'user_scans.id', 'user_scans.questionnaire_position', 'user_scans.user_id', 'user_scans.scan_id')
            ->where('user_positions.scan_id', $scanid)
            ->where('users.company_id', $companyId)
            ->where(function ($query) {
                $query->where('users.permission_level_id', '=', 0)
                    ->orWhere('users.permission_level_id', '=', NULL);
            })
            ->Join('user_scans', 'user_scans.user_id', '=', 'users.id', 'left outer')
            ->get();


        $companyDetailsArray['avaragePosition'] = array();
        $companyDetailsArray['notstarted'] = 0;
        $avaragePosition = array();
        foreach ($userArray as $key => $user) {
            if ($user->position >= 0) {
                $avaragePosition[] = $user->position;
            }
        }
        if (count($avaragePosition) > 0) {
            $companyDetailsArray['avaragePosition'] = $this->calculate_average($avaragePosition);
            $companyDetailsArray['avaragePositionQuantity'] = count($avaragePosition);

        } else {
            $companyDetailsArray['avaragePosition'] = 0;
            $companyDetailsArray['avaragePositionQuantity'] = 0;

        }
        //dd($companyDetailsArray);
        return $companyDetailsArray;

    }

    public function getHighestFinalizedScanId()
    {
        $id = DB::table('company_rapports')
            ->where('company_id', $this->id)
            ->where('finalized', true)
            ->max('scan_id');
        return $id;
    }


    public function getWGPosition($companyId = 0, $scanid = 1)
    {

        $WGArray = DB::table('users')->select('users.id', 'users.company_id', 'users.email', 'users.created_at',
            'user_positions.id', 'user_positions.position', 'user_positions.user_id', 'user_positions.scan_id')
            ->where('user_positions.scan_id', $scanid)
            ->where('users.company_id', $companyId)
            ->where('users.permission_level_id', '>', 3)
            ->Join('user_positions as user_positions', 'user_positions.user_id', '=', 'users.id', 'left outer')
            ->get();

        if (array_key_exists(0, $WGArray)) {
            $WGArray = $WGArray[0];
            if (!array_key_exists('position', $WGArray)) {
                $WGArray['position'] = 0;
            }
        } else {
            $WGArray['position'] = 0;
            $WGArray['email'] = 'not started yet';
            $WGArray['scan_id'] = 0;
        }


        return $WGArray;

    }

    public function calculate_average($arr)
    {

        $count = count($arr); //total numbers in array
        $total = 0;
        foreach ($arr as $value) {
            $total = $total + $value; // total value of array numbers
        }
        $average = ($total / $count); // get average value
        return round($average);
    }

}
