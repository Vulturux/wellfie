<?php
/**
 * Created by PhpStorm.
 * User: jinsecamps
 * Date: 07/06/17
 * Time: 12:16
 */

namespace App\Models;


class CompanyScan extends \Eloquent
{
    protected $table = 'company_scans';

    protected $fillable = array('company_id', 'scan_id', 'scan_finished', 'rapport_generated', 'rapport_filename');
}