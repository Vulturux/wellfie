<?php namespace App\Models;

use Zizaco\Confide\UserValidator as ConfideUserValidator;
use Zizaco\Confide\UserValidatorInterface;
use Zizaco\Confide\ConfideUserInterface;
use Illuminate\Support\Facades\Lang as Lang;
use Illuminate\Support\Facades\App as App;
use Illuminate\Support\MessageBag;

class UserValidator extends ConfideUserValidator implements UserValidatorInterface
{
    public $rules = [
        'create' => [
            'username' => 'required|alpha_dash', // Confide does not define this as required!
            'email'    => 'required|email',
            'password' => 'required|min:4',
        ],
        'update' => [
            'username' => 'alpha_dash',
            'email'    => 'required|email',
            'password' => 'required|min:4',
        ]
    ];

    /**
     * Override Confide validate handler
     *
     * @param ConfideUserInterface $user
     * @return boolean True if the $user is valid.
     */
    public function validate(ConfideUserInterface $user, $ruleset = 'create')
    {
        $result = parent::validate($user, $ruleset);

        // Check errors on user validation.
        if (!isset($user->errors)) return $result;

        $errMessages = $user->errors->getMessages();
        $attribute = 'username';

        // Check for username attr errors.
        if (isset($errMessages[$attribute])) {
            // Look for key we wish to override.
            $confideErrMsg = Lang::get('confide::confide.alerts.duplicated_credentials');
            $key = array_search($confideErrMsg, $errMessages[$attribute]);
            // Check existing key in error messages
            if ($key !== FALSE) {
                // Unset message.
                unset($errMessages[$attribute][$key]);
                // Merge err messages into new Illuminate\Support\MessageBag.
                $this->resetErrorMsg($user, $errMessages);
                // Add new err message.
                $this->attachErrorMsg(
                    $user,
                    'Deze gebruikersnaam is reeds in gebruik. Gelieve een andere gebruikersnaam te kiezen.',
                    $attribute
                );
            }
        }
        return $result;
    }

    /**
     * We cannot remove messages from \Illuminate\Support\MessageBag so instead re-define MessageBag on the user obj.
     *
     * @param ConfideUserInterface $user
     * @param $messages
     * @see http://stackoverflow.com/questions/18592200/remove-message-from-messagebag
     */
    protected function resetErrorMsg(ConfideUserInterface $user, $messages)
    {
        // Create new message bag.
        $messageBag = App::make(Messagebag::class);
        $messageBag->merge($messages);
        $user->errors = $messageBag;
    }

}