<?php namespace App\Models;

class CompanyRapport extends \Eloquent
{
    protected $fillable = array('company_id', 'scan_id', 'finalized', 'rapport');

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
}