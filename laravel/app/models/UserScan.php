<?php
/**
 * Created by PhpStorm.
 * User: jinsecamps
 * Date: 07/06/17
 * Time: 12:17
 */

namespace App\Models;


class UserScan extends \Eloquent
{
    protected $table = 'user_scans';

    protected $fillable = ['user_id', 'scan_id', 'questionnaire_position'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function position(){
        return Position::where('pos_id', '=', $this->questionnaire_position)->first();
    }
}