<?php namespace App\Models;


class QuestionsTitle extends \Eloquent {

    protected $fillable = ['title', 'question_cat_id', 'type'];
    protected $table = 'questions_titles';

    public function question() {
        return $this->hasMany('App\Models\Question');
    }


    public function category()
    {
        return $this->belongTo('App\Models\QuestionsCategory', 'questions_cat_id');
    }

}