<?php namespace App\Models;

class WerkgeverWerknemer extends \Eloquent {

	protected $fillable = ['werkgever_werknemer'];
    protected $table = 'werkgever_werknemer';

}