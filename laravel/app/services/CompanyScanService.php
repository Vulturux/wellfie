<?php
/**
 * Created by PhpStorm.
 * User: jinsecamps
 * Date: 09/06/17
 * Time: 11:17
 */

namespace App\Services;

use App\Models\CompanyScan;

class CompanyScanService
{
    public function getLatestByCompanyId($companyId){
        return CompanyScan::where('company_id', '=', $companyId)
            ->orderBy('scan_id', 'desc')
            ->first();
    }

    public function createNewAfterLatest($companyId){
        $currentCompanyScan = $this->getLatestByCompanyId($companyId);

        return CompanyScan::firstOrCreate([
            'company_id' => $companyId,
            'scan_id' => isset($currentCompanyScan) ? ++$currentCompanyScan->scan_id : 1,
            'scan_finished' => false,
            'rapport_generated' => false
        ]);
    }

    public function getAllScansByCompany($companyId){
        return CompanyScan::where('company_id', '=', $companyId)->get();
    }

    public function getByScanId($companyId, $scanId){
        return CompanyScan::where('company_id', '=', $companyId)
            ->where('scan_id', '=', $scanId)
            ->first();
    }
}