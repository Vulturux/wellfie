<?php namespace App\Services;

use App\Models\CompanyRapport;
use App\Models\CompanyScan;
use App\Models\UserResponse;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Knp\Snappy\Pdf;

class RapportService
{
    protected $userResponse;
    protected $userScanService;


    public function __construct(UserScanService $userScanService)
    {
        $this->userScanService = $userScanService;
    }

    public function generate($data)
    {
        $companyId = $data[0];
        $scanId = $data[1];
        $userId = $data[2];
        $rapport = [];

        //$scanId = 1;

        $user = User::find($userId);
        Log::info('Rapport requested for company '.$user->company->company_name . " (". $user->company_id.")");
        $completedScans = $this->userScanService->getCompletedUserScansForCurrentScan($companyId, $scanId);

        if (count($completedScans) > 0) {
            $globalRapport = $this->getGlobalRapport($scanId, $user);
            $globalRapport['werknemersCompletedScan'] = count($completedScans);

            $pdf = View::make('werkgevers.totaalresultaat-pdf', $globalRapport)->render();

            $this->writeToDisk($globalRapport, $pdf);
        }
    }

    protected function getAllCompanyEmployees($scanId, $companyId)
    {
        $userResponse = new UserResponse();
        return $userResponse->allCompanyWerknemers($scanId, $companyId);
    }

    protected function getGlobalRapport($scanId, $user)
    {
        $rapport = [];

        $userId = $user->id;
        $companyId = $user->company_id;
        $rapport['company_id'] = $companyId;
        $rapport['scan_id'] = $scanId;

        $userResponse = new UserResponse();
        $employees = $this->userScanService->getCompletedUserScansForCurrentScan($companyId, $scanId, true);

//        $employees = $userResponse->getAllCompanyEmployeesId($scanId, $companyId);
        $rapport['scoresBeleidFeedback'] = $userResponse->allScores($userId, $scanId, 2);
        $rapport['scoresAll'] = $userResponse->allMedianScores($companyId, $scanId);
        $rapport['scoresWG'] = $rapport['scoresBeleidFeedback'];
        $rapport['scoreWGVraag14'] = $userResponse->oneQuestion($userId, $scanId, 116);
        $rapport['scoreWNVraag28'] = $userResponse->oneQuestionCheckboxesWerknemers($employees, $scanId, 28);
        $rapport['scoreWNVraag99'] = $userResponse->oneQuestionCheckboxesWerknemers($employees, $scanId, 99);

        $scores = [];

        foreach ($employees as $employee) {
            $scores[$employee] = $userResponse->scoreFixed($employee, $scanId);
        }

        $rapport['scoresLevelOne'] = $userResponse->totalMedianCatscore($employees, $scores, 2, $scanId);
        $rapport['scoresLevelOneEvaluation'] = $rapport['scoresLevelOne']['catevaluation'];
        $rapport['scoresLevelOnePoints'] = $rapport['scoresLevelOne']['sumpoints'];

        $rapport['scoresLevelTwo'] = $userResponse->totalMedianCatscore($employees, $scores, 3, $scanId);
        $rapport['scoresLevelTwoEvaluation'] = $rapport['scoresLevelTwo']['catevaluation'];
        $rapport['scoresLevelTwoPoints'] = $rapport['scoresLevelTwo']['sumpoints'];

        $rapport['scoresLevelThree'] = $userResponse->totalMedianCatscore($employees, $scores, 4, $scanId);
        $rapport['scoresLevelThreeEvaluation'] = $rapport['scoresLevelThree']['catevaluation'];
        $rapport['scoresLevelThreePoints'] = $rapport['scoresLevelThree']['sumpoints'];

        $rapport['scoresLevelFour'] = $userResponse->totalMedianCatscore($employees, $scores, 5, $scanId);
        $rapport['scoresLevelFourEvaluation'] = $rapport['scoresLevelFour']['catevaluation'];
        $rapport['scoresLevelFourPoints'] = $rapport['scoresLevelFour']['sumpoints'];

        $rapport['scoresLevelFive'] = $userResponse->totalMedianCatscore($employees, $scores, 6, $scanId);
        $rapport['scoresLevelFiveEvaluation'] = $rapport['scoresLevelFive']['catevaluation'];
        $rapport['scoresLevelFivePoints'] = $rapport['scoresLevelFive']['sumpoints'];

        return $rapport;
    }

    protected function writeToDisk($rapport, $pdf)
    {
        try {
            $path = storage_path('data') . "/" . $rapport['company_id'];

            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }

            $date = new \DateTime();
            $filename = $rapport['company_id'] . '_' . $rapport['scan_id'] . '_' . $date->format('Ymd_Hi');
            $fullFilename = $path . '/' . $filename;

            // Generate rapport for website
            file_put_contents($fullFilename . '.json', serialize($rapport));

            // Generate pdf rapport
            $snappy = new Pdf(array(
                    'path' => sys_get_temp_dir(),
                    'xvfb' => TRUE,
                )
            );

            if (App::environment() == "production") {
                $pdf = str_replace(Config::get('app.url'), 'http://localhost', $pdf);
            }

            $snappy->setBinary('/usr/local/bin/wkhtmltopdf');
            $snappy->generateFromHtml($pdf, $fullFilename . '.pdf');

            // Add filename to company
            $companyScanService = new CompanyScanService();
            $companyScan = $companyScanService->getByScanId($rapport['company_id'], $rapport['scan_id']);
            $companyScan->rapport_generated = true;
            $companyScan->rapport_filename = $filename;
            $companyScan->save();

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }

    }

    public function readFromDisk($companyId, $scanId, $type = "")
    {
        $companyScanRapport = CompanyScan::where('company_id', '=', $companyId)
            ->where('scan_id', '=', $scanId)->first();

        if (isset($companyScanRapport) && $companyScanRapport->rapport_generated && !empty($companyScanRapport->rapport_filename)) {
            $path = storage_path('data') . "/" . $companyId;
            $fullFilename = $path . '/' . $companyScanRapport->rapport_filename;

            try {
                switch ($type) {
                    case 'pdf':
                        return $fullFilename . '.pdf';
                        break;
                    default:
                        return unserialize(file_get_contents($fullFilename . '.json'));
                }
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }

        }

        return false;
    }


}
