<?php

namespace App\Services;

use App\Models\Invite;
use App\Models\User;
use App\Models\UserResponse;
use App\Models\UserScan;

/**
 * Class UserService
 *
 * @package \App\Services
 */
class UserService
{

    /**
     * @var UserScanService
     */
    private $userScanService;

    public function __construct(UserScanService $userScanService)
    {

        $this->userScanService = $userScanService;
    }
    
    public function getUserByInviteCode($inviteCode){
                return User::whereHas('invites', function($q) use ($inviteCode){
            $q->where('invitecode', '=', $inviteCode);
        })
          ->first();
    }

    public function delete($id){
        $usersQuery = User::where('id', $id);
        $user = $usersQuery->first();

        if($user){

            $userResponsesQuery = UserResponse::where('user_id', $user->id);
            $invitesQuery = Invite::where('user_id', $user->id);
            $userScansQuery = UserScan::where('user_id', $user->id);

            $usersQuery->delete();
            $userResponsesQuery->delete();
            $invitesQuery->delete();
            $userScansQuery->delete();

            return true;
        }

        return false;
    }

    public function deleteByCompanyId($id){
        $usersQuery = User::where('company_id', $id);
        $users = $usersQuery->get();

        if($users->count() > 0){

            $userIds = array_pluck($users, 'id');

            $userResponsesQuery = UserResponse::whereIn('user_id', $userIds);
            $invitesQuery = Invite::whereIn('user_id', $userIds);
            $userScansQuery = UserScan::whereIn('user_id', $userIds);

            $usersQuery->delete();
            $userResponsesQuery->delete();
            $invitesQuery->delete();
            $userScansQuery->delete();

            return true;
        }

        return false;
    }
}

    
