<?php
/**
 * Created by PhpStorm.
 * User: jinsecamps
 * Date: 23/06/17
 * Time: 11:18
 */

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\UserResponse;
use App\Models\Question;

class ExportService
{
    public function __construct()
    {

    }

    public function getWellfieDataWithFilters($filterData)
    {
        $query = DB::table('companies');

        if (!empty($filterData['companyId'])) $query->where('companies.id', '=', $filterData['companyId']);
        if (!empty($filterData['sectorId'])) $query->where('region', $filterData['sectorId']);
        if (!empty($filterData['regionId'])) $query->where('region', '=', $filterData['regionId']);

        if (!empty($filterData['employeeCountMin']) || !empty($filterData['employeeCountMax'])) {
            $queryString = "";
            if (!empty($filterData['employeeCountMin']) && !empty($filterData['employeeCountMax'])) {
                $queryString = "count(company_id) >= " . $filterData['employeeCountMin'] .
                    " AND count(company_id) <= " . $filterData['employeeCountMax'];
            } else if (!empty($filterData['employeeCountMax'])) {
                $queryString = "count(company_id) >= " . $filterData['employeeCountMax'];
            } else if (!empty($filterData['employeeCountMin'])) {
                $queryString = "count(company_id) >= " . $filterData['employeeCountMin'];
            }

            $query
                ->join(DB::raw("
        (select count(company_id) as employeeCounter, company_id
        from users
        group by company_id
        having " . $queryString . ") companycounter
        "), 'companies.id', '=', 'companycounter.company_id');
        }

        $query
            ->join('users', function ($q) use (&$filterData) {
                $q->on('users.company_id', '=', 'companies.id');
                if (strpos("x" . $filterData['positionId'], 'WG') !== false) {
                    $q->where('users.permission_level_id', '>', 0);
                }
                if (strpos("x" . $filterData['positionId'], 'WN') !== false) {
                    $q->where('users.permission_level_id', '=', 0);
                }
            })
            ->join('company_scans', function ($q) use (&$filterData) {
                $q->on('company_scans.company_id', '=', 'companies.id');
                if (!empty($filterData['scanId'])) {
                    $q->where('company_scans.scan_id', '=', $filterData['scanId']);
                }
                if (strpos("x" .$filterData['positionId'], 'CW') !== false) {
                    $q->where('company_scans.scan_finished', '=', true);
                }
            });
        if (empty($filterData['exportType'])) {
            $query->join('user_scans', function ($q) use (&$filterData) {
                $q->on('user_scans.user_id', '=', 'users.id');
                if (strpos("x" .$filterData['positionId'], 'Comp') !== false) {
                    $q->where('user_scans.questionnaire_position', '>=', 5);
                }
                if (!empty($filterData['periodFrom'])) {
                    $q->where('user_scans.created_at', '>=', $filterData['periodFrom']);
                }
                if (!empty($filterData['periodTo'])) {
                    $q->where('user_scans.created_at', '<=', $filterData['periodTo']);
                }
                if (!empty($filterData['scanId'])) {
                    $q->where('user_scans.scan_id', '=', $filterData['scanId']);
                }
            });
        } else {
            $query->leftJoin('user_scans', function ($q) use (&$filterData) {
                $q->on('user_scans.user_id', '=', 'users.id');
                if (strpos("x" . $filterData['positionId'], 'Comp') !== false) {
                    $q->where('user_scans.questionnaire_position', '>=', 5);
                }
                if (!empty($filterData['periodFrom'])) {
                    $q->where('user_scans.created_at', '>=', $filterData['periodFrom']);
                }
                if (!empty($filterData['periodTo'])) {
                    $q->where('user_scans.created_at', '<=', $filterData['periodTo']);
                }
                if (!empty($filterData['scanId'])) {
                    $q->where('user_scans.scan_id', '=', $filterData['scanId']);
                }
            });
        }


        if (empty($filterData['exportType'])) {
            $query
                ->join('responses', 'responses.user_id', '=', 'users.id')
                ->leftJoin('answers', 'responses.answer_id', '=', 'answers.id')
                ->join('questions', 'responses.questions_id', '=', 'questions.id')
                ->select('questions.question', 'questions.id', 'answers.answer', 'users.company_id', 'users.id as userId',
                    'users.permission_level_id', 'responses.answer_id', 'responses.open_answer')
                ->orderBy('users.id');
        } else {
            $query->select(
                'companies.company_name',
                'users.company_id',
                'companies.ondernemingsnummer',
                'users.id as userId',
                'users.email',
                'users.function',
                'user_scans.created_at',
                'user_scans.updated_at as scanFinishedAt',
                'company_scans.scan_finished',
                'users.permission_level_id',
                'users.username',
                'user_scans.questionnaire_position'
            )->orderBy('users.company_id');
        }

        $firstRow = !empty($filterData['exportType']) ?
            [
                0 => 'company_id',
                1 => 'user_id',
                2 => 'level',
                3 => 'datum aangemaakt',
                4 => 'naam bedrijf',
                5 => 'datum voltooid',
                6 => 'naam werkgever',
                7 => 'functietitel',
                8 => 'e-mail',
                9 => 'status',
                10 => 'ondernemingsnummer'
            ]
            : $this->generateQuestionsHeader(
                strpos("x" . $filterData['positionId'], 'WG') !== false,
                strpos("x" . $filterData['positionId'], 'WN') !== false);

        \Excel::create($filterData['filename'], function ($excel) use ($firstRow, $filterData, $query) {
            $sheetName = !empty($filterData['exportType']) ? 'Administratieve gegevens' : 'Antwoorden-vragen';
            $excel->sheet($sheetName, function ($sheet) use ($firstRow, $query, $filterData) {
                $sheet->appendRow($firstRow);
                $rowForCurrentUser = 1;
                $currentUser = null;
                $query->chunk(200, function ($batch) use ($sheet, $filterData, &$rowForCurrentUser, &$currentUser, $firstRow) {
                    foreach ($batch as $row) {
                        if (!empty($filterData['exportType'])) $sheet->appendRow($this->exportAdministrativeData($row));
                        else {
                            if (is_null($currentUser) || $currentUser !== $row->userId) {
                                $currentUser = $row->userId;
                                $rowForCurrentUser++;
                                $sheet->setCellValue('A' . $rowForCurrentUser, $row->company_id);
                                $sheet->setCellValue('B' . $rowForCurrentUser, $row->userId);
                                $sheet->setCellValue('C' . $rowForCurrentUser, $row->permission_level_id > 0 ? 'WG' : 'WN');
                            }
                            $this->writeUserToExport($row, $rowForCurrentUser, $firstRow, $sheet);
                        }
                    }
                });
            });
        })->store('xls', storage_path('excel/exports'));

    }

    public function generateQuestionsHeader($includeWG, $includeWN)
    {
        $query = Question::select('questions.id', 'questions.question');
        if($includeWN || $includeWG) {
            $query->join('questions_titles', function ($q) use ($includeWN, $includeWG) {
                $q->on('questions.question_titles_id', '=', 'questions_titles.id');
                if ($includeWN && !$includeWG) {
                    $q->where('questions_titles.question_cat_id', '>=', 6);
                } else if (!$includeWN && $includeWG) {
                    $q->where('questions_titles.question_cat_id', '<', 6);
                }
            });
        }
        $questions = $query->get();

        $header = [
            0 => 'Company_id',
            1 => 'User_id',
            2 => 'Level'
        ];
        $offset = count($header);
        for($i = 0; $i < count($questions); $i++) {
            $header[$i+$offset] = $questions[$i]->question;
        }
        return $header;
    }

    public function exportAdministrativeData($data)
    {
        return [
            0 => $data->company_id, //company_id
            1 => $data->userId,
            2 => $data->permission_level_id > 0 ? 'WG' : 'WN',
            3 => $data->created_at ? $data->created_at : 'Niet Aangemaakt', //userscan->created_at
            4 => $data->company_name, //company=> name
            5 => $data->scanFinishedAt ? $data->scanFinishedAt : 'Niet Voltooid',
            6 => $data->username, //user_name
            7 => $data->function, //user->function
            8 => $data->email, //user->email
            9 => $data->questionnaire_position ? $data->questionnaire_position : '/',
            10 => $data->ondernemingsnummer //company_>onr
        ];
    }

    public function num2alpha($n)
    {
        for ($r = ""; $n >= 0; $n = intval($n / 26) - 1)
            $r = chr($n % 26 + 0x41) . $r;
        return $r;
    }

    public function writeUserToExport($user, $rowForCurrentUser, $firstRow, $sheet)
    {
        $currentQuestionKey = array_search($user->question, $firstRow);
        $column = $this->num2alpha($currentQuestionKey);
        $sheet->setCellValue($column . $rowForCurrentUser, empty($user->answer) ? $user->open_answer : $user->answer);
    }

    public function export($filterData = [])
    {

        $filterData = array_merge([
            'scanId' => 1,
            'companyId' => NULL,
            'positionId' => NULL,
            'permissionLevelId' => NULL,
            'employeeCountMin' => NULL,
            'employeeCountMax' => NULL,
            'regionId' => NULL,
            'sectorId' => NULL,
            'periodFrom' => NULL,
            'periodTo' => NULL,
            'exportType' => NULL
        ], $filterData);

        $header = [
            0 => 'company_id',
            1 => 'user_id',
            2 => 'level',
        ];

        $colOffset = count($header) - 1;

        if (!is_null($filterData['companyId'])) {
            $filterData['companyId'] = [$filterData['companyId']];
        }

        // header row QUESTIONS

        // NULL = Antwoorden | 1 = Gegevens
        if ($filterData['exportType'] === null) {

            if (!is_null($filterData['permissionLevelId'])) {
                // question_cat_id < 6 == WG   => permission level id = 6
                // question_cat_id >= 6 == WN  => permission level id = 0
                $allquestions = DB::table('questions')
                    ->select('questions.*')
                    ->leftJoin('questions_titles as titles', 'questions.question_titles_id', '=', 'titles.id')
                    ->orderBy('questions.id', 'ASC')
                    ->where('titles.question_cat_id', $filterData['permissionLevelId'] === 'WN' ? '<' : '>=', 6)
                    ->get();
            } else {
                $allquestions = \App\Models\Question::all();
            }

            foreach ($allquestions as $key => $question) {
                $header[$question->id + $colOffset] = substr(strip_tags($question->question), 0, 50);
            }

        }

        $export = [
            $header
        ];

        $companiesIds = $this->getCompanyIds(
            $filterData['companyId'],
            $filterData['regionId'],
            $filterData['sectorId'],
            $filterData['employeeCountMin'],
            $filterData['employeeCountMax'],
            $filterData['positionId']
        );

        foreach ($companiesIds as $companyId) {
            $employees = $this->getUsers($filterData['scanId'], $companyId, $filterData['periodFrom'], $filterData['periodTo'], $filterData['permissionLevelId'], $filterData['positionId']);
            $rows = $this->addRowsFromWerknemers($employees, $filterData['scanId'], $header, $filterData['exportType'], $colOffset);
            $export = array_merge($export, $rows);
        }

        return ($export);
    }

    public function addRowsFromWerknemers($werknemers, $scanid, $header, $exportType = null, $colOffset)
    {

        $rows = [];

        if ($exportType && isset($werknemers[0])) {
            $companyWG = DB::table('users')
                ->join('companies', 'users.company_id', '=', 'companies.id')
                ->where('users.company_id', '=', $werknemers[0]->company_id)
                ->where('users.permission_level_id', '>', 0)
                ->first();
        }

        foreach ($werknemers as $key => $werknemer) {

            $row = array_combine(array_keys($header), array_fill(0, count($header), '-'));
            $row[0] = (int)$werknemer->company_id;
            $row[1] = (int)$werknemer->id;
            if ($werknemer->permission_level_id > 0) {
                $row[2] = "WG";
            } else {
                $row[2] = "WN";
            };

            if ($exportType) {

                if (isset($companyWG)) {
                    $row[3] = $werknemer->created_at;
                    $row[4] = $companyWG->company_name;
                    $row[5] = $werknemer->scan_finished ? $companyWG->updated_at : 'niet voltooid';
                    $row[6] = $companyWG->username;
                    $row[7] = $companyWG->function;
                    $row[8] = $companyWG->email;
                    $row[9] = $companyWG->confirmed ? 1 : 0;
                    $row[10] = $companyWG->ondernemingsnummer;
                } else {
                    $row[3] = '/';
                    $row[4] = '/';
                    $row[5] = '/';
                    $row[6] = '/';
                    $row[7] = '/';
                    $row[8] = '/';
                    $row[9] = '/';
                    $row[10] = '/';
                }

            } else {
                $responseObj = new UserResponse();
                $antwoordvraag = (array)$responseObj->allResponses($werknemer->id, $scanid);

                foreach ($antwoordvraag as $key => $vraag) {
                    if (!is_null($vraag->questions_id) && isset($row[$vraag->questions_id + 2])) {
                        if ($vraag->answer_id !== null) {
                            if (($row[$vraag->questions_id + $colOffset] !== '-') && !empty($vraag->answer)) {
                                $row[$vraag->questions_id + $colOffset] = $row[$vraag->questions_id + $colOffset] . ' / ' . $vraag->answer;
                            } else {
                                $row[$vraag->questions_id + $colOffset] = $vraag->answer;
                            }
                        } else {
                            $row[$vraag->questions_id + $colOffset] = $vraag->open_answer;
                        }
                    }
                }
            }
            $rows[] = $row;
        }
        return $rows;
    }

    // USED in export
    public function getUsers($scan_id = 1, $company_id = NULL, $periodFrom = NULL, $periodTo = NULL, $permission_level_id = NULL, $position_id = NULL)
    {

        $query = DB::table('users')->select(
            'users.id',
            'users.username',
            'users.company_id',
            'users.permission_level_id',
            'users.created_at',
            'user_scans.questionnaire_position',
            'user_scans.scan_id',
            'company_scans.scan_finished'
        )
            ->join('user_scans', 'user_scans.user_id', '=', 'users.id')
            ->join('company_scans', function ($q) use ($scan_id) {
                $q->on('company_scans.company_id', '=', 'users.company_id');
                if (!is_null($scan_id)) {
                    $q->where('company_scans.scan_id', '=', $scan_id);
                }
            });

        if (!is_null($permission_level_id)) {
            $query->where('users.permission_level_id', $permission_level_id ? '>=' : '<', 6);
            $query->where('users.permission_level_id', '>=', 0);
        }

        if (!is_null($position_id)) {
            $query->where('user_scans.questionnaire_position', '>=', 5);
            switch ($position_id) {
                case 'WN':
                    $query->where('users.permission_level_id', '=', 0);
                    break;
                case 'WG':
                    $query->where('users.permission_level_id', '=', 6);
                    break;
                case 'FINISHED':
                    $query->where('company_scans.scan_finished', '=', true);
                    break;
            }
        }


//        if (!is_null($position_id) && $position_id != 10) {
//            $query->where('user_scans.questionnaire_position', '>=', $position_id);
//        }

//        if (!is_null($scan_id)) {
//            $query->where('user_scans.scan_id', $scan_id);
//        }

        if (!is_null($company_id)) {
            $query->where('users.company_id', $company_id);
        }

        if (!is_null($periodFrom)) {
            $query->where('users.created_at', '>=', $periodFrom);
        }

        if (!is_null($periodTo)) {
            $query->where('users.created_at', '<', $periodTo);
        }
        $werknemers = $query->distinct()->get();

        $werknemers = (array)$werknemers;

        return ($werknemers);
    }

    public function getCompanyIds($companyId, $regionId, $sectorId, $employeeCountMin, $employeeCountMax, $positionId)
    {

        $companies = array();

        $query = DB::table('users')->select(
            'users.id',
            'users.username',
            'users.company_id',
            'users.permission_level_id'
        )->distinct();

        $query->where('users.permission_level_id', '>', 5);

        if (!is_null($companyId)) {
            $query->where('users.company_id', '=', $companyId);
        }

        if (!is_null($regionId)) {
            $query->join('responses as region', 'region.user_id', '=', 'users.id');
            $query->where('region.questions_id', '=', 102);
            $query->where('region.answer_id', '=', $regionId);
        }

        if (!is_null($employeeCountMax) || !is_null($employeeCountMin)) {
            $query->join('responses as employeecount', 'employeecount.user_id', '=', 'users.id');

            $query->where('employeecount.questions_id', '=', 103);

            if (!is_null($employeeCountMin)) {
                $query->where('employeecount.open_answer', '>', $employeeCountMin);
            }

            if (!is_null($employeeCountMin)) {
                $query->where('employeecount.open_answer', '<', $employeeCountMax);
            }
        }

        if (!is_null($sectorId)) {
            $query->join('responses as sector', 'sector.user_id', '=', 'users.id');
            $query->where('sector.questions_id', '=', 104);
            $query->where('sector.answer_id', '=', $sectorId);
        }

        if (!is_null($positionId)) {
            $query->join('user_scans', 'user_scans.user_id', '=', 'users.id');
            $query->where('user_scans.questionnaire_position', '=', 5);
        }

        $werkgevers = (array)$query->get();

        foreach ($werkgevers as $werkgever) {
            $companies[] = $werkgever->company_id;
        }

        return $companies;
    }
}