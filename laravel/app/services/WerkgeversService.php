<?php namespace App\Services;

use App\Models\Question;
use App\Models\UserPosition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use App\Repositories\UserResponseRepository;
use App\Repositories\PositionRepository;
use App\Repositories\UserPositionRepository;

class WerkgeversService extends \App\Services\Base\BaseEmploymentService
{
    protected $userScanService;
    protected $companyScanService;
    protected $companyService;

    public function __construct(UserResponseRepository $userResponseRepo, PositionRepository $positionRepo, UserPositionRepository $userPositionRepo, Question $questionRepo, UserScanService $userScanService, CompanyScanService $companyScanService, CompanyService $companyService)
    {
        parent::__construct($userResponseRepo, $positionRepo, $userPositionRepo, $questionRepo);
        $this->userScanService = $userScanService;
        $this->companyScanService = $companyScanService;
        $this->companyService = $companyService;
    }

    /**
     * Validates & inserts post data (Skips updates for existing values).
     *
     * @param array $data
     */
    public function validateAnswers(array $data)
    {
        $data['questions'] = $this->questionRepo->getCatRequiredQuestions($data['cat']);
        $input = Input::except('_token', 'category', 'submitted');
        $submitted = intval(Input::get('submitted', 0));
        $rules = [];

        /*
         * submitted is used only when someone is impersonating someone (so he doesn't submit the form again)
         * this flag is set in getCatQuestionsAnswers function
         */
        if (!$submitted) {

            foreach ($data['questions'] as $q) {
                $id = $q['question_id'];
                $rules[$id] = 'required';

                if (strpos($q['layout_type_class'], 'number') !== false) {
                    $rules[$id] = 'required|integer';
                } else {
                    $rules[$id] = 'required';
                }
            }

            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                // Get stage path.
                $stagePath = $this->positionRepo->getFirstWhere(array('cat_id' => $data['cat']))->path;
                // Redirect.
                return \Redirect::action('WerkgeversController@stage', $stagePath)
                    ->withErrors($validator)
                    ->withInput($input);
            }

            // Get authenticated user.
            $user = Auth::user();
            // Get questions keyed by id.
            $questions = Question::whereIn('id', array_keys($input))
                ->get()
                ->keyBy('id');

            if(!empty($input['102']) || !empty($input['104']))
                $this->companyService->addSectorAndRegion(Auth::user()->company_id, $input['102'], $input['104']);

            foreach ($input as $question_id => $inputvalue) {

                /**
                 * UserResponseTypes:
                 * - 1 = radio
                 * - 2 = text
                 * - 3 = checkbox
                 */

                if ($questions[$question_id]->type == 3) {

                    foreach ($inputvalue as $checkbox) {

                        // You can only have one corresponding question_id / answer_id / scan_id for a user
                        $userResponse = $this->userResponseRepo->getFirstWhere(array(
                            'questions_id' => $question_id,
                            'answer_id' => $checkbox,
                            'user_id' => $user->id,
                            'scan_id' => $data['scanId']
                        ));

                        if (!$userResponse) {
                            $this->userResponseRepo->create(array(
                                'questions_id' => $question_id,
                                'answer_id' => $checkbox,
                                'user_id' => $user->id,
                                'scan_id' => $data['scanId'],
                                'type' => $questions[$question_id]->type
                            ));
                        }

                    }

                } else {

                    // You can only have one corresponding question_id / scan_id for a user
                    $userResponse = $this->userResponseRepo->getFirstWhere(array(
                        'questions_id' => $question_id,
                        'user_id' => $user->id,
                        'scan_id' => $data['scanId']
                    ));

                    if (!$userResponse) {

                        $attributes = array(
                            'questions_id' => $question_id,
                            'user_id' => $user->id,
                            'scan_id' => $data['scanId'],
                            'type' => $questions[$question_id]->type
                        );

                        if ($questions[$question_id]->type == 2) {
                            $attributes['open_answer'] = $inputvalue;
                        } else {
                            $attributes['answer_id'] = $inputvalue;
                        }

                        $this->userResponseRepo->create($attributes);
                    }

                }

            }

        }

        $userScan = $this->userScanService->updateUserScan();
        $data['navPos'] = $userScan->questionnaire_position;

        if ($data['navPos'] < 5) {
            // direct volgende vragenlijst
            return Redirect::to('werkgevers/stage');
        } elseif ($data['navPos'] >= 5) {
            return Redirect::to('werkgevers/eindresultaat/' . $userScan->scan_id);
        }
    }

    public function initializeNewScan($companyId){
        $companyScan = $this->companyScanService->createNewAfterLatest($companyId);
        return $this->userScanService->createUserScan($companyScan->scan_id, Auth::user()->id);
    }

}