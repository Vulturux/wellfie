<?php namespace App\Services;

use App\Models\Question;
use App\Models\UserPosition;
use App\Models\UserResponse;
use App\Services\UserScanService;
use App\Repositories\UserResponseRepository;
use App\Repositories\PositionRepository;
use App\Repositories\UserPositionRepository;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class WerknemersService extends \App\Services\Base\BaseEmploymentService
{
    protected $userScanService;
    protected $companyScanService;

    public function __construct(UserResponseRepository $userResponseRepo, PositionRepository $positionRepo, UserPositionRepository $userPositionRepo, Question $questionRepo, UserScanService $userScanService, CompanyScanService $companyScanService)
    {
        parent::__construct($userResponseRepo, $positionRepo, $userPositionRepo, $questionRepo);
        $this->userScanService = $userScanService;
        $this->companyScanService = $companyScanService;

    }

    public function validateAnswers($data) 
    {
        $data['questions'] = $this->questionRepo->getCatRequiredQuestions($data['cat']);

        $input = Input::except('_token', 'category', 'submitted');
        $inputQuestions = array_keys($input);

        $rules = [];
        foreach($data['questions'] as $q)
        {
            $id = $q['question_id'];
            $rules[$id] = 'required';

            // Additional rule for max 3 options (WN last section: Werk, werkgemeenschap en leiding)
            if ($id === 99) $rules[$id] .= '|array|max:3';
        }

        $validator = Validator::make($input, $rules);

        if ($validator->fails())
        {
            return \Redirect::to('/werknemers')
                ->withErrors($validator)
                ->withInput($input);
        }

        // TODO: why this?
//        foreach ($data['questions'] as $q)
//        {
//            if (array_search($q['question_id'], $inputQuestions) === false) {
//                exit(var_dump($q));
//            }
//        }
        // Check answers per page

        foreach ($input as $question_id => $inputvalue) {
            $questionType = Question::find($question_id);

            // 2 = text, 3 = checkbox, 1 = radio
            if($questionType->type == 2) {
                $responseArr = array(
                    'questions_id' => $question_id,
                    'open_answer'  => $inputvalue,
                    'user_id'  => Auth::user()->id,
                    'scan_id' => $data['scanId'],
                    'type' => 2
                );

                // You can only have one corresponding question_id / scan_id for a user
                $response = UserResponse::where(array('questions_id' => $question_id, 'user_id' => Auth::user()->id, 'scan_id' => $data['scanId']))->get();
                $response = $response->lists('id');
                if (!$response) {
                    UserResponse::create($responseArr);
                }

            } elseif($questionType->type == 3) {
                foreach ($inputvalue as $checkbox) {
                    $responseArr = array(
                        'questions_id' => $question_id,
                        'answer_id'  => $checkbox,
                        'user_id'  => Auth::user()->id,
                        'scan_id' => $data['scanId'],
                        'type' => 3
                    );

                    // You can only have one corresponding question_id / answer_id / scan_id for a user
                    $response = UserResponse::where(array('questions_id' => $question_id, 'answer_id' => $checkbox, 'user_id' => Auth::user()->id, 'scan_id' => $data['scanId']))->get();
                    $response = $response->lists('id');
                    if (!$response) {
                        UserResponse::create($responseArr);
                    }
                }
            } else {
                $responseArr = array(
                    'questions_id' => $question_id,
                    'answer_id'  => $inputvalue,
                    'user_id'  => Auth::user()->id,
                    'scan_id' => $data['scanId'],
                    'type' => 1
                );

                // You can only have one corresponding question_id / scan_id for a user
                $response = UserResponse::where(array('questions_id' => $question_id, 'user_id' => Auth::user()->id, 'scan_id' => $data['scanId']))->get();
                $response = $response->lists('id');
                if (!$response) {
                    UserResponse::create($responseArr);
                }
            }
        }

        //add or update userScan
        $userScan = $this->userScanService->updateUserScan();
        $data['navPos'] = $userScan->questionnaire_position;

        if ($data['navPos'] == 1) {
            // direct volgende vragenlijst
            return Redirect::to('werknemers');
        } elseif ($data['navPos'] >= 5 ) {
            return Redirect::to('werknemers/eindresultaat/');
        } else {
            // tussenresultaat tonen
            return Redirect::to('werknemers/tussenresultaat/'.$data['navPos']);
        }
    }

    public function initializeNewScan($companyId, $userId){
        $companyScan = $this->companyScanService->getLatestByCompanyId($companyId);
        $this->userScanService->createUserScan($companyScan->scan_id, $userId, $initPos = 0);
    }

}