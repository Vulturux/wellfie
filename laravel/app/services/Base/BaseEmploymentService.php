<?php 

namespace App\Services\Base;

use App\Repositories\UserResponseRepository;
use App\Repositories\PositionRepository;
use App\Repositories\UserPositionRepository;
use App\Models\Question; // -> Refactor

/** 
 * abstract class BaseEmploymentService
 */
abstract class BaseEmploymentService
{
	/**
	 * @var $userResponseRepo
	 */
    protected $userResponseRepo;

    /**
	 * @var $positionRepo
	 */
    protected $positionRepo;

	/**
	 * @var $userPositionRepo
	 */
    protected $userPositionRepo;

	/**
	 * @var $questionRepo
	 */
    protected $questionRepo;

    /**
     * BaseEmploymentService constructor.
     *
     * @todo refactor QuestionRepo (Question) to actual QuestionRepo!
     */
    public function __construct(UserResponseRepository $userResponseRepo, PositionRepository $positionRepo, UserPositionRepository $userPositionRepo, Question $questionRepo)
    {
        $this->userResponseRepo = $userResponseRepo;
        $this->positionRepo = $positionRepo;
        $this->userPositionRepo = $userPositionRepo;
        $this->questionRepo = $questionRepo;
    }

    /**
     * Get position vars for given title.
     *
     * @param string $title
     * @return array $data
     */
    public function getPositionForRoute($title)
    {
    	return $data = $this->positionRepo->getFirstWhere(
    		array('title' => $title),
    		array('title', 'pos_id AS navPos', 'cat_id as cat')
    	)->toArray();
    }

    /**
     * Returns array with survey for view.
     *
     * @param int $catId
     * @return array $questions
     */
    public function getCatQuestionsAnswers($userId, $catId, $scanId)
    {
        // Check for existing user response data.
        $userResponses = $this->getUserResponses($userId, $catId, $scanId);

        // Get questions for cat id.
    	$questions = $this->questionRepo->getCatQuestionsAnswers($catId);

      	// Add defaults to data from stored user responses.
        foreach ($userResponses as $question_id => $userResponse) {
            // Multiple values per UserResponse.
            foreach ($userResponse as $option) {
               // Iterate question blocks
                foreach ($questions as $blockIndex => $block) {
                     // Checkboxes & radios.
                    if ($option->answer_id) {
                        // Check existing block for question id.
                        if (isset($block[$question_id]['answer'])) {
                            // Get answer ids & search for array key.
                            $answer_ids = array_column($block[$question_id]['answer'], 'id');
                            $key = array_search($option->answer_id, $answer_ids);
                            if ($key !== false) {
                                // Set checked flag.
                                $questions[$blockIndex][$question_id]['answer'][$key]['checked'] = true;
                                break;
                            }
                        }
                    }
                    // Open answers.
                    elseif ($option->open_answer) {
                        // Check existing block for question id.
                        if (isset($block[$question_id]['answer'])) {
                            // Set default value.
                            $questions[$blockIndex][$question_id]['answer'][0]['value'] = $option->open_answer;
                        }
                    }
                } 
            }
        }

        // Return questions with a submitted flag.
    	return array(
    		'questions' => $questions,
    		'submitted' => $userResponses->isEmpty() ? false : true,
		);
    }

    /** 
     * Get user responses for given attributes.
     *
     * @param int $userId
     * @param int $catId
     * @param int $scanId
     * @return Illuminate\Database\Eloquent\Collection
     */
    private function getUserResponses($userId, $catId, $scanId)
    {
        // Keyed with relation ORM.
        $relationAttributes = array(
            'question.questionTitle' => array(
                'question_cat_id' => $catId
            )
        );

        $attributes = array(
            'user_id' => $userId,
            'scan_id' => $scanId,
        );

        $select = array(
            'questions_id', 'answer_id', 'open_answer'
        );

        // Get user responses grouped by questions_id.
        return $this->userResponseRepo->getAllWhereHas(
            $relationAttributes,
            $attributes,
            $select
        )->groupBy('questions_id');
    }

    /**
     * @param int $userId
     * @param int $navPos
     * @return bool;
     */
    public function updateUserPosition($userId, $navPos)
    {
        // Get user position.
        $userPos = $this->userPositionRepo->getFirstWhere(array('user_id' => $userId));
        // Check existing.
        if ($userPos) {
            // Set position.
            $userPos->position = $navPos;
            // Check if max position update is required.
            if (!$userPos->position_max || $userPos->position_max < $userPos->position) {
                // Set max position.
                $userPos->position_max = $userPos->position;
            }
            // Update position(s).
            return $userPos->save();
        }
        return false;
    }

}