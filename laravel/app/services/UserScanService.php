<?php namespace App\Services;

use App\Models\UserScan;
use App\Models\Company;
use App\Models\User;
use App\Services\CompanyScanService;
use Illuminate\Support\Facades\Auth;

class UserScanService
{
    protected $companyScanService;

    public function __construct(CompanyScanService $companyScanService = null)
    {
        $this->companyScanService = $companyScanService;
    }

    public function getLatestUserScan($userId)
    {
        return UserScan::where('user_id', '=', $userId)
            ->orderBy('scan_id', 'desc')
            ->first();
    }

    public function getUserScanByUserId($userId, $scanId){
        $query = UserScan::query();

        return $query
            ->where('user_id', '=', $userId)
            ->where('scan_id', '=', $scanId)
            ->first();
    }

    public function createUserScan($scan_id, $userId, $initPos =-1)
    {
        return UserScan::firstOrCreate([
            'user_id' => $userId,
            'scan_id' => $scan_id,
            'questionnaire_position' => $initPos
        ]);
    }

    public function updateUserScan()
    {
        $userScan = $this->getLatestUserScan(Auth::user()->id);
        $userScan->questionnaire_position++;
        $userScan->save();
        return $userScan;
    }

    public function getCompletedUserScansForCurrentScan($companyId, $scanId, $getIdsOnly = false){
        $query = UserScan::whereHas('user', function ($q) use ($companyId) {
            $q
                ->where('company_id', '=', $companyId)
                ->where('function', '=', 'werknemer');
        })
            ->where('scan_id', '=', $scanId)
            ->where('questionnaire_position', '>', 4);

        return $getIdsOnly ? $query->lists('user_id') : $query->get();
    }

    public function canRapportBeGenerated($companyId, $scanId){
        return count($this->getCompletedUserScansForCurrentScan($companyId, $scanId)) >= 10;
    }

    public function getActiveScanCount($year = null, $week = null, $permissionLevel = null){
        $query = UserScan::query();
        if(isset($year)) $query->whereRaw('extract(year from created_at) ='. $year);
        if(isset($year) && isset($week)) $query->whereRaw('extract(week from created_at) ='. $week);
        return count($query->get());
    }

    public function getCompletedCount($year = null, $week = null){
        $query = UserScan::query();
        if(isset($year)) $query->whereRaw('extract(year from created_at) ='. $year);
        if(isset($year) && isset($week)) $query->whereRaw('extract(week from created_at) ='. $week);
        $query->where('questionnaire_position', '>', 4);
        return count($query->get());
    }
}
