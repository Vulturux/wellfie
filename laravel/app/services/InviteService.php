<?php
/**
 * Created by PhpStorm.
 * User: jinsecamps
 * Date: 13/06/17
 * Time: 17:29
 */

namespace App\Services;

use App\Models\Invite;
use App\Models\User;
use App\Queue\SendInviteMailJob;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Mail;

class InviteService
{
    protected $userScanService;
    protected $companyScanService;

    public function __construct(UserScanService $userScanService, CompanyScanService $companyScanService)
    {
        $this->userScanService = $userScanService;
        $this->companyScanService = $companyScanService;
    }

    public function getInvitedEmployeesByCompanyId($companyId, $scanId)
    {
        $invQuery = Invite::query();
        return $invQuery
            ->where('company_id', '=', $companyId)
            ->where('scan_id', '=', $scanId)
            ->with('user')
            ->get();
    }

    public function getInviteById($id){
        return Invite::find($id);
    }

    public function createandSendInvite($email = false, $subject = false, $message = false, $type)
    {
        // check if invite already exists for this scan and user
        $userScan = $this->userScanService->getLatestUserScan(\Auth::user()->id);
        $user = $userScan->user()->first();

        $existingUser = User::where('email', '=', $email)->first();

        // is there already an invite sent for this email and this scan_id?
        $invite = Invite::where('email', $email)->where('scan_id', $userScan->scan_id)->first();
        if(!isset($invite)){
            $invite = Invite::create([
                'email' => $email,
                'invitecode' => str_replace('-', '', Uuid::generate(4)),
                'company_id' => $user->company_id,
                'user_id' => isset($existingUser) ? $existingUser->id : null,
                'scan_id' => $userScan->scan_id,
                'type' => $type
            ]);
        }

        // check if user has already initiated a user scan
        $existingUserScan =
            isset($existingUser) ? $this->userScanService->getUserScanByUserId($existingUser->id, $userScan->scan_id) : null;

        // if not, send invite
        if(!isset($existingUserScan)){
            $this->sendInvite($email, $subject, $message, $invite->invitecode);
        }
    }

    public function sendInvite($email, $subject, $message, $inviteCode){
        $data = [
            'email' => $email,
            'subject' => $subject,
            'mailcontent' => $message,
            'invitecode' => $inviteCode
        ];

        Mail::queue('werkgevers.mailinvite', $data, function ($message) use ($data) {
            $message->to($data['email'])->subject($data['subject']);
        });
    }

    public function getInviteByCode($inviteCode){
        return Invite::where('invitecode', '=', $inviteCode)->first();
    }

    public function checkIfValidInvite($invitecode)
    {
        $invite = $this->getInviteByCode($invitecode);

        if (isset($invite)) {
            $companyScan = $this->companyScanService->getLatestByCompanyId($invite->company_id);
            return isset($companyScan) && $invite->scan_id === $companyScan->scan_id ? $invite : false;
        } else {
            return false;
        }

    }

    public function delete(){

    }
}
