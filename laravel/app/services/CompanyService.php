<?php

namespace App\Services;
use App\Models\Company;
use App\Models\CompanyRapport;
use App\Models\Invite;
use App\Models\UserResponse;

/**
 * Class CompanyService
 *
 * @package \App\Services
 */
class CompanyService
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {

        $this->userService = $userService;
    }

    public function delete($id){
        $companyQuery = Company::where('id', $id);
        $company = $companyQuery->first();

        if($company){

            $this->userService->deleteByCompanyId($id);
            $companyQuery->delete();

            return true;
        }

        return false;

    }

    public function addSectorAndRegion($companyId, $region, $sector) {
        $company = Company::find($companyId);
        if($company) {
            if(!empty($region)) $company->region = $region;
            if(!empty($sector)) $company->sector = $sector;
            $company->save();
        }
    }
}
