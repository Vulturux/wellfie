Bij het ontwerp van een nieuwe werkpost en de aankoop van nieuw materiaal
is het belangrijk dat er rekening wordt gehouden met ergonomische aspecten/criteria.
Dit om de werkomstandigheden te verbeteren en risico’s op de werkvloer te reduceren.
Ook is het aangewezen dat een preventieadviseur reeds van bij aanvang van het aankoop- en ontwerpproces actief wordt betrokken.
