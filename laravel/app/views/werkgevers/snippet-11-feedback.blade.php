<strong>Bezoek arbeidsgeneesheer voorafgaand aan de werkhervatting</strong><br>
Zowel onderworpen als niet-onderworpen medewerkers die minimum vier weken arbeidsongeschikt zijn,
hebben recht op een bezoek aan de arbeidsgeneesheer, met het oog op aanpassing van de werkpost bij hun werkhervatting.<br>
<br>
Medewerkers moeten een schriftelijke aanvraag aan hun werkgever richten. De werkgever verwittigt de
arbeidsgeneesheer, waarna de medewerker binnen een termijn van 8 kalenderdagen wordt uitgenodigd.<br>
<br>
De werkgever brengt de arbeidsgeneesheer op de hoogte van elke arbeidsongeschiktheid van 4 weken of meer.
Hij brengt ook de medewerkers op de hoogte van hun recht op een bezoek bij de arbeidsgeneesheer voorafgaand
aan de werkhervatting bij een arbeidsongeschiktheid van 4 weken of meer.<br>
<br>
Tot voor kort verliep de procedure via de werkgever. Sinds het recente K.B. van 24 april 2014 kan de
arbeidsongeschikte medewerker rechtstreeks en discreet een bezoek voorafgaand aan de werkhervatting bij de
arbeidsgeneesheer vragen.<br>
<br>
Rekening houdend met de gezondheidstoestand van de medewerker en met de werkpost, formuleert de
arbeidsgeneesheer maatregelen en adviezen zodat de werkgever aangepast werk kan geven aan de
medewerker vanaf de werkhervatting.<br><br>
Voor ouder wordende medewerkers met gezondheidsproblemen is dit een belangrijk middel om met
eventuele leeftijdsgebonden beperkingen toch op een goede manier aan het werk te blijven.
