<strong>Extra betaalde vakantiedagen</strong><br>

U kan uw oudere medewerkers extra betaalde vakantiedagen bovenop de puur wettelijke vakantiedagen toekennen eventueel
op basis van anciënniteit bij uw onderneming. Bovendien kan u uw medewerkers informeren over het systeem van
"tijdskrediet" en “landingsbanen".<br>
<br>

<strong>Tijdskrediet</strong> is een systeem waarbij de medewerker tijdelijk minder kan werken of
stoppen zonder dat er een einde aan de arbeidsovereenkomst wordt gesteld. De medewerker kan tijdskrediet
opnemen met motief (om meer tijd te besteden aan de opvoeding van de kinderen, om opnieuw te gaan studeren,
om een ziek familielid te ondersteunen).Tijdens deze onderbrekingsperiode kan de medewerker een uitkering
ontvangen die door de RVA wordt toegekend.<br>

<br>

<strong>Tijdskrediet zonder motief</strong>  zal na de uitvoering van het regeerakkoord geen recht meer geven op RVA uitkering.<br>

<br>

<strong>Landingsbanen</strong> zijn dan de specifieke vorm van tijdskrediet voor oudere medewerkers. Vanaf
1 januari 2015 is dit met uitkering in principe mogelijk vanaf de leeftijd van 60 jaar
(in uitzonderlijke omstandigheden vanaf 55 jaar), waarbij enkel een halftijdse of 1/5e vermindering
mogelijk is met een hogere uitkering van de RVA dan in het ‘normale’ systeem. De werkgever kan hier bovenop een financiële aanvulling geven.



