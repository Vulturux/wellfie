<strong>{{ $scoresAll['65']['avarageAnswerFloat'] }} op 10:</strong> Dat is de mate waarin uw
medewerkers vinden dat in het <strong>bedrijf vanuit duidelijke waarden gewerkt wordt</strong>. <br>

<i>Tips:</i><br>

<ul>
	<li>Benoem welke fundamentele principes uw bedrijf typeren in de manier waarop wordt gewerkt. (Bijvoorbeeld: de waarden respect, kwaliteit, creativiteit, discipline,..).</li>
    <li>Zorg ervoor dat medewerkers deze waarden kennen én toepassen in hun dagelijks werk.</li>
    <li>Bewaak dat grote en kleine beslissingen in lijn zijn met die waarden.</li>
</ul>