<strong>Bijkomende preventieve onderzoeken</strong><br>
Als werkgever kan u bijkomende preventieve medische onderzoeken aanbieden.<br><br>
Deze onderzoeken kunnen gericht zijn op het vroegtijdig opsporen van aandoeningen, maar ook consultaties of coaching in verband met levensstijl zijn zeer nuttig. Medewerkers brengen tot een gezondere levensstijl heeft effect op hun gezondheid in het algemeen, 
vermindert ziekteverzuim en zorgt ervoor dat mensen langer gezond aan het werk blijven.