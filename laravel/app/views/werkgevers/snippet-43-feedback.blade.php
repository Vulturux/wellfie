1. Ga na welke afdeling/dienst wat doet bij de aanwerving van nieuwkomers en leer van de
‘best practices’ uit uw eigen onderneming.<br>
2. Interview een nieuwkomer: wat vonden ze goed en wat hebben ze gemist in die beginperiode?<br>
3. Wat verwacht u dat de nieuwkomer over een jaar kent en kan? Splits dit op in stukken en verdeel
 dit over het komende jaar op basis van de prioriteit.<br>
4. Zorg dat de rolbeschrijving van de p/meter duidelijk is en dat hij/zij de juiste competenties 
en attitude heeft om iemand te begeleiden en op te leiden.<br>