U rapporteert dat in uw onderneming medewerkers geregeld langdurig eenzelfde houding moeten aannemen. Dit is een belangrijke risicofactor voor het ontstaan van overbelastingletsels ter hoogte van spieren, pezen, zenuwen en gewrichten. Dergelijke letsels leiden vaak tot krachtverlies, gevoelsvermindering en beperkte coördinatie waardoor de medewerker bepaalde taken niet meer naar behoren kan uitvoeren. Beter voorkomen dan genezen dus!<br>
<br>
Als werkgever moet u zorgen dat langdurige staande belasting vermeden wordt (K.B. arbeidsplaatsen, afdeling VII, 2012) en dat de werkomstandigheden die een risico inhouden worden aangepakt. Dit is de taak van de werkgever met gericht ergonomisch advies.
<ul>
<li>Voer een specifieke risicoanalyse uit.</li>
<li>Bekijk dan, samen met de medewerker, hoe de werkomstandigheden kunnen verbeterd worden.
<ul>
<li>Optimalisatie van technische aspecten: correcte inrichting van de werkplek, gebruik van hulpmiddelen en gereedschappen om werk lichter te maken, ...</li>
<li>Aanpassing van werkorganisatie: afwisseling van taken en variatie in werktempo en –volgorde taken mogelijk maken, inlassen van meerdere korte pauzes, ...</li>
<li>Correcte werkmethode: instructies opstellen, training en opleiding aanbieden.</li>
</ul></li>
</ul>