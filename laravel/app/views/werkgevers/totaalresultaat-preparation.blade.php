@extends('layoutsWerkgever.master')


@section('content')
    <script src="//code.highcharts.com/highcharts.js"></script>
    <script src="//code.highcharts.com/modules/exporting.js"></script>

    <div class="box-header clearfix">
        <h3 class="main-title">Totaalresultaten worden voorbereid</h3>
    </div>

    <div class="tabs-content eindresultaat-preparation">

        <!-- ONLY WHEN MORE THEN 9 EMPLOYEES FINISHED -->

        <div class="content active">

            <div class="row detail-results">

                <p class="large-12">

                <div class="intro">
                    Momenteel worden de resultaten berekend. Naargelang het aantal deelnemers kan dit enkele minuten in
                    beslag nemen.<br />
                    De pagina zal zichtzelf herladen als deze gereed zijn. U kan ook op een later moment terugkomen om
                    deze te raadplegen of te downloaden als PDF bestand.
                </div>
                <br /><br />
                <div class="gears large-offset-5">
                    <img src="/images/common/gears.gif" />
                </div>

            </div>
        </div> <!--row -->
    </div><!-- END PANEL1-->
    <div class="button-container text-center">
        <ul class="button-group">
            <li>  <a href="/werkgevers/eindresultaat/{{$scan_id}}" class="button main"><i class="fi-play"></i> Bekijk
                    alvast uw beleidsfeedback </a></li></li>
        </ul>
    </div>
    <script>
      var scanId = '{{ $scan_id }}';
      setInterval(function () {
        checkRapport();
      }, 5000);
      function checkRapport() {
        $.ajax({
          url: '/ajax/check-rapport/' + scanId,
          success: function (data) {
            if (data.rapport) {
              location.reload();
            }
          }
        });
      }
    </script>

@stop
