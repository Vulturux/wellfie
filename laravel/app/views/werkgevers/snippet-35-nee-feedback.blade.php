<strong>Risicoanalyse ergonomie</strong><br>
U antwoordde dat er de laatste vijf jaar geen risicoanalyse ergonomie werd uitgevoerd in uw onderneming. De welzijnswet van 1996 verplicht werkgevers tot risicobeheersing om de veiligheid en gezondheid van de medewerkers te garanderen. Ergonomie wordt in die wet expliciet vermeld als één van de acht welzijnsdomeinen die aandacht verdient. Een specifieke risicoanalyse ergonomie focust zich op het in kaart brengen van mogelijke ergonomische knelpunten en het objectiveren van de werkbelasting en mogelijke overbelasting. Een risicoanalyse ergonomie dient uitgevoerd te worden bij:
<ul>
<li>beeldschermwerk (K.B. van 1993 betreffende het werken met beeldschermapparatuur)</li>
<li>manueel hanteren van lasten: tillen, duwen, trekken en verplaatsen van lasten (K.B. van 1993 betreffende het manueel hanteren van lasten), hierbij ook aandacht voor repetitieve manuele handelingen</li>
<li>statisch staand werk (K.B. Arbeidsmiddelen betreffende de werkzitplaatsen en rustzitplaatsen).</li>
</ul>
Indien u hieromtrent meer informatie of advies wenst, kan u terecht bij uw preventieadviseur ergonomie.