<strong>Verbondenheid</strong><br>
Uw medewerkers blijken zich niet verbonden te voelen met hun werk, zij voelen zich niet echt thuis op het werk. De
gevoelens van verbondenheid kunnen versterkt worden door medewerkers te tonen dat zij bijdragen aan een groter
geheel en door gezamenlijke activiteiten te organiseren.