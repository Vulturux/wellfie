1. Zorg dat het duidelijk is voor iedere medewerker welke competenties hij minimaal in huis moet hebben 
om de functie naar behoren te kunnen vervullen.<br>
2. Maak een stappenplan zodat zowel leidinggevende als medewerker een groeitraject kunnen opstellen.<br>
3. Laat senior medewerkers juniors helpen in hun ontwikkeling.<br>