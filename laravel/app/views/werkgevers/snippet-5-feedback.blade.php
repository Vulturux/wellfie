<strong>Arbeidsgeneeskundige toezicht</strong><br>
Afhankelijk van de bedrijvigheid, kunnen in een onderneming gezondheidsrisico’s aanwezig zijn die arbeidsgeneeskundig toezicht vereisen.<br>
Het gaat dan over medewerkers die blootgesteld zijn aan:
<ul>
    <li>een fysisch, biologisch of chemisch agens (bijvoorbeeld lawaai, hepatitis B, lood)</li>
    <li>een belasting van ergonomische aard of zwaar werk, monotoon of tijdsgebonden werk dat kan leiden tot een
    fysieke of mentale belasting (bijvoorbeeld tillen of verplaatsen van lasten)</li>
    <li>een identificeerbaar risico voor psychosociale belasting (bijvoorbeeld ploegenarbeid).</li>
</ul>
<br>
Andere categorieën van medewerkers voor wie preventieve medische onderzoeken wettelijk verplicht
zijn, zijn medewerkers:
<ul>
    <li>met een veiligheidsfunctie (bijvoorbeeld heftruckchauffeurs, operatoren van gevaarlijke installaties, dragers van dienstwapens, ...)</li>
    <li>met een functie van verhoogde waakzaamheid (bijvoorbeeld toezicht op werking van gevaarlijke installaties, ...)</li>
</ul>
<br>
Voor deze medewerkers zijn volgende vormen van preventieve medische onderzoeken verplicht:
<ul>
    <li>de voorafgaande gezondheidsbeoordeling: bij aanwerving of werkverandering
    <li>de periodieke gezondheidsbeoordelingen: de periodiciteit hangt af van het risico
    <li>het onderzoek bij werkhervatting: na een ononderbroken afwezigheid van minstens 4 weken te wijten aan
    ziekte, een aandoening, ongeval of bevalling. Dat onderzoek moet uiterlijk binnen de 8 werkdagen na de werkhervatting plaatsvinden.</li>
</ul>
<br>

Tijdens deze onderzoeken kan de arbeidsgeneesheer met de medewerker bekijken in welke mate eventuele gezondheidsproblemen moeilijkheden geven bij het uitoefenen van het werk of het werk de gezondheid van de medewerker kan schaden. De arbeidsgeneesheer kan eventueel werkaanpassingen voorstellen.