De medewerkers van uw onderneming geven aan zich ziek te melden om redenen die op het werk te vinden zijn. 
Dit wijst er waarschijnlijk toch op dat er werk-gerelateerde hinderpalen aanwezig zijn. De beste manier 
om hier een beter zicht op te krijgen, is door het uitvoeren van een risicoanalyse psychosociale aspecten
 in samenwerking met de preventieadviseur psychosociale van uw externe dienst voor preventie en bescherming op het werk. 
 U kan ook het psychosociaal welzijn van uw medewerkers in kaart laten brengen door de psychosociale barometer, een 
 screeningsinstrument op individueel niveau dat besproken kan worden in een consult bij de arbeidsgeneesheer.
