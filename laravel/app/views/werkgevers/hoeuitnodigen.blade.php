@extends('layoutsWerknemer.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">Hoe uitnodigen?</h3>
</div>

<div class="row visualisation-container">


</div>
<div class="row" style="margin-top: 20px;">



    <div class="large-12 columns">
        <h4 style="text-align: center;">Hoe nodigt u uw medewerkers uit om deel te nemen aan Wellfie?</h4>
        <div id="video">
            <iframe src="//player.vimeo.com/video/116746605?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="600" height="461" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>        </div>
        </div>

        <div class="button-container text-center">
            <ul class="button-group">
                <li><a href="/werkgevers/werknemersuitnodigen" class="button main"><i class="fi-play"></i> Volgende stap</a></li>
            </ul>
        </div>
    </div> <!-- end large 12 columns -->

</div> <!-- end row -->

@stop
