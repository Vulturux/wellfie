U rapporteert dat in uw onderneming medewerkers geregeld in een lawaaierige omgeving moeten werken. Langdurige en herhaalde blootstelling aan overmatig geluid leidt tot achteruitgang van het gehoor. Tevens is lawaai een extra mentale belasting. De normen voor geluid op de werkplek zijn wettelijk bepaald (K.B. betreffende de bescherming van de gezondheid en de veiligheid van de medewerkers tegen de risico's van lawaai op het werk, 2006).

<ul>
	<li>Voer een specifieke risicoanalyse uit om mogelijke risico’s te inventariseren.
	<li>Bekijk dan, samen met de medewerker en preventieadviseur, hoe werkomstandigheden kunnen worden verbeterd en overmatig geluid worden bestreden.
		<ul>
		<li>Pak het geluid aan bij de bron: schaf lawaai-arme toestellen aan, installeer geluidsdempers,
		omkast lawaaierige machineonderdelen, stel zware roterende apparaten trillingvrij op, ...</li>
		 <li>Voorzie de werkomgeving van geluiddempende panelen, stel lawaaierige toestellen zoals
		compressoren en printers op buiten de lokalen.</li>
		<li>Voorzie aanvullende maatregelen op het vlak van werkorganisatie om de blootstelling van de
		medewerker te beperken: bijvoorbeeld beperk de tijd dat een medewerker lawaaierig werk moet
		uitvoeren door een beurtrol.
		<li>Bied persoonlijke beschermingsmiddelen aan indien problemen niet aan de bron kunnen
		worden aangepakt.</li>
		</ul></li>
</ul>