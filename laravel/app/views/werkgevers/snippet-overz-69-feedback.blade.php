<strong>{{ $scoresAll['69']['avarageAnswerFloat'] }} op 10:</strong>
Dat is de mate waarin uw medewerkers vinden dat ze <strong>op elk moment de betekenis van hun bijdrage aan het gemeenschappelijk resultaat kunnen zien</strong>.<br>

<i>Tips:</i><br>
<ul>
    <li>Zorg in de eerste plaats dat mensen weten welk resultaat van hen verwacht wordt.</li>
    <li>Vier successen en geef eer wie eer toekomt.</li>
    <li>Vraag u af of de mensen ’s avonds met een gevoel van trots naar huis gaan.</li>
</ul>
	
