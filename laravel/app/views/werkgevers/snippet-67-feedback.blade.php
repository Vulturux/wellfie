U rapporteert dat in uw onderneming medewerkers geregeld zware lasten moeten tillen, dragen, trekken of duwen. Medewerkers die dit frequent moeten doen, hebben meer kans op letsels aan de bovenste ledematen, nek en lage rug. Bijna de helft van het ziekteverzuim wordt veroorzaakt door deze letsels. Beter voorkomen dan genezen dus!
Als werkgever moet u zorgen dat de fysieke belasting geen gevaar oplevert voor de veiligheid en gezondheid van de 
medewerker (K.B. manueel hanteren van lasten, 1993). Werkomstandigheden die een risico inhouden, moeten worden aangepakt en medewerkers moeten op de juiste manier tillen, dragen, trekken en duwen. Dit is de taak van de werkgever met gericht ergonomisch advies.
<ul>
<li>Voer een specifieke risicoanalyse uit.</li>
<li>Bekijk dan, samen met de medewerker en de preventieadviseur, hoe de werkomstandigheden kunnen
verbeterd worden.
<ul>
	<li>Optimalisatie van technische aspecten: correcte inrichting van de werkplek, gebruik van
hulpmiddelen en gereedschappen om werk lichter te maken zoals tilliften of goed rijdende
karren, verwijderen van obstakels, ...
	<li>Aanpassing van werkorganisatie: afwisseling van taken mogelijk maken, inlassen van
meerdere korte pauzes, collega erbij roepen, ...
	<li>Correcte werkmethode: instructies opstellen, training en opleiding aanbieden.</li>
</ul></li>
</ul>