@extends('layoutsWerkgever.masterpdf')


@section('content')

<div class="content" id="panel0">

    <h1>UW WELLFIE RAPPORT</h1>


    <hr>
</div>

@if ($werknemersCompletedScan >9)
<div class="content" id="panel1">

<div class="row detail-results">

    <div class="large-12  end columns">

    <div class="intro"><i>Hieronder ziet u in welke mate uw inschatting van het welzijn in uw onderneming 
        overeenkomt met deze van uw medewerkers</i><br><br>
        <i class="small">Noot: De interactieve grafieken zijn enkel raadpleegbaar via de website.</i>
    </div><br><br>

    <h5 class="verdiep">Verdieping 1</h5>
        <h4>Gezondheid en functionele capaciteiten</h4>

        <!-- basis values pressing a) Leefgewoonten 118 leefgewoonten / 44  en dan stuk waarden -->
        <?php
            $scorevr=array(0,0,0,0,0,0,0,0);
            foreach ($scoreWGVraag14 as $key => $Wg14Antwood) {
                if ($Wg14Antwood['answer'] == 'Hun verantwoordelijke') { $scorevr[0] = 100; }
                if ($Wg14Antwood['answer'] == 'HR-medewerker') { $scorevr[1] = 100; }
                if ($Wg14Antwood['answer'] == 'Arbeidsgeneesheer') { $scorevr[2] = 100; }
                if ($Wg14Antwood['answer'] == 'Interne preventieadviseur') { $scorevr[3] = 100; }
                if ($Wg14Antwood['answer'] == 'Vertrouwenspersoon') { $scorevr[4] = 100; }
                if ($Wg14Antwood['answer'] == 'De werkgever') { $scorevr[5] = 100; }
                if ($Wg14Antwood['answer'] == 'Andere') { $scorevr[6] = 100; }
                if ($Wg14Antwood['answer'] == 'niemand') { $scorevr[7] = 100; }
            }
        $verbondenheid = ($scoresAll['72']['avarageAnswerInteger'] + $scoresAll['73']['avarageAnswerInteger'])/2;
        $ontslagintentie= ($scoresAll['78']['avarageAnswerInteger']+$scoresAll['79']['avarageAnswerInteger']+$scoresAll['80']['avarageAnswerInteger']+$scoresAll['81']['avarageAnswerInteger'])/4;
        $motivatie = ($scoresAll['82']['avarageAnswerInteger']+$scoresAll['83']['avarageAnswerInteger']+$scoresAll['84']['avarageAnswerInteger']+$scoresAll['85']['avarageAnswerInteger'])/4;

        ?>



        <table class="detail-results">
            <tr>
                <th valign="top"></th>
                <th>Werkgever</th>
                <th>Medewerkers</th>
            </tr>
            <tr>
                <td valign="top">Aandacht voor gezondheid is belangrijk</td>
                <td valign="top" class="werkgever-detail">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['117']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['117']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['117']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens  @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['117']['answer'] == 'eens')eens  @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['117']['answer'] == 'helemaal eens')helemaal eens  @else &nbsp; @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                      <tr>
                        <td>helemaal oneens</td>
                        <td>
                            <div class="progress-bar">
                                  <span class="percent">{{ $scoresAll['43']['answers']['161']['countpercentage'] }}%</span>
                                 <div class="bar" style="width:{{ $scoresAll['43']['answers']['161']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                      </tr>
                      <tr>
                        <td>oneens</td>
                        <td>
                              <div class="progress-bar">
                                  <span class="percent">{{ $scoresAll['43']['answers']['162']['countpercentage'] }}%</span>
                                  <div class="bar" style="width:{{ $scoresAll['43']['answers']['162']['countpercentage'] }}%"></div>
                              </div>
                        </td>
                      </tr>
                       <tr>
                        <td>noch eens, noch oneens</td>
                        <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['43']['answers']['163']['countpercentage'] }}%</span>
                                    <div class="bar" style="width:{{ $scoresAll['43']['answers']['163']['countpercentage'] }}%"></div>
                                </div>
                        </td>
                      </tr>
                      <tr>
                        <td>eens</td>
                        <td>
                              <div class="progress-bar">
                                  <span class="percent">{{ $scoresAll['43']['answers']['164']['countpercentage'] }}%</span>
                                  <div class="bar" style="width:{{ $scoresAll['43']['answers']['164']['countpercentage'] }}%"></div>
                              </div>
                        </td>
                      </tr>
                      <tr>
                        <td>helemaal eens</td>
                        <td>
                              <div class="progress-bar">
                                  <span class="percent">{{ $scoresAll['43']['answers']['165']['countpercentage'] }}%</span>
                                  <div class="bar" style="width: {{ $scoresAll['43']['answers']['165']['countpercentage'] }}%"></div>
                              </div>
                        </td>
                      </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">Aandacht voor gezonde leefgewoonten is belangrijk</td>
                <td valign="top" class="werkgever-detail">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['118']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['118']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['118']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens  @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['118']['answer'] == 'eens')eens  @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['118']['answer'] == 'helemaal eens')helemaal eens  @else &nbsp; @endif</td></tr>
                    </table>
                </td>


                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['44']['answers']['166']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['44']['answers']['166']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['44']['answers']['167']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['44']['answers']['167']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['44']['answers']['168']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['44']['answers']['168']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['44']['answers']['169']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['44']['answers']['169']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['44']['answers']['170']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['44']['answers']['170']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">Aandacht voor het werkvermogen is belangrijk</td>
                <td valign="top" class="werkgever-detail">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['119']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['119']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['119']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens  @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['119']['answer'] == 'eens')eens  @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['119']['answer'] == 'helemaal eens')helemaal eens  @else &nbsp; @endif</td></tr>
                    </table>
                    
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['45']['answers']['171']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['45']['answers']['171']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['45']['answers']['172']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['45']['answers']['172']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['45']['answers']['173']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['45']['answers']['173']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['45']['answers']['174']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['45']['answers']['174']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['45']['answers']['175']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['45']['answers']['175']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <hr>

        <!-- b) Ziekteverzuim wegens werk: keer thuis om redenen die op het werk te vinden zijn?	 120 meestal niet	 / 35 hoeveel keer-->
        <strong>Ziekte door het werk</strong><br>


        <table class="detail-results">
            <tr>
                <th  valign="top"><strong>Werkgever</strong><br> <em>De medewerkers melden zich ziek om redenen die op het werk te vinden zijn</em></th>
                <th valign="top"><strong>Medewerkers</strong><br> <em>Aantal keer afwezig in de afgelopen 12 maanden om redenen die op het werk te vinden zijn</em></th>
            </tr>
            <tr>
                <td>{{ $scoresWG['120']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>geen enkele keer</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['35']['answers']['109']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['35']['answers']['109']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>1 keer</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['35']['answers']['110']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['35']['answers']['110']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>2 keer</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['35']['answers']['111']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['35']['answers']['111']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>3 of 4 keer</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['35']['answers']['112']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['35']['answers']['112']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>5 keer of meer</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['35']['answers']['113']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['35']['answers']['113']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

         <strong>Waar kan de medewerker terecht bij het bespreken van het werkvermogen?</strong><br>

    <table>
        <tr>
            <th></th>
            <th  valign="top"><strong>Werkgever</strong></th>
            <th valign="top"><strong>% Medewerkers</strong></th>
        </tr>
        <tr>
            <td>uw verantwoordelijke</td>
            <td> @if ( $scorevr[0]  > 0)<li>Uw verantwoordelijke</li>@endif</td>
            <td>
                <div class="progress-bar">
                    <span class="percent">{{ $scoreWNVraag28['71']['countpercentage'] }}%</span>
                    <div class="bar" style="width: {{ $scoreWNVraag28['71']['countpercentage'] }}%"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>HR medewerker</td>
            <td>@if ( $scorevr[1]  > 0)<li>HR-medewerker</li>@endif</td>
            <td>
                <div class="progress-bar">
                    <span class="percent">{{ $scoreWNVraag28['72']['countpercentage'] }}%</span>
                    <div class="bar" style="width: {{ $scoreWNVraag28['72']['countpercentage'] }}%"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Arbeidsgen.</td>
            <td></td>
            <td>
                <div class="progress-bar">
                    <span class="percent">{{ $scoreWNVraag28['73']['countpercentage'] }}%</span>
                    <div class="bar" style="width: {{ $scoreWNVraag28['73']['countpercentage'] }}%"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Int. preventieadv.</td>
            <td>@if ( $scorevr[2]  > 0)<li>Interne preventieadviseur</li>@endif</td>
            <td>
                <div class="progress-bar">
                    <span class="percent">{{ $scoreWNVraag28['74']['countpercentage'] }}%</span>
                    <div class="bar" style="width: {{ $scoreWNVraag28['74']['countpercentage'] }}%"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Vertrouwenspers.</td>
            <td> @if ( $scorevr[3]  > 0)<li>Vertrouwenspersoon</li>@endif</td>
            <td>
                <div class="progress-bar">
                    <span class="percent">{{ $scoreWNVraag28['75']['countpercentage'] }}%</span>
                    <div class="bar" style="width: {{ $scoreWNVraag28['75']['countpercentage'] }}%"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Werkgever</td>
            <td>  @if ( $scorevr[4]  > 0)<li>Werkgever</li> @endif</td>
            <td>
                <div class="progress-bar">
                    <span class="percent">{{ $scoreWNVraag28['796']['countpercentage'] }}%</span>
                    <div class="bar" style="width: {{ $scoreWNVraag28['796']['countpercentage'] }}%"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Andere</td>
            <td></td>
            <td>
                <div class="progress-bar">
                    <span class="percent">{{ $scoreWNVraag28['76']['countpercentage'] }}%</span>
                    <div class="bar" style="width: {{ $scoreWNVraag28['76']['countpercentage'] }}%"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Niemand</td>
            <td>@if ( $scorevr[6]  > 0)<li>Niemand</li> @endif</td>
            <td>
                <div class="progress-bar">
                    <span class="percent">{{ $scoreWNVraag28['77']['countpercentage'] }}%</span>
                    <div class="bar" style="width: {{ $scoreWNVraag28['77']['countpercentage'] }}%"</div>
                </div>
            </td>
        </tr>
        <tr>
            <td>Weet het niet</td>
            <td>@if ( $scorevr[7]  > 0)<li>Weet het niet</li>@endif</td>
            <td>
                <div class="progress-bar">
                    <span class="percent">{{ $scoreWNVraag28['78']['countpercentage'] }}%</span>
                    <div class="bar" style="width: {{ $scoreWNVraag28['78']['countpercentage'] }}%"></div>
                </div>
            </td>
        </tr>

    </table>
        


    </div> <!-- end columns -->






           

</div> <!--row -->



<div class="row detail-results">

    <div class="large-12 large-offset-0 end columns">
        <h5 class="verdiep">Verdieping 2</h5>
        <h4>Competenties</h4>

        <table class="detail-results">
        <tr>
            <th valign="top"></th>
            <th valign="top">Werkgever</th>
            <th valign="top">Medewerkers</th>
        </tr>
        <tr>
            <td valign="top">Voorziet u een p/meter en een inwerktraject voor nieuwkomers?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['145']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['145']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['52']['answers']['188']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['52']['answers']['188']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['52']['answers']['189']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['52']['answers']['189']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">Heeft u een uitgewerkt opleidingsplan met bijbehorend budget voor alle functies?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['146']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['146']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['53']['answers']['190']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['53']['answers']['190']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['53']['answers']['191']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['53']['answers']['191']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">Staan de medewerkers open voor opleiding?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['147']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['147']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['54']['answers']['192']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['54']['answers']['192']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['54']['answers']['193']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['54']['answers']['193']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">Heeft iedere medewerker in de afgelopen 3 jaar minstens één opleiding gevolgd?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['148']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['148']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['55']['answers']['194']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['55']['answers']['194']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['55']['answers']['195']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['55']['answers']['195']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">Houden de leidinggevenden feedbackgesprekken met hun medewerkers waarin ze de sterktes en zwaktes van de medewerker bespreken?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['149']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['149']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['56']['answers']['196']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['56']['answers']['196']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['56']['answers']['197']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['56']['answers']['197']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">Worden de competenties van iedere medewerker ten volle benut?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['150']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['150']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['57']['answers']['198']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['57']['answers']['198']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['57']['answers']['199']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['57']['answers']['199']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">Weten de medewerkers goed wat ze nog willen of kunnen met hun loopbaan?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['153']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['153']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['59']['answers']['202']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['59']['answers']['202']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['59']['answers']['203']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['59']['answers']['203']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">Stimuleert de onderneming het delen van kennis?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['154']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['154']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['60']['answers']['204']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['60']['answers']['204']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['60']['answers']['205']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['60']['answers']['205']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">Hebben de leidinggevenden oog voor de inzet van medewerkers?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['155']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['155']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['61']['answers']['206']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['61']['answers']['206']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['61']['answers']['207']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['61']['answers']['207']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">Is er een lerende en open gesprekscultuur?</td>
            <td class="werkgever-detail">
                <table class="werkgever-detail">
                    <tr><td>@if ($scoresWG['156']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                    <tr><td>@if ($scoresWG['156']['answer'] == 'nee')nee  @else &nbsp;  @endif</td></tr>
                </table>
            </td>
            <td><table class="medewerker-detail">
                    <tr>
                        <td>Ja</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['62']['answers']['208']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['62']['answers']['208']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Nee</td>
                        <td>
                            <div class="progress-bar">
                                <span class="percent">{{ $scoresAll['62']['answers']['209']['countpercentage'] }}%</span>
                                <div class="bar" style="width: {{ $scoresAll['62']['answers']['209']['countpercentage'] }}%"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>

    </div> <!--column -->

</div> <!--row -->

<div class="row detail-results">

    <div class="large-12  columns">
            <h5 class="verdiep">Verdieping 3</h5>

            <h4>Waarden, houding en motivatie</h4>

            <table class="detail-results">
                <tr>
                    <th valign="top"></th>
                    <th valign="top">Score Werkgever</th>
                    <th valign="top">Gemiddelde score medewerkers</th>
                </tr>
                <tr>
                    <td>Onderneming met toekomst</td>
                    <td class="werkgever-detail">{{ $scoresWG['158']['answer'] }}</td>
                    <td class="werknemer-detail">{{ $scoresAll['64']['avarageAnswerFloat'] }}</td>
                </tr>
                <tr>
                    <td>Duidelijkheid organisatiewaarden</td>
                    <td class="werkgever-detail">{{ $scoresWG['159']['answer'] }}</td>
                    <td class="werknemer-detail">{{ $scoresAll['65']['avarageAnswerFloat'] }}</td>
                </tr>
                <tr>
                    <td>Medewerkers tellen als persoon</td>
                    <td class="werkgever-detail">{{ $scoresWG['160']['answer'] }}</td>
                    <td class="werknemer-detail">{{ $scoresAll['66']['avarageAnswerFloat'] }}</td>
                </tr>
                <tr>
                    <td>Medewerkers worden gestimuleerd</td>
                    <td class="werkgever-detail">{{ $scoresWG['161']['answer'] }}</td>
                    <td class="werknemer-detail">{{ $scoresAll['67']['avarageAnswerFloat'] }}</td>
                </tr>
                <tr>
                    <td>Talenten Benutten</td>
                    <td class="werkgever-detail"><span class="small">(vraag niet gesteld aan werkgever)</span></td>
                    <td class="werknemer-detail">{{ $scoresAll['68']['avarageAnswerFloat'] }}</td>
                </tr>
                <tr>
                    <td>Betekenis aan resultaat zien</td>
                    <td class="werkgever-detail"><span class="small">(vraag niet gesteld aan werkgever)</span></td>
                    <td class="werknemer-detail">{{ $scoresAll['69']['avarageAnswerFloat'] }}</td>
                </tr>
                <tr>
                    <td>We doen hier wat we zeggen en we zeggen wat we doen</td>
                    <td class="werkgever-detail"><span class="small">(vraag niet gesteld aan werkgever)</span></td>
                    <td class="werknemer-detail">{{ $scoresAll['70']['avarageAnswerFloat'] }}</td>
                </tr>
                <tr>
                    <td>Waarden sluiten aan bij de waarden van organisatie</td>
                    <td class="werkgever-detail"><span class="small">(vraag niet gesteld aan werkgever)</span></td>
                    <td class="werknemer-detail">{{ $scoresAll['71']['avarageAnswerFloat'] }}</td>
                </tr>
            </table>

            <table class="detail-results">
            <tr>
                <th valign="top"></th>
                <th valign="top">Werkgever</th>
                <th valign="top">Medewerkers</th>

            </tr>
            <tr>
                <th colspan="3" class="header"><div class="subtitle">Verbondenheid</div></th>
            </tr>
            <tr>
                <td valign="top">
                    Ik kan met anderen op mijn werk praten over wat ik echt belangrijk vind<br></td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['162']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['162']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['162']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['162']['answer'] == 'eens')eens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['162']['answer'] == 'helemaal eens')helemaal eens @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['72']['answers']['300']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['72']['answers']['300']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['72']['answers']['301']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['72']['answers']['301']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['72']['answers']['302']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['72']['answers']['302']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['72']['answers']['303']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['72']['answers']['303']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['72']['answers']['304']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['72']['answers']['304']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td valign="top">
                    Ik voel me een deel van een groep op het werk<br></td>
                <td class="werkgever-detail" valign="top">
                    <i class="small">Deze vraag werd niet aan u gesteld. Zie hiernaast de antwoorden van de medewerkers.</i>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['73']['answers']['305']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['73']['answers']['305']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['73']['answers']['306']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['73']['answers']['306']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['73']['answers']['307']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['73']['answers']['307']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['73']['answers']['308']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['73']['answers']['308']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['73']['answers']['309']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['73']['answers']['309']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="3" class="header"><div class="subtitle">Werk - Privé</div></th>
            </tr>
            <tr>
                <td valign="top">Mijn werkeisen zorgen voor hinder in mijn privéleven</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['163']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['163']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['163']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['163']['answer'] == 'eens')eens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['163']['answer'] == 'helemaal eens')helemaal eens @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['74']['answers']['310']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['74']['answers']['310']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['74']['answers']['311']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['74']['answers']['311']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['74']['answers']['312']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['74']['answers']['312']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['74']['answers']['313']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['74']['answers']['313']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['74']['answers']['314']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['74']['answers']['314']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">Mijn privéleven zorgt voor hinder tijdens mijn werk</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['164']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['164']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['164']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['164']['answer'] == 'eens')eens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['164']['answer'] == 'helemaal eens')helemaal eens @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['75']['answers']['315']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['75']['answers']['315']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['75']['answers']['316']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['75']['answers']['316']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['75']['answers']['317']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['75']['answers']['317']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['75']['answers']['318']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['75']['answers']['318']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['75']['answers']['319']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['75']['answers']['319']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="header"><div class="subtitle">Tevredenheid</div></td>
            </tr>
            <tr>
                <td valign="top" >Ik ben, alles bij elkaar genomen, tevreden met mijn werk</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['165']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['165']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['165']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['165']['answer'] == 'eens')eens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['165']['answer'] == 'helemaal eens')helemaal eens @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['76']['answers']['320']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['76']['answers']['320']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['76']['answers']['321']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['76']['answers']['321']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['76']['answers']['322']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['76']['answers']['322']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['76']['answers']['323']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['76']['answers']['323']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['76']['answers']['324']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['76']['answers']['324']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="header"><div class="subtitle">Vertrouwen</div></td>
            </tr>
            <tr>
                <td valign="top">Ik heb vertrouwen dat deze onderneming mij eerlijk zal behandelen</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['167']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['167']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['167']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['167']['answer'] == 'eens')eens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['167']['answer'] == 'helemaal eens')helemaal eens @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['77']['answers']['325']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['77']['answers']['325']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['77']['answers']['326']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['77']['answers']['326']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['77']['answers']['327']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['77']['answers']['327']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['77']['answers']['328']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['77']['answers']['328']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['77']['answers']['329']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['77']['answers']['329']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="3" class="header"><div class="subtitle">Ontslagintentie</div></th>
            </tr>
            <tr>
                <td valign="top">Tegenwoordig heb ik zin om mijn baan op te zeggen</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['168']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['168']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['168']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['168']['answer'] == 'eens')eens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['168']['answer'] == 'helemaal eens')helemaal eens @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['78']['answers']['330']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['78']['answers']['330']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['78']['answers']['331']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['78']['answers']['331']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['78']['answers']['332']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['78']['answers']['332']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['78']['answers']['333']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['78']['answers']['333']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['78']['answers']['334']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['78']['answers']['334']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td valign="top">Momenteel ben ik actief op zoek naar een andere baan</td>
                <td class="werkgever-detail" valign="top">

                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['79']['answers']['335']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['79']['answers']['335']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['79']['answers']['336']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['79']['answers']['336']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['79']['answers']['337']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['79']['answers']['337']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['79']['answers']['338']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['79']['answers']['338']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['79']['answers']['339']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['79']['answers']['339']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td valign="top">Wanneer een andere onderneming mij een baan zou aanbieden, dan zou ik niet aarzelen om ze aan te nemen</td>
                <td class="werkgever-detail" valign="top">
                    <i class="small">Deze vraag werd niet aan u gesteld. Zie hiernaast de antwoorden van de medewerkers.</i>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['80']['answers']['340']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['80']['answers']['340']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['80']['answers']['341']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['80']['answers']['341']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['80']['answers']['342']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['80']['answers']['342']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['80']['answers']['343']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['80']['answers']['343']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['80']['answers']['344']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['80']['answers']['344']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th colspan="3" class="header"><div class="subtitle">Motivatie</div></th>
            </tr>
            <tr>
                <td valign="top">Ik doe moeite voor mijn job omdat de dingen die ik doe in deze baan voor mij persoonlijk heel betekenisvol zijn</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['166']['answer'] == 'helemaal oneens')helemaal oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['166']['answer'] == 'oneens')oneens  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['166']['answer'] == 'noch eens, noch oneens')noch eens, noch oneens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['166']['answer'] == 'eens')eens @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['166']['answer'] == 'helemaal eens')helemaal eens @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal niet</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['82']['answers']['350']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['82']['answers']['350']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>slechts een beetje </td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['82']['answers']['351']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['82']['answers']['351']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een beetje</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['82']['answers']['352']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['82']['answers']['352']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>matig</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['82']['answers']['353']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['82']['answers']['353']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>in sterke mate</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['82']['answers']['354']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['82']['answers']['354']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>in heel sterke mate</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['82']['answers']['355']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['82']['answers']['355']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['82']['answers']['356']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['82']['answers']['356']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td valign="top">Ik doe moeite voor mijn job omdat ik het zinvol vind om moeite te doen voor mijn werk</td>
                <td class="werkgever-detail" valign="top">                        <i class="small">Deze vraag werd niet aan u gesteld. Zie hiernaast de antwoorden van de medewerkers.</i>

                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal niet</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['83']['answers']['357']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['83']['answers']['357']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>slechts een beetje </td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['83']['answers']['358']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['83']['answers']['358']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een beetje</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['83']['answers']['359']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['83']['answers']['359']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>matig</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['83']['answers']['360']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['83']['answers']['360']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>in sterke mate</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['83']['answers']['361']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['83']['answers']['361']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>in heel sterke mate</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['83']['answers']['362']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['83']['answers']['362']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['83']['answers']['363']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['83']['answers']['363']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td valign="top">Ik doe moeite voor mijn job omdat ik deze baan graag doe</td>
                <td class="werkgever-detail" valign="top">
                    <i class="small">Deze vraag werd niet aan u gesteld. Zie hiernaast de antwoorden van de medewerkers.</i>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal niet</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['84']['answers']['364']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['84']['answers']['364']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>slechts een beetje </td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['84']['answers']['365']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['84']['answers']['365']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een beetje</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['84']['answers']['366']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['84']['answers']['366']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>matig</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['84']['answers']['367']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['84']['answers']['367']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>in sterke mate</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['84']['answers']['368']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['84']['answers']['368']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>in heel sterke mate</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['84']['answers']['369']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['84']['answers']['369']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>helemaal</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['84']['answers']['370']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['84']['answers']['370']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
        </div> <!-- end columns -->
</div> <!--row -->







<div class="row detail-results last">

        <div class="large-12 end columns">
            <h5 class="verdiep">Verdieping 4</h5>
            <h4>Werk, werkgemeenschap en leiding</h4>


            <table class="detail-results">
            <tr>
                <th valign="top"></th>
                <th valign="top">Werkgever</th>
                <th valign="top">Medewerkers</th>
            </tr>
            <tr>
                <td valign="top">De medewerker moet zware lasten tillen, dragen, trekken of duwen?</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['169']['answer'] == 'nooit')nooit @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['169']['answer'] == 'eens per maand of minder')eens per maand of minder  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['169']['answer'] == 'eens per week')eens per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['169']['answer'] == 'een paar keer per week')een paar keer per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['169']['answer'] == 'dagelijks')dagelijks @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>nooit</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['86']['answers']['378']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['86']['answers']['378']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per maand of minder</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['86']['answers']['379']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['86']['answers']['379']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['86']['answers']['380']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['86']['answers']['380']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['86']['answers']['381']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['86']['answers']['381']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['86']['answers']['382']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['86']['answers']['382']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">De medewerker moet repetitieve (herhaaldelijk dezelfde) bewegingen</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['170']['answer'] == 'nooit')nooit @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['170']['answer'] == 'eens per maand of minder')eens per maand of minder  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['170']['answer'] == 'eens per week')eens per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['170']['answer'] == 'een paar keer per week')een paar keer per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['170']['answer'] == 'dagelijks')dagelijks @else &nbsp;  @endif</td></tr>
                    </table>

                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>nooit</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['87']['answers']['383']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['87']['answers']['383']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per maand of minder</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['87']['answers']['384']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['87']['answers']['384']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['87']['answers']['385']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['87']['answers']['385']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['87']['answers']['386']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['87']['answers']['386']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['87']['answers']['387']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['87']['answers']['387']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            </tr>
            <tr>
                <td valign="top">De medewerker moet langdurig zelfde houding aannemen</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['171']['answer'] == 'nooit')nooit @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['171']['answer'] == 'eens per maand of minder')eens per maand of minder  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['171']['answer'] == 'eens per week')eens per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['171']['answer'] == 'een paar keer per week')een paar keer per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['171']['answer'] == 'dagelijks')dagelijks @else &nbsp;  @endif</td></tr>
                    </table>

                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>nooit</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['88']['answers']['388']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['88']['answers']['388']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per maand of minder</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['88']['answers']['389']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['88']['answers']['389']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['88']['answers']['390']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['88']['answers']['390']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['88']['answers']['391']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['88']['answers']['391']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['88']['answers']['392']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['88']['answers']['392']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">De medewerker moet beeldschermwerk uitvoeren</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['172']['answer'] == 'nooit')nooit @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['172']['answer'] == 'eens per maand of minder')eens per maand of minder  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['172']['answer'] == 'eens per week')eens per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['172']['answer'] == 'een paar keer per week')een paar keer per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['172']['answer'] == 'dagelijks')dagelijks @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>nooit</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['89']['answers']['393']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['89']['answers']['393']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per maand of minder</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['89']['answers']['394']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['89']['answers']['394']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['89']['answers']['395']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['89']['answers']['395']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['89']['answers']['396']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['89']['answers']['396']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['89']['answers']['397']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['89']['answers']['397']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">De medewerker moet met trillend gereedschap werken</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['173']['answer'] == 'nooit')nooit @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['173']['answer'] == 'eens per maand of minder')eens per maand of minder  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['173']['answer'] == 'eens per week')eens per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['173']['answer'] == 'een paar keer per week')een paar keer per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['173']['answer'] == 'dagelijks')dagelijks @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>nooit</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['90']['answers']['398']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['90']['answers']['398']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per maand of minder</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['90']['answers']['399']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['90']['answers']['399']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['90']['answers']['400']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['90']['answers']['400']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['90']['answers']['401']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['90']['answers']['401']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['90']['answers']['402']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['90']['answers']['402']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">De medewerker moet bij te hoge of te lage temperatuur werken</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['174']['answer'] == 'nooit')nooit @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['174']['answer'] == 'eens per maand of minder')eens per maand of minder  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['174']['answer'] == 'eens per week')eens per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['174']['answer'] == 'een paar keer per week')een paar keer per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['174']['answer'] == 'dagelijks')dagelijks @else &nbsp;  @endif</td></tr>
                    </table>

                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>nooit</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['91']['answers']['403']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['91']['answers']['403']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per maand of minder</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['91']['answers']['404']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['91']['answers']['404']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['91']['answers']['405']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['91']['answers']['405']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['91']['answers']['406']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['91']['answers']['406']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['91']['answers']['407']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['91']['answers']['407']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">De medewerker moet in slecht verlichte lokalen werken</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['175']['answer'] == 'nooit')nooit @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['175']['answer'] == 'eens per maand of minder')eens per maand of minder  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['175']['answer'] == 'eens per week')eens per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['175']['answer'] == 'een paar keer per week')een paar keer per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['175']['answer'] == 'dagelijks')dagelijks @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>nooit</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['92']['answers']['408']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['92']['answers']['408']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per maand of minder</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['92']['answers']['409']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['92']['answers']['409']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['92']['answers']['410']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['92']['answers']['410']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['92']['answers']['411']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['92']['answers']['411']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['92']['answers']['412']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['92']['answers']['412']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">De medewerker moet in slecht geventileerde lokalen werken</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['176']['answer'] == 'nooit')nooit @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['176']['answer'] == 'eens per maand of minder')eens per maand of minder  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['176']['answer'] == 'eens per week')eens per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['176']['answer'] == 'een paar keer per week')een paar keer per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['176']['answer'] == 'dagelijks')dagelijks @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>nooit</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['93']['answers']['413']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['93']['answers']['413']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per maand of minder</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['93']['answers']['414']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['93']['answers']['414']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['93']['answers']['415']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['93']['answers']['415']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['93']['answers']['416']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['93']['answers']['416']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['93']['answers']['417']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['93']['answers']['417']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top">De medewerker moet in een lawaaierige omgeving werken</td>
                <td class="werkgever-detail" valign="top">
                    <table class="werkgever-detail">
                        <tr><td>@if ($scoresWG['177']['answer'] == 'nooit')nooit @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['177']['answer'] == 'eens per maand of minder')eens per maand of minder  @else &nbsp;  @endif</td></tr>
                        <tr><td>@if ($scoresWG['177']['answer'] == 'eens per week')eens per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['177']['answer'] == 'een paar keer per week')een paar keer per week @else &nbsp; @endif</td></tr>
                        <tr><td>@if ($scoresWG['177']['answer'] == 'dagelijks')dagelijks @else &nbsp;  @endif</td></tr>
                    </table>
                </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>nooit</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['94']['answers']['418']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['94']['answers']['418']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per maand of minder</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['94']['answers']['419']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['94']['answers']['419']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['94']['answers']['420']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['94']['answers']['420']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['94']['answers']['421']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['94']['answers']['421']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>
                                <div class="progress-bar">
                                    <span class="percent">{{ $scoresAll['94']['answers']['422']['countpercentage'] }}%</span>
                                    <div class="bar" style="width: {{ $scoresAll['94']['answers']['422']['countpercentage'] }}%"></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>



            <br><hr><br>

            <h4>Opleiding ergonomie</h4>

            <table class="detail-results">
                <tr>
                    <th valign="top"></th>
                    <th valign="top">Werkgever</th>
                    <th valign="top">Medewerkers</th>
                </tr>
                <tr>
                    <td valign="top">De medewerker krijgt voldoende opleiding</td>
                    <td class="werkgever-detail">
                        <table class="werkgever-detail">
                            <tr><td>@if ($scoresWG['178']['answer'] == 'ja')ja @else &nbsp; @endif</td></tr>
                            <tr><td>@if ($scoresWG['178']['answer'] == 'nee')nee @else &nbsp; @endif</td></tr>
                        </table>
                    </td>
                    <td><table class="medewerker-detail">
                            <tr>
                                <td>Ja</td>
                                <td>
                                    <div class="progress-bar">

                                        <span class="percent"><?php echo ($scoresAll['98']['answers'][429]['countpercentage']); ?> %</span>
                                        <div class="bar" style="width: <?php echo  ($scoresAll['98']['answers'][429]['countpercentage']); ?>%"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Nee</td>
                                <td>
                                    <div class="progress-bar">
                                        <span class="percent"><?php echo ($scoresAll['98']['answers'][430]['countpercentage']); ?> %</span>
                                        <div class="bar" style="width: <?php echo $scoresAll['98']['answers'][430]['countpercentage']; ?>%"></div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                        </tr>
            </table>
            <br>
            <strong>De ontwikkel- en ontziemaatregelen die als 3 belangrijkste werden aangegeven door uw medewerkers</strong>

            <table class="detail-results">
                <tr>
                    <th></th>
                    <th valign="top">Medewerkers</th>
                </tr>
                <tr><td>Tijdskrediet</td><td> {{ round($scoreWNVraag99['431']['countpercentage']) }} %</td></tr>
                <tr><td>Deeltijds werken</td><td>  {{ round($scoreWNVraag99['432']['countpercentage']) }}  %</td></tr>
                <tr><td>Bijkomende verlofdagen</td><td>  {{ round($scoreWNVraag99['433']['countpercentage']) }}  %</td></tr>
                <tr><td>Jobverrijking of loopbaanontwikkeling</td><td> {{ round($scoreWNVraag99['434']['countpercentage']) }}  %</td></tr>
                <tr><td>Jobrotatie</td><td> {{ round($scoreWNVraag99['435']['countpercentage']) }}  %</td></tr>
                <tr><td>Taakaanpassing</td><td>     {{ round($scoreWNVraag99['436']['countpercentage']) }}  %</td></tr>
                <tr><td>Minder fysieke werkbelasting</td><td> {{ round($scoreWNVraag99['437']['countpercentage']) }}  %</td></tr>
                <tr><td>Premie op basis van aantal jaren dienst</td><td>  {{ round($scoreWNVraag99['438']['countpercentage']) }}  %</td></tr>
                <tr><td>Flexibel werken</td><td> {{ round($scoreWNVraag99['439']['countpercentage']) }}  %</td></tr>
                <tr><td>Ondersteuning van de gezondheid</td><td>   {{ round($scoreWNVraag99['440']['countpercentage']) }}  %</td></tr>
                <tr><td>Opleidingen op maat van specifieke doelgroepen</td><td>  {{ round($scoreWNVraag99['441']['countpercentage']) }}  %</td></tr>
                <tr><td>Positieve beeldvorming tegenover oudere medewerkers</td><td> {{ round($scoreWNVraag99['442']['countpercentage']) }}  %</td></tr>
            </table>

            <br><hr><br>


        </div> <!-- end columns -->

    </div> <!--row -->

</div><!-- END PANEL1-->





<div class="content" id="panel2">

<!-- START PANEL2-->
<div class="row huis-resultaat">
    <div class="medium-12 columns ">
        <div class="tussenresultaat visualisation">
            <img src="{{Config::get('app.url')}}/images/visualisation/licht-uit.png" class="viz-image">

            <div class="viz-level ">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping1_{{ $scoresLevelOneEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping1_mannetje_{{ $scoresLevelOneEvaluation }}.png">
            </div>

            <div class="viz-level">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping2_{{ $scoresLevelTwoEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping2_mannetje_{{ $scoresLevelTwoEvaluation }}.png">
            </div>

            <div class="viz-level ">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping3_{{ $scoresLevelThreeEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping3_mannetje_{{ $scoresLevelThreeEvaluation }}.png">
            </div>

            <div class="viz-level">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping4_{{ $scoresLevelFourEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping4_mannetje_{{ $scoresLevelFourEvaluation }}.png">
            </div>

            <div class="viz-level">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping5_{{ $scoresLevelFiveEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="{{Config::get('app.url')}}/images/visualisation/verdieping5_mannetje_{{ $scoresLevelFiveEvaluation }}.png">
            </div>

        </div>
    </div>
</div>

<div class="row detail-results huis-resultaat-table">
    <div class="medium-12 columns huis-resultaat-row">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Verdieping 1</span>Gezondheid en functionele capaciteiten</h5>
            </div>
            <div class="large-3  columns">
                <h6>Medische vragen</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelOne[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelOne[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelOne[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-3  columns">
                <h6>Levensstijl</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelOne[1]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelOne[1]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelOne[1]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-3  columns">
                <h6>Verzuim</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelOne[2]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelOne[2]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelOne[2]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-3  columns">
                <h6>Burn-out</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelOne[3]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelOne[3]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelOne[3]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="medium-12 columns huis-resultaat-row seperator">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Verdieping 2</span>Competenties</h5>
            </div>
            <div class="large-6  columns">
                <h6>Zichtbaarheid op arbeidsmarkt</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelTwo[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelTwo[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelTwo[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-6 end columns">
                <h6>Talentmanagement</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelTwo[1]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelTwo[1]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelTwo[1]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="medium-12 columns huis-resultaat-row seperator">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Verdieping 3</span>Waarden, houding en motivatie</h5>
            </div>
            <div class="large-6  columns">
                <h6>Waarden en normen</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelThree[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelThree[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelThree[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-6 end columns">
                <h6>Persoonskenmerken</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelThree[1]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelThree[1]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelThree[1]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="medium-12 columns huis-resultaat-row seperator">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Verdieping 4</span>Werk, werkgemeenschap en leiding</h5>
            </div>
            <div class="large-6 end columns">

                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelFour[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelFour[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelFour[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="medium-12 columns huis-resultaat-row seperator">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Het Dak</span>Werkvermogen</h5>
            </div>
            <div class="large-6 end columns">

                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelFive[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelFive[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelFive[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div><br>
            </div>
        </div>
    </div>


</div> <!-- end row huis-resultaat-table -->
<div class="row detail-results">

    <div class="large-8 large-offset-2 end columns">

        <h5 class="verdiep">Verdieping 1</h5>

        <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelOneEvaluation }}}.png')">Gezondheid en functionele capaciteiten</h4>

        <!-- 2. GEZONDHEID -->
        <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelOne[0]['medianevaluation'] }}}.png')">Medische vragen</h5>


        <div class="feedback analyse">
            {{$scoresAll['14']['answers']['43']['countpercentage']}}% van uw medewerkers heeft aandoeningen of ziekten van het bewegingsapparaat waarmee ze bij het uitvoeren van hun taken moeten  rekening houden.
            <div class="action">Een ergonomisch risico-analyse en eventueel interventie in de zin van hulpmiddelen, opleidingen kunnen  voor deze medewerkers nuttig zijn.</div>
        </div>

        <div class="feedback analyse">
            <?php
            $leefstijlziekte = round(($scoresAll['15']['answers']['45']['countpercentage']+$scoresAll['17']['answers']['49']['countpercentage']+$scoresAll['18']['answers']['51']['countpercentage'])/3);
            ?>
            {{ $leefstijlziekte }}% van uw medewerkers heeft een leefstijl gerelateerde aandoening

            <div class="action">
                Betere leefgewoonten in de zin van niet roken, gezonde eetgewoonten, voldoende beweging kunnen deze aandoeningen voorkomen.
            </div>
        </div>

        <div class="feedback analyse">
            {{$scoresAll['20']['answers']['55']['countpercentage']}}% van uw medewerkers heeft een depressie of burn-out (doorgemaakt).

            <?php if ($scoresAll['20']['answers']['55']['countpercentage']>50) { ?>
                <div class="action">
                    Meer dan 10 % van uw medewerkers geeft aan in het verleden een burn-out of depressie doorgemaakt te hebben of
                    op dit moment door te maken. Dit is een zeer ernstig te nemen signaal vanuit uw medewerkerspopulatie en wijst op
                    een hoge psychosociale belasting.
                </div>
            <?php } ?>
        </div>


        <div class="feedback analyse">
            <?php
            $aandoening = round(($scoresAll['14']['answers']['43']['countpercentage']+
                           $scoresAll['15']['answers']['45']['countpercentage']+
                           $scoresAll['17']['answers']['49']['countpercentage']+
                           $scoresAll['18']['answers']['51']['countpercentage']+
                           $scoresAll['20']['answers']['55']['countpercentage']+
                           $scoresAll['21']['answers']['57']['countpercentage'])/6);
            ?>
            {{ $aandoening }}% een aandoening die mogelijk een effect op zijn arbeidscapaciteit heeft of zal hebben.
        </div>




        <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelOne[1]['medianevaluation'] }}}.png')" >Leefstijl</h5>

        <div class="feedback analyse">
            {{$scoresAll['23']['answers']['61']['countpercentage']}}% van uw medewerkers rookt
        </div>
        <div class="feedback analyse">
            {{$scoresAll['24']['answers']['64']['countpercentage']}}% van uw medewerkers beweegt te weinig
        </div>
        <div class="feedback analyse">
            <?php
            $gezondeten = max ($scoresAll['25']['answers']['66']['countpercentage'], $scoresAll['26']['answers']['68']['countpercentage']);
            ?>
            {{ $gezondeten }}% van uw medewerkers eet te weinig groenten of fruit
        </div>
        <div class="feedback analyse">
            {{$scoresAll['27']['answers']['69']['countpercentage']}}% van uw medewerkers drinkt te veel alcohol
        </div>
        <div class="feedback">
            Ongezonde leefgewoonten zijn oorzaak van heel wat aandoeningen die maken dat mensen sneller uitvallen en eerder stoppen met werken. Hart- en vaatlijden en diabetes worden in de hand gewerkt door roken, ongezonde eetgewoonten en te weinig bewegen. Het verband tussen roken en longkanker en chronische bronchitis is voldoende aangetoond. Overdreven alcoholgebruik veroorzaakt naast lichamelijke aandoeningen, ook ongevallen en psychosociale problemen. Aandacht voor en het bevorderen van gezonde leefgewoonten op de werkvloer is een zinvolle investering.
        </div>


        <!-- 2. GEZONDHEID LEEFSTIJL -->
        <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelOne[2]['medianevaluation'] }}}.png')">Verzuim</h5>

        <?php
        $levenstijl="ok";
        if ($scoresAll['23']['medianPoints']+$scoresAll['24']['medianPoints']+$scoresAll['25']['medianPoints']+$scoresAll['26']['medianPoints']+$scoresAll['28']['medianPoints'] > 0) {
            $levenstijl="nok";
        }
        ?>

        <!-- dagen & keer samen -->
        @if($scoresAll['33']['medianPoints'] > 0 || $scoresAll['34']['medianPoints'] > 0)
            <div class="feedback">@include('werkgevers/snippet-overz-31-feedback')</div>
        @endif
        <!-- keer wegens redenen die op het werk..-->
        @if($scoresAll['35']['medianPoints'] > 0)
            <div class="feedback">@include('werkgevers/snippet-overz-33-feedback')</div>
        @endif
        <!-- keer toch gaan werken-->
        @if($scoresAll['36']['medianAnswer'] !== 'geen enkele keer')
            <div class="feedback">@include('werkgevers/snippet-overz-34-feedback')</div>
        @endif

        <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelOne[3]['medianevaluation'] }}}.png')">Burn-out</h5>

        <!-- GEZONDHEID : burnout-->
        <?php
        $burnoutScore = $scoresAll['37']['medianPoints']+$scoresAll['38']['medianPoints'];
        ?>
        @if($burnoutScore <= 5)
            <div class="feedback">@include('werkgevers/snippet-overz-35-1-feedback')</div>
        @elseif ($burnoutScore > 13)
            <div class="feedback">@include('werkgevers/snippet-overz-35-2-feedback')</div>
        @else
        <!-- no fb -->
        @endif

        <?php
        $bevlogenheidsScore = $scoresAll['39']['medianPoints']+$scoresAll['40']['medianPoints']+ $scoresAll['41']['medianPoints']+$scoresAll['42']['medianPoints'];
        ?>
        @if($bevlogenheidsScore <= 19)
            <div class="feedback slecht">@include('werkgevers/snippet-overz-37-1-feedback')</div>
        @elseif ($bevlogenheidsScore > 25)
            <div class="feedback goed">@include('werkgevers/snippet-overz-37-2-feedback')</div>
        @else
        <!-- no fb -->
        @endif

    </div>
</div>






<div class="row  detail-results">
    <div class="large-8 large-offset-2 end columns">

                <h5 class="verdiep">Verdieping 2</h5>

                <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelTwoEvaluation }}}.png')">Competenties</h4>

                <!-- COMPETENTIES : rekrutering-->

                <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelTwo[0]['medianevaluation'] }}}.png')" >Zichtbaarheid op arbeidsmarkt</h5>

                <div class="feedback analyse">
                    {{$scoresAll['46']['answers'][176]['countpercentage']}} % van de medewerkers kent de kanalen om vacatures te vinden om elders te solliciteren.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['47']['answers']['178']['countpercentage']}} % van de medewerkers weet waar interne vacatures terug te vinden zijn.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['48']['answers']['180']['countpercentage']}} % van de medewerkers gebruikt sociale media als professioneel netwerk- en sollicitatiekanaal.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['49']['answers']['182']['countpercentage']}} %  van de medewerkers kent de financiële tegemoetkomingen voor de aanwerving van oudere medewerkers als hij een andere job wil zoeken.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['50']['answers']['184']['countpercentage']}} % van de medewerkers weet waar hij terecht kan voor opleidingen.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['51']['answers']['186']['countpercentage']}} % van de medewerkers weet hoe zich voor te bereiden op een selectieprocedure.
                </div>


                <!-- COMPETENTIES : talentmanagement-->

                <h5></h5>
                <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelTwo[1]['medianevaluation'] }}}.png')" >Talentmanagement</h5>


                <div class="feedback analyse">
                    {{$scoresAll['52']['answers']['188']['countpercentage']}} % van de medewerkers geeft aan een peter of meter te hebben gehad bij aanwerving.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['53']['answers']['190']['countpercentage']}} % van de medewerkers zegt een opleidingsplan met bijhorend budget te hebben.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['54']['answers']['192']['countpercentage']}} % van de medewerkers zegt graag opleidingen te volgen.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['55']['answers']['194']['countpercentage']}} % van de medewerkers zegt de afgelopen 3 jaar minstens één opleiding te hebben gevolgd.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['56']['answers']['196']['countpercentage']}} %  van de medewerkers heeft dankzij feedbackgesprekken met zijn leidinggevende een zicht op zijn sterktes en zwaktes.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['57']['answers']['198']['countpercentage']}} % van de medewerkers vindt dat zijn competenties ten volle benut worden.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['58']['answers']['200']['countpercentage']}} % van de medewerkers ziet zichzelf nog andere taken of functies opnemen in de onderneming.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['59']['answers']['202']['countpercentage']}} % van de medewerkers weet goed wat hij nog kan of wil met zijn loopbaan.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['60']['answers']['204']['countpercentage']}} % van de medewerkers vindt dat het delen van kennis in de onderneming wordt gestimuleerd.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['61']['answers']['206']['countpercentage']}} % van de medewerkers vindt dat er voldoende oog is voor de inzet van medewerkers.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['62']['answers']['208']['countpercentage']}} % van de medewerkers vindt dat er een lerende en open gesprekscultuur is in de onderneming.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['63']['answers']['210']['countpercentage']}} % van de medewerkers weet welke doelstellingen hij moet halen en waar hij zich op moet toeleggen.
                </div>

    </div>
</div>


<div class="row  detail-results">

    <div class="large-8 large-offset-2 end columns">

        <h5 class="verdiep">Verdieping 3</h5>

        <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelThreeEvaluation }}}.png')">Waarden, houding en motivatie</h4>

        <!-- WAARDEN : vkw-->

        <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelThree[0]['medianevaluation'] }}}.png')" >Waarden en normen</h5>

        <!-- waarden en normen: uniek voor werkgevers -->

            <div class="feedback">@include('werkgevers/snippet-overz-64-feedback')</div>

            <div class="feedback">@include('werkgevers/snippet-overz-65-feedback')</div>

            <div class="feedback">@include('werkgevers/snippet-overz-66-feedback')</div>

            <div class="feedback">@include('werkgevers/snippet-overz-67-feedback')</div>


            <!-- waarden en normen: ook bij  medewerkers -->

            <div class="feedback">@include('werkgevers/snippet-overz-68-feedback')</div>

            <div class="feedback">@include('werkgevers/snippet-overz-69-feedback')</div>

            <div class="feedback">@include('werkgevers/snippet-overz-70-feedback')</div>

            <div class="feedback">@include('werkgevers/snippet-overz-71-feedback')</div>


        <!-- WAARDEN : verbondenheid -->

        <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelThree[1]['medianevaluation'] }}}.png')">Persoonskenmerken</h5>

        <?php
        $verbondenheidScore = $scoresAll['72']['medianPoints']+$scoresAll['73']['medianPoints'];
        $priveScore = $scoresAll['74']['medianPoints']+$scoresAll['75']['medianPoints'];
        $tevredenheidsScore = $scoresAll['76']['medianPoints'];
        $vertrouwenmanagementScore = $scoresAll['77']['medianPoints'];
        $ontslagScore = $scoresAll['78']['medianPoints']+$scoresAll['79']['medianPoints']+$scoresAll['80']['medianPoints']+$scoresAll['81']['medianPoints'];
        $autonoomScore = $scoresAll['82']['medianPoints']+$scoresAll['83']['medianPoints']+$scoresAll['84']['medianPoints']+$scoresAll['85']['medianPoints'];
        ?>

        @if($verbondenheidScore >= 1)
            <div class="feedback slecht">@include('werkgevers/snippet-overz-72-1-feedback')</div>
        @endif
        @if(($scoresAll['72']['medianAnswer']=='eens' || $scoresAll['72']['medianAnswer']=='helemaal eens') && ($scoresAll['73']['medianAnswer']=='eens' || $scoresAll['73']['medianAnswer']=='helemaal eens'))
            <div class="feedback goed">@include('werkgevers/snippet-overz-72-2-feedback')</div>
        @endif

        @if($priveScore >= 1)
            <!-- slecht -->
            <div class="feedback slecht">@include('werkgevers/snippet-overz-74-2-feedback')</div>
        @endif
        @if(($scoresAll['74']['medianAnswer']=='oneens' || $scoresAll['74']['medianAnswer']=='helemaal oneens') && ($scoresAll['75']['medianAnswer']=='oneens' || $scoresAll['75']['medianAnswer']=='helemaal oneens'))
        <!-- goed -->
            <div class="feedback slecht">@include('werkgevers/snippet-overz-74-1-feedback')</div>
        @endif

        @if($scoresAll['76']['medianAnswer']=='helemaal oneens')
            <div class="feedback slecht">@include('werkgevers/snippet-overz-76-1-feedback')</div>
        @endif
        @if($scoresAll['76']['medianAnswer']=='helemaal eens')
            <div class="feedback goed">@include('werkgevers/snippet-overz-76-2-feedback')</div>
        @endif

        @if($scoresAll['77']['medianAnswer']=='helemaal oneens')
            <div class="feedback slecht">@include('werkgevers/snippet-overz-77-1-feedback')</div>
        @endif
        @if($scoresAll['77']['medianAnswer']=='eens' || $scoresAll['77']['medianAnswer']=='helemaal eens')
            <div class="feedback goed">@include('werkgevers/snippet-overz-77-2-feedback')</div>
        @endif

        @if($ontslagScore <= 0.25)
            <div class="feedback goed">@include('werkgevers/snippet-overz-78-1-feedback')</div>
        @endif
        @if(($scoresAll['78']['medianAnswer']=='eens' || $scoresAll['78']['medianAnswer']=='helemaal eens') && ($scoresAll['79']['medianAnswer']=='eens' || $scoresAll['79']['medianAnswer']=='helemaal eens') && ($scoresAll['80']['medianAnswer']=='eens' || $scoresAll['80']['medianAnswer']=='helemaal eens') && ($scoresAll['81']['medianAnswer']=='oneens' || $scoresAll['81']['medianAnswer']=='helemaal oneens'))
            <div class="feedback slecht">@include('werkgevers/snippet-overz-78-2-feedback')</div>
        @endif

        @if($autonoomScore >= 0.5)
            <div class="feedback slecht">@include('werkgevers/snippet-overz-82-1-feedback')</div>
        @endif
        @if(($scoresAll['82']['medianAnswer']=='eens' || $scoresAll['82']['medianAnswer']=='helemaal eens') && ($scoresAll['83']['medianAnswer']=='eens' || $scoresAll['83']['medianAnswer']=='helemaal eens') && ($scoresAll['84']['medianAnswer']=='eens' || $scoresAll['84']['medianAnswer']=='helemaal eens') && ($scoresAll['85']['medianAnswer']=='eens' || $scoresAll['85']['medianAnswer']=='helemaal eens'))
            <div class="feedback goed">@include('werkgevers/snippet-overz-82-2-feedback')</div>
        @endif

    </div>

</div>


<div class="row  detail-results light">

    <div class="large-8 large-offset-2 end columns">

        <h5 class="verdiep">Verdieping 4</h5>

        <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelFourEvaluation }}}.png')">Werk, werkgemeenschap en leiding</h4>

        <div class="feedback analyse">
            <strong>Ergonomisch Risico</strong>


            <table>
                <tr>
                    <td>Nooit</td>
                    <td>% nooit</td>
                    <td>% eens per maand of minder</td>
                    <td>% eens per week</td>
                    <td>% een paar keer per week</td>
                    <td>% dagelijks</td>
                </tr>
                <tr>
                    <td>Zware lasten</td>
                    <td>{{$scoresAll['86']['answers']['378']['countpercentage']}}</td>
                    <td>{{$scoresAll['86']['answers']['379']['countpercentage']}}</td>
                    <td>{{$scoresAll['86']['answers']['380']['countpercentage']}}</td>
                    <td>{{$scoresAll['86']['answers']['381']['countpercentage']}}</td>
                    <td>{{$scoresAll['86']['answers']['382']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Repetitieve beweging</td>
                    <td>{{$scoresAll['87']['answers']['383']['countpercentage']}}</td>
                    <td>{{$scoresAll['87']['answers']['384']['countpercentage']}}</td>
                    <td>{{$scoresAll['87']['answers']['385']['countpercentage']}}</td>
                    <td>{{$scoresAll['87']['answers']['386']['countpercentage']}}</td>
                    <td>{{$scoresAll['87']['answers']['387']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Zelfde houding</td>
                    <td>{{$scoresAll['88']['answers']['388']['countpercentage']}}</td>
                    <td>{{$scoresAll['88']['answers']['389']['countpercentage']}}</td>
                    <td>{{$scoresAll['88']['answers']['390']['countpercentage']}}</td>
                    <td>{{$scoresAll['88']['answers']['391']['countpercentage']}}</td>
                    <td>{{$scoresAll['88']['answers']['392']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Beeldschermwerk</td>
                    <td>{{$scoresAll['89']['answers']['393']['countpercentage']}}</td>
                    <td>{{$scoresAll['89']['answers']['394']['countpercentage']}}</td>
                    <td>{{$scoresAll['89']['answers']['395']['countpercentage']}}</td>
                    <td>{{$scoresAll['89']['answers']['396']['countpercentage']}}</td>
                    <td>{{$scoresAll['89']['answers']['397']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Trillend gereedschap werken</td>
                    <td>{{$scoresAll['90']['answers']['398']['countpercentage']}}</td>
                    <td>{{$scoresAll['90']['answers']['399']['countpercentage']}}</td>
                    <td>{{$scoresAll['90']['answers']['400']['countpercentage']}}</td>
                    <td>{{$scoresAll['90']['answers']['401']['countpercentage']}}</td>
                    <td>{{$scoresAll['90']['answers']['402']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Extreme temperatuur</td>
                    <td>{{$scoresAll['91']['answers']['403']['countpercentage']}}</td>
                    <td>{{$scoresAll['91']['answers']['404']['countpercentage']}}</td>
                    <td>{{$scoresAll['91']['answers']['405']['countpercentage']}}</td>
                    <td>{{$scoresAll['91']['answers']['406']['countpercentage']}}</td>
                    <td>{{$scoresAll['91']['answers']['407']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Slecht verlichte lokalen</td>
                    <td>{{$scoresAll['92']['answers']['408']['countpercentage']}}</td>
                    <td>{{$scoresAll['92']['answers']['409']['countpercentage']}}</td>
                    <td>{{$scoresAll['92']['answers']['410']['countpercentage']}}</td>
                    <td>{{$scoresAll['92']['answers']['411']['countpercentage']}}</td>
                    <td>{{$scoresAll['92']['answers']['412']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Slecht geventileerde lokalen</td>
                    <td>{{$scoresAll['93']['answers']['413']['countpercentage']}}</td>
                    <td>{{$scoresAll['93']['answers']['414']['countpercentage']}}</td>
                    <td>{{$scoresAll['93']['answers']['415']['countpercentage']}}</td>
                    <td>{{$scoresAll['93']['answers']['415']['countpercentage']}}</td>
                    <td>{{$scoresAll['93']['answers']['417']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Lawaaierige omgeving</td>
                    <td>{{$scoresAll['94']['answers']['418']['countpercentage']}}</td>
                    <td>{{$scoresAll['94']['answers']['419']['countpercentage']}}</td>
                    <td>{{$scoresAll['94']['answers']['420']['countpercentage']}}</td>
                    <td>{{$scoresAll['94']['answers']['421']['countpercentage']}}</td>
                    <td>{{$scoresAll['94']['answers']['422']['countpercentage']}}</td>
                </tr>
            </table>

            <?php
            $blootstelling = (($scoresAll['86']['medianPoints']+$scoresAll['87']['medianPoints']+$scoresAll['88']['medianPoints']+$scoresAll['89']['medianPoints']+$scoresAll['90']['medianPoints']+$scoresAll['91']['medianPoints']+$scoresAll['92']['medianPoints']+$scoresAll['93']['medianPoints']+$scoresAll['94']['medianPoints'])/9);

            ?>

            <?php if ($blootstelling > 0) { ?>
                <div class="action">@include('werkgevers/snippet-overz-86-feedback')</div>
            <?php } ?>

        </div>

        <div class="feedback analyse">

            <strong>Werkplekinrichting</strong><br>
            <br>
            <table>
                <tr>
                    <td></td>
                    <td>% Ja</td>
                    <td>% Nee</td>
                </tr>
                <tr>
                    <td>Goede werkplekinrichting</td>
                    <td>{{$scoresAll['95']['answers']['423']['countpercentage']}}</td>
                    <td>{{$scoresAll['95']['answers']['424']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Instelbaar meubilair</td>
                    <td>{{$scoresAll['96']['answers']['425']['countpercentage']}}</td>
                    <td>{{$scoresAll['96']['answers']['426']['countpercentage']}}</td>
                </tr>
                <tr>
                    <td>Hulpmiddelen zoals laptophouder, ...</td>
                    <td>{{$scoresAll['97']['answers']['427']['countpercentage']}}</td>
                    <td>{{$scoresAll['97']['answers']['428']['countpercentage']}}</td>
                </tr>
            </table>

            <?php
            $ergonomisch = ($scoresAll['95']['answers']['424']['countpercentage']+$scoresAll['96']['answers']['426']['countpercentage']+$scoresAll['97']['answers']['428']['countpercentage'])/3;
            ?>
            <?php if ($ergonomisch > 50) { ?>
                <div class="action">@include('werkgevers/snippet-overz-95-feedback')</div>
            <?php } ?>
        </div>

        <div class="feedback analyse">

            <table>
                <tr>
                    <td></td>
                    <td>% Ja</td>
                    <td>% Nee</td>
                </tr>
                <tr>
                    <td>Voldoende Mogelijkheid</td>
                    <td>{{$scoresAll['98']['answers']['429']['countpercentage']}}</td>
                    <td>{{$scoresAll['98']['answers']['430']['countpercentage']}}</td>
                </tr>
            </table>


            <?php if ($scoresAll['98']['answers']['430']['countpercentage'] > 49) { ?>
                <div class="action slecht">@include('werkgevers/snippet-overz-98-2-feedback')</div>
            <?php } else { ?>
                <div class="action goed">@include('werkgevers/snippet-overz-98-1-feedback')</div>
            <?php } ?>
        </div>

    </div>
</div> <!-- end row -->


<div class="row detail-results">
    <div class="large-8 large-offset-2 end columns">
        <h5 class="verdiep">Het dak</h5>
        <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelFiveEvaluation }}}.png')">Werkvermogen</h4>

        <?php
        $werkvermogenScore = $scoresAll['29']['medianPoints']+$scoresAll['30']['medianPoints']+$scoresAll['31']['medianPoints']+$scoresAll['32']['medianPoints'];
        ?>

        @if($werkvermogenScore <= 9)
            <div class="feedback">@include('werkgevers/snippet-overz-27-3-feedback')</div>
        @elseif ($werkvermogenScore > 15)
            <div class="feedback">@include('werkgevers/snippet-overz-27-1-feedback')</div>
        @else
            <div class="feedback">@include('werkgevers/snippet-overz-27-2-feedback')</div>
        @endif

        @if($scoresAll['30']['medianPoints'] > $scoresAll['31']['medianPoints'])
            <div class="feedback">@include('werkgevers/snippet-overz-27-4-feedback')</div>
        @endif
        @if($scoresAll['31']['medianPoints'] > $scoresAll['30']['medianPoints'])
            <div class="feedback">@include('werkgevers/snippet-overz-27-5-feedback')</div>
        @endif
        @if(($scoresAll['30']['medianPoints'] == $scoresAll['31']['medianPoints']))
            <div class="feedback">@include('werkgevers/snippet-overz-27-6-feedback')</div>
        @endif


    </div>
</div>


</div> <!-- END PANEL2-->

@endif


<div class="content" id="panel3">
    @include('werkgevers.eindresultaat-include', array('scoresBeleidFeedback'=>$scoresBeleidFeedback))
</div> <!-- END PANEL3-->


<div class="content" id="panel4">

    De Wellfie resultaten blijven steeds consulteerbaar. Wilt u de informatie exporteren dan kan dit hier via de export:
    {{ HTML::link('/werkgevers/pdfexport/'.$scan_id, 'pdf export', array('id' => 'pdfexport'))}}

</div>

</div> <!-- END TABS CONTENT-->


</div> <!-- end large 12 columns -->




@stop
