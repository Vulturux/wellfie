<strong>Progressieve werkhervatting</strong><br>
Een medewerker die het werk gedeeltelijk wenst te hervatten, kan dit reeds tijdens de lopende periode van arbeidsongeschiktheid,
onder volgende voorwaarden:<br>
<ul>
    <li>de medewerker is minimaal 1 werkdag arbeidsongeschikt</li>
    <li>de medewerker blijft verder nog minimaal 50 % arbeidsongeschikt</li>
    <li>er is geen voorafgaandelijk schriftelijk akkoord vereist van de geneesheer medisch adviseur van de
    mutualiteit, wel een verplichte aanvraag (ten laatste de werkdag voorafgaand aan werkhervatting)</li>
    <li>de medewerker mag gedurende een toegestane periode een bepaald maximum aantal uren arbeid verrichten.</li>
</ul>
Als werkgever moet u zich hiermee akkoord verklaren, u bent hiertoe niet verplicht. De progressieve werkhervatting is een instrument om medewerkers die dreigen langdurig ziek te zijn, sneller terug in het arbeidsproces op te nemen.