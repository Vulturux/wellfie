Loop als leidinggevende ook geregeld rond op de werkvloer om hier en daar een kort praatje te
maken. De stap naar boven wordt een stuk kleiner, als boven ook de stap naar beneden heeft gezet.