<strong>Ongewenst grensoverschrijdend gedrag op het werk (OGGW)</strong><br>
Proficiat! U antwoordde dat uw onderneming een beleid rond het actiegebied ‘OGGW’ opgesteld heeft.
Dit is inderdaad wat door de wet voorgeschreven wordt (K.B. betreffende preventie van psychosociale 
risico’s op het werk, 2014). Wanneer er echter ook acties opgezet worden om dit beleid onder de 
aandacht te brengen en levendig te houden, zou de preventie van OGGW en andere psychosociale risico’s volledig zijn.
 U kan bijvoorbeeld een campagne uitwerken over respectvol gedrag.