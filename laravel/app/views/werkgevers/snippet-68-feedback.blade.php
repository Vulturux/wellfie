U rapporteert dat in uw onderneming medewerkers geregeld repetitieve bewegingen moeten uitvoeren. Medewerkers die dit frequent moeten doen, hebben meer kans op overbelastingletsels ter hoogte van spieren, pezen, zenuwen en gewrichten. Dergelijke letsels leiden vaak tot krachtverlies, gevoelsvermindering en beperkte coördinatie waardoor de medewerker bepaalde taken niet meer naar behoren kan uitvoeren. Beter voorkomen dan genezen dus!<br>
<br>
Als werkgever moet u ervoor zorgen dat langdurige repetitieve belasting vermeden wordt en werkomstandigheden die een risico inhouden worden aangepakt. Dit is de taak van de werkgever met gericht ergonomisch advies.
<ul>
<li>Voer een specifieke risicoanalyse uit.</li>
<li>Bekijk dan, samen met de medewerker en de preventieadviseur, hoe de werkomstandigheden kunnen
verbeterd worden.
<ul>
<li>Optimalisatie van technische aspecten: correcte inrichting van de werkplek (zodat
reikafstanden beperkt blijven), gebruik van hulpmiddelen en gereedschappen om werk lichter
te maken, ...</li>
<li>Aanpassing van werkorganisatie: afwisseling van taken en variatie in werktempo en –volgorde
mogelijk maken, inlassen van meerdere korte pauzes, ... </li>
<li>Correcte werkmethode: instructies opstellen, training en opleiding aanbieden. </li>
</ul></li>
</ul>