<strong>Financiële aanvulling tijdskrediet</strong><br>
U kan overwegen om een financiële aanvulling (bovenop de uitkering die ze van de Rijksdienst voor arbeidsvoorziening (RVA) ontvangen) toe te kennen aan medewerkers die op tijdskrediet gaan. Bevraag u bij een organisatie die juridisch advies aanbiedt over de mogelijkheden en verdere modaliteiten.
