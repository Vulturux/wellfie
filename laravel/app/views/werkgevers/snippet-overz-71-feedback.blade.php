<strong>{{ $scoresAll['71']['avarageAnswerFloat'] }} op 10:</strong>
Dat is de mate waarin uw medewerkers vinden dat <strong>hun persoonlijke waarden aansluiten op de waarden van het bedrijf</strong>.<br>

<i>Tips:</i><br>
<ul>
    <li>Peil naar de persoonlijke waarden van mensen bij aanwerving</li>
    <li>Moedig medewerkers aan om hun eigen mening te geven, ook al is die
    afwijkend dan die van jezelf of de meerderheid.</li>
    <li>Creëer een veilige sfeer waarin moeilijke situaties kunnen besproken worden.</li>
</ul>