<strong>{{ $scoresAll['70']['avarageAnswerFloat'] }} op 10:</strong>
Dat is de mate waarin <strong>uw medewerkers vinden dat men hier doet wat men zegt en zegt wat men doet</strong>.<br>

<i>Tips:</i><br>

<ul>
	<li>Geef zelf het goede voorbeeld.</li>
	<li>Kom je beloften na.</li>
	<li>Geef uitleg bij keuzes die je maakt.</li>
</ul>