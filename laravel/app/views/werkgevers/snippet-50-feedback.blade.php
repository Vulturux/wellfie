Indien een heroriëntering naar een andere werkgever nodig is, dan kan u een
outplacementbegeleiding aanbieden aan uw medewerker. Tijdens zo’n begeleiding
 krijgt uw medewerker de kans om samen met zijn loopbaancoach een analyse te maken 
 van zijn kennis, ervaring en wensen in functie van de mogelijkheden op de arbeidsmarkt.
  In sommige gevallen bent u als werkgever verplicht om een outplacementbegeleiding aan te bieden. 
Voor meer informatie hierover kan u contact opnemen met een organisatie 
gespecialiseerd in outplacement en loopbaanbegeleiding.