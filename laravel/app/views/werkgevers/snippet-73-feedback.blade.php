U rapporteert dat in uw onderneming medewerkers geregeld met slechte verlichting moeten werken. Goede werkomstandigheden betekenen ook voldoende en correcte verlichting. Dit is essentieel voor een goede gezondheid van de ogen.<br>
<br>
<ul>
<li>Voer een specifieke risicoanalyse uit om mogelijke risico’s te inventariseren.
<li>Bekijk dan, samen met de medewerker en preventieadviseur, hoe de werkomstandigheden kunnen verbeterd worden. Enkele tips:
<ul>
<li>Verlicht de werkplek, bij voorkeur, door inval van voldoende daglicht.</li>
<li>Vermijd verblindende lichtbronnen zoals niet-afgeschermde lampen, reflecterende
oppervlakken en zonlicht.</li>
<li>Pak de oorzaak aan. Bijvoorbeeld: verplaats een werktafel zodat er geen storende spiegelingen
van kunstlicht, daglicht of reflecterende wanden meer invallen op het werkvlak of computerscherm.</li>
</ul></li>
</ul>