@extends('layoutsWerknemer.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">Opvolging</h3>
</div>

<div class="row visualisation-container">


</div>
<div class="row" style="margin-top: 20px;">



    <div class="large-8 columns">

        Wanneer de bevraging bij de medewerkers afgesloten wordt, kunnen de resultaten opgehaald worden:<br>
        <ul>
            <li>indien minstens 10 medewerkers de vragenlijst hebben afgewerkt kan je na afsluiten beleidsfeedback én een groepsrapport van de medewerkers raadplegen.</li>
            <li>indien minder dan 10 vragenlijsten werden ingevuld wordt na het afsluiten enkel de beleidsfeedback aangemaakt. Dit om de privacy van de medewerkers te garanderen.</li>
        </ul>
        <br>

        ps: Werknemers die de vragenlijst invullen, krijgen altijd persoonlijke feedback.<br>

        <br>
        <div data-alert class="alert-box radius round orange-bg">
            <strong color=""><i class="step fi-alert size-36"></i> <span class="sentence">Let op. Je kan de vragenlijst maar één keer afsluiten</span></strong>
        </div>

    </div>
    <div class="large-4 columns">
        <div class="panel">

            U heeft <strong>{{ $aantalWN}} medewerkers uitgenodigd</strong>.<br><br>
            Hiervan hebben er momenteel<br> <strong>{{ $werknemersCompletedScan }} medewerkers  de vragenlijst  afgewerkt</strong>.</em><br>




            <div class="button-container text-center">


                <ul class="button-group">
                    <?php if ($werknemersCompletedScan > 0) { ?>
                        <li><a href="/werkgevers/hoeresultateninterpreteren" class="button main"><i class="fi-play"></i> Definitief afsluiten</a></li>
                    <?php } ?>
                </ul>

            </div>

        </div>
    </div> <!-- end large 12 columns -->

</div> <!-- end row -->

@stop
