<strong>Spontane consultatie</strong><br>
Elke medewerker, onderworpen aan het gezondheidstoezicht of niet, kan op eigen initiatief een
spontane consultatie bij de arbeidsgeneesheer vragen wanneer hij meent gezondheidsklachten te
hebben die te wijten zijn aan de werkomgeving. Tot voor kort verliep deze aanvraag via de werkgever.
Sinds het recente K.B. van 24 april 2014 kan de medewerker zich direct en discreet bij de arbeidsgeneesheer
aanbieden. De werkgever vermeldt hiertoe naam en contactgegevens van de arbeidsgeneesheer op een voor de
medewerkers gemakkelijk toegankelijke plaats.<br>
<br>
De arbeidsgeneesheer kan samen met de medewerker bekijken of er inderdaad een effect van de werkomgeving
is op de gezondheidstoestand dan wel of de eventuele gezondheidsproblemen moeilijkheden geven bij de uitvoering
van het werk. Tegelijkertijd kan bekeken worden hoe de problemen kunnen worden aangepakt. De arbeidsgeneesheer kan
eventueel een werkaanpassing vragen. Zeker voor ouder wordende medewerkers is dit een belangrijk middel om met
eventuele leeftijdsgebonden beperkingen toch op een goede manier aan het werk te blijven.