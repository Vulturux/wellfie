@extends('layoutsWerkgever.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">{{{ $title }}}</h3>
    <div style="position: absolute;top:20px;right:25px"><a href="/werkgevers/eindresultaat"><i class="fi-eye"></i> Feedback werkgever</a></div>
</div>

<div class="row">

    <div class="large-12 columns vragen-content-container">

        Als werkgever kun je de Wellfie scan op een later tijdstip opnieuw uitvoeren. De (oude) resultaten blijven hier behouden.<br><br>
            <ul>
                <?php
                for ($x = 1; $x <= $scan_id; $x++) {
                    if ($x == $scan_id) {
                        if ($position == 10) {
                            echo '<li><a href="/werkgevers/wellfierapport/'.$x.'/1" target="_blank">Bekijk resultaten van scan '.$x.'</a></li>';
                        } else {
                            echo 'Scan '.$x.' is nog niet afgesloten';
                        }
                    } else {
                        echo '<li><a href="/werkgevers/wellfierapport/'.$x.'/1" target="_blank">Bekijk resultaten van scan '.$x.'</a></li>';
                    }
                }
                ?>
            </ul>

            <br>
            <div class="scan-new panel">
               <?php if ($position == 10) { ?>
                    Start een <a href="/werkgevers/startnewscan/">nieuwe scan op als werkgever</a>. Dit is niet herroepbaar. De vorige scan resultaten
                    zullen evenwel raadpleegbaar blijven via deze pagina.
                <?php } else { ?>
                    Je kan pas een nieuwe scan starten wanneer de huidige is afgesloten.
               <?php } ?>


            </div>

        {{ Form::close() }}
    </div> <!-- end large 12 columns -->

</div> <!-- end row -->



@stop
