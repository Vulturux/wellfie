<strong>Telewerk</strong><br>
Overweeg om uw medewerkers te laten telewerken van thuis, dit kan bijvoorbeeld één vaste dag per week.
Gelieve wel voor ogen te houden dat indien u dit wenst in te voeren, er moet voldaan worden
aan een aantal verplichte voorwaarden. U kan hiervoor terecht bij een organisatie die juridisch advies aanbiedt.<br>
<br>

Volgens de officiële definitie is telewerk een vorm van organisatie en/of uitvoering van het werk waarin,
met gebruikmaking van informatietechnologie, in het kader van een arbeidsovereenkomst werkzaamheden die ook op
de ondernemingslocatie van de werkgever zouden kunnen worden uitgevoerd, op regelmatige basis en niet-incidenteel buiten
die ondernemingslocatie worden uitgevoerd.<br>
