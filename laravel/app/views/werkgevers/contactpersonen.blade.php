@extends('layoutsWerknemer.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">Contactpersonen</h3>
</div>

<div class="row visualisation-container">


</div>
<div class="row">
    <div class="large-12 columns">
        {{ Form::open(array('url' => 'werkgevers/contactpersonen', 'class'=>'contactpersonen', 'id' => 'js-contactpersonen')) }}

            @if($errors->has())
                <div class="alert alert-error alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                </div>
            @endif

            <h4>Wellfie verantwoordelijke</h4>
            <p class="info"><i class="fi-info"></i>
               COPY TE KRIJGEN De Wellfie verantwoordelijke is verantwoordelijk voor het in goede banen leiden van het Wellfie project. Hij/zij is
                het aanspreekpunt voor de werknemers en is ook verantwoordelijk om de vragenlijst aan werkgevers kant in te vullen ...

            <div class="row">
                <div class="large-8 columns end">
                    <label>E-mail</label>
                    <input type="text" placeholder="" name="emailwellfiecoach" value ="{{ $wellfiecoach->email }}" />
                </div>
            </div>

            <div class="row">
                <div class="large-12 columns ">
                    <strong>Welke functies vervult de wellfie verantwoordelijke in het bedrijf? </strong>
                </div>
            </div>

            <div class="row">
                <div class="large-8 columns checkboxes end">

                    <input id="CEO" name="CEO" type="checkbox"  value="checked" @if( $wellfiecoach->permission_level_id > 5) checked @endif>
                    <label for="CEO">CEO</label><br>
                </div>
            </div>

            <div class="row">
                <div class="large-8 columns checkboxes end">
                    <input id="HR" name="HR" type="checkbox"  value="checked" @if( $wellfiecoach->permission_level_id == 5 || $wellfiecoach->permission_level_id == 7) checked @endif>
                    <label for="HR">HR manager</label>
                </div>
            </div>

            <div class="button-container text-center">
                <ul class="button-group">
                    <li>{{ Form::submit('Ok, volgende stap', array('class' => 'button main')) }}</li>
                </ul>
            </div>

        </form>


    </div>


</div>


</div>

@stop
