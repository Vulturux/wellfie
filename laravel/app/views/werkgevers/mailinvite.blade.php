<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wellfie</title>
    <style type="text/css">
        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        /* Prevent WebKit and Windows mobile changing default text sizes */

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Remove spacing between tables in Outlook 2007 and up */

        img {
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            border: none;
            -ms-interpolation-mode: bicubic;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;

            background: #CCC;
        }

        a {
            text-decoration: none;
            color: #e58625;
        }

        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        table.mobileContainer {
            background: #fff;
        }

        tr.row-logo td {
            padding: 30px 0;
        }

        img.logo {
            display: block;
            width: 146px;
            margin: 0 auto;
        }

        tr.row-intro td, tr.row-button td, tr.row-advantage td, tr.row-anonimity td, tr.row-easy td, tr.row-login td {
            padding: 0 30px 30px 30px;
        }

        tr.row-advantage td, tr.row-login td {
            background: #ececec;
        }

        tr.row-anonimity td, tr.row-login td {
            padding-bottom: 0;
        }

        .title:after {
            content: "";
            display: block;
            width: 60px;
            height: 3px;
            background: #e58625;
            margin: 5px auto 15px auto;
        }

        @media only screen and (max-width: 600px) {
            table.wrap {
                background-color: #363636;
            }

            table.mobileContainer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 400px) {
            p {
                text-align: justify;
            }

            p.img-right {
                position: relative;
                padding-right: 0;
                text-align: justify;
            }

            p.img-right img.icon {
                position: relative;
                display: block;
                top: auto;
                right: auto;
                margin: 30px auto 0 auto;
            }
        }
    </style>
</head>

<body style="margin: 0 !important; padding: 0 !important;">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="wrap">
    <tr>
        <td align="center" valign="top">
            <!-- logos header -->
            <table id="header" class="mobileContainer" width="600" border="0" cellspacing="0" cellpadding="0"
                    style="width:600px;">
                <tr class="row-logo">
                    <td align="center">
                        <img class="logo" src="{{Config::get('app.url')}}/images/mail/logo_wellfie.png"
                                alt="Wellfie" width="146" height="85" />
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0 30px 30px 30px;">
                        <img class="icon"
                             src="{{Config::get('app.url')}}/images/mail/list-icon.png"
                             alt="Wellfie Vragenlijst" width="92" height="92" style="float: right;" />

                        <font style="font-family: Arial, sans-serif; font-size: 14px; line-height: 24px; color: #4c4c4c; padding-bottom: 20px; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">{{nl2br($mailcontent)}}</font>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0 30px 30px 30px;">
                        <table align="center" cellpadding="0" cellspacing="0" border="0"
                                style="background-color: #e58625; border-radius:5px;">
                            <tr>
                                <td valgin="middle" class="button"
                                        style="color: #FFFFFF; padding: 15px 50px; text-decoration: none; font-family: Arial,sans-serif; font-size: 16px;">
                                    <a href="{{Config::get('app.url')}}/users/createfrominvite/<?php echo $invitecode ?>" style="color: #ffffff; padding: 5px 15px; text-decoration: none;">
                                        NU DEELNEMEN
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 30px 30px 15px 30px; background: #ececec; text-align: center;">
                        <font class="title"
                                style="font-family: Arial, sans-serif; font-size: 24px; font-weight: bold; color: #e58625; text-align: center; padding: 30px 0 15px 0; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Direct
                            voordeel</font>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0 30px 30px 30px; background: #ececec;">
                        <font style="font-family: Arial, sans-serif; font-size: 14px; line-height: 24px; color: #4c4c4c; padding-bottom: 20px; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Deelname
                            levert u een direct voordeel op: u krijgt meteen na het invullen van de vragenlijst uw
                            persoonlijk resultaat en gerichte tips omtrent de bevraagde onderwerpen. Van bepaalde delen
                            van de data wordt een rapport met groepsresultaten gemaakt. Zo kunnen we nagaan aan welke
                            punten eerst gewerkt moet worden om onze medewerkers aan het werk te houden.</font>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 30px 30px 15px 30px; text-align: center;">
                        <font class="title"
                                style="font-family: Arial, sans-serif; font-size: 24px; font-weight: bold; color: #e58625; text-align: center; padding: 30px 0 15px 0; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Anonimiteit</font>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0 30px 30px 30px;">
                        <font style="font-family: Arial, sans-serif; font-size: 14px; line-height: 24px; color: #4c4c4c; padding-bottom: 20px; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">We
                            willen benadrukken dat de bevraging in alle anonimiteit zal verlopen. <strong>Wij zullen
                                nooit inzage hebben in de individuele resultaten.</strong> De resultaten van de
                            medewerkers zullen steeds worden verwerkt tot groepsresultaten waarbij een individuele
                            medewerker nooit herkenbaar zal zijn. Er worden alleen groepsresultaten aangemaakt indien er
                            minstens 10 medewerkers de vragenlijst ingevuld hebben.</font>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 15px 30px 15px 30px; text-align: center;">
                        <font class="title"
                                style="display: block; font-family: Arial, sans-serif; font-size: 24px; font-weight: bold; color: #e58625; text-align: center; padding: 0 0 15px 0; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Snel
                            en makkelijk!</font>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0 30px 30px 30px;">
                        <font style="font-family: Arial, sans-serif; font-size: 14px; line-height: 24px; color: #4c4c4c; padding-bottom: 20px; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">U
                            krijgt twee weken de tijd om de vragenlijst in te vullen. Deelname is vrijwillig. Maar hoe
                            meer medewerkers deelnemen, hoe betrouwbaarder de resultaten zullen zijn en hoe beter we
                            zullen weten welke de prioriteiten van onze onderneming zullen zijn.</font><br />
                        <br />
                        <font style="font-family: Arial, sans-serif; font-size: 14px; line-height: 24px; color: #4c4c4c; padding-bottom: 20px; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Het
                            invullen van de vragenlijst duurt <strong>ongeveer 20 minuten</strong>. U kan de vragenlijst
                            steeds onderbreken en later weer oproepen en verder invullen.</font><br />
                        <br />
                        <font style="font-family: Arial, sans-serif; font-size: 14px; line-height: 24px; color: #4c4c4c; padding-bottom: 20px; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Alvast
                            bedankt voor uw medewerking!<br />Bedrijfsleider en ‘Wellfie’-promotor</font>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 15px 30px 0 30px; background: #ececec;">
                        <font style="font-family: Arial, sans-serif; font-size: 14px; line-height: 24px; color: #4c4c4c; padding-bottom: 20px; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;">Eens
                            je de invitatieprocedure doorlopen hebt, kun je het platform bereiken via de normale login
                            link: <a
                                    href="{{Config::get('app.url')}}/users/login">{{Config::get('app.url')}}/users/login</a></font><br />
                        <br />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>
