@extends('layoutsWerknemer.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">Tussentijdse resultaten van {{{ $title }}}</h3>
</div>

<div class="row visualisation-container">

    <div class="medium-12 columns ">
        <div class="tussenresultaat visualisation">
            <img src="{{ URL::asset('images/visualisation/licht-uit.png'); }}" class="viz-image">
            <div class="viz-level {{ $levelClass }}">
                <img src="/images//visualisation/verdieping{{ $level }}_{{ $catevaluation }}.png">
            </div>
        </div>
    </div>


    <div class="viz-sideinfo  {{ $levelClass }} arrow_box">

            <div class="viz-sideinfo-block first">
                <img src="/images/icons/face{{ $catevaluation }}.png" class="smiley"><span class="title big">TOTAAL</span>
            </div>

            @foreach ($scores as $score)
                <div class="viz-sideinfo-block">
                    <img src="/images/icons/face{{ $score['evaluation'] }}.png" class="smiley"><span class="title small">{{ $score['title'] }}</span>
                </div>
            @endforeach
    </div>


</div>
<div class="row" style="margin-top: 20px;">

    <div class="large-12 columns">
        <div class="button-container text-center">
            <ul class="button-group">
                <li><a href="/werkgevers" class="button main"><i class="fi-play"></i> Ga verder</a></li>
            </ul>
        </div>
    </div> <!-- end large 12 columns -->

</div> <!-- end row -->


@stop
