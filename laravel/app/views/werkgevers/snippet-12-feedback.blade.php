<strong>Re-integratieonderzoek</strong><br>
Een medewerker die door zijn eigen arts (huisarts, specialist) om medische reden definitief ongeschikt verklaard is voor zijn normale functie, kan een ‘re-integratie of herinschakelingsonderzoek’ vragen. Tot voor kort diende dit aangetekend en via de werkgever te gebeuren. Sinds het nieuwe K.B. kan de medewerker zich direct en discreet bij de arbeidsgeneesheer aanbieden. <br>
<br>
Zowel onderworpen als niet-onderworpen medewerkers kunnen van deze procedure gebruik maken. Na kennisname van het standpunt van de behandelende arts van de betrokken medewerker, en na eventuele bijkomende onderzoeken die hij noodzakelijk acht, neemt de arbeidsgeneesheer een beslissing over de geschiktheid van de medewerker. Hij geeft advies omtrent de restgeschiktheid en voorwaarden voor verdere tewerkstelling.<br>
<br>
De werkgever licht de arbeidsgeneesheer in wanneer hij geen aangepast of ander werk kan geven. In dit geval kan immers op de beslissing van de arbeidsgeneesheer een medisch ontslag volgen om redenen van medische overmacht.<br>
<br>