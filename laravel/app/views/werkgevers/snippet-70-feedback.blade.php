U rapporteert dat in uw onderneming medewerkers geregeld beeldschermwerk moeten uitvoeren. Medewerkers die meer dan 4u per dag en 20u per week beeldschermwerk uitvoeren hebben meer kans op klachten ter hoogte van polsen, ellebogen, schouders, nek en rug. Oorzaken hiervan zijn ongunstige houdingen en te weinig beweging op het werk.<br>
<br>
Werkomstandigheden die een risico inhouden moeten worden aangepakt (K.B. werken met beeldschermapparatuur, 1993). Dit is de taak van de werkgever met gericht ergonomisch advies.

<ul>
	<li>Voer een specifieke risicoanalyse uit.</li>
	<li>Bekijk dan, samen met de medewerker en preventieadviseur, hoe de werkomstandigheden kunnen verbeterd worden.
	<ul>
	<li>Optimalisatie van technische aspecten: correcte inrichting van de werkplek rekening houdend met
	ruimte, doorgangen en omgevingsfactoren zoals verlichting en verluchting; gebruik van
	ergonomisch meubilair en hulpmiddelen (bijvoorbeeld laptophouder, extern toetsenbord, ...)
	<li>Aanpassing van werkorganisatie: beweging op het werk toelaten, langdurig zitten onderbreken,
	afwisseling taken mogelijk maken, inlassen van meerdere korte pauzes, ...
	<li>Correcte werkmethode: instructies opstellen, training en opleiding aanbieden</li>
	</ul></li>
</ul>
