U rapporteert dat in uw onderneming medewerkers geregeld met trillend gereedschap moeten werken. Medewerkers die dit frequent moeten doen, hebben meer kans op werk-gerelateerde klachten door blootstelling aan hoge trillingsniveaus (zowel hand-arm trillingen als globale lichaamstrillingen kunnen ongunstig zijn).<br>
<br>
Werkomstandigheden die een risico inhouden moeten worden aangepakt (K.B. betreffende de bescherming van de gezondheid en de veiligheid van de medewerkers tegen de risico’s van mechanische trillingen op het werk, 2005). Dit is de taak van de werkgever met gericht ergonomisch advies.
<ul>
	<li>Voer een specifieke risicoanalyse uit.</li>
	<li>Bekijk dan, samen met de medewerker en de preventieadviseur, hoe de werkomstandigheden kunnen
	verbeterd worden.
	<ul>
		<li>Optimalisatie van de technische aspecten: correcte inrichting van de werkplek, onderhoud en kwaliteit van gereedschap, handgrepen, demping van trillingen, ...</li>
		<li>Aanpassing van werkorganisatie: afwisseling van taken mogelijk maken, inlassen van meerdere korte pauzes, ...</li>
			<li>Correcte werkmethode: instructies opstellen, training en opleiding aanbieden.</li>
		</ul>
	</li>
</ul>