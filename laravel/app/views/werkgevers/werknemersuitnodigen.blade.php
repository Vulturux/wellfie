@extends('layoutsWerknemer.master')


@section('content')


    <div class="box-header clearfix">
        <h3 class="main-title">Nodig uw medewerkers uit</h3>
    </div>

    <div class="row werknemers-uitnodigen">

        <div class="large-12 columns">


            {{--<ul class="tabs tabs-top">--}}
                {{--@for($i=1; $i <= $scan_id; $i++)--}}
                    {{--<li class="tab-title @if($i === $current_scan_id) active @endif" role="presentation"><a--}}
                                {{--href="{{url('werkgevers/werknemersuitnodigen?scan='.$i)}}" role="tab" tabindex="0"--}}
                                {{--aria-selected="true" aria-controls="panel2-1">Scan {{$i}}</a></li>--}}
                {{--@endfor--}}
            {{--</ul>--}}

            <div class="uitnodigen">

                <div class="content @if($scan_id === $current_scan_id) active @endif" id="panel1">

                    <div class="small-12">

                        {{ Form::open(array('url' => 'werkgevers/postinvite', 'class'=>'postinvite', 'id' => 'js-invitemail')) }}

                        <h4>Nodig nieuwe medewerkers uit tot Wellfie (Scan {{$current_scan_id}})</h4>
                        <div class="small">Ga naar <strong>Reeds uitgenodigde werknemers</strong> om gekende werknemers een nieuwe invitatielink te sturen.
                        </div>
                        <br>
                        <div id="errors" class="hide small"></div>
                        <div id="email-fields">
                            <div class="row email">
                                <div class="small-2 columns">
                                    <label class="inline">To</label>
                                </div>
                                <div class="small-10 columns">
                                    <input type="email" placeholder="e-mail" name="email[]" class="required"><a href="#"
                                            class="alert remove-contact js-delete-contact"></a>
                                </div>
                            </div>
                        </div>
                        <div class=" small-offset-2 button-container text-right">
                            <ul class="button-group">
                                @if($scan_id === $current_scan_id && $current_scan_id !== 1)
                                    <li><a href="#" class="button flat js-previous-scan"><i class="fi-adult"></i> uit
                                            vorige scan</a></li>
                                @endif
                                <li><a href="#" class="button flat" data-reveal-id="upload-modal"><i
                                                class="fi-page"></i> excel importeren</a></li>
                                <li><a href="#" class="js-add-mailfield button flat"><i class="fi-plus"></i> medewerker
                                        toevoegen</a></li>
                            </ul>
                        </div>

                        <div class="row onderwerp">
                            <div class="small-2 columns">
                                <label class="inline">Onderwerp</label>
                            </div>
                            <div class="small-10 columns">
                                <input type="text" placeholder="" value="Wellfie: uitnodiging" name="onderwerp"
                                        class="required">
                            </div>
                        </div>

                        <div class="row bericht">
                            <div class="small-2 columns">
                                <label class="inline">Bericht</label>
                            </div>
                            <div class="small-10 columns">
                                <label>
                                    <textarea name="bericht" class="required"><?php echo $defaultMailMessage ?></textarea>
                                </label>
                            </div>
                        </div>
                        <div class="button-container text-center">
                            <ul class="button-group">
                                <li>
                                    {{ HTML::link('/werkgevers/uitgenodigdewerknemers?scan='.$scan_id, 'Reeds uitgenodigde werknemers', array('class' => 'button secondary'))  }}

                                </li>
                                <li>{{ Form::submit('versturen en ga naar volgende stap', array('class' => 'button main js-submitInvite')) }}</li>
                            </ul>
                        </div>

                        {{ Form::close() }}

                    </div>  <!-- end small-9 -->

                </div>   <!-- end PANEL #1 -->

            </div> <!-- end large-12columns -->

        </div>   <!-- end row -->

        <div id="upload-modal" class="reveal-modal" data-reveal>
            <h1>Importeer je medewerkers met één file.</h1>
            <p class="info"><i class="fi-info"></i><strong> Wat moet de excel of cvs bevatten?</strong><br>
                Wellfie heeft enkel de e-mail adressen nodig van uw medewerkers. Wil alle mederwerkers importeren dan
                volstaat
                een <strong>.xls of .cvs</strong> formaat met in de <strong>eerste kolom alle e-mail adressen</strong>.
                De eerste rij zien we als de header en wordt niet geïmporteerd.<br>

                <img src="/images/common/screenshot-excel-contact-upload.png"><br>
            </p>
            <hr>
            {{ Form::open(array('url' => 'werkgevers/postWerknemersuitnodigen', 'files' => true, 'class'=>'excel', 'id' => 'js-excelupload')) }}
            {{ Form::file('excelfile', ['class' => 'excel']) }}
            {{ Form::submit('upload & import contacts', array('class' => 'button main')) }}</li>
            {{ Form::close() }}

        </div>

        @stop

        @section('script')
            <script>
              invite.init({
                scanId: {{$current_scan_id}}
              });
            </script>
@endsection
