<strong>{{ $scoresAll['64']['avarageAnswerFloat'] }} op 10:</strong>
Dat is de mate waarin uw medewerkers <strong>vinden dat ze werken in een bedrijf met toekomst.</strong> <br>
<i>Tips:</i><br>

<ul>
    <li>Creëer een duidelijke toekomststrategie. Weet u waar binnen 1-5 jaar wilt staan met uw bedrijf? En hoe u dat gaat aanpakken?</li>
    <li>Communiceer deze strategie. Deel de toekomstplannen met het personeel, zodat elkeen er kan aan meewerken. Eén maal per jaar is al een goed begin, maar best herhaalt u dit geregeld.</li>
    <li>Onderschat niet welke goede ideeën medewerkers kunnen hebben omtrent de toekomst van het bedrijf. Voorzie daarom dialoogmomenten of maak gebruik van de informele contacten om hierover van gedachten te wisselen.</li>
</ul>