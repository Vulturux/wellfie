<strong>{{ $scoresAll['67']['avarageAnswerFloat'] }} op 10:</strong>
Dat is de mate waarin uw medewerkers vinden dat ze in het bedrijf <strong>gestimuleerd worden om het beste van zichzelf te geven</strong>. <br>

<i>Tips:</i><br>

<ul>
	<li>Ken de talenten en verwachtingen van de medewerkers.</li>
    <li>Laat de werkverdeling hierop aansluiten.</li>
    <li>Geef mensen passende uitdagingen.</li>
</ul>