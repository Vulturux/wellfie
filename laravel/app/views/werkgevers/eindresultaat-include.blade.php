
<div class="row detail-results">
    <div class="large-8 large-offset-2 end columns">
        <h4 class="title-block-werkgevers">Gezondheid en functionele capaciteiten</h4>
   
        <h5 class="subtitle">Gezondheid</h5>

            <!--ID 107 - VR 5 -->
            <!--ID 108 - VR 6 -->
            @if($scoresBeleidFeedback['107']['answer'] == 'Ja' || $scoresBeleidFeedback['108']['answer']=='nee')
                <div class="feedback slecht">@include('werkgevers/snippet-5-feedback')</div>
            @endif

            <!--ID 109 - VR 7 -->
            <!--spontane consultatie: ja of neen -->
            @if($scoresBeleidFeedback['109']['answer'] == 'Ja' || $scoresBeleidFeedback['109']['answer']=='nee')
                <div class="feedback slecht">@include('werkgevers/snippet-7-feedback')</div>
            @endif

            <!--andere onderzoeken: ja of neen -->
            @if($scoresBeleidFeedback['110']['answer'] == 'Ja')
                <div class="feedback slecht">@include('werkgevers/snippet-8-7ja-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['110']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-8-7nee-feedback')</div>
            @endif

            <!--ID 112 - vr 10
            //progressieve werkhervatting: als neen -->
            @if($scoresBeleidFeedback['112']['answer'] == 'Niet gekend')
                <div class="feedback slecht">@include('werkgevers/snippet-10-feedback')</div>
            @endif

            <!--ID 113 - vr 11 -->
            <!--voorwerkhervatting als neen -->
            @if($scoresBeleidFeedback['113']['answer'] == 'Niet gekend')
                <div class="feedback slecht">@include('werkgevers/snippet-11-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['113']['answer'] == 'Gekend')
                <div class="feedback goed">@include('werkgevers/snippet-11-gekend-feedback')</div>
            @endif

            <!--ID 114 - vr 12 -->
            <!--re-integratie: als neen -->
            @if($scoresBeleidFeedback['114']['answer'] == 'Niet gekend')
                <div class="feedback slecht">@include('werkgevers/snippet-12-feedback')</div>
            @endif

        <h5 class="subtitle">Werkvermogen</h5>

            <!--ID 115 - vr 13 -->
            <!--re-integratie: als neen -->
            @if($scoresBeleidFeedback['115']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-13-nee-feedback')</div>
            @endif

            <!--ID 105	- vr 3 -->
            <!--indien onregelmatige dienst en/of wisselende ploegdienst aangeduid -->
            @if($scoresBeleidFeedback['105']['answer'] == 'onregelmatige dienst' || $scoresBeleidFeedback['105']['answer']== 'wisselende ploegdienst' )
                <div class="feedback slecht">@include('werkgevers/snippet-13-onreg-feedback.')</div>
            @endif

            <!--ID 121 - VR 19 -->
            @if($scoresBeleidFeedback['107']['answer'] == 'nooit' || $scoresBeleidFeedback['107']['answer']=='meestal niet')
                <div class="feedback slecht">@include('werkgevers/snippet-19-feedback')</div>
            @endif

        <h5 class="subtitle">Welzijnsbeleid</h5>

            <!--122	(20) Ongewenst grensoverschrijdend gedrag (OGGW) -->
            @if($scoresBeleidFeedback['122']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-20-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['122']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-20-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['122']['answer'] == 'beleid aanwezig')
                <div class="feedback slecht">@include('werkgevers/snippet-20-beleid-feedback')</div>
            @endif

            <!-- 123	(21) Feiten door derden	-->
            @if($scoresBeleidFeedback['123']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-21-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['123']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-21-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['123']['answer'] == 'beleid aanwezig')
                <div class="feedback slecht">@include('werkgevers/snippet-21-beleid-feedback')</div>
            @endif

            <!-- 124	(22) Schokkende gebeurtenissen	-->
            @if($scoresBeleidFeedback['124']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-22-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['124']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-22-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['124']['answer'] == 'beleid aanwezig')
                <div class="feedback slecht">@include('werkgevers/snippet-22-beleid-feedback')</div>
            @endif

            <!-- 125	(23) Gezondheid en leefstijl werknemers	-->
            @if($scoresBeleidFeedback['125']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-23-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['125']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-23-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['125']['answer'] == 'beleid aanwezig' || $scoresBeleidFeedback['125']['answer'] == 'beleid aanwezig & acties ondernomen')
                <div class="feedback slecht">@include('werkgevers/snippet-23-beleid-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['125']['answer'] == 'acties ondernomen')
                <div class="feedback slecht">@include('werkgevers/snippet-23-actieszonderbeleid-feedback')</div>
            @endif

            <!-- 126	(24) Alcohol, medicatie en drugs	-->
            @if($scoresBeleidFeedback['126']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-24-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['126']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-24-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['126']['answer'] == 'beleid aanwezig')
                <div class="feedback slecht">@include('werkgevers/snippet-24-beleid-feedback')</div>
            @endif

            <!-- 127	(25) Roken	-->
            @if($scoresBeleidFeedback['127']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-25-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['127']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-25-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['127']['answer'] == 'beleid aanwezig' || $scoresBeleidFeedback['127']['answer'] == 'beleid aanwezig & acties ondernomen')
                <div class="feedback slecht">@include('werkgevers/snippet-25-beleid-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['127']['answer'] == 'acties ondernomen')
                <div class="feedback slecht">@include('werkgevers/snippet-25-actieszonderbeleid-feedback')</div>
            @endif

            <!-- 128	(26) Ziekteverzuim	-->
            @if($scoresBeleidFeedback['128']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-26-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['128']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-26-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['128']['answer'] == 'beleid aanwezig')
                <div class="feedback slecht">@include('werkgevers/snippet-26-beleid-feedback')</div>
            @endif

            <!-- 129	(27) Voldoende beweging	 -->
            @if($scoresBeleidFeedback['129']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-27-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['129']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-27-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['129']['answer'] == 'beleid aanwezig' || $scoresBeleidFeedback['129']['answer'] == 'beleid aanwezig & acties ondernomen')
                <div class="feedback slecht">@include('werkgevers/snippet-27-beleid-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['129']['answer'] == 'acties ondernomen')
                <div class="feedback slecht">@include('werkgevers/snippet-27-actieszonderbeleid-feedback')</div>
            @endif

            <!-- 130	(28) Gezonde voeding	-->
            @if($scoresBeleidFeedback['130']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-28-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['130']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-28-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['130']['answer'] == 'beleid aanwezig' || $scoresBeleidFeedback['130']['answer'] == 'beleid aanwezig & acties ondernomen')
                <div class="feedback slecht">@include('werkgevers/snippet-28-beleid-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['130']['answer'] == 'acties ondernomen')
                <div class="feedback slecht">@include('werkgevers/snippet-28-actieszonderbeleid-feedback')</div>
            @endif

            <!-- 131	(29) Stresspreventie	-->
            @if($scoresBeleidFeedback['131']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-29-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['131']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-29-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['131']['answer'] == 'beleid aanwezig')
                <div class="feedback slecht">@include('werkgevers/snippet-29-beleid-feedback')</div>
            @endif

            <!-- 132	(30) Vertrouwenspersoon	-->
            @if($scoresBeleidFeedback['132']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-30-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['132']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-30-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['132']['answer'] == 'beleid aanwezig')
                <div class="feedback slecht">@include('werkgevers/snippet-30-beleid-feedback')</div>
            @endif


            <!-- 133	(31) Preventieadviseur psychosociale aspecten	-->
            @if($scoresBeleidFeedback['133']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-31-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['133']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-31-belang-feedback')</div>
            @endif


            <!-- 134	(32) Jaaractieplan (JAP) of Globaal Preventie Plan -->
            @if($scoresBeleidFeedback['134']['answer'] == 'niet belangrijk')
                <div class="feedback slecht">@include('werkgevers/snippet-32-nietbelang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['134']['answer'] == 'belangrijk maar geen beleid/acties')
                <div class="feedback slecht">@include('werkgevers/snippet-32-belang-feedback')</div>
            @endif
            @if($scoresBeleidFeedback['134']['answer'] == 'beleid aanwezig')
                <div class="feedback slecht">@include('werkgevers/snippet-32-beleid-feedback')</div>
            @endif


            <!-- 135	(33) Arbeidshygiëne  -->
             <!-- 136	(34) Veiligheid -->
             @if($scoresBeleidFeedback['135']['answer'] == 'nee' || $scoresBeleidFeedback['136']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-33-nee-feedback')</div>
            @endif
            
            <!-- 137	(35) Ergonomie -->
            @if($scoresBeleidFeedback['137']['answer'] == 'nee')
             <div class="feedback slecht">@include('werkgevers/snippet-35-nee-feedback')</div>
            @endif
            
            <!-- 139	(37) -->
            @if($scoresBeleidFeedback['137']['answer'] == 'nee')
            <div class="feedback slecht">@include('werkgevers/snippet-36-nee-feedback')</div>
            @endif


</div>
</div>




<div class="row detail-results">
<div class="large-8 large-offset-2 end columns">
    <h4 class="title-block-werkgevers">Competenties</h4>

    <h5 class="subtitle">Rekrutering en selectie</h5>
            <!-- 139	(37) Stemt u uw rekruteringskanalen af op de doelgroep die u wil bereiken?	-->
            @if($scoresBeleidFeedback['139']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-37-feedback')</div>
            @endif

            <!-- 140	(38) Maakt u vacatures altijd eerst intern bekend?	 -->
            @if($scoresBeleidFeedback['140']['answer'] == 'nee')
                 <div class="feedback slecht">@include('werkgevers/snippet-38-feedback')</div>
            @endif

            <!-- 141	(39) Rekruteert u via sociale media (Linked-In, Facebook)?	-->
            @if($scoresBeleidFeedback['141']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-39-feedback')</div>
            @endif

            <!-- 142	(40) Kent u de financiële tegemoetkomingen voor de werkgever bij de aanwerving van een oudere werknemer?	-->
            @if($scoresBeleidFeedback['142']['answer'] == 'nee')
            <div class="feedback slecht">@include('werkgevers/snippet-40-feedback')</div>
            @endif


            <!-- 143	(41) Kent u de mogelijkheden om (oudere) werkzoekenden via een stage te laten kennis maken met uw onderneming?	-->
            @if($scoresBeleidFeedback['143']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-41-feedback')</div>
            @endif

            <!-- 144	(42) Kent u de technieken om in een interview competenties of vaardigheden correct in te schatten?	-->
            @if($scoresBeleidFeedback['144']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-42-feedback')</div>
            @endif

    <h5 class="subtitle">Inwerktraject</h5>
            <!-- 145	(43) Voorziet u een p/meter en een inwerktraject voor nieuwkomers?	-->
            @if($scoresBeleidFeedback['145']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-43-feedback')</div>
            @endif

    <h5 class="subtitle">Opleiding</h5>
            <!-- 146	(44) Heeft u een uitgewerkt opleidingsplan met bijbehorend budget voor alle functies?	 -->
            @if($scoresBeleidFeedback['146']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-44-feedback')</div>
            @endif

            <!-- 148	(46) Heeft iedere medewerker in de afgelopen 3 jaar minstens één opleiding gevolgd?	-->
            @if($scoresBeleidFeedback['148']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-46-feedback')</div>
            @endif

    <h5 class="subtitle">Feedbackgesprekken</h5>

            <!-- 149	(47) Houden de leidinggevenden feedbackgesprekken met hun medewerkers waarin ze de sterktes en zwaktes van de medewerker bespreken?	 -->
            @if($scoresBeleidFeedback['149']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-47-feedback')</div>
            @endif

    <h5 class="subtitle">Competenties</h5>

            <!-- 150	(48) Worden de competenties van iedere medewerker ten volle benut? -->
            @if($scoresBeleidFeedback['150']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-48-feedback')</div>
            @endif

    <h5 class="subtitle">Loopbaanevolutie</h5>

            <!-- 151	(49) Zijn er doorstroom- en heroriëntatiemogelijkheden in de onderneming?	-->
            @if($scoresBeleidFeedback['151']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-49-feedback')</div>
            @endif

            <!-- 152	(50) Denkt de onderneming mee indien een heroriëntering naar een andere werkgever nodig is?	 -->
            @if($scoresBeleidFeedback['152']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-50-feedback')</div>
            @endif

            <!-- 153	(51) Weten de medewerkers goed wat ze nog willen of kunnen met hun loopbaan? -->
            @if($scoresBeleidFeedback['153']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-51-feedback')</div>
            @endif

    <h5 class="subtitle">Kennismanagement</h5>

            <!--154	(52) Stimuleert de onderneming het delen van kennis?	 -->
            @if($scoresBeleidFeedback['139']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-52-feedback')</div>
            @endif

    <h5 class="subtitle">Inzet</h5>

        <!--155	(53) Hebben de leidinggevenden oog voor de inzet van medewerkers?	-->
            @if($scoresBeleidFeedback['139']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-53-feedback')</div>
            @endif

    <h5 class="subtitle">Gesprekscultuur</h5>

            <!--156	(54) Is er een lerende en open gesprekscultuur?	-->
            @if($scoresBeleidFeedback['139']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-54-feedback')</div>
            @endif

    <h5 class="subtitle">Doelstellingen</h5>

            <!-- 157	(55) Staan er duidelijke doelstellingen per functie en per medewerker in de coachingsdocumenten? -->
            @if($scoresBeleidFeedback['139']['answer'] == 'nee')
                <div class="feedback slecht">@include('werkgevers/snippet-55-feedback')</div>
            @endif


</div>
</div>

<div class="row detail-results" style="border-bottom:0;">
     <div class="large-8 large-offset-2 end columns">
        <h4 class="title-block-werkgevers">Werk, werkgemeenschap en leiding</h4>

        <?php $waarden=0;
        ?>


        <!-- 169	(67) Zware lasten tillen, dragen, trekken of duwen?	 -->
        @if($scoresBeleidFeedback['169']['answer'] == 'een paar keer per week' )
            <div class="feedback slecht">@include('werkgevers/snippet-67-feedback')</div>
            <?php $waarden++; ?>
        @endif

        <!-- 170	(68) Repetitieve bewegingen met bovenlichaam, nek, polsen of handen uitvoeren?	 -->
        @if($scoresBeleidFeedback['170']['answer'] == 'eens per week' || $scoresBeleidFeedback['170']['answer'] == 'een paar keer per week' || $scoresBeleidFeedback['170']['answer'] == 'eens per week' )
            <div class="feedback slecht">@include('werkgevers/snippet-68-feedback')</div>
            <?php $waarden++; ?>
        @endif

        <!-- 171	(69) Langdurig in eenzelfde houding staan, zitten, knielen of bukken?	 -->
        @if($scoresBeleidFeedback['171']['answer'] == 'eens per week' || $scoresBeleidFeedback['171']['answer'] == 'een paar keer per week' || $scoresBeleidFeedback['171']['answer'] == 'eens per week' )
            <div class="feedback slecht">@include('werkgevers/snippet-69-feedback')</div>
            <?php $waarden++; ?>
        @endif

        <!-- 172	(70) Beeldschermwerk uitvoeren?		-->
        @if($scoresBeleidFeedback['172']['answer'] == 'eens per week' || $scoresBeleidFeedback['172']['answer'] == 'een paar keer per week' || $scoresBeleidFeedback['172']['answer'] == 'eens per week' )
            <div class="feedback slecht">@include('werkgevers/snippet-70-feedback')</div>
            <?php $waarden++; ?>
        @endif

        <!-- 173	(71) Werken met trillend gereedschap?	-->
        @if($scoresBeleidFeedback['173']['answer'] == 'eens per week' || $scoresBeleidFeedback['173']['answer'] == 'een paar keer per week' || $scoresBeleidFeedback['173']['answer'] == 'eens per week' )
            <div class="feedback slecht">@include('werkgevers/snippet-71-feedback')</div>
            <?php $waarden++; ?>
        @endif

        <!-- 174	(72) Werken bij te hoge of te lage temperatuur?	 -->
        @if($scoresBeleidFeedback['174']['answer'] == 'eens per week' || $scoresBeleidFeedback['174']['answer'] == 'een paar keer per week' || $scoresBeleidFeedback['174']['answer'] == 'eens per week' )
            <div class="feedback slecht"> @include('werkgevers/snippet-72-feedback')</div>
            <?php $waarden++; ?>
        @endif

        <!-- 175	(73) Werken in slecht verlichte ruimtes?	-->
        @if($scoresBeleidFeedback['175']['answer'] == 'eens per week' || $scoresBeleidFeedback['175']['answer'] == 'een paar keer per week' || $scoresBeleidFeedback['175']['answer'] == 'eens per week' )
            <div class="feedback slecht">@include('werkgevers/snippet-73-feedback')</div>
            <?php $waarden++; ?>
        @endif

        <!-- 176	(74) Werken in slecht geventileerde ruimtes?	-->
        @if($scoresBeleidFeedback['176']['answer'] == 'eens per week' || $scoresBeleidFeedback['176']['answer'] == 'een paar keer per week' || $scoresBeleidFeedback['176']['answer'] == 'eens per week' )
            <div class="feedback slecht">@include('werkgevers/snippet-74-feedback')</div>
            <?php $waarden++; ?>
        @endif


        <!-- 177	(75) Werken in lawaaierige omgeving?	-->
        @if($scoresBeleidFeedback['176']['answer'] == 'eens per week' || $scoresBeleidFeedback['176']['answer'] == 'een paar keer per week' || $scoresBeleidFeedback['176']['answer'] == 'eens per week' )
            <div class="feedback slecht">@include('werkgevers/snippet-75-feedback')</div>
            <?php $waarden++; ?>
        @endif

        <?php if ($waarden>0) {
            echo '<div class="feedback slecht">Uw interne of externe dienst voor preventie en bescherming op het werk kan u hierin adviseren en begeleiden.</div>';
            $waarden++;
        } ?>

        <!-- 178	(76) Hebben uw werkgevers voldoende mogelijkheid om opleidingen te volgen om risico’...	-->
        @if($scoresBeleidFeedback['178']['answer'] == 'ja')
             <div class="feedback slecht">@include('werkgevers/snippet-76-ja-feedback')</div>
        @endif
        @if($scoresBeleidFeedback['178']['answer'] == 'nee')
             <div class="feedback slecht">@include('werkgevers/snippet-76-nee-feedback')</div>
        @endif

        <!-- 179	(77) Rekening gehouden met ergonomische aspecten/criteria?	 -->
        <!-- 180	(78) De preventieadviseur actief betrokken?	 -->
        @if(($scoresBeleidFeedback['179']['answer'] == 'nooit' || $scoresBeleidFeedback['179']['answer'] == 'soms') && ($scoresBeleidFeedback['180']['answer'] == 'nooit') || $scoresBeleidFeedback['180']['answer'] == 'soms')
             <div class="feedback slecht">@include('werkgevers/snippet-77-feedback')</div>
        @endif


        <!-- 181	(79) Wordt de arbeidsgeneesheer om advies gevraagd?	-->
        <!-- 182	(80) Bezoekt de ergonoom de individuele werkplek? -->
         @if(($scoresBeleidFeedback['181']['answer'] == 'nooit' || $scoresBeleidFeedback['181']['answer'] == 'soms') && ($scoresBeleidFeedback['182']['answer'] == 'nooit') || $scoresBeleidFeedback['182']['answer'] == 'soms')
             <div class="feedback slecht">@include('werkgevers/snippet-79-feedback')</div>
        @endif


        <!-- 183	(81) Is er bereidheid bij uw werkgevers om na hun pensioen verder te werken?	GEEN FEEDBACK -->
        <!-- 184	(82) Is het een optie om oudere werkgevers deeltijds te laten werken?	Ja EN Nee-->
         @if($scoresBeleidFeedback['184']['answer'] == 'nee')
            <div class="feedback slecht">@include('werkgevers/snippet-82-feedback')</div>
         @endif

        <!-- 185	(83) Kunnen jullie oudere werkgevers meer vrije dagen gunnen?  Ja EN Nee	-->
         @if($scoresBeleidFeedback['185']['answer'] == 'nee')
             <div class="feedback slecht">@include('werkgevers/snippet-83-feedback')</div>
         @endif

        <!-- 186	(84) Sta je open voor telewerk?	 Ja EN Nee-->
         @if($scoresBeleidFeedback['186']['answer'] == 'nee')
            <div class="feedback slecht">@include('werkgevers/snippet-84-feedback')</div>
         @endif

        <!-- 187	(85) Ben je bereid om oudere werkgevers een extra premie te geven om hen te belonen...	Ja EN Nee-->
         @if($scoresBeleidFeedback['187']['answer'] == 'nee')
         <div class="feedback slecht">@include('werkgevers/snippet-85-feedback')</div>
         @endif

        <!-- 188	(86) Bent u bereid om oudere werkgevers die op tijdskrediet landingsbaan gaan een financiële bijpassing te geven?	-->
         @if($scoresBeleidFeedback['188']['answer'] == 'nee')
         <div class="feedback slecht">@include('werkgevers/snippet-86-feedback')</div>
         @endif

        <!-- 189	(87) Bent u bereid om werkgevers aan de slag te houden/aan te nemen die reeds op wettelijk (rust)persioen zijn?	-->
         @if($scoresBeleidFeedback['189']['answer'] == 'nee')
            <div class="feedback slecht">@include('werkgevers/snippet-87-feedback')</div>
         @endif

        <!-- 190	(88) Bent u bereid mensen vroeger dan de wettelijke pensioenleeftijd te laten uittreden?	-->
         @if($scoresBeleidFeedback['190']['answer'] == 'nee')
            <div class="feedback slecht">@include('werkgevers/snippet-88-feedback')</div>
         @endif




</div>
</div>