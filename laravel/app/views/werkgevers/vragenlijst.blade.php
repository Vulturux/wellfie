@extends('layoutsWerkgever.master')

@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">{{ $title }}</h3>
</div>

@if(count($errors) > 0)
    <div class="row">
        <div class="large-12 columns alert-box">
            <h3>{{ Lang::trans('validation.all_questions_required') }}</h3>
        </div>
    </div>
@endif

<div class="row">

    <div class="large-12 columns vragen-content-container">

        {{ Form::open(array('url' => 'werkgevers/postanswers', 'class'=>'vragenlijst', 'id' => 'js-vragenform')) }}

            {{-- Add hidden cat id --}}
            {{ Form::hidden('category', $cat) }}

            {{-- Add hidden submitted flag --}}
            @if ($submitted) {{ Form::hidden('submitted', true) }} @endif

            @foreach ($questions as $questionblock)
                <div class="question-block">
                <h4 class="{{ $questionblock['titletype'] }}">{{ $questionblock['title'] }}</h4>

                    @foreach ($questionblock as $questionrow)

                        @if(is_array($questionrow))
                            <div class="row vraag {{ $questionrow['question']['layout_type_class'] }}">

                                <div class="large-6 columns col-question">
                                    <label class="question inline">
                                        {{ $questionrow['question']['question'] }}
                                        @if($questionrow['question']['more_info'] !== ''
                                         && $questionrow['question']['more_info'] !== null)
                                               <i class="fi-info js-questions-show-info"></i>
                                        @endif
                                    </label>
                                    @if($questionrow['question']['more_info'] !== ''
                                        && $questionrow['question']['more_info'] !== null)
                                        <div class="info" style="display: none;"><i class="fi-info"></i> {{ $questionrow['question']['more_info'] }} </div>
                                    @endif
                                </div>

                                <div class="large-6 columns col-answers-container @if ($errors->has($questionrow['question']['question_id'])) alert-box @endif">

                                        <?php $count = 0 ?>
                                        @foreach ($questionrow['answer'] as $answers)

                                            <?php $count++ ?>
                                            <!-- OPEN FIELD -->
                                            @if($questionrow['question']['type'] == 2)

                                                <div class="small-6 columns end">
                                                    @if(strpos($questionrow['question']['layout_type_class'], "number") !== false)
                                                        <input type="number" placeholder="" name="{{ $questionrow['question']['question_id'] }}" value="@if (isset($answers['value'])){{$answers['value']}}@else{{Input::old($questionrow['question']['question_id'])}}@endif">
                                                    @else
                                                        <input type="text" placeholder="" name="{{ $questionrow['question']['question_id'] }}" value="@if (isset($answers['value'])){{$answers['value']}}@else{{Input::old($questionrow['question']['question_id'])}}@endif">
                                                    @endif
                                                </div>
                                            @endif

                                            @if($answers['id'] > 0)

                                                <!-- CHECKBOXES -->
                                                @if($questionrow['question']['type'] == 3)
                                                    <div class="large-12 columns checkboxes col-answer">
                                                        <input id="{{ $answers['id'] }}" name="{{ $questionrow['question']['question_id'] }}[]" type="checkbox" @if (is_array(Input::old($questionrow['question']['question_id']))) @if (in_array($answers['id'], Input::old($questionrow['question']['question_id']))) checked @endif @elseif (isset($answers['checked'])) checked @endif value="{{ $answers['id'] }}" >
                                                        <label for="{{ $answers['id'] }}" style="vertical-align: top;">{{ $answers['answer'] }}
                                                            @if($answers['answerinfo'] !== ''
                                                             && $answers['answerinfo'] !== null)
                                                                  <i class="fi-info js-answers-show-info"></i>
                                                            @endif
                                                        </label>
                                                        @if($answers['answerinfo'] !== ''
                                                         && $answers['answerinfo'] !== null)
                                                        <div class="answers-info" style="display: none; margin-left: 10px; width: 90%;"><i class="fi-info"></i> {{ $answers['answerinfo'] }} </div>
                                                        @endif
                                                    </div>
                                                @endif

                                                <!-- RADIO -->
                                                @if($questionrow['question']['type'] == 1)

                                                    @if($questionrow['question']['layout_type_class'] == "janee" || $questionrow['question']['layout_type_class'] == "janee zoja")
                                                        <div class="large-4 columns col-answer <?php if ($count==2) { echo "end"; } ?>">
                                                            <input type="radio" name="{{ $questionrow['question']['question_id'] }}" value="{{ $answers['id'] }}" id="{{ $answers['id'] }}" {{ Input::old($questionrow['question']['question_id']) == $answers['id'] || isset($answers['checked']) ? 'checked='.'"'.'checked'.'"' : '' }}><label for="{{ $answers['id'] }}"  class="radio-answer" >{{ $answers['answer'] }}</label>
                                                            <span class="points">{{ $answers['points'] }}</span>

                                                        </div>
                                                    @elseif($questionrow['question']['layout_type_class'] == "janee notrequired")
                                                        <div class="large-4 columns col-answer <?php if ($count==2) { echo "end"; } ?>">
                                                            <input type="radio" name="{{ $questionrow['question']['question_id'] }}" value="{{ $answers['id'] }}" id="{{ $answers['id'] }}" {{ Input::old($questionrow['question']['question_id']) == $answers['id'] || isset($answers['checked']) ? 'checked='.'"'.'checked'.'"' : '' }}><label for="{{ $answers['id'] }}"  class="radio-answer" >{{ $answers['answer'] }}</label>
                                                            <span class="points">{{ $answers['points'] }}</span>

                                                        </div>
                                                    @elseif ($questionrow['question']['layout_type_class'] == "janeeweetniet")
                                                        <div class="large-4 columns col-answer  <?php if ($count==3) { echo "end"; } ?>"">
                                                            <input type="radio" name="{{ $questionrow['question']['question_id'] }}" value="{{ $answers['id'] }}" id="{{ $answers['id'] }}" {{ Input::old($questionrow['question']['question_id']) == $answers['id'] || isset($answers['checked']) ? 'checked='.'"'.'checked'.'"' : '' }}><label for="{{ $answers['id'] }}"  class="radio-answer">{{ $answers['answer'] }}</label>
                                                            <span class="points">{{ $answers['points'] }}</span>
                                                        </div>
                                                    @elseif ($questionrow['question']['layout_type_class'] == "ten")
                                                              <div class="large-1 columns col-answer  <?php if ($count==11) { echo "end"; } ?>"">
                                                                <input type="radio" name="{{ $questionrow['question']['question_id'] }}" value="{{ $answers['id'] }}" id="{{ $answers['id'] }}" {{ Input::old($questionrow['question']['question_id']) == $answers['id'] || isset($answers['checked']) ? 'checked='.'"'.'checked'.'"' : '' }}><label for="{{ $answers['id'] }}"  class="radio-answer">{{ $answers['answer'] }}</label>
                                                                <span class="points">{{ $answers['points'] }}</span>
                                                            </div>
                                                    @else
                                                        <div class="large-12 columns col-answer">
                                                            <input type="radio" name="{{ $questionrow['question']['question_id'] }}" value="{{ $answers['id'] }}" id="{{ $answers['id'] }}" {{ Input::old($questionrow['question']['question_id']) == $answers['id'] || isset($answers['checked']) ? 'checked="checked"' : '' }}><label for="{{ $answers['id'] }}"  class="radio-answer">{{ $answers['answer'] }}</label>
                                                            <span class="points">{{ $answers['points'] }}</span>

                                                        </div>
                                                    @endif
                                                @endif

                                            @endif
                                        @endforeach

                                </div>   <!-- end col-answers-container -->

                            </div>
                        @endif

                    @endforeach
                </div>
            @endforeach

            <div class="button-container text-center">
                <ul class="button-group">
                 <!--    <li><a href="#" class="button lessimportant"><i class="fi-stop"></i> bewaren en stoppen</a></li> -->
                    @if ($submitted)
                        <li>{{ Form::button('bewaren en volgende', array('class' => 'button main disabled')) }}</li>
                    @else
                        <li>{{ Form::submit('bewaren en volgende', array('class' => 'button main')) }}</li>
                    @endif
                </ul>
            </div>

        {{ Form::close() }}
    </div> <!-- end large 12 columns -->

</div> <!-- end row -->



@stop
