U rapporteert dat in uw onderneming medewerkers geregeld moeten werken in slecht/minder goed geventileerde ruimtes. 
Dit heeft een nefaste invloed op de concentratie en vermoeidheid. Pak dit probleem aan.
<ul>
<li>Voer een specifieke risicoanalyse uit om mogelijke risico’s te inventariseren.</li>
<li>Bekijk dan, samen met de medewerker en preventieadviseur, hoe de werkomstandigheden kunnen
verbeterd worden.
<ul>
<li>Zorg voor een goede ventilatie van de werkplaats door deuren, ramen en/of poorten
regelmatig open te zetten.</li>
<li>Plaats een goed afgestelde airconditioning.</li>
</ul></li>
</ul>