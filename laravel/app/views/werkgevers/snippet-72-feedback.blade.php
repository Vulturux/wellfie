U rapporteert dat in uw onderneming medewerkers geregeld moeten werken bij te hoge of te lage temperatuur. Extreme temperaturen maken het werk zwaarder en hebben een nefaste invloed op het welzijn, de gezondheid en de productiviteit van de medewerkers.<br>
<br>
Werkomstandigheden die een risico inhouden moeten worden aangepakt (K.B. betreffende de thermische omgevingsfactoren, 2012). Dit is in de eerste plaats de taak van de werkgever.

<ul>
<li>Voer een specifieke risicoanalyse uit.</li>
<li>Neem op basis daarvan een aantal preventiemaatregelen om overschrijding van de wettelijke
actiewaarden voor koude (minimale luchttemperaturen in °C) en warmte (maximum WBGT-waarde)
te voorkomen.</li>
<li>Wanneer overschrijding niet uit te sluiten valt, moet er een programma van technische en
organisatorische maatregelen klaarliggen dat onmiddellijk kan toegepast worden wanneer de temperatuur effectief lager/hoger is dan de actiewaarde. Dit programma wordt voor advies voorgelegd aan de preventieadviseur en het comité (of syndicale afvaardiging of aan medewerkers via de rechtstreekse participatie). Voor medewerkers die behoren tot bijzonder gevoelige risicogroepen wordt het programma, na advies van de arbeidsgeneesheer, aangepast.</li>
</ul>
