<strong>{{ $scoresAll['66']['avarageAnswerFloat'] }} op 10:</strong> Dat is de mate waarin uw medewerkers vinden dat ze in het bedrijf <strong>meetellen als persoon</strong>. <br>

<i>Tips:</i><br>

<ul>
	<li>Geef niet alleen aandacht aan wat uw medewerkers doen, maar ook aan wat ze denken en voelen. </li>
    <li>Iedere medewerker is anders. Kijk waar u die verschillen positief kunt benutten. </li>
    <li>Versterk het zelfvertrouwen door waardering, bijvoorbeeld door ook te benoemen wat men goed doet, door iemand een verantwoordelijkheid te geven...
    </li>
</ul>