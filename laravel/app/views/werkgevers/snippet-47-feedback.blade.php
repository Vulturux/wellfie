1. De kernkwadrant van Daniel Ofman is een handig model om sterktes en uitdagingen in
kaart te brengen en zo makkelijk bespreekbaar te maken tijdens een HR gesprek met medewerkers.<br>
2. Zorg voor een evenwicht tussen feedback over leerpunten en feedback over sterke punten. 
Een medewerker zal meer vertrouwen geven aan een leidinggevende die ook zijn sterke kanten ziet en benoemt.<br>
3. Geef uw feedback ook in kader van de eigen leerwensen van de medewerker.<br>