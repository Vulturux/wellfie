<strong>{{ $scoresAll['68']['avarageAnswerFloat'] }} op 10:</strong>
Dat is de mate waarin uw <strong>medewerkers vinden dat hun opdracht hen de kans biedt hun talenten te benutten en zichzelf verder te ontwikkelen</strong>.

<i>Tips</i>:<br>

<ul>
<li>Houd regelmatig loopbaangesprekken met uw medewerkers.</li>
    <li>Zet mensen vooral in op hun sterkte kanten.</li>
    <li>Maak opleidingsbudget vrij en zie dit als een investering i.p.v. een kost.</li>
</ul>