@extends('layoutsWerkgever.master')


@section('content')
<script src="//code.highcharts.com/highcharts.js"></script>
<script src="//code.highcharts.com/modules/exporting.js"></script>

<div class="box-header clearfix">
    <h3 class="main-title">Totaalresultaten</h3>
</div>

<dl class="tabs totaalresultaat" data-tab>
    <dd class="active"><a href="#panel1">Vergelijking  WG-WN</a></dd>
    <dd><a href="#panel2">Het werkvermogen van uw medewerkers</a></dd>
    <dd><a href="#panel3">Uw beleidsfeedback</a></dd>

</dl>

<div class="tabs-content eindresulataat">

<div class="content active" id="panel1">

<div class="row detail-results">

    <div class="large-12  end columns">

    <div class="panel enquete">
        Wellfie is momenteel in beta. U kan ons helpen door  <a href="https://wellfie.typeform.com/to/OawNEY">kort en
            anoniem volgende enquete </a> in te vullen.
    </div>

    <div class="intro"><i>Hieronder ziet u in welke mate uw inschatting van het welzijn in uw onderneming overeenkomt met deze van uw medewerkers</i></i></div><br><br>

    <h5 class="verdiep">Verdieping 1</h5>
        <h4>Gezondheid en functionele capaciteiten</h4>

        <!-- basis values pressing a) Leefgewoonten 118 leefgewoonten / 44  en dan stuk waarden -->
        <?php
            $scorevr=array(0,0,0,0,0,0,0,0);
            foreach ($scoreWGVraag14 as $key => $Wg14Antwood) {
                if ($Wg14Antwood['answer'] == 'Hun verantwoordelijke') { $scorevr[0] = 100/count($scoreWGVraag14); }
                if ($Wg14Antwood['answer'] == 'HR-medewerker') { $scorevr[1] = 100/count($scoreWGVraag14); }
                if ($Wg14Antwood['answer'] == 'Arbeidsgeneesheer') { $scorevr[2] = 100/count($scoreWGVraag14); }
                if ($Wg14Antwood['answer'] == 'Interne preventieadviseur') { $scorevr[3] = 100/count($scoreWGVraag14); }
                if ($Wg14Antwood['answer'] == 'Vertrouwenspersoon') { $scorevr[4] = 100/count($scoreWGVraag14); }
                if ($Wg14Antwood['answer'] == 'De werkgever') { $scorevr[5] = 100/count($scoreWGVraag14); }
                if ($Wg14Antwood['answer'] == 'Andere') { $scorevr[6] = 100/count($scoreWGVraag14); }
                if ($Wg14Antwood['answer'] == 'niemand') { $scorevr[7] = 100/count($scoreWGVraag14); }
            }
        $verbondenheid = ($scoresAll['72']['avarageAnswerInteger'] + $scoresAll['72']['avarageAnswerInteger'])/2;
        $ontslagintentie= ($scoresAll['78']['avarageAnswerInteger']+$scoresAll['79']['avarageAnswerInteger']+$scoresAll['80']['avarageAnswerInteger']+$scoresAll['81']['avarageAnswerInteger'])/4;
        $motivatie = ($scoresAll['82']['avarageAnswerInteger']+$scoresAll['83']['avarageAnswerInteger']+$scoresAll['84']['avarageAnswerInteger']+$scoresAll['85']['avarageAnswerInteger'])/4;

        ?>

        <script>
            $(document).ready(function() {
                $(function () {
                    $('#gra-bespreken-werkvermogen').highcharts({

                        chart: {
                            type: 'column'
                        },

                        title: {
                            text: 'Waar kan medewerker terecht bij bespreken van werkvermogen?'
                        },

                        xAxis: {
                            categories: ['uw verantwoordelijke', 'HR medewerker', 'Arbeidsgen.', 'Int. preventieadv.', 'Vertrouwenspers', 'Werkgever', 'Andere', 'Niemand', 'Weet het niet']
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            allowDecimals: false,
                            min: 0,
                            title: {
                                text: '%'
                            }
                        },
                        tooltip:false,
                        plotOptions: {
                            column: {
                                stacking: 'normal'
                            }
                        },

                        series: [{
                                name: 'Werkgever',
                                data: [{{ $scorevr[0] }}, {{ $scorevr[1] }}, {{ $scorevr[2] }}, {{ $scorevr[3] }}, {{ $scorevr[4] }}, {{ $scorevr[5] }}, {{ $scorevr[6] }}],
                                stack: 'werkgever'
                            }, {
                                name: 'Gemiddelde medewerker',
                                data: [{{ $scoresAll['28']['answers']['71']['countpercentage'] }}, {{ $scoresAll['28']['answers']['72']['countpercentage'] }}, {{ $scoresAll['28']['answers']['73']['countpercentage'] }},
                                {{ $scoresAll['28']['answers']['74']['countpercentage'] }}, {{ $scoresAll['28']['answers']['75']['countpercentage'] }},
                                {{ $scoresAll['28']['answers']['796']['countpercentage'] }},
                                    {{ $scoresAll['28']['answers']['76']['countpercentage'] }},
                                {{ $scoresAll['28']['answers']['77']['countpercentage'] }}, {{ $scoresAll['28']['answers']['78']['countpercentage'] }}],
                                stack: 'Gemiddelde percentage medewerker'
                        }]
                    });

                    var OneensLabels = ["Helemaal oneens", "Oneens", 'noch eens, noch oneens', 'Eens', 'Helemaal eens'];

                    $('#gra-leefgewoonten').highcharts({
                        chart: {
                            type: 'scatter'
                        },
                        title: {
                            text: 'Leefgewoonten, gezondheid, werkvermogen'
                        },
                        xAxis: {
                            categories: ['Belang gezondheid', 'Belang leefgewoonten', 'Belang werkvermogen vr oudere medewerkers'],
                            title: {
                                text: null
                            }
                        },
                        yAxis: {
                            min: 0,
                            max: 4,
                            title: {
                                text: null
                            },
                            labels: {
                                overflow: 'justify',
                                formatter: function() {
                                    return OneensLabels[this.value];
                                }
                            }
                        },
                        tooltip: false ,
                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },

                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'Score Werkgever',
                            data: [({{ $scoresWG['117']['order'] }}-1),  ({{ $scoresWG['118']['order'] }}-1), ( {{ $scoresWG['119']['order'] }}-1)],
                            marker: { radius: 6 }
                          }, {
                            name: 'Gemiddelde score medewerker',
                            data: [({{ $scoresAll['43']['avarageAnswerInteger'] }}-1),  ({{ $scoresAll['44']['avarageAnswerInteger'] }}-1), ({{ $scoresAll['45']['avarageAnswerInteger'] }}-1)]
                        }]
                    });
                });
            });
        </script>

        <div id="gra-leefgewoonten" style="min-width: 300px; height: 250px; margin: 0 auto"></div>
<br>


        <table class="detail-results">
            <tr>
                <th valign="top"></th>
                <th>Werkgever</th>
                <th>medewerker</th>
            </tr>
            <tr>
                <th valign="top">Belang gezondheid</th>
                <td valign="top" class="werkgever-detail">{{ $scoresWG['117']['answer'] }} </td>
                <td><table class="medewerker-detail">
                      <tr>
                        <td>helemaal oneens</td><td>{{ $scoresAll['43']['answers']['161']['countpercentage'] }}%</td>
                      </tr>
                      <tr>
                        <td>oneens</td><td>{{ $scoresAll['43']['answers']['162']['countpercentage'] }}% </td>
                      </tr>
                        <tr>
                        <td>noch eens, noch oneens</td><td>{{ $scoresAll['43']['answers']['163']['countpercentage'] }}%</td>
                      </tr>
                      <tr>
                        <td>eens</td><td>{{ $scoresAll['43']['answers']['164']['countpercentage'] }}% </td>
                      </tr>
                      <tr>
                        <td>helemaal eens</td><td>{{ $scoresAll['43']['answers']['165']['countpercentage'] }}% </td>
                      </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">Belang leefgewoonten</th>
                <td valign="top" class="werkgever-detail">{{ $scoresWG['118']['answer'] }} </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td><td>{{ $scoresAll['44']['answers']['166']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>oneens</td><td>{{ $scoresAll['44']['answers']['167']['countpercentage'] }}% </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td><td>{{ $scoresAll['44']['answers']['168']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>eens</td><td>{{ $scoresAll['44']['answers']['169']['countpercentage'] }}% </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td><td>{{ $scoresAll['44']['answers']['170']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">Belang gezondheid</th>
                <td valign="top" class="werkgever-detail">{{ $scoresWG['119']['answer'] }} </td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>helemaal oneens</td><td>{{ $scoresAll['45']['answers']['171']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>oneens</td><td>{{ $scoresAll['45']['answers']['172']['countpercentage'] }}% </td>
                        </tr>
                        <tr>
                            <td>noch eens, noch oneens</td><td>{{ $scoresAll['45']['answers']['173']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>eens</td><td>{{ $scoresAll['45']['answers']['174']['countpercentage'] }}% </td>
                        </tr>
                        <tr>
                            <td>helemaal eens</td><td>{{ $scoresAll['45']['answers']['175']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <hr>

        <!-- b) Ziekteverzuim wegens werk: keer thuis om redenen die op het werk te vinden zijn?	 120 meestal niet	 / 35 hoeveel keer-->
        <strong>Ziekte door het werk</strong><br>

        <table>
            <tr><td>Werkgever <br> <em>medewerkers melden zich ziek om redenen die op het werk te vinden zijn</em></td>
                <td>Gem. medewerker <br> <em>keer in de afgelopen 12 maanden thuis om redenen op het werk</em></td>
            </tr>
           <tr>
               <td>{{ $scoresWG['120']['answer'] }}</td>
               <td>{{ $scoresAll['35']['avarageAnswer'] }}</td>
           </tr>
        </table>

        <!-- c) Belang gezondheid: 117  gezondheid / 43 -->
        <!--  <strong>Belang gezondheid</strong><br>  -->
     <!--      {{ $scoresWG['117']['order'] }} vs  -->
      <!--     {{ $scoresAll['43']['avarageAnswerInteger'] }}<br>

           <!-- d) Belang leefgewoonten  119 werkvermogen /  45-->
        <!-- <strong>Belang leefgewoonten</strong><br> -->
        <!--  {{ $scoresWG['119']['order'] }} vs  -->
       <!--   {{ $scoresAll['45']['avarageAnswerInteger'] }}<br> -->


          <!-- e) Belang werkvermogen 116 bespreken werkvermogen /  28 -->
        <!-- (gn vergelijking want 2 verschillende vragen) 109 spontane consultatie / 12 -->
        <!-- (gn vergelijking want 2 verschillende vragen) 109 spontane consultatie / 12 -->
        <!-- Vindt u het belangrijk dat er door uw werkgever/in uw onderneming aandacht besteed wordt aan het behoud van werkvermogen van oudere medewerkers?’ -->

        <hr>

        <div id="gra-bespreken-werkvermogen" style="min-width: 300px; height: 350px; margin: 0 auto"></div>

<!--
        {{ $scoresAll['28']['answers']['71']['countpercentage'] }} uw verantwoordelijke<br>
        {{ $scoresAll['28']['answers']['72']['countpercentage'] }} HR medewerker<br>
        {{ $scoresAll['28']['answers']['73']['countpercentage'] }} Arbeidsgeneesheer<br>
        {{ $scoresAll['28']['answers']['74']['countpercentage'] }} Interne preventieadviseur<br>
        {{ $scoresAll['28']['answers']['75']['countpercentage'] }} Vertrouwenspersoon<br>
        {{ $scoresAll['28']['answers']['76']['countpercentage'] }} Andere<br>
        {{ $scoresAll['28']['answers']['77']['countpercentage'] }} Niemand<br>
        {{ $scoresAll['28']['answers']['78']['countpercentage'] }} Ik weet het niet<br> -->



        <!-- e) Belang werkvermogen oudere medewerkers 119  bespreken werkvermogen /  45 -->
        <!-- (gn vergelijking want 2 verschillende vragen) 109 spontane consultatie / 12 -->
        <!-- (gn vergelijking want 2 verschillende vragen) 109 spontane consultatie / 12 -->
     <!--   <strong>Belang werkvermogen vr oudere medewerkers</strong><br>
        <!-- Vindt u het belangrijk dat er door uw werkgever/in uw onderneming aandacht besteed wordt aan het behoud van werkvermogen van oudere medewerkers?’ -->
      <!--  {{ $scoresWG['119']['order'] }} vs
        {{ $scoresAll['45']['avarageAnswerInteger'] }}<br> -->


    </div> <!-- end columns -->






            <script>
                $(document).ready(function() {
                    $(function () {


                        $('#gra-Waarden').highcharts({
                            chart: {
                                type: 'scatter'
                            },
                            title: {
                                text: 'Waarden, houding en motivatie'
                            },
                            xAxis: {
                                categories: ['Onderneming met toekomst',
                                    'Duidelijke waarden',
                                    'Medewerkers tellen als persoon',
                                    'medewerker gestimuleerd ',

                                    'Verbondenheid',
                                    'Balans werk-privé:',
                                    'Balans privé-werk',
                                    'Arbeidstevredenheid',
                                    'Vertrouwen in management',
                                    'Ontslagintentie',
                                    'Motivatie'],
                                title: {
                                    text: null
                                }
                            },
                            yAxis: {
                                min: 1,
                                max: 10,
                                title: {
                                    text: '0 is uiterst negatief - 10 is uiterst positief ',
                                    align: 'high'
                                },
                                labels: {
                                    overflow: 'justify'
                                }
                            },
                            tooltip: false ,
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    }
                                }
                            },

                            credits: {
                                enabled: false
                            },
                            series: [{
                                name: 'Score Werkgever',
                                data: [{{ $scoresWG['158']['answer'] }},
                                       {{ $scoresWG['159']['answer'] }},
                                        {{ $scoresWG['160']['answer'] }},
                                        {{ $scoresWG['161']['answer'] }},
                                        {{ $scoresWG['162']['order'] }},
                                        {{ $scoresWG['163']['order'] }},
                                        {{ $scoresWG['164']['order'] }},
                                        {{ $scoresWG['165']['order'] }},
                                        {{ $scoresWG['167']['order'] }},
                                        {{ $scoresWG['168']['order'] }},
                                        {{ $scoresWG['166']['order'] }}
                                    ],
                                marker: {
                                        radius: 6
                                }
                              }, {
                                    name: 'Gemiddelde score medewerker',
                                    data: [ {{ $scoresAll['64']['avarageAnswerInteger'] }},
                                            {{ $scoresAll['65']['avarageAnswerInteger'] }},
                                            {{ $scoresAll['66']['avarageAnswerInteger'] }},
                                            {{ $scoresAll['67']['avarageAnswerInteger'] }},
                                            {{ $verbondenheid  }},
                                            {{ $scoresAll['74']['avarageAnswerInteger'] }},
                                            {{ $scoresAll['75']['avarageAnswerInteger'] }},
                                            {{ $scoresAll['76']['avarageAnswerInteger'] }},
                                            {{ $scoresAll['77']['avarageAnswerInteger'] }},
                                            {{ $ontslagintentie  }},
                                            {{$motivatie  }}]
                            }]
                        });


                        var CompetentiesLabels = ["Ja", "Nee"];

                        $('#gra-Competenties').highcharts({
                                        chart: {
                                            type: 'scatter'
                                        },
                                        title: {
                                            text: 'competenties'
                                        },
                                        xAxis: {
                                            categories: ['fin. tegemoetkoming',
                                                'inwerktraject',
                                                'opleidingsplan',
                                                'open vr opleiding',
                                                'opleiding afgelopen 3 jaar',
                                                'feedbackgesprekken',
                                                'competenties benutten',
                                                'loopbaan',
                                                'delen van kennis',
                                                'oog voor inzet',
                                                'open gesprekscultuur'],
                                            title: {
                                                text: null
                                            }
                                        },
                                        yAxis: {
                                            min: 0,
                                            max: 1,
                                            title: {
                                                text: null
                                            },
                                            labels: {
                                                overflow: 'justify',
                                                formatter: function() {
                                                        return CompetentiesLabels[this.value];
                                                }
                                            }
                                        },
                                        plotOptions: {
                                            bar: {
                                                dataLabels: {
                                                    enabled: true
                                                }
                                            }
                                        },
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            verticalAlign: 'bottom',
                                            y: 20,
                                            floating: true,
                                            borderWidth: 0,
                                            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#fff'),
                                            shadow: false
                                        },
                                        credits: {
                                            enabled: false
                                        },
                                        tooltip: false ,
                                        series: [{
                                            name: 'Score Werkgever',
                                            data: [
                                                {{ $scoresWG['142']['order'] }},
                                                {{ $scoresWG['145']['order'] }},
                                                {{ $scoresWG['146']['order'] }},
                                                {{ $scoresWG['147']['order'] }},
                                                {{ $scoresWG['148']['order'] }},
                                                {{ $scoresWG['149']['order'] }},
                                                {{ $scoresWG['150']['order'] }},
                                                {{ $scoresWG['153']['order'] }},
                                                {{ $scoresWG['154']['order'] }},
                                                {{ $scoresWG['155']['order'] }} ,
                                                {{ $scoresWG['156']['order'] }}
                                            ],
                                            stack: 'Werkgever',
                                            marker: {
                                                radius: 6
                                            }
                                        }, {
                                             name: 'Gemiddelde score medewerker',
                                             data: [
                                                {{ $scoresAll['49']['avaragePoints'] }},
                                                {{ $scoresAll['52']['avaragePoints'] }},
                                                {{ $scoresAll['53']['avaragePoints'] }},
                                                {{ $scoresAll['54']['avaragePoints'] }},
                                                {{ $scoresAll['54']['avaragePoints'] }},
                                                {{ $scoresAll['56']['avaragePoints'] }},
                                                {{ $scoresAll['57']['avaragePoints'] }},
                                                {{ $scoresAll['59']['avaragePoints'] }},
                                                {{ $scoresAll['60']['avaragePoints'] }},
                                                {{ $scoresAll['61']['avaragePoints'] }},
                                                {{ $scoresAll['62']['avaragePoints'] }}
                                             ],
                                             stack: 'Gemiddelde score medewerker'
                                        }]
                         });


                        var FreqLabels = ["Nooit", "Eens per maand of minder", 'Eens per week', 'Een paar keer per week', 'Dagelijks'];

                        $('#gra-werkgever-lasten-uitgebreid').highcharts({
                                chart: {
                                    type: 'scatter'
                                },
                                title: {
                                    text: 'Ergonomische risico'
                                },
                                xAxis: {
                                    categories: ['Zware lasten', 'Repetitieve beweging', 'Zelfde houding', 'Beeldschermwerk', 'Trillend gereedschap werken', 'Extreme temperatuur', 'Slecht verlichte lokalen', 'Slecht geventileerde lokalen', 'Lawaaierige omgeving']
                                },
                                credits: {
                                    enabled: false
                                },
                                yAxis: {
                                    min: 0,
                                    max: 4,
                                    title: {
                                        text: 'frequentie'
                                    },
                                    labels: {
                                        overflow: 'justify',
                                        formatter: function() {
                                            return FreqLabels[this.value];
                                        }
                                    }
                                },
                                legend: {
                                    reversed: true
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal'
                                    }
                                },
                                tooltip : false,
                                series: [{
                                        name: 'Score werkgever',
                                        data: [ ({{ $scoresWG['169']['order'] }}-1),
                                                ({{ $scoresWG['170']['order'] }}-1),
                                                ({{ $scoresWG['171']['order'] }}-1),
                                                ({{ $scoresWG['172']['order'] }}-1),
                                                ({{ $scoresWG['173']['order'] }}-1),
                                                ({{ $scoresWG['174']['order'] }}-1),
                                                ({{ $scoresWG['175']['order'] }}-1),
                                                ({{ $scoresWG['176']['order'] }}-1),
                                                ({{ $scoresWG['177']['order'] }}-1)],
                                        stack: 'Werkgever',
                                        marker: {
                                            radius: 6
                                        },
                                    },{
                                        name: 'Gemiddelde score medewerker',
                                        data: [ ({{$scoresAll['86']['avarageAnswerInteger']}}-1),
                                                ({{$scoresAll['87']['avarageAnswerInteger']}}-1),
                                                ({{$scoresAll['88']['avarageAnswerInteger']}}-1),
                                                ({{$scoresAll['89']['avarageAnswerInteger']}}-1),
                                                ({{$scoresAll['90']['avarageAnswerInteger']}}-1),
                                                ({{$scoresAll['91']['avarageAnswerInteger']}}-1),
                                                ({{$scoresAll['92']['avarageAnswerInteger']}}-1),
                                                ({{$scoresAll['93']['avarageAnswerInteger']}}-1),
                                                ( {{$scoresAll['94']['avarageAnswerInteger']}}-1)],
                                        stack: 'medewerker'
                                }]

                        });


                        $('#gra-werkgever-opleiding').highcharts({
                                chart: {
                                    type: 'bar'
                                },
                                title: {
                                    text: '% medewerkers die voldoende opleiding krijgt om ergonomische risico te voorkomen'
                                },
                                xAxis: {
                                    categories: ['']
                                },
                                credits: {
                                    enabled: false
                                },
                                yAxis: {
                                    min: 0,
                                    max: 100,
                                    title: {
                                        text: 'ja / nee'
                                    }
                                },
                                legend: {
                                    reversed: true
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal'
                                    }
                                },
                                series: [{
                                            name: 'werkgever nee',
                                            data: [(100-({{ $scoresWG['169']['order'] }}*100))],
                                            stack: 'Werkgever',
                                            color: '#6aa4e7'
                                        },{
                                            name: 'werkgever ja',
                                            data: [{{ $scoresWG['169']['order'] }}*100],
                                            stack: 'Werkgever',
                                            color: '#61e789'
                                        }, {
                                            name: 'Gemiddelde medewerker nee',
                                            data: [(100-({{ $scoresAll['98']['avarageAnswerInteger'] }}*100))],
                                            stack: 'medewerker',
                                            color: '#f24932'
                                        }, {
                                            name: 'Gemiddelde medewerker ja',
                                            data: [{{ $scoresAll['98']['avarageAnswerInteger'] }}*100],
                                            stack: 'medewerker',
                                            color: '#f2904a'
                                        }]

                        });


                            // Make monochrome colors and set them as default for all pies
                            Highcharts.getOptions().plotOptions.pie.colors = (function () {
                                var colors = [],
                                    base = '#ec7c3a',
                                    i;

                                for (i = 0; i < 10; i += 1) {
                                    // Start out with a darkened base color (negative brighten), and end
                                    // up with a much brighter color
                                    colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
                                }
                                return colors;
                            }());

                            // Build the chart
                            $('#gra-werkgever-maatregels-vr-ouderen').highcharts({
                                chart: {
                                    plotBackgroundColor: null,
                                    plotBorderWidth: null,
                                    plotShadow: false
                                },
                                credits: {
                                    enabled: false
                                },
                                title: {
                                    text: 'De 3 belangrijkste ontzie- en ontwikkelmaatregelen'
                                },
                                tooltip: {
                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                },
                                plotOptions: {
                                    pie: {
                                        allowPointSelect: true,
                                        cursor: 'pointer',
                                        dataLabels: {
                                            enabled: true,
                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                            style: {
                                                color: '#ec7c3a' || 'black'
                                            }
                                        }
                                    }
                                },
                                series: [{
                                    type: 'pie',
                                    name: 'Browser share',
                                    data: [
                                        ['Tijdskrediet', {{ $scoreWNVraag99['431']['countpercentage'] }}],
                                        ['Deeltijds werken',  {{ $scoreWNVraag99['432']['countpercentage'] }}],
                                        ['Bijkomende verlofdagen',  {{ $scoreWNVraag99['433']['countpercentage'] }}],
                                        ['Jobverrijking of loopbaanontwikkeling',  {{ $scoreWNVraag99['434']['countpercentage'] }}],
                                        ['Jobrotatie', {{ $scoreWNVraag99['435']['countpercentage'] }}],
                                        ['Taakaanpassing',      {{ $scoreWNVraag99['436']['countpercentage'] }}],
                                        ['Minder fysieke werkbelasting',  {{ $scoreWNVraag99['437']['countpercentage'] }}],
                                        ['Premie op basis van aantal jaren dienst',  {{ $scoreWNVraag99['438']['countpercentage'] }}],
                                        ['Flexibele werken', {{ $scoreWNVraag99['439']['countpercentage'] }}],
                                        ['Ondersteuning van de gezondheid',      {{ $scoreWNVraag99['440']['countpercentage'] }}],
                                        ['Opleidingen op maat van specifieke doelgroepen',  {{ $scoreWNVraag99['441']['countpercentage'] }}],
                                        ['Positieve beeldvorming tegenover oudere medewerkers',  {{ $scoreWNVraag99['442']['countpercentage'] }}]
                                    ],

                                }]
                            });



                });
                });
            </script>

</div> <!--row -->
<div class="row detail-results">

    <div class="large-12 large-offset-0 end columns">
        <h5 class="verdiep">Verdieping 2</h5>
        <h4>Competenties</h4>

        <div id="gra-Competenties" style="min-width: 400px;  height: 290px; margin: 0 auto"></div>
<br>
      <table class="detail-results">
            <tr>
                <th valign="top"></th>
                <th valign="top">Werkgever</th>
                <th valign="top">medewerker</th>
            </tr>
            <tr>
                <th valign="top">inwerktraject</th>
                <td class="werkgever-detail">{{ $scoresWG['145']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['52']['answers']['188']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['52']['answers']['189']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">opleidingsplan</th>
                <td class="werkgever-detail">{{ $scoresWG['146']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['53']['answers']['190']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['53']['answers']['191']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">open voor opleiding?</th>
                <td class="werkgever-detail">{{ $scoresWG['147']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['54']['answers']['192']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['54']['answers']['193']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">opleiding afgelopen 3 jaar</th>
                <td class="werkgever-detail">{{ $scoresWG['148']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['55']['answers']['194']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['55']['answers']['195']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">feedbackgesprekken</th>
                <td class="werkgever-detail">{{ $scoresWG['149']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['56']['answers']['196']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['56']['answers']['197']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">competenties benutten</th>
                <td class="werkgever-detail">{{ $scoresWG['150']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['57']['answers']['198']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['57']['answers']['199']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">loopbaan</th>
                <td class="werkgever-detail">{{ $scoresWG['153']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['59']['answers']['202']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['59']['answers']['203']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">delen van kennis</th>
                <td class="werkgever-detail">{{ $scoresWG['154']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['60']['answers']['204']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['60']['answers']['205']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">oog voor inzet </th>
                <td class="werkgever-detail">{{ $scoresWG['155']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['61']['answers']['206']['countpercentage'] }}%</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['61']['answers']['207']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">lerende en open gesprekscultuur </th>
                <td class="werkgever-detail">{{ $scoresWG['156']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Ja</td><td>{{ $scoresAll['62']['answers']['208']['countpercentage'] }} %</td>
                        </tr>
                        <tr>
                            <td>Nee</td><td>{{ $scoresAll['62']['answers']['209']['countpercentage'] }}% </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>


    </div> <!--column -->

</div> <!--row -->

<div class="row detail-results">

    <div class="large-12  columns">
            <h5 class="verdiep">Verdieping 3</h5>

            <h4>Waarden, houding en motivatie</h4>
            <div id="gra-Waarden" style="min-width: 300px; height: 450px; margin: 0 auto"></div>


           <!-- <table>
                <tr>
                    <td>Onderneming met toekomst</td>
                    <td>{{ $scoresWG['158']['answer'] }}</td>
                    <td>{{ $scoresAll['64']['avarageAnswer'] }}</td>
                </tr>
                <tr>
                    <td>Duidelijke waarden</td>
                    <td>{{ $scoresWG['159']['answer'] }}</td>
                    <td>{{ $scoresAll['65']['avarageAnswer'] }}</td>
                </tr>
                <tr>
                    <td>Medewerkers tellen als persoon</td>
                    <td>{{ $scoresWG['160']['answer'] }}</td>
                    <td>{{ $scoresAll['66']['avarageAnswer'] }}</td>
                </tr>
                <tr>
                    <td>medewerker gestimuleerd </td>
                    <td>{{ $scoresWG['161']['answer'] }}</td>
                    <td>{{ $scoresAll['67']['avarageAnswer'] }}</td>
                </tr>
                <tr>
                    <td>Talenten benutten </td>
                    <td>0</td>
                    <td>{{ $scoresAll['68']['avarageAnswer'] }}</td>
                </tr>
                <tr>
                    <td>bijdrage aan het gemeenschappelijk</td>
                    <td>0</td>
                    <td>{{ $scoresAll['69']['avarageAnswer'] }}</td>
                </tr>
                <tr>
                    <td>Verantwoordelijkheid</td>
                    <td>0</td>
                    <td>{{ $scoresAll['70']['avarageAnswer'] }} </td>
                </tr>
                <tr>
                    <td>Aansluiting waarden</td>
                    <td>0</td>
                    <td>{{ $scoresAll['71']['avarageAnswer'] }} </td>
                </tr>
                <tr>
                    <td>Verbondenheid</td>
                    <td>{{ $scoresWG['162']['order'] }}</td>
                    <td>!! 2 vragen {{ $scoresAll['72']['avarageAnswerInteger'] }} + {{ $scoresAll['72']['avarageAnswerInteger'] }}</td>
                </tr>
                <tr>
                    <td>Balans werk-privé:</td>
                    <td>{{ $scoresWG['163']['order'] }}</td>
                    <td>{{ $scoresAll['74']['avarageAnswerInteger'] }}</td>
                </tr>
                <tr>
                    <td>Balans privé-werk</td>
                    <td>{{ $scoresWG['164']['order'] }}</td>
                    <td>{{ $scoresAll['75']['avarageAnswerInteger'] }}</td>
                </tr>
                <tr>
                    <td>Arbeidstevredenheid</td>
                    <td>{{ $scoresWG['165']['order'] }}</td>
                    <td>{{ $scoresAll['76']['avarageAnswerInteger'] }}</td>
                </tr>
                <tr>
                    <td>Vertrouwen in management</td>
                    <td>{{ $scoresWG['167']['order'] }}</td>
                    <td>{{ $scoresAll['77']['avarageAnswerInteger'] }} </td>
                </tr>
                <tr>
                    <td>Ontslagintentie  (4 vragen)</td>
                    <td>{{ $scoresWG['168']['order'] }}</td>
                    <td>!! 4 vragen  {{ $scoresAll['78']['avarageAnswerInteger'] }} + {{ $scoresAll['79']['avarageAnswerInteger'] }}  + {{ $scoresAll['80']['avarageAnswerInteger'] }} +  {{ $scoresAll['81']['avarageAnswerInteger'] }} </td>
                </tr>
                <tr>
                    <td>Motivatie (4 vragen)</td>
                    <td>{{ $scoresWG['166']['order'] }}</td>
                    <td>!! 4 vragen  {{ $scoresAll['82']['avarageAnswerInteger'] }} + {{ $scoresAll['83']['avarageAnswerInteger'] }}  + {{ $scoresAll['84']['avarageAnswerInteger'] }} +  {{ $scoresAll['85']['avarageAnswerInteger'] }} </td>
                </tr>


            </table> -->
        </div> <!-- end columns -->
</div> <!--row -->







<div class="row detail-results last">

        <div class="large-12 end columns">
            <h5 class="verdiep">Verdieping 4</h5>
            <h4>Werk, werkgemeenschap en leiding</h4>

            <div id="gra-werkgever-lasten-uitgebreid" style="min-width: 400px;  height:450px; margin: 0 auto"></div>

            <table class="detail-results">
            <tr>
                <th valign="top"></th>
                <th valign="top">Werkgever</th>
                <th valign="top">medewerker</th>

            </tr>
            <tr>
                <th valign="top">Zware lasten tillen, dragen, trekken of duwen?</th>
                <td class="werkgever-detail" valign="top">{{ $scoresWG['169']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Nooit</td>
                            <td>{{$scoresAll['86']['answers']['378']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>Eens per maand of minder</td>
                            <td>{{$scoresAll['86']['answers']['379']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>{{$scoresAll['86']['answers']['380']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>{{$scoresAll['86']['answers']['381']['countpercentage']}} % </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>{{$scoresAll['86']['answers']['382']['countpercentage']}} % </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">Repetitieve (herhaaldelijk dezelfde) bewegingen</th>
                <td class="werkgever-detail" valign="top">{{ $scoresWG['170']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Nooit</td>
                            ge<td>{{$scoresAll['87']['answers']['383']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>Eens per maand of minder</td>
                            <td>{{$scoresAll['87']['answers']['384']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>{{$scoresAll['87']['answers']['385']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>{{$scoresAll['87']['answers']['386']['countpercentage']}} % </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>{{$scoresAll['87']['answers']['387']['countpercentage']}} % </td>
                        </tr>
                    </table>
                </td>
            </tr>

            </tr>
            <tr>
                <th valign="top">Langdurig zelfde houding</th>
                <td class="werkgever-detail" valign="top">{{ $scoresWG['171']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Nooit</td>
                            <td>{{$scoresAll['88']['answers']['388']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>Eens per maand of minder</td>
                            <td>{{$scoresAll['88']['answers']['389']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>{{$scoresAll['88']['answers']['390']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>{{$scoresAll['88']['answers']['391']['countpercentage']}} % </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>{{$scoresAll['88']['answers']['392']['countpercentage']}} % </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">Beeldschermwerk</th>
                <td class="werkgever-detail" valign="top">{{ $scoresWG['172']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Nooit</td>
                            <td>{{$scoresAll['89']['answers']['393']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>Eens per maand of minder</td>
                            <td>{{$scoresAll['89']['answers']['394']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>{{$scoresAll['89']['answers']['395']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>{{$scoresAll['89']['answers']['396']['countpercentage']}} % </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>{{$scoresAll['89']['answers']['397']['countpercentage']}} % </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">Trillend gereedschap werken</th>
                <td class="werkgever-detail" valign="top">{{ $scoresWG['173']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Nooit</td>
                            <td>{{$scoresAll['90']['answers']['398']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>Eens per maand of minder</td>
                            <td>{{$scoresAll['90']['answers']['399']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>{{$scoresAll['90']['answers']['400']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>{{$scoresAll['90']['answers']['401']['countpercentage']}} % </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>{{$scoresAll['90']['answers']['402']['countpercentage']}} % </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">Bij te hoge of te lage temperatuur werken</th>
                <td class="werkgever-detail" valign="top">{{ $scoresWG['174']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Nooit</td>
                            <td>{{$scoresAll['91']['answers']['403']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>Eens per maand of minder</td>
                            <td>{{$scoresAll['91']['answers']['404']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>{{$scoresAll['91']['answers']['405']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>{{$scoresAll['91']['answers']['406']['countpercentage']}} % </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>{{$scoresAll['91']['answers']['407']['countpercentage']}} % </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">Slecht verlichte lokalen</th>
                <td class="werkgever-detail" valign="top">{{ $scoresWG['175']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Nooit</td>
                            <td>{{$scoresAll['92']['answers']['408']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>Eens per maand of minder</td>
                            <td>{{$scoresAll['92']['answers']['409']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>{{$scoresAll['92']['answers']['410']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>{{$scoresAll['92']['answers']['411']['countpercentage']}} % </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>{{$scoresAll['92']['answers']['412']['countpercentage']}} % </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">Slecht geventileerde lokalen</th>
                <td class="werkgever-detail" valign="top">{{ $scoresWG['176']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Nooit</td>
                            <td>{{$scoresAll['93']['answers']['413']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>Eens per maand of minder</td>
                            <td>{{$scoresAll['93']['answers']['414']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>{{$scoresAll['93']['answers']['415']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>{{$scoresAll['93']['answers']['416']['countpercentage']}} % </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>{{$scoresAll['93']['answers']['417']['countpercentage']}} % </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <th valign="top">Lawaaierige omgeving</th>
                <td class="werkgever-detail" valign="top">{{ $scoresWG['177']['answer'] }}</td>
                <td><table class="medewerker-detail">
                        <tr>
                            <td>Nooit</td>
                            <td>{{$scoresAll['94']['answers']['418']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>Eens per maand of minder</td>
                            <td>{{$scoresAll['94']['answers']['419']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>eens per week</td>
                            <td>{{$scoresAll['94']['answers']['420']['countpercentage']}}  %</td>
                        </tr>
                        <tr>
                            <td>een paar keer per week</td>
                            <td>{{$scoresAll['94']['answers']['421']['countpercentage']}} % </td>
                        </tr>
                        <tr>
                            <td>dagelijks</td>
                            <td>{{$scoresAll['94']['answers']['422']['countpercentage']}} % </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>

            <br><hr><br>

            <div id="gra-werkgever-opleiding" style="min-width: 400px; height: 250px; margin: 0 auto"></div>

             <br><hr><br>

            <div id="gra-werkgever-maatregels-vr-ouderen" style="min-width: 400px;  height: 450px; margin: 0 auto"></div>


            <table class="detail-results">
                <tr>
                    <th>Tijdskrediet</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['431']['countpercentage'] }} %</td
                </tr>
                <tr>
                    <th>Deeltijds werken</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['432']['countpercentage'] }} %</td
                </tr>
                <tr>
                    <th>Bijkomende verlofdagen</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['433']['countpercentage'] }} %</td>
                </tr>
                <tr>
                    <th>Jobverrijking of loopbaanontwikkeling</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['434']['countpercentage'] }} %</td>
                </tr>
                <tr>
                    <th>Jobrotatie</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['435']['countpercentage'] }} %</td>
                </tr>
                <tr>
                    <th>Taakaanpassing</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['436']['countpercentage'] }} %</td>
                </tr>
                <tr>
                    <th>Minder fysieke werkbelasting</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['437']['countpercentage'] }} %</td>
                </tr>
                <tr>
                    <th>Premie op basis van aantal jaren dienst</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['438']['countpercentage'] }} %</td>
                </tr>
                <tr>
                    <th>Flexibele werken</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['439']['countpercentage'] }} %</td>
                </tr>
                <tr>
                    <th>Ondersteuning van de gezondheid</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['440']['countpercentage'] }} %</td>
                </tr>
                <tr>
                    <th>Opleidingen op maat van specifieke doelgroepen</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['441']['countpercentage'] }} %</td>
                </tr>
                <tr>
                    <th>Positieve beeldvorming tegenover oudere medewerkersn</th>
                    <td class="medewerker-detail">{{ $scoreWNVraag99['442']['countpercentage'] }} %</td>
                </tr>
            </table>
        </div> <!-- end columns -->

    </div> <!--row -->

</div><!-- END PANEL1-->





<div class="content" id="panel2">

<!-- START PANEL2-->
<div class="row huis-resultaat">

    <div class="medium-12 columns ">
        <div class="tussenresultaat visualisation">
            <img src="{{ URL::asset('images/visualisation/licht-uit.png'); }}" class="viz-image">

            <div class="viz-level ">
                <img src="/images/visualisation/verdieping1_{{ $scoresLevelOneEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="/images/visualisation/verdieping1_mannetje_{{ $scoresLevelOneEvaluation }}.png">
            </div>

            <div class="viz-level">
                <img src="/images/visualisation/verdieping2_{{ $scoresLevelTwoEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="/images/visualisation/verdieping2_mannetje_{{ $scoresLevelTwoEvaluation }}.png">
            </div>

            <div class="viz-level ">
                <img src="/images/visualisation/verdieping3_{{ $scoresLevelThreeEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="/images/visualisation/verdieping3_mannetje_{{ $scoresLevelThreeEvaluation }}.png">
            </div>

            <div class="viz-level">
                <img src="/images/visualisation/verdieping4_{{ $scoresLevelFourEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="/images/visualisation/verdieping4_mannetje_{{ $scoresLevelFourEvaluation }}.png">
            </div>

            <div class="viz-level">
                <img src="/images/visualisation/verdieping5_{{ $scoresLevelFiveEvaluation }}.png">
            </div>
            <div class="viz-level mannetje">
                <img src="/images/visualisation/verdieping5_mannetje_{{ $scoresLevelFiveEvaluation }}.png">
            </div>

        </div>
    </div>

</div>

<div class="row detail-results huis-resultaat-table">
    <div class="medium-12 columns ">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Verdiep 1</span>Gezondheid en functionele capaciteiten</h5>
            </div>
            <div class="large-3  columns">
                <h6>Medische vragen</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelOne[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelOne[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelOne[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-3  columns">
                <h6>Levensstijl</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelOne[1]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelOne[1]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelOne[1]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-3  columns">
                <h6>Verzuim</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelOne[2]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelOne[2]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelOne[2]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-3  columns">
                <h6>Burnout</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelOne[3]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelOne[3]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelOne[3]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="medium-12 columns seperator">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Verdiep 2</span>Competenties</h5>
            </div>
            <div class="large-6  columns">
                <h6>Zichtbaarheid op arbeidsmarkt</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelTwo[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelTwo[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelTwo[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-6 end columns">
                <h6>Talentmanagement</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelTwo[1]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelTwo[1]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelTwo[1]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="medium-12 columns seperator">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Verdiep 3</span>Waarden, houding en motivatie</h5>
            </div>
            <div class="large-6  columns">
                <h6>Waarden en normen</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelThree[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelThree[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelThree[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
            <div class="large-6 end columns">
                <h6>Persoonskenmerken</h6>
                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelThree[1]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelThree[1]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelThree[1]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div class="medium-12 columns seperator">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Verdiep 4</span>Werk, werkgemeenschap en leiding</h5>
            </div>
            <div class="large-6 end columns">

                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelFour[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelFour[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelFour[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="medium-12 columns seperator">
        <div class="row">
            <div class="large-12  columns">
                <h5><span class="verdiep">Het Dak</span>Werkvermogen</h5>
            </div>
            <div class="large-6 end columns">

                <div class="results">
                    <ul>
                        <li class="goed">{{ $scoresLevelFive[0]['pointssectionArray']['goed'] }} <span class="percentage-sign">%</span></li>
                        <li class="neutraal">{{ $scoresLevelFive[0]['pointssectionArray']['neutraal'] }}  <span class="percentage-sign">%</span></li>
                        <li class="slecht">{{ $scoresLevelFive[0]['pointssectionArray']['slecht'] }}  <span class="percentage-sign">%</span></li>
                    </ul>
                </div><br>
            </div>
        </div>
    </div>


</div> <!-- end row huis-resultaat-table -->
<div class="row detail-results">

    <div class="large-8 large-offset-2 end columns">

        <strong>Van de medewerkers die de vragenlijst ingevuld hebben, heeft:</strong><br><br>

        <h5 class="verdiep">Verdieping 1</h5>

        <h4 class="title-block" style="background-image:url('/images/icons/face{{{ $scoresLevelOneEvaluation }}}.png')">Gezondheid en functionele capaciteiten</h4>

        <!-- 2. GEZONDHEID -->
        <h5 class="smiley"  style="background-image:url('/images/icons/face-small{{{ $scoresLevelOne[0]['medianevaluation'] }}}.png')" }}>Medische vragen</h5>


        <div class="feedback analyse">
            {{$scoresAll['14']['answers']['43']['countpercentage']}}% aandoeningen of ziekten van het bewegingsapparaat waarmee ze bij het uitvoeren van hun taken moeten  rekening houden.
            <div class="action">Een ergonomisch risico-evaluatie en eventueel interventie in de zin van hulpmiddelen, opleidingen kunnen  voor deze medewerkers nuttig zijn.</div>
        </div>

        <div class="feedback analyse">
            <?php
            $leefstijlziekte = round(($scoresAll['15']['answers']['45']['countpercentage']+$scoresAll['17']['answers']['49']['countpercentage']+$scoresAll['18']['answers']['51']['countpercentage'])/3);
            ?>
            {{ $leefstijlziekte }}% een leefstijl gerelateerde aandoening

            <div class="action">
                Betere leefgewoonten in de zin van niet roken, gezonde eetgewoonten, voldoende beweging kunnen deze aandoeningen voorkomen.
            </div>
        </div>

        <div class="feedback analyse">
            {{$scoresAll['20']['answers']['55']['countpercentage']}}% een depressie of burn-out (doorgemaakt).

            <?php if ($scoresAll['20']['answers']['55']['countpercentage']>50) { ?>
                <div class="action">
                    Meer dan 10 % van uw medewerkers geeft aan in het verleden een burn-out of depressie doorgemaakt te hebben of
                    op dit moment door te maken. Dit is een zeer ernstig te nemen signaal vanuit uw medewerkerspopulatie en wijst op
                    een hoge psychosociale belasting.
                </div>
            <?php } ?>
        </div>


        <div class="feedback analyse">
            <?php
            $aandoening = round(($scoresAll['14']['answers']['43']['countpercentage']+
                           $scoresAll['15']['answers']['45']['countpercentage']+
                           $scoresAll['17']['answers']['49']['countpercentage']+
                           $scoresAll['18']['answers']['51']['countpercentage']+
                           $scoresAll['20']['answers']['55']['countpercentage']+
                           $scoresAll['21']['answers']['57']['countpercentage'])/6);
            ?>
            {{ $aandoening }}% een aandoening die mogelijk een effect op zijn arbeidscapaciteit heeft of zal hebben.
        </div>




        <h5 class="smiley"  style="background-image:url('/images/icons/face-small{{{ $scoresLevelOne[1]['medianevaluation'] }}}.png')" }}>Leefstijl</h5>

        <div class="feedback analyse">
            {{$scoresAll['23']['answers']['61']['countpercentage']}}% rookt
        </div>
        <div class="feedback analyse">
            {{$scoresAll['24']['answers']['64']['countpercentage']}}% beweegt te weinig
        </div>
        <div class="feedback analyse">
            <?php
            $gezondeten = ($scoresAll['25']['answers']['66']['countpercentage']+$scoresAll['26']['answers']['68']['countpercentage'])/2;
            ?>
            {{ $gezondeten }}% eet te weinig groenten of fruit
        </div>
        <div class="feedback analyse">
            {{$scoresAll['27']['answers']['69']['countpercentage']}}% drinkt te veel alcohol
        </div>
        <div class="feedback">
            Ongezonde leefgewoonten zijn oorzaak van heel wat aandoeningen die maken dat mensen sneller uitvallen en eerder stoppen met werken. Hart- en vaatlijden en diabetes worden in de hand gewerkt door roken, ongezonde eetgewoonten en te weinig bewegen. Het verband tussen roken en longkanker en chronische bronchitis is voldoende aangetoond. Overdreven alcoholgebruik veroorzaakt naast lichamelijke aandoeningen, ook ongevallen en psychosociale problemen. Aandacht voor en het bevorderen van gezonde leefgewoonten op de werkvloer is een zinvolle investering.
        </div>


        <!-- 2. GEZONDHEID LEEFSTIJL -->
        <h5 class="smiley"  style="background-image:url('/images/icons/face-small{{{ $scoresLevelOne[2]['medianevaluation'] }}}.png')" }}>Verzuim</h5>

        <?php
        $levenstijl="ok";
        if ($scoresAll['23']['medianPoints']+$scoresAll['24']['medianPoints']+$scoresAll['25']['medianPoints']+$scoresAll['26']['medianPoints']+$scoresAll['28']['medianPoints'] > 0) {
            $levenstijl="nok";
        }
        ?>

        <!-- dagen & keer samen -->
        @if($scoresAll['33']['medianPoints'] > 0 || $scoresAll['34']['medianPoints'] > 0)
            <div class="feedback">@include('werkgevers/snippet-overz-31-feedback')</div>
        @endif
        <!-- keer wegens redenen die op het werk..-->
        @if($scoresAll['35']['medianPoints'] > 0)
            <div class="feedback">@include('werkgevers/snippet-overz-33-feedback')</div>
        @endif
        <!-- keer toch gaan werken-->
        @if($scoresAll['36']['medianAnswer'] !== 'geen enkele keer')
            <div class="feedback">@include('werkgevers/snippet-overz-34-feedback')</div>
        @endif

        <h5 class="smiley"  style="background-image:url('/images/icons/face-small{{{ $scoresLevelOne[3]['medianevaluation'] }}}.png')" }}>Burnout</h5>

        <!-- GEZONDHEID : burnout-->
        <?php
        $burnoutScore = $scoresAll['37']['medianPoints']+$scoresAll['38']['medianPoints'];
        ?>
        @if($burnoutScore <= 6)
            <div class="feedback">@include('werkgevers/snippet-overz-35-1-feedback')</div>
        @elseif ($burnoutScore > 13)
            <div class="feedback">@include('werkgevers/snippet-overz-35-2-feedback')</div>
        @else
        <!-- no fb -->
        @endif

        <?php
        $bevlogenheidsScore = $scoresAll['39']['medianPoints']+$scoresAll['40']['medianPoints']+ $scoresAll['41']['medianPoints']+$scoresAll['42']['medianPoints'];
        ?>
        @if($bevlogenheidsScore <= 19)
            <div class="feedback slecht">@include('werkgevers/snippet-overz-37-1-feedback')</div>
        @elseif ($bevlogenheidsScore > 25)
            <div class="feedback goed">@include('werkgevers/snippet-overz-37-2-feedback')</div>
        @else
        <!-- no fb -->
        @endif

    </div>
</div>






<div class="row  detail-results">
    <div class="large-8 large-offset-2 end columns">

                <h5 class="verdiep">Verdieping 2</h5>

                <h4 class="title-block" style="background-image:url('/images/icons/face{{{ $scoresLevelTwoEvaluation }}}.png')">Competenties</h4>

                <!-- COMPETENTIES : rekrutering-->

                <h5 class="smiley"  style="background-image:url('/images/icons/face-small{{{ $scoresLevelTwo[0]['medianevaluation'] }}}.png')" }}>Zichtbaarheid op arbeidsmarkt</h5>

                <div class="feedback analyse">
                    {{$scoresAll['46']['answers'][176]['countpercentage']}} % van de medewerkers kent de kanalen om vacatures te vinden om elders te solliciteren.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['47']['answers']['178']['countpercentage']}} % van de medewerkers weet waar interne vacatures terug te vinden zijn.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['48']['answers']['180']['countpercentage']}} % van de medewerkers gebruikt sociale media als professioneel netwerk- en sollicitatiekanaal.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['49']['answers']['182']['countpercentage']}} %  van de medewerkers kent de financiële tegemoetkomingen voor de aanwerving van oudere medewerkers als hij een andere job wil zoeken.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['50']['answers']['184']['countpercentage']}} % van de medewerkers weet waar hij terecht kan voor opleidingen.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['51']['answers']['186']['countpercentage']}} % van de medewerkers weet hoe zich voor te bereiden op een selectieprocedure.
                </div>


                <!-- COMPETENTIES : talentmanagement-->

                <h5></h5>
                <h5 class="smiley"  style="background-image:url('/images/icons/face-small{{{ $scoresLevelTwo[1]['medianevaluation'] }}}.png')" }}>Talentmanagement</h5>


                <div class="feedback analyse">
                    {{$scoresAll['52']['answers']['188']['countpercentage']}} % van de medewerkers geeft aan een peter of meter te hebben gehad bij aanwerving.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['53']['answers']['190']['countpercentage']}} % van de medewerkers zegt een opleidingsplan met bijhorend budget te hebben.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['54']['answers']['192']['countpercentage']}} % van de medewerkers zegt graag opleidingen te volgen.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['55']['answers']['194']['countpercentage']}} % van de medewerkers zegt de afgelopen 3 jaar minstens één opleiding te hebben gevolgd.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['56']['answers']['196']['countpercentage']}} %  van de medewerkers heeft dankzij feedbackgesprekken met zijn leidinggevende een zicht op zijn sterktes en zwaktes.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['57']['answers']['198']['countpercentage']}} % van de medewerkers vindt dat zijn competenties ten volle benut worden.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['58']['answers']['200']['countpercentage']}} % van de medewerkers ziet zichzelf nog andere taken of functies opnemen in de onderneming.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['59']['answers']['202']['countpercentage']}} % van de medewerkers weet goed wat hij nog kan of wil met zijn loopbaan.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['60']['answers']['204']['countpercentage']}} % van de medewerkers vindt dat het delen van kennis in de onderneming wordt gestimuleerd.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['61']['answers']['206']['countpercentage']}} % van de medewerkers vindt dat er voldoende oog is voor de inzet van medewerkers.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['62']['answers']['208']['countpercentage']}} % van de medewerkers vindt dat er een lerende en open gesprekscultuur is in de onderneming.
                </div>

                <div class="feedback analyse">
                    {{$scoresAll['63']['answers']['210']['countpercentage']}} % van de medewerkers weet welke doelstellingen hij moet halen en waar hij zich op moet toeleggen.
                </div>

    </div>
</div>


<div class="row  detail-results">

    <div class="large-8 large-offset-2 end columns">

        <h5 class="verdiep">Verdieping 3</h5>

        <h4 class="title-block" style="background-image:url('/images/icons/face{{{ $scoresLevelThreeEvaluation }}}.png')">Waarden, houding en motivatie</h4>

        <!-- WAARDEN : vkw-->

        <h5 class="smiley"  style="background-image:url('/images/icons/face-small{{{ $scoresLevelThree[0]['medianevaluation'] }}}.png')" }}>Waarden en normen</h5>

        <!-- uniek voor medewerkers -->
        @if($scoresAll['64']['medianAnswer'] < 6)
        <div class="feedback">@include('werkgevers/snippet-overz-64-feedback')</div>
        @endif
        @if($scoresAll['65']['medianAnswer'] < 6)
        <div class="feedback">@include('werkgevers/snippet-overz-65-feedback')</div>
        @endif
        @if($scoresAll['66']['medianAnswer'] < 6)
        <div class="feedback">@include('werkgevers/snippet-overz-66-feedback')</div>
        @endif
        @if($scoresAll['67']['medianPoints'] < 6)
        <div class="feedback">@include('werkgevers/snippet-overz-67-feedback')</div>
        @endif

        <!-- ook bij  medewerkers -->
        @if($scoresAll['68']['medianPoints'] < 6)
        <div class="feedback">@include('werkgevers/snippet-overz-68-feedback')</div>
        @endif
        @if($scoresAll['69']['medianPoints'] < 6)
        <div class="feedback">@include('werkgevers/snippet-overz-69-feedback')</div>
        @endif
        @if($scoresAll['70']['medianPoints'] < 6)
        <div class="feedback">@include('werkgevers/snippet-overz-70-feedback')</div>
        @endif
        @if($scoresAll['71']['medianPoints'] < 6)
        <div class="feedback">@include('werkgevers/snippet-overz-71-feedback')</div>
        @endif


        <!-- WAARDEN : verbondenheid -->

        <h5 class="smiley"  style="background-image:url('/images/icons/face-small{{{ $scoresLevelThree[1]['medianevaluation'] }}}.png')" }}>Persoonskenmerken</h5>

        <?php
        $verbondenheidScore = $scoresAll['72']['medianPoints']+$scoresAll['73']['medianPoints'];
        $priveScore = $scoresAll['74']['medianPoints']+$scoresAll['75']['medianPoints'];
        $tevredenheidsScore = $scoresAll['76']['medianPoints'];
        $vertrouwenmanagementScore = $scoresAll['77']['medianPoints'];
        $ontslagScore = $scoresAll['78']['medianPoints']+$scoresAll['79']['medianPoints']+$scoresAll['80']['medianPoints']+$scoresAll['81']['medianPoints'];
        $autonoomScore = $scoresAll['82']['medianPoints']+$scoresAll['83']['medianPoints']+$scoresAll['84']['medianPoints']+$scoresAll['85']['medianPoints'];
        ?>

        @if($verbondenheidScore >= 1)
            <div class="feedback slecht">@include('werkgevers/snippet-overz-72-1-feedback')</div>
        @endif
        @if(($scoresAll['72']['medianAnswer']=='eens' || $scoresAll['72']['medianAnswer']=='helemaal eens') && ($scoresAll['73']['medianAnswer']=='eens' || $scoresAll['73']['medianAnswer']=='helemaal eens'))
            <div class="feedback goed">@include('werkgevers/snippet-overz-72-2-feedback')</div>
        @endif


        @if($priveScore >= 1)
            <!-- slecht -->
            <div class="feedback slecht">@include('werkgevers/snippet-overz-74-2-feedback')</div>
        @endif
        @if(($scoresAll['74']['medianAnswer']=='oneens' || $scoresAll['74']['medianAnswer']=='helemaal oneens') && ($scoresAll['75']['medianAnswer']=='oneens' || $scoresAll['75']['medianAnswer']=='helemaal oneens'))
        <!-- goed -->
            <div class="feedback slecht">@include('werkgevers/snippet-overz-74-1-feedback')</div>
        @endif

        @if($scoresAll['76']['medianAnswer']=='helemaal oneens')
            <div class="feedback slecht">@include('werkgevers/snippet-overz-76-1-feedback')</div>
        @endif
        @if($scoresAll['76']['medianAnswer']=='helemaal eens')
            <div class="feedback goed">@include('werkgevers/snippet-overz-76-2-feedback')</div>
        @endif

        @if($scoresAll['77']['medianAnswer']=='helemaal oneens')
            <div class="feedback slecht">@include('werkgevers/snippet-overz-77-1-feedback')</div>
        @endif
        @if($scoresAll['77']['medianAnswer']=='eens' || $scoresAll['77']['medianAnswer']=='helemaal eens')
            <div class="feedback goed">@include('werkgevers/snippet-overz-77-2-feedback')</div>
        @endif

        @if($ontslagScore <= 0.25)
            <div class="feedback goed">@include('werkgevers/snippet-overz-78-1-feedback')</div>
        @endif
        @if(($scoresAll['78']['medianAnswer']=='eens' || $scoresAll['78']['medianAnswer']=='helemaal eens') && ($scoresAll['79']['medianAnswer']=='eens' || $scoresAll['79']['medianAnswer']=='helemaal eens') && ($scoresAll['80']['medianAnswer']=='eens' || $scoresAll['80']['medianAnswer']=='helemaal eens') && ($scoresAll['81']['medianAnswer']=='oneens' || $scoresAll['81']['medianAnswer']=='helemaal oneens'))
            <div class="feedback slecht">@include('werkgevers/snippet-overz-78-2-feedback')</div>
        @endif

        @if($autonoomScore >= 0.5)
            <div class="feedback slecht">@include('werkgevers/snippet-overz-82-1-feedback')</div>
        @endif
        @if(($scoresAll['82']['medianAnswer']=='eens' || $scoresAll['82']['medianAnswer']=='helemaal eens') && ($scoresAll['83']['medianAnswer']=='eens' || $scoresAll['83']['medianAnswer']=='helemaal eens') && ($scoresAll['84']['medianAnswer']=='eens' || $scoresAll['84']['medianAnswer']=='helemaal eens') && ($scoresAll['85']['medianAnswer']=='eens' || $scoresAll['85']['medianAnswer']=='helemaal eens'))
            <div class="feedback goed">@include('werkgevers/snippet-overz-82-2-feedback')</div>
        @endif

    </div>

</div>


<div class="row  detail-results light">

    <div class="large-8 large-offset-2 end columns">

        <h5 class="verdiep">Verdieping 4</h5>

        <h4 class="title-block" style="background-image:url('/images/icons/face{{{ $scoresLevelFourEvaluation }}}.png')">Werk, werkgemeenschap en leiding</h4>

        <div class="feedback analyse">
            <script>

                $(document).ready(function() {

                        /**
                         * Grid-light theme for Highcharts JS
                         * @author Torstein Honsi
                         */

                        // Load the fonts
                        Highcharts.createElement('link', {
                            href: 'http://fonts.googleapis.com/css?family=Dosis:400,600',
                            rel: 'stylesheet',
                            type: 'text/css'
                        }, null, document.getElementsByTagName('head')[0]);

                        Highcharts.theme = {
                            colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
                                "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
                            chart: {
                                backgroundColor: null,
                                style: {
                                    fontFamily: "Dosis, sans-serif"
                                }
                            },
                            title: {
                                style: {
                                    fontSize: '16px',
                                    fontWeight: 'bold',
                                    textTransform: 'uppercase'
                                }
                            },
                            tooltip: {
                                borderWidth: 0,
                                backgroundColor: 'rgba(219,219,216,0.8)',
                                shadow: false
                            },
                            legend: {
                                itemStyle: {
                                    fontWeight: 'bold',
                                    fontSize: '13px'
                                }
                            },
                            xAxis: {
                                gridLineWidth: 1,
                                labels: {
                                    style: {
                                        fontSize: '12px'
                                    }
                                }
                            },
                            yAxis: {
                                minorTickInterval: 'auto',
                                min: 0,
                                max: 100,
                                title: {
                                    style: {
                                        textTransform: 'uppercase'
                                    }
                                },
                                labels: {
                                    style: {
                                        fontSize: '12px'
                                    }
                                }
                            },
                            plotOptions: {
                                candlestick: {
                                    lineColor: '#404048'
                                }
                            },


                            // General
                            background2: '#F0F0EA'

                        };

                            // Apply the theme
                        Highcharts.setOptions(Highcharts.theme);

                    $(function () {
                        $('#werkgever-lasten').highcharts({
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: 'Ergonomisch risico'
                        },
                        xAxis: {
                            categories: ['Zware lasten', 'Repetitieve beweging', 'Zelfde houding', 'Beeldschermwerk', 'Trillend gereedschap werken', 'Extreme temperatuur', 'Slecht verlichte lokalen', 'Slecht geventileerde lokalen', 'Lawaaierige omgeving']
                        },
                        credits: {
                                    enabled: false
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: '%'
                            }
                        },
                        legend: {
                            reversed: true
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                        series: [{
                            name: 'eens per week',
                            data: [ {{$scoresAll['86']['answers']['380']['countpercentage']}},
                                    {{$scoresAll['87']['answers']['385']['countpercentage']}},
                                    {{$scoresAll['88']['answers']['390']['countpercentage']}},
                                    {{$scoresAll['89']['answers']['395']['countpercentage']}},
                                    {{$scoresAll['90']['answers']['400']['countpercentage']}},
                                    {{$scoresAll['91']['answers']['405']['countpercentage']}},
                                    {{$scoresAll['92']['answers']['410']['countpercentage']}},
                                    {{$scoresAll['93']['answers']['415']['countpercentage']}},
                                    {{$scoresAll['94']['answers']['420']['countpercentage']}}],
                            color: '#ebc21c'
                        }, {
                            name: 'een paar keer per week',
                            data: [ {{$scoresAll['86']['answers']['381']['countpercentage']}},
                                    {{$scoresAll['87']['answers']['386']['countpercentage']}},
                                    {{$scoresAll['88']['answers']['391']['countpercentage']}},
                                    {{$scoresAll['89']['answers']['396']['countpercentage']}},
                                    {{$scoresAll['90']['answers']['401']['countpercentage']}},
                                    {{$scoresAll['91']['answers']['406']['countpercentage']}},
                                    {{$scoresAll['92']['answers']['411']['countpercentage']}},
                                    {{$scoresAll['93']['answers']['416']['countpercentage']}},
                                    {{$scoresAll['94']['answers']['421']['countpercentage']}}],
                                color: '#eb7e1c',
                            }, {
                            name: 'dagelijks',
                            data: [ {{$scoresAll['86']['answers']['382']['countpercentage']}},
                                    {{$scoresAll['87']['answers']['387']['countpercentage']}},
                                    {{$scoresAll['88']['answers']['392']['countpercentage']}},
                                    {{$scoresAll['89']['answers']['397']['countpercentage']}},
                                    {{$scoresAll['90']['answers']['402']['countpercentage']}},
                                    {{$scoresAll['91']['answers']['407']['countpercentage']}},
                                    {{$scoresAll['92']['answers']['412']['countpercentage']}},
                                    {{$scoresAll['93']['answers']['417']['countpercentage']}},
                                    {{$scoresAll['94']['answers']['422']['countpercentage']}}],
                            color: '#ff0000',
                        }]
                        });
                    });

                    $(function () {
                        $('#werkgever-werkplekinrichting').highcharts({
                                chart: {
                                    type: 'bar'
                                },
                                title: {
                                    text: 'werkplekinrichting'
                                },
                                xAxis: {
                                    categories: ['Goede werkplekinrichting', 'Instelbaar meubilair', 'Hulpmiddelen zoals laptophouder, ...']
                                },
                                credits: {
                                    enabled: false
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: '%'
                                    }
                                },
                                legend: {
                                    reversed: true
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal'
                                    }
                                },
                                series: [{
                                    name: 'Ja',
                                    data: [ {{$scoresAll['95']['answers']['423']['countpercentage']}},
                                            {{$scoresAll['96']['answers']['425']['countpercentage']}},
                                            {{$scoresAll['97']['answers']['427']['countpercentage']}}],
                                    color: '#6cc147'
                                    }, {
                                    name: 'nee',
                                    data: [  {{$scoresAll['95']['answers']['424']['countpercentage']}},
                                             {{$scoresAll['96']['answers']['426']['countpercentage']}},
                                             {{$scoresAll['97']['answers']['428']['countpercentage']}}],
                                    color: '#eb521c'
                                    }
                                ]
                        });
                    });

                    $(function () {
                        $('#werkgever-risicovermijden').highcharts({
                                chart: {
                                    type: 'bar'
                                },
                                title: {
                                    text: 'Mogelijkheid tot opleiding om risico te vermijden'
                                },
                                xAxis: {
                                    categories: ['Goede Mogelijkheid']
                                },
                                credits: {
                                    enabled: false
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: '%'
                                    }
                                },
                                legend: {
                                    reversed: true
                                },
                                plotOptions: {
                                    series: {
                                        stacking: 'normal'
                                    }
                                },
                                series: [{
                                        name: 'Ja',
                                        data: [{{$scoresAll['98']['answers']['429']['countpercentage']}}],
                                        color: '#6cc147'
                                    }, {
                                        name: 'nee',
                                        data: [{{$scoresAll['98']['answers']['430']['countpercentage']}}],
                                        color: '#eb521c'
                                }
                                ]
                        });
                    });

                    });
            </script>

            <div id="werkgever-lasten" style="min-width: 300px; max-width: 800px; height: 550px; margin: 0 auto"></div>

        <!--    <strong>86 Zware lasten tillen, dragen, trekken of duwen? g</strong>
            {{$scoresAll['86']['answers']['380']['countpercentage']}} eens per week
            {{$scoresAll['86']['answers']['381']['countpercentage']}} een paar keer per week
            {{$scoresAll['86']['answers']['382']['countpercentage']}} dagelijks

            <strong>87 Repetitieve (herhaaldelijk dezelfde) bewegingeng</strong>
            {{$scoresAll['87']['answers']['385']['countpercentage']}} eens per week
            {{$scoresAll['87']['answers']['386']['countpercentage']}} een paar keer per week
            {{$scoresAll['87']['answers']['387']['countpercentage']}} dagelijks

            <strong>88 Langdurig zelfde houding</strong>
            {{$scoresAll['88']['answers']['390']['countpercentage']}} eens per week
            {{$scoresAll['88']['answers']['391']['countpercentage']}} een paar keer per week
            {{$scoresAll['88']['answers']['392']['countpercentage']}} dagelijks

            <strong>89 Beeldschermwerk</strong>
            {{$scoresAll['89']['answers']['395']['countpercentage']}} eens per week
            {{$scoresAll['89']['answers']['396']['countpercentage']}} een paar keer per week
            {{$scoresAll['89']['answers']['397']['countpercentage']}} dagelijks

            <strong>90 Met trillend gereedschap werkeng</strong>
            {{$scoresAll['90']['answers']['400']['countpercentage']}} eens per week
            {{$scoresAll['90']['answers']['401']['countpercentage']}} een paar keer per week
            {{$scoresAll['90']['answers']['402']['countpercentage']}} dagelijks

            <strong>91 Bij te hoge of te lage temperatuur werkeng</strong>
            {{$scoresAll['91']['answers']['405']['countpercentage']}} eens per week
            {{$scoresAll['91']['answers']['406']['countpercentage']}} een paar keer per week
            {{$scoresAll['91']['answers']['407']['countpercentage']}} dagelijks

            <strong>92 Slecht verlichte lokaleng</strong>
            {{$scoresAll['92']['answers']['410']['countpercentage']}} eens per week
            {{$scoresAll['92']['answers']['411']['countpercentage']}} een paar keer per week
            {{$scoresAll['92']['answers']['412']['countpercentage']}} dagelijks

            <strong>93 slecht geventileerde</strong>
            {{$scoresAll['93']['answers']['415']['countpercentage']}} eens per week
            {{$scoresAll['93']['answers']['416']['countpercentage']}} een paar keer per week
            {{$scoresAll['93']['answers']['417']['countpercentage']}} dagelijks

            <strong>94 Lawaaierige omgeving</strong>
            {{$scoresAll['94']['answers']['420']['countpercentage']}} eens per week
            {{$scoresAll['94']['answers']['421']['countpercentage']}} een paar keer per week
            {{$scoresAll['94']['answers']['422']['countpercentage']}} dagelijks
            -->

            <?php
            $blootstelling = $scoresAll['86']['medianPoints']+$scoresAll['87']['medianPoints']+$scoresAll['88']['medianPoints']+$scoresAll['89']['medianPoints']+$scoresAll['90']['medianPoints']+$scoresAll['91']['medianPoints']+$scoresAll['92']['medianPoints']+$scoresAll['93']['medianPoints']+$scoresAll['94']['medianPoints'];
            ?>

            <?php if ($blootstelling > 0) { ?>
                <div class="action">@include('werkgevers/snippet-overz-86-feedback')</div>
            <?php } ?>

        </div>

        <div class="feedback analyse">
            <div id="werkgever-werkplekinrichting" style="min-width: 300px; max-width: 800px; height: 450px; margin: 0 auto"></div>

           <!-- op vraag 95 {{$scoresAll['95']['answers']['423']['countpercentage']}} % WN dat “ja heeft geantwoord
            {{$scoresAll['95']['answers']['424']['countpercentage']}} % WN dat “nee” heeft geantwoord<br>
            op vraag 96 {{$scoresAll['96']['answers']['425']['countpercentage']}} % WN dat “ja heeft geantwoord
            {{$scoresAll['96']['answers']['426']['countpercentage']}} % WN dat “nee” heeft geantwoord<br>
            op vraag 97 {{$scoresAll['97']['answers']['427']['countpercentage']}} % WN dat “ja heeft geantwoord
            {{$scoresAll['97']['answers']['428']['countpercentage']}} % WN dat “nee” heeft geantwoord<br>
            -->
            <!-- indien > 50 % “nee” heeft geantwoord -->
            <?php
            $ergonomisch = ($scoresAll['95']['answers']['424']['countpercentage']+$scoresAll['96']['answers']['426']['countpercentage']+$scoresAll['97']['answers']['428']['countpercentage'])/3;
            ?>
            <?php if ($ergonomisch > 50) { ?>
                <div class="action">@include('werkgevers/snippet-overz-95-feedback')</div>
            <?php } ?>
        </div>

        <div class="feedback analyse">
            <div id="werkgever-risicovermijden" style="min-width: 300px; max-width: 800px; height: 250px; margin: 0 auto"></div>

            <!-- {{$scoresAll['98']['answers']['429']['countpercentage']}} % WN dat “ja “heeft geantwoord
            {{$scoresAll['98']['answers']['430']['countpercentage']}} % WN dat “nee” heeft geantwoord -->

            <?php if ($scoresAll['98']['answers']['430']['countpercentage'] > 49) { ?>
                <div class="action slecht">@include('werkgevers/snippet-overz-98-2-feedback')</div>
            <?php } else { ?>
                <div class="action goed">@include('werkgevers/snippet-overz-98-1-feedback')</div>
            <?php } ?>
        </div>

    </div>
</div> <!-- end row -->


<div class="row detail-results">
    <div class="large-8 large-offset-2 end columns">
        <h5 class="verdiep">Het dak</h5>
        <h4 class="title-block" style="background-image:url('/images/icons/face{{{ $scoresLevelFiveEvaluation }}}.png')">Werkvermogen</h4>

        <?php
        $werkvermogenScore = $scoresAll['29']['medianPoints']+$scoresAll['30']['medianPoints']+$scoresAll['31']['medianPoints']+$scoresAll['32']['medianPoints'];
        ?>

        @if($werkvermogenScore <= 9)
            <div class="feedback">@include('werkgevers/snippet-overz-27-3-feedback')</div>
        @elseif ($werkvermogenScore > 15)
            <div class="feedback">@include('werkgevers/snippet-overz-27-1-feedback')</div>
        @else
            <div class="feedback">@include('werkgevers/snippet-overz-27-2-feedback')</div>
        @endif

        @if($scoresAll['30']['medianPoints'] > $scoresAll['31']['medianPoints'])
            <div class="feedback">@include('werkgevers/snippet-overz-27-4-feedback')</div>
        @endif
        @if($scoresAll['31']['medianPoints'] > $scoresAll['30']['medianPoints'])
            <div class="feedback">@include('werkgevers/snippet-overz-27-5-feedback')</div>
        @endif
        @if(($scoresAll['30']['medianPoints'] == $scoresAll['31']['medianPoints']))
            <div class="feedback">@include('werkgevers/snippet-overz-27-6-feedback')</div>
        @endif


    </div>
</div>


</div> <!-- END PANEL2-->


<div class="content" id="panel3">
    over te nemen
</div> <!-- END PANEL3-->

</div> <!-- END TABS CONTENT-->


</div> <!-- end large 12 columns -->




@stop