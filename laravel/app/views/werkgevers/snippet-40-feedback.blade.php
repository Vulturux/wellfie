Financiële tegemoetkomingen bij aanwerving van oudere medewerkers is een complexe materie. U kan
hieromtrent algemene informatie terugvinden op <a href="http://www.socialezekerheid.be">www.socialezekerheid.be</a>. Voor een individuele case
kan u terecht bij verschillende organisaties die juridisch advies aanbieden.