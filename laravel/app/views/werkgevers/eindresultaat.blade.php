@extends('layoutsWerkgever.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">{{{ $title }}} vragen werkgever</h3>
</div>


@include('werkgevers.eindresultaat-include', array('scoresBeleidFeedback'=>$scoresBeleidFeedback))
<div class="button-container text-center">
    <ul class="button-group">
        <li><a href="/werkgevers/dashboard" class="button main"><i class="fi-play"></i> Naar het dashboard</a></li>
    </ul>
</div>





@stop
