@extends('layoutsSuperadmin.master')

@section('content')

<h1>All answers</h1>

{{ link_to_route('answers.edit', 'Edit',  array($answer->id), array('class' => 'button tiny')) }}
{{ link_to_route('answers.index', 'Back to overview',  array(), array('class' => 'button tiny')) }}

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>answer_id</th>
            <td>{{ $answer->id }}</td>
        </tr>
        <tr>
            <th>question</th>
            <td>{{ $answer->question_id }}  - {{ $answer->question->question }}</td>
        </tr>
        <tr>
            <th>answer</th>
            <td>{{ $answer->answer }}</td>
        </tr>
        <tr>
            <th>order</th>
            <td>{{ $answer->order }}</td>
        </tr>
        <tr>
            <th>points</th>
            <td>{{ $answer->points }}</td>
        </tr>
    </thead>

</table>


@stop
