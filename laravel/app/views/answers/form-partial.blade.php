
        <div class="row">
            <div class="small-3 columns">Question</div>
            <div class="small-9 columns">
                {{ Form::select('questions_id', $questionslist, $answer->questions_id ) }}
            </div>
        </div> <!-- end row -->

        <div class="row">
            <div class="small-3 columns">{{ Form::label('answer', 'answer:') }}</div>
            <div class="small-9 columns">{{ Form::text('answer') }}
                @if ($errors->has('answer'))
                <small class="error"><?php echo  $errors->first('answer')  ?></small>
                @endif
            </div>
        </div> <!-- end row -->


        <div class="row">
            <div class="small-3 columns">{{ Form::label('points', 'points:') }}</div>
            <div class="small-9 columns">{{ Form::text('points') }}
                @if ($errors->has('answer_nr'))
                <small class="error"><?php echo  $errors->first('answer_nr')  ?></small>
                @endif
            </div>
        </div> <!-- end row -->

        <div class="row">
            <div class="small-3 columns">{{ Form::label('order', 'order:') }}</div>
            <div class="small-9 columns"> {{ Form::text('order') }}
                @if ($errors->has('order'))
                <small class="error"><?php echo  $errors->first('order')  ?></small>
                @endif
            </div>
        </div> <!-- end row -->



