@extends('layoutsSuperadmin.master')


@section('content')

    <h1>Answers overview</h1>

    <p>{{ link_to_route('answers.create', 'Add new answer', array(), array('class' => 'button success tiny')) }}</p>

    @if ($answers->count())
        <table class=" table-striped table-bordered">
            <thead>
            <tr>
                <th>id</th>
                <th>question</th>
                <th>answer</th>
                <th>order / pnt</th>
                <th></th>
            </tr>
            </thead>

            <tbody>
            @foreach ($answers as $answer)
                <tr>
                    <td>{{ $answer->id }}</td>
                    <td>{{ $answer->questions_id }} {{ $answer->question->question }} </td>
                    <td>{{ $answer->answer }}</td>
                    <td>{{ $answer->order }} {{ $answer->points }}</td>
                    <td>{{ link_to_route('answers.show', 'Show',  array($answer->id), array('class' => 'button tiny')) }}
                    {{ link_to_route('answers.edit', 'Edit',  array($answer->id), array('class' => 'button tiny')) }}
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('answers.destroy', $answer->id))) }}
                            {{ Form::submit('Delete', array('class'=> 'button tiny alert')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>

        {{ $answers->links() }}

    @else
        There are no answers
    @endif

@stop
