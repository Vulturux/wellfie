@extends('layoutsSuperadmin.master')

@section('content')

    <h1>Edit Answer</h1>

    @if ($errors->any())
         @include('answers/form-errors')
    @endif

    {{ Form::model($answer, array('method' => 'PATCH', 'route' => array('answers.update', $answer->id))) }}

    <div class="form">

        @include('answers/form-partial')

        <div class="row">
             <div class="small-12 columns action-buttons">
                 {{ link_to_route('answers.show', 'Cancel', $answer-> id, array('class' => 'button secondary')) }}
                 {{ Form::submit('Update', array('class' => 'button success')) }}

             </div>
         </div> <!-- end row -->
    </div>
    {{ Form::close() }}

@stop
