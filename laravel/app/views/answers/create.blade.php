@extends('layoutsSuperadmin.master')


@section('content')

    <h1>Create Answer</h1>

    @if ($errors->any())
        @include('answers/form-errors')
    @endif


    {{ Form::open(array('route' => 'answers.store')) }}

        <div class="form">

            @include('answers/form-partial')

            <div class="row">
                <div class="small-12 columns action-buttons">
                     {{ Form::submit('Submit', array('class' => 'button')) }}
                </div>
            </div>
        </div>

    {{ Form::close() }}

@stop
