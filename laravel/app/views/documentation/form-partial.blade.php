<div class="row">
    <div class="small-3 columns">{{ Form::label('title', 'titel:') }}</div>
    <div class="small-9 columns">{{ Form::text('title') }}
        @if ($errors->has('title'))
            <small class="error"><?php echo $errors->first('title')  ?></small>
        @endif
    </div>
</div> <!-- end row -->


<div class="row">
    <div class="small-3 columns">{{ Form::label('description', 'omschrijving:') }}</div>
    <div class="small-9 columns">{{ Form::text('description') }}
        @if ($errors->has('description'))
            <small class="error"><?php echo $errors->first('description')  ?></small>
        @endif
    </div>
</div> <!-- end row -->

<div class="row type-row">
    <div class="small-3 columns">{{ Form::label('type', 'type:') }}</div>
    <div class="small-9 columns"> {{  Form::select('type', array('' => '- Kies type -', 'brochure' => 'Voorbeelddocument', 'afbeelding' => 'Campagnemateriaal', 'video' => 'Video'))}}
        @if ($errors->has('type'))
            <small class="error"><?php echo $errors->first('type')  ?></small>
        @endif
    </div>
</div> <!-- end row -->

<div class="row link-row">
    <div class="small-3 columns">{{ Form::label('link', 'link:') }}</div>
    <div class="small-9 columns">
        <div class="hide type" data-type="url">{{ Form::text('link') }}</div>
        <div class="hide type" data-type="file">{{ Form::file('link') }}</div>
        @if ($errors->has('link'))
            <small class="error"><?php echo $errors->first('link')  ?></small>
        @endif
    </div>
</div> <!-- end row -->

<div class="row">
    <div class="small-3 columns"></div>
    <div class="small-9 columns embed">
        @if(isset($documentation))
            @if($documentation->type == 'video')
                {{$documentation->link}}
            @else
                /documentatie/file/{{$documentation->link}}
            @endif
        @endif
    </div>
</div> <!-- end row -->


<script>
    $(document).ready(function () {
        doSelectMagic();
        setEvents();

        videoEmbed.invoke($('.embed'));
    });

    function setEvents() {
        var $typeSelect = $('.type-row').find('select');
        $typeSelect.change(function (e) {
            doSelectMagic();

        });
    }

    function doSelectMagic() {
        var $typeSelect = $('.type-row').find('select');
        var $linkRow = $('.link-row');
        var $linkLabel = $linkRow.find('label');

        var $selected = $typeSelect.find('option:selected');

        if ($selected.val() == '') {
            showHideLinkType('');
        }
        else if ($selected.val() == 'video') {
            showHideLinkType('url');
            $linkLabel.text('url:');
        } else {
            showHideLinkType('file');
            $linkLabel.text('bestand:');
        }

    }

    function showHideLinkType(type) {
        var $linkRow = $('.link-row');

        if (type == '') {
            $linkRow.addClass('hide');
        } else {
            $linkRow.removeClass('hide');

            var $linkTypes = $linkRow.find('.type');

            $linkTypes.each(function (i, el) {
                var $el = $(el);
                if ($el.data('type') === type) {
                    $el.removeClass('hide');
                    $el.find('input').prop('disabled', false);

                } else {
                    $el.addClass('hide');
                    $el.find('input').prop('disabled', true);

                }
            })
        }

    }
</script>
