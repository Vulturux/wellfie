<div class="error-message">
    {{ implode('', $errors->all('<div class="error">:message </div> <!-- end row -->')) }}
</div>