@extends('layoutsSuperadmin.master')


@section('content')

    <div class="box-header clearfix">
        <h3 class="main-title">Documentatie aanmaken</h3>
    </div>

    <div class="row superadmin">

        <div class="large-12 columns">

            <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
            <script>
                tinymce.init({
                    plugins: ["code","link"],
                    selector:'textarea',
                    menu: [ ],
                    toolbar: [
                        "bold italic | link  | bullist numlist | list | code"
                    ]
                });

            </script>

            @if ($errors->any())
                @include('documentation/form-errors')
            @endif


            {{ Form::open(array('route' => 'superadmin.documentation.store', 'files' => true)) }}

                <div class="form">

                    @include('documentation/form-partial')

                    <div class="row">
                        <div class="small-12 columns action-buttons">
                             {{ Form::submit('Opslaan', array('class' => 'button')) }}
                        </div>
                    </div>
                </div>

            {{ Form::close() }}

        </div>
    </div> <!-- end row -->

@stop
