@extends('layoutsSuperadmin.master')


@section('content')

    <div class="box-header clearfix">
        <h3 class="main-title">Documentation overzicht</h3>
    </div>

    <div class="row superadmin">

        <div class="large-12 columns">

            <p>{{ link_to_route('superadmin.documentation.create', 'Nieuwe documentatie toevoegen', array(), array('class' => 'button success tiny')) }}</p>

            @if ($documentation->count())
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th width="5%">id</th>
                        <th width="30%">titel &amp; omschrijving</th>
                        <th>type</th>
                        <th width="30%"></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($documentation as $doc)
                        <tr>
                            <td>{{ $doc->id }}</td>
                            <td>
                                <h4>{{ $doc->title }}</h4>
                                {{ $doc->description }}
                            </td>
                            <td>
                                {{-- */$arr = array('brochure' => 'Voorbeelddocument', 'afbeelding' => 'Campagnemateriaal', 'video' => 'Video');/* --}}
                                {{ $arr[$doc->type] }}
                            </td>
                            <td>
                                {{ link_to_route('superadmin.documentation.show', 'Aanpassen',  array($doc->id), array('class' => 'button tiny')) }}

                                {{ Form::open(array('method' => 'DELETE', 'route' => array('superadmin.documentation.destroy', $doc->id), 'class' => 'delete')) }}
                                {{ Form::submit('Verwijderen', array('class'=> 'button tiny alert')) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>

                {{ $documentation->links() }}

            @else
                Nog geen documentatie in de database
             @endif

        </div>
    </div> <!-- end row -->
@stop
