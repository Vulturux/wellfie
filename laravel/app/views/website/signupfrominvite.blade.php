@extends('layoutsWebsite.master')


@section('content')

<div class="home-content-block hoewerkthet" style="background-color: #FFF">
    <div class="row">
        <div class="large-12 columns">
            <h2>SIGN UP WERKNEMERS VIA INVITE VOOR {{ $email }}</h2>

            <form method="POST" action="{{{ URL::to('/users/storefrominvite/') }}}" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">


                <fieldset>

                    @if (Session::get('error'))
                        <div class="alert alert-error alert-danger">
                            @foreach (Session::get('error') as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif

                    @if (Session::get('notice'))
                    <div class="alert">{{ Session::get('notice') }}</div>
                    @endif


                    <div class="form-group">
                        <label for="email">{{{ Lang::get('confide::confide.e_mail') }}} </label>
                        {{ $email }}
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="username">{{{ Lang::get('confide::confide.username') }}} <i>(Een gebruikersnaam mag alleen letters, cijfers,  _ of  -  bevatten maar geen spaties)</i></label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.username') }}}" type="text" name="username" id="username" value="{{{ Input::old('username') }}}">
                    </div>
                    <div class="form-group">
                        <label for="password">{{{ Lang::get('confide::confide.password') }}}</label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">{{{ Lang::get('confide::confide.password_confirmation') }}}</label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation">
                    </div>

                    <div class="form-group  js-group-disclaimer-checkbox">
                        <input id="disclaimer-checkbox" type="checkbox"><label for="checkbox">Ik ga akkoord met <a href="#" data-reveal-id="myModal">de algemene voorwaarden</a> omtrent de vertrouwelijkheid.</label>
                    </div>

                    {{ Form::hidden('email', $email) }}
                    {{ Form::hidden('invitecode', $invitecode) }}
                    {{ Form::hidden('invitecodeId', $invitecodeId) }}

                    <div class="form-actions form-group">
                        <div class="false-button" style="display:block; height:70px;">
                            <a href="#" class="btn btn-primary btn-submit js-btn-submit-disabled" style="background-color: #EFEFEF; padding: 20px 30px; position:relative; top: 15px;">{{{ Lang::get('confide::confide.signup.submit') }}}</a>
                        </div>
                        <div class="main-button" style="display:none;">
                            <button type="submit" class="btn btn-primary btn-submit" >{{{ Lang::get('confide::confide.signup.submit') }}}</button>
                        </div>
                    </div>


                </fieldset>
            </form>


        </div>  <!-- end large-12  -->
    </div> <!-- end row -->
</div>

<div id="myModal" class="reveal-modal" data-reveal="" style="height:350px; overflow-y: scroll;">
    <h2>Disclaimer Wellfie Werknemer</h2>
    <div class="disclaimer-text">
        <ol>
            <li>Groep IDEWE, gevestigd te Interleuvenlaan 58, 3001 Leuven  treedt op als <strong>verantwoordelijke voor de verwerking</strong> van de door u meegedeelde gegevens.  </li>
            <li>Strikte vertrouwelijkheid zal worden gewaarborgd aan de deelnemers.  Alle gegevens vallen onder het beroepsgeheim en zullen enkel gebruikt worden voor het leveren van
            persoonlijke feedback en in groepsanalyses, waarbij individuele personen niet identificeerbaar zullen zijn.  Individuele gegevens zullen onder geen enkel beding aan uw werkgever of andere
                werknemers van uw organisatie of aan andere derden buiten IDEWE doorgespeeld worden.  Groep IDEWE waakt over de naleving van het beroepsgeheim en heeft procedures opgesteld waardoor
                de vertrouwelijkheid gewaarborgd wordt. Iedere persoon betrokken bij de gegevensinvoer en -analyse is aan de confidentialiteitsregel onderworpen. Deze verwerkingen zijn geregistreerd bij de
                Commissie ter bescherming van de persoonlijke levenssfeer (<a href="http://www.privacycommission.be">www.privacycommission.be</a>) onder de codes: verwerker HM 00085419 en verwerkingen VT 005023692, VT 0050223690,
            VT 005026464 en VT 000363056. Dit studieprotocol werd voor advies voorgelegd aan de Commissie voor Medische Ethiek OG nr. 117. De gegevens blijven eigendom van groep IDEWE.    </li>
            <li>De gegevens zullen uitsluitend worden verwerkt voor de volgende <strong>doeleinden</strong>: het onderzoeken van het werkvermogen van ondernemingen en individuele werknemers . De aangehaalde
            doeleinden steunen op de wet van 4 augustus 1996 betreffende het welzijn van de werknemers bij hun werk en de uitvoeringsbesluiten van deze wet, op het Algemeen Reglement
            op de Arbeidsbescherming en op CAO 104 “Uitvoering werkgelegenheidsplan oudere werknemers in de onderneming”.  De gegevens worden bewaard en kunnen later gebruikt worden
            voor wetenschappelijk onderzoek betreffende het werkvermogen van ondernemingen en werknemers. Deze onderzoeksresultaten kunnen worden gepubliceerd,
            maar het betreft enkel resultaten van grote groepen werknemers en werkgevers waarin noch ondernemingen noch individuele werknemers herkenbaar zullen zijn.</li>
            <li>De verwerking van <strong>gegevens betreffende de gezondheid</strong> gebeurt steeds onder de verantwoordelijkheid van de bevoegde beoefenaar van de gezondheidszorg en conform de wetgeving verwerking van persoonsgegevens. </li>
            <li>De gegevens zijn <strong>toegankelijk</strong> voor de volgende categorieën van <strong>ontvangers</strong>: de onderzoekers van groep IDEWE.  Bij
                IDEWE kan u op eenvoudig verzoek een lijst bekomen van de betrokken ontvangers.
                Al deze categorieën van ontvangers zijn gebonden aan een wettelijke of contractuele geheimhoudingsplicht. </li>
            <li><strong>Indien uw gegevens terugvoerbaar zijn</strong>, heeft u het recht <strong>mededeling</strong> te verkrijgen van de u betreffende gegevens.  Indien deze onjuist, onvolledig of niet
                relevant (meer) zijn, kan u de <strong>verbetering  of verwijdering ervan vragen</strong>. Indien u van dit recht gebruik wenst te maken, kan u een schriftelijk gedagtekend
                en ondertekend verzoek – vergezeld van een kopie van de voor- en achterzijde van uw identiteitskaart – richten aan groep IDEWE , Interleuvenlaan 58, 3001 Leuven.
                U kan het verzoek ter plaatse bij groep IDEWE overhandigen of via de post of per e-mail toesturen aan groep IDEWE.</li>
        </ol>

    </div>
</div>

@stop
