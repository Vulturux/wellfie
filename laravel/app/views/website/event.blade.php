@extends('layoutsWebsite.master')


@section('content')


    <div class="home-content-block website-page faq">
        <div class="row">
            <div class="large-8 large-offset-2 columns">
                <a href="/nieuws" class="right button button-flat tiny">Terug naar overzicht</a>

                <h1>{{$event->title}}</h1>
                <p class="event-datetime">{{$event->datetime->formatLocalized('%A %d %B %Y')}}</p>
                <p>{{$event->description}}</p>


            </div>  <!-- end large-10  -->
        </div> <!-- end row -->
    </div>

@stop
