@extends('layoutsWebsite.master')


@section('content')


<div class="home-content-block website-page wat">
    <div class="row">
        <div class="large-12 columns">

            <div class="row">
                <div class=" medium-8 large-offset-2 end columns">
                    <h2>Wat is werkvermogen?</h2>

                    <p>Het werkvermogen geeft aan in welke mate een medewerker zowel lichamelijk als geestelijk in
                        staat is om zijn huidig werk uit te voeren. Het werkvermogen wordt bepaald door de balans
                        tussen individuele kenmerken (gezondheid, competenties, waarden en houding) en de eisen die door het werk worden gesteld.
                        Wanneer er een goed evenwicht is, zal het werkvermogen dus ook goed zijn.</p>

                    <p>Het "huis van het werkvermogen" (Ilmarinen, 2005) geeft een visuele voorstelling van de
                        onderliggende factoren die het niveau van het werkvermogen bepalen en is daarom het ideale raamwerk voor onze tool "Wellfie".
                        Het werkvermogen bevindt zich helemaal bovenaan
                        in het dak van het huis en steunt op vier verdiepingen die samen het werkvermogen beïnvloeden.
                    </p>

                    <h4>Gelijkvloers =<br> <span class="light">lichamelijke en geestelijke gezondheid</span></h4>
                     <p>Representeert de gezondheid en functionele capaciteiten van de medewerker. Beide eigenschappen vormen de basisverdieping van het huis.
                       Een goede lichamelijke en psychische gezondheid is noodzakelijk om het werk naar behoren te kunnen uitvoeren.</p>

                    <h4>Eerste verdieping =<br> <span class="light">competenties</span></h4>
                    <p>De medewerker moet over de geschikte kennis en vaardigheden beschikken om zijn taken te kunnen uitoefenen.
                           Het is van belang dat een medewerker zich deskundig voelt in zijn dagelijkse werkopdrachten.</p>

                    <h4>Tweede verdieping<br> <span class="light">waarden, houding en motivatie</span></h4>
                    <p>Belichaamt de waarden, houding en motivatie waarmee een medewerker zijn job uitoefent. Wanneer hij
                             zijn taken waardevol en nuttig vindt en gemotiveerd is, dan zal hij de job langer kunnen en willen
                             uitoefenen. Deze verdieping beschikt bovendien ook over een balkon, wat symbool staat voor de invloed
                             van de persoonlijke omgeving op de waarden, houding en motivatie van de medewerker. Datgene wat hij belangrijk vindt en hoe sterk
                             hij voor een bepaalde taak gemotiveerd is, wordt sterk beïnvloed door de directe sociale omgeving (familie, vrienden, gezin, ...).</p>

                    <h4>Derde verdieping<br> <span class="light">werkinhoud en -eisen, werkomgeving en -organisatie, collega’s en manier van leidinggeven</span></h4>
                    <p>Deze verdieping wordt voornamelijk bepaald en meest direct beïnvloed door de onderneming.</p>

                    <h4>Trappen</h4>
                    <p>De verschillende verdiepingen van het huis zijn met elkaar verbonden via trappen. Deze symboliseren de interactie en invloed tussen de verdiepingen.</p>

                    <h4>Dak<br> <span class="light">het werkvermogen</span></h4>

                    Het dak, zal pas stevig zijn als ook de vier onderliggende verdiepingen degelijk en goed onderhouden zijn.
                    Wanneer één of meerdere verdiepingen aan renovatie toe zijn, zal dit het werkvermogen van de medewerker ondermijnen.
                    <br>
                    <br>
                        <div class="bronnen small">
                            <strong class="small">Bronnen:</strong>
                            <ul>
                                <li class="small">
                                    <a href="http://www.werk.be/sites/default/files/Fiches Huis van werkvermogen LR.pdf">http://www.werk.be/sites/default/files/Fiches Huis van werkvermogen LR.pdf</a>
                                </li>
                                <li class="small">
                                   <a href="http://www.ttl.fi/en/health/wai/multidimensional_work_ability_model/Pages/default.aspx">http://www.ttl.fi/en/health/wai/multidimensional_work_ability_model/Pages/default.aspx</a>

                                </li>
                            </ul>
                        </div>

                </div>
            </div> <!-- end row -->
        </div>  <!-- end large-10  -->
    </div> <!-- end row -->
</div>

@stop
