@extends('layoutsWebsite.master')


@section('content')

<div class="home-content-block hoewerkthet" style="background-color: #FFF">
    <div class="row">
        <div class="large-12 columns">
            <h2>Forgot Password</h2>

            <form method="POST" action="{{ URL::to('/users/forgot_password') }}" accept-charset="UTF-8">

                @if (Session::get('error'))
                <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
                @endif

                @if (Session::get('notice'))
                <div class="alert">{{{ Session::get('notice') }}}</div>
                @endif

                <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

                <div class="form-group">
                    <label for="email">{{{ Lang::get('confide::confide.e_mail') }}}</label>
                    <div class="input-append input-group">
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
            <span class="input-group-btn">
                <input class="btn btn-default" type="submit" value="{{{ Lang::get('confide::confide.forgot.submit') }}}">
            </span>
                    </div>
                </div>


            </form>


        </div>  <!-- end large-12  -->
    </div> <!-- end row -->
</div>


@stop
