@extends('layoutsWebsite.master')


@section('content')


    <div class="home-content-block website-page faq">
        <div class="row">
            <div class="large-12 columns">
                <h2>Documentatie</h2>

                <div class="row">
                    @if( ! $afbeelding->count() == 0)
                        <div class="medium-{{12/$numCols}} columns doc-block">
                            <div class="doc-block-title">
                                <i class="fi-photo large"></i>
                                <span>Campagnemateriaal</span>
                            </div>

                            <div class="doc-block-content">

                                @foreach($afbeelding as $doc)
                                    <a href="/documentatie/file/{{$doc->link}}?download=true" class="doc-block-item"
                                       title="{{$doc->title}}">
                                        <h5>
                                            <i class="fi-photo"></i>
                                            {{$doc->title}}
                                        </h5>
                                        <span class="label secondary round"><i class="fi-download"></i> Downloaden</span>

                                        @if(!empty($doc->description))
                                            <p>{{$doc->description}}</p>
                                        @endif
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if( ! $brochure->count() == 0)
                        <div class="medium-{{12/$numCols}} columns doc-block">
                            <div class="doc-block-title">
                                <i class="fi-page-copy"></i>
                                <span>Voorbeelddocumenten</span>
                            </div>

                            <div class="doc-block-content">
                                @foreach($brochure as $doc)
                                    <a href="/documentatie/file/{{$doc->link}}?download=true" class="doc-block-item"
                                       title="{{$doc->title}}">
                                        <h5>
                                            <i class="fi-page-copy"></i>
                                            {{$doc->title}}
                                        </h5>
                                        <span class="label secondary round"><i class="fi-download"></i> Downloaden</span>

                                        @if(!empty($doc->description))
                                            <p>{{$doc->description}}</p>
                                        @endif

                                    </a>

                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if( ! $video->count() == 0)
                        <div class="medium-{{12/$numCols}} columns doc-block">
                            <div class="doc-block-title">
                                <i class="fi-video"></i>
                                <span>Video</span>
                            </div>

                            <div class="doc-block-content">
                                @foreach($video as $doc)
                                    <a href="{{$doc->link}}" target="_blank" class="doc-block-item"
                                       title="{{$doc->title}}">
                                        <h5>
                                            <i class="fi-video"></i>
                                            {{$doc->title}}
                                        </h5>
                                        <span class="label secondary round"><i class="fi-arrow-right"></i> Bekijken</span>

                                        @if(!empty($doc->description))
                                            <p>{{$doc->description}}</p>
                                        @endif

                                    </a>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>  <!-- end large-10  -->
        </div> <!-- end row -->
    </div>

@stop
