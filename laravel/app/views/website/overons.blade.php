@extends('layoutsWebsite.master')


@section('content')


    <div class="home-content-block website-page wat">
        <div class="row">
            <div class="large-12 columns">

                <div class="row">
                    <div class="medium-12 end columns">
                        <h2>Experten achter Wellfie</h2>
                    </div>
                </div>

                <div class="row" style="margin-top: 30px">
                    <div class="medium-6 columns">


                        <div class="overons-logo text-center"><a href="http://www.idewe.be"><img
                                        src="/images/common/idewe-color-big.jpg" alt="Idewe"></a></div>

                        Is een externe dienst voor preventie en bescherming op het werk in België. Een ‘referentie in de
                        preventie’. Als promotor van het
                        ‘Wellfie’-project wil Groep IDEWE een maatschappelijke meerwaarde bieden door het verbeteren van
                        het werkklimaat en het verhogen van de
                        arbeidsparticipatie van medewerkers in alle leeftijdsgroepen. De input voor de onderdelen
                        arbeidsgeneeskunde, ergonomie, psychosociale aspecten
                        en leefgewoonten werd door Groep IDEWE aangebracht.<br><a href="http://www.idewe.be">www.idewe.be</a><br><br>
                    </div>

                    <div class="medium-6 columns">


                        <div class="overons-logo text-center"><a href="http://www.acerta.be"><img
                                        src="/images/common/acerta-color-big.jpg" alt="Acerta"></a></div>

                        Ondersteunt bedrijven en instellingen in hun totale HR-beleid. Als partner in de ontwikkeling
                        van de ‘Wellfie’ wil Acerta bijdragen aan het
                        verhogen van de arbeidstevredenheid en zelfontwikkeling van medewerkers in alle
                        leeftijdsgroepen. De aanbevelingen om een gericht HR-beleid te voeren met aandacht voor
                        competenties en het talent van medewerkers werden door Acerta Consult aangereikt, evenals de
                        informatie over sociaal recht, mogelijke overheidssteun en personeelsfiscaliteit. Meer dan 100
                        medewerkers staan klaar om
                        verder advies, informatie en operationele ondersteuning te verlenen aan ondernemingen bij het
                        uitbouwen en realiseren van een
                        duurzaam personeelsbeleid.<br><a href="http://www.acerta.be">www.acerta.be</a><br>

                    </div>
                </div>
                <div class="row"  style="margin-top: 30px">
                    <div class="medium-6 columns">

                        <div class="overons-logo text-center"><a href="http://www.etion.be"><img
                                        src="/images/common/etion-color-big.jpg" alt="Etion"></a></div>
                        ETION is het forum voor geëngageerd ondernemen. Beslissingsnemers ontmoeten er elkaar en
                        anderen, om ervaring en
                        kennis uit te wisselen over waar we fundamenteel en op lange termijn naar toe willen.
                        Waardengericht ondernemerschap
                        is nodig voor duurzaam succes en vertrekt vanuit een
                        geïntegreerde kijk op individu, organisatie en samenleving.<br>
                        <a href="http://www.etion.be">www.etion.be</a><br>
                        <br>


                    </div>
                </div> <!-- end row -->
            </div>  <!-- end large-10  -->
        </div> <!-- end row -->
    </div>

@stop
