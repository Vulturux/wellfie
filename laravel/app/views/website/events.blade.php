@extends('layoutsWebsite.master')


@section('content')


    <div class="home-content-block website-page faq">
        <div class="row">
            <div class="large-12 columns">
                <h1 class="text-center">Nieuws omtrent Wellfie</h1>

                @if($events->count()>0)
                    @foreach($events as $event)
                    <div class="row event">
                        <div class="large-12 columns">
                            <h3><i class="icon-calendar"></i> {{$event->title}}</h3>
                            <p class="event-datetime">{{$event->datetime->formatLocalized('%A %d %B %Y')}}</p>
                            <p>{{truncateHtml($event->description, 500)}}</p>
                            <p><a href="/nieuws/{{$event->id}}" class="button button-flat small">Lees meer</a></p>
                        </div>
                    </div>
                    @endforeach
                @else
                    <p>Geen nieuws</p>
                @endif
            </div>  <!-- end large-10  -->
        </div> <!-- end row -->
    </div>

@stop
