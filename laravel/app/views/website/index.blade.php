@extends('layoutsWebsite.master')


@section('content')

    <div class="headerpic home" style="background-image: url('images/front-header-images/front-triangle-collage.jpg');">
        <div class="row">
            <div class="headerpic-block">
                <h3>Samen bouwen aan het werkvermogen</h3>
                <h4>Uw medewerkers met goesting aan het werk houden.</h4>
                <div class="button-container"><a href="users/create/" class="btn button">Scan het werkvermogen van uw
                        medewerkers</a></div>
            </div>
        </div>
    </div>


    <div class="home-content-block wat">
        <div class="row">
            <div class="large-12 columns">
                <h2>Wat is wellfie?</h2>
                <div class="row">
                    <div class="large-4 medium-4 columns">
                        <div class="panel">
                            <div class="icon-block">
                                <div class="icon-128 antenna"></div>
                            </div>
                            <h4>Totaalbeeld<br>werkvermogen</h4>
                            De <strong>gratis online tool</strong> Wellfie geeft u als werkgever snel en eenvoudig een
                            totaalbeeld van
                            het werkvermogen binnen uw onderneming. Wellfie helpt het werkvermogen bij de medewerkers te
                            verbeteren, zodat zij zo lang mogelijk productief en met zin aan het werk blijven. Dit is
                            belangrijk in het kader van <strong>werkbaar werk</strong>.

                        </div>
                    </div>
                    <div class="large-4 medium-4 columns ">
                        <div class="panel">
                            <div class="icon-block">
                                <div class="icon-128 steps"></div>
                            </div>
                            <h4>Wellfie scant<br>én adviseert</h4>
                            U als werkgever en elke medewerker die de scan invult, krijgen <strong>meteen aangepaste
                                tips en
                                suggesties</strong> om het werkvermogen te verbeteren. Bouwen aan het werkvermogen is
                            immers een
                            gedeelde verantwoordelijkheid: u biedt kansen en middelen, de medewerker koestert zijn
                            gezondheid en werkt mee aan een goed werkklimaat.

                        </div>
                    </div>
                    <div class="large-4 medium-4 columns">
                        <div class="panel">
                            <div class="icon-block">
                                <div class="icon-128 binocular"></div>
                            </div>
                            <h4>Lange<br>termijn</h4>
                            De resultaten uit de Wellfie groepsrapporten kan u gebruiken als basis voor het verplichte
                            werkgelegenheidsplan in het kader van <strong>cao 104</strong>. Bovendien kan u Wellfie ook op lange termijn
                            inzetten om progressief het <strong>leeftijdsbewust personeels-en welzijnsbeleid</strong> te
                            verbeteren.
                        </div>
                    </div>
                </div> <!-- end row -->
            </div>  <!-- end large-10  -->
        </div> <!-- end row -->
    </div>

    <div class="selfiestick-bg">
        <div class="row">
            <div class="large-5 columns selfiestick text-center">
                <img src="/images/front-header-images/front-selfiestick.png">
            </div>
            <div class="large-7 columns">
                <div class="whitepane">
                    <h2>Belangrijk om weten</h2>
                    <div class="row">
                        <div class="large-12 columns">
                            <ul>
                                <li>Gebruik van Wellfie is volledig gratis!</li>
                                <li>Wellfie is een eenvoudige tool en kan zonder externe hulp worden gebruikt</li>
                                <li>Wellfie bevat zowel een werkgevers- als een medewerkersvragenlijst</li>
                                <li>Het invullen van de vragenlijst duurt ongeveer 20 minuten</li>
                                <li>Resultaten zullen nooit herleid kunnen worden naar individuele medewerkers.
                                    Anonimiteit is verzekerd!
                                </li>
                                <li>Indien er toch iets fout loopt of u heeft een dringende vraag kan u steeds de
                                    helpdesk contacteren via <a href="mailto:info@wellfie.be">info@wellfie.be</a>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>  <!-- end large-10  -->
        </div>


    </div>
    <div class="home-content-block hoewerkthet" style="background-color: #FFF">
        <div class="row">
            <div class="large-12 columns">
                <h2>Hoe werkt het?</h2>
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        Met "Wellfie" bouwen werkgevers en medewerkers samen aan het werkvermogen zodat
                        iedereen met plezier zijn job kan (blijven) uitoefenen. Bouwen of verbouwen hoeft
                        niet complex te zijn. Werkvermogen en de factoren die er een invloed op hebben werden
                        door de Finse professor Ilmarinen voorgesteld als een huis. Het dak is het werkvermogen,
                        dat steunt op vier verdiepingen: gezondheid, competenties, waarden en werk.
                        Een sterk werkvermogen ontstaat wanneer de vier verdiepingen stevig en verbonden zijn.
                        Kan het huis van uw onderneming ook het noodweer trotseren?<br>
                        <br>

                        Voor elke medewerker wordt nagegaan hoe stevig de verschillende verdiepingen in zijn
                        <i>"Huis van werkvermogen"</i> zijn. Hij dient hiervoor de medewerkers-vragenlijst in te vullen.
                        Het huis zal er sterk en fris uitzien als de vier verdiepingen stevig en goed onderhouden
                        zijn en het dak (werkvermogen) goed kunnen ondersteunen.

                    </div>
                    <div class="large-6 medium-6 columns">
                        <img src="images/common/house-pieter.png" style="width:50%; float:right;"> Uw medewerker ziet
                        onmiddellijk wat er goed gaat of wat er beter kan: meteen na het invullen van de vragenlijst
                        krijgt hij gerichte en persoonlijke tips en adviezen om zijn "Huis van werkvermogen" te
                        stabiliseren of eventueel te verbouwen.<br>
                        <br>
                        U als werkgever ontvangt naast gerichte feedback (op uw eigen vragenlijst) ook een groepsrapport
                        met een overzicht van
                        de resultaten van de medewerkers. Dit zijn groepsresultaten, de resultaten zullen nooit
                        herleid kunnen worden naar individuele medewerkers. Anonimiteit is verzekerd!


                        <div class="text-center cta-container">
                            @if(Auth::guest())
                                <a href="/users/create" class="button button-flat">Registreren</a>
                            @else
                                @if(Auth::user()->permission_level_id == 0)
                                    <a href="/werknemers" class="button button-flat">Starten met Wellfie</a>
                                @elseif(Auth::user()->permission_level_id == 6)
                                    <a href="/werkgevers" class="button button-flat">Starten met Wellfie</a>
                                @endif
                            @endif
                        </div>

                    </div>
                </div> <!-- end row -->
            </div>  <!-- end large-10  -->
        </div> <!-- end row -->
    </div>

    @if($events->count()>0)
        <div class="home-content-block nieuws" style="background-color: #EEE">
            <div class="row">
                <div class="large-12 columns">
                    <h2>Nieuws</h2>
                    @foreach($events as $event)
                        <div class="row event">
                            <div class="large-12 columns">
                                <h3><i class="icon-calendar"></i> {{$event->title}}</h3>
                                <p class="event-datetime">{{$event->datetime->formatLocalized('%A %d %B %Y')}}</p>
                                <p>{{truncateHtml($event->description, 500)}}</p>
                                <a href="/nieuws/{{$event->id}}" class="button right button-flat small">Lees
                                    meer</a>
                            </div>
                        </div>
                    @endforeach
                </div>  <!-- end large-10  -->
            </div> <!-- end row -->
        </div>
    @endif

    <div class="home-content-block qoutes box-shadow" style="background-color: #fff">
        <div class="row">

            <div class="large-3 columns">
                <div class="quote">
                    Het is mijn droom dat elke medewerker ambassadeur voor ons bedrijf wordt. En dat zal maar gebeuren
                    als iedereen hier goed in zijn vel zit.
                </div>
                <div class="quote-arrow"></div>
                <div class="quote-footer">
                    <div class="quote-image">
                        <img src="images/quotes/GertVanCauter-sm.jpg">
                    </div>
                    <div class="quote-source"> Gert Van Cauter<br>
                        <span class="function">Mede-zaakvoerder<br>
                        <strong>Multitechnieken Van Cauter</strong></span>
                    </div>
                </div>
            </div>  <!-- end large-3  -->
            <div class="large-3 columns">
                <div class="quote">
                    Norriq wil mensen laten uitblinken in hun kerncompetenties zodat ze samen met andere schitterende
                    collega’s
                    het verschil kunnen maken bij onze klanten.
                    Zo dragen we bij aan de transformatie van vele verschillende branches door nieuwe zakelijke
                    oplossingen te
                    implementeren, en helpen we onze klanten dé referentie te zijn in hun domein.
                </div>
                <div class="quote-arrow"></div>
                <div class="quote-footer">
                    <div class="quote-image">
                        <img src="images/quotes/JosBalette-sm.jpg">
                    </div>
                    <div class="quote-source">Jos Balette<br>
                        <span class="function">HR Director <br> <strong>Norriq</strong></span>
                    </div>
                </div>
            </div>  <!-- end large-3  -->
            <div class="large-3 columns">
                <div class="quote">
                    Wellfie geeft ons op een eenvoudige manier zicht op de betrokkenheid en bevlogenheid van onze
                    medewerkers.
                    Leidinggevenden en medewerkers krijgen ook tips waarmee ze direct aan de slag kunnen.
                </div>
                <div class="quote-arrow"></div>
                <div class="quote-footer">
                    <div class="quote-image">
                        <img src="images/quotes/ErikNotelaers-sm.jpg">
                    </div>
                    <div class="quote-source">Erik Notelaers<br>
                        <span class="function">Adjunct-directeur <br><strong>Faculty Club</strong></span>
                    </div>
                </div>
            </div>  <!-- end large-3  -->
            <div class="large-3 columns">
                <div class="quote">
                    Wellfie betrekt alle domeinen gerelateerd aan 'werkbaar werk’. Ook medewerkers krijgen waar voor
                    hun' inspanningen’.
                </div>
                <div class="quote-arrow"></div>
                <div class="quote-footer">
                    <div class="quote-image">
                        <img src="images/quotes/SvenVerhoeven-sm.jpg">
                    </div>
                    <div class="quote-source">Sven Verhoeven<br>
                        <span class="function">Zaakvoerder<br><strong> Isolatie Verhoeven</strong></span>
                    </div>
                </div>
            </div>  <!-- end large-3  -->
        </div>
    </div>
    <div class="home-content-block box-shadow" style="background-color: #ddd">
        <div class="row">
            <div class="large-12 columns">
                <h2>Deelnemen aan Wellfie?</h2>
                <div class="row">
                    <div class="large-6  columns">
                        <h3>U bent werkgever:</h3>
                        <ol>
                            <li>U registreert zich.</li>
                            <li>U vult de werkgeversvragenlijst in en ontvangt onmiddellijk beleidsfeedback.</li>
                            <li>U nodigt uw medewerkers uit om eveneens een vragenlijst in te vullen.</li>
                            <li>U ontvangt beleidsfeedback en een groepsrapport met de resultaten van uw medewerkers,
                                als leidraad om acties te ondernemen INDIEN u beschikt over 10 ingevulde
                                medewerkersvragenlijsten. Uw medewerkers ontvangen persoonlijke feedback.
                                <br>
                                <i>-of-</i><br>
                                U ontvangt beleidsfeedback maar geen groepsrapport met de resultaten van uw medewerkers
                                indien u beschikt over minder dan 10 ingevulde medewerkersvragenlijsten. Uw medewerkers
                                ontvangen persoonlijke feedback.
                            </li>
                        </ol>
                        <br>
                        <div class="button-container">
                            <a href="users/create" class="btn button special-bg">Scan het werkvermogen van uw
                                medewerkers</a><br>
                        </div>
                    </div>
                    <div class="large-6  columns">
                        <h3>U bent medewerker:</h3>
                        <ol>
                            <li>U kan inloggen op dit platform via een persoonlijke link die u van uw werkgever
                                ontvangt.
                            </li>
                            <li>U vult de medewerkersvragenlijst in en ontvangt onmiddellijk individuele feedback</li>
                        </ol>
                    </div>
                    <br>


                </div> <!-- end row -->
            </div>  <!-- end large-10  -->
        </div>
    </div>

@stop
