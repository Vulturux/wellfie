<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WELLFIE - Your Company's wellbeing in a snapshot</title>
</head>

<body class="home">

<div class="contain-to-grid">
    <nav class="top-bar" data-topbar role="navigation">
        <div class="section-container">
            <ul class="title-area">
                <li class="name">
                    <h1 ><a href="../" class="logo">Wellfie</a></h1>
                </li>
                <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>

            <section class="top-bar-section">
                <!-- Right Nav Section -->

                
<ul class="right">
    <li class="" ><a href="/werkvermogen">Wat is werkvermogen?</a></li>
    <li class=""><a href="/hoebouwenaanwerkvermogen">Hoe bouwen aan werkvermogen?</a></li>
    <li class=""><a href="/links">Links</a></li>
   <!-- <li class=" active "><a href="/faq">FAQ</a></li> -->

    <li class=""><a href="/overons">Over ons</a></li>
    <li><a href="/users/login">Login</a></li>
</ul>



            </section>
        </div>
    </nav>
</div>


    <!--[if lt IE 9]>
            <div class="home-content-block wat"><div class="row"><div class="panel">You need to upgrade your browser. IE8 or lower is not supported.</div></div></div>
    <![endif]-->

    

<div class="home-content-block website-page faq">
    <div class="row">
        <div class="large-12 columns">
            <h2>FAQ</h2>


            
                <div class="row">
                    <div class="question medium-8 large-offset-2 end columns">
                                                <span class="category-naam label secondary round">starten</span>
                                            <h3>
                             <span class="icon">
                                 <i class="step fi-comment size-72"></i>
                             </span>
                             Is er een voorwaarde om mee te doen?
                         </h3>



                        <p>Er zijn geen formele voorwaarden om mee te doen. De tool is gratis ter beschikking van elke ondernemer die het belangrijk vindt dat de medewerkers gemotiveerd en gelukkig zijn in hun job. Bovendien is er geen externe ondersteuning nodig om gebruik te kunnen maken van Wellfie.</p><br>

                    </div>
                </div> <!-- end row -->

             
                <div class="row">
                    <div class="question medium-8 large-offset-2 end columns">
                                                <span class="category-naam label secondary round">starten</span>
                                            <h3>
                             <span class="icon">
                                 <i class="step fi-comment size-72"></i>
                             </span>
                             Kan een klein bedrijf ook mee doen?
                         </h3>



                        <ul>
<li><strong>Telt je bedrijf minder dan 10 medewerkers?</strong><br /> Je kan de medewerkers de vragenlijst laten invullen. Daarna krijgen zij persoonlijke feedback met tips. Je krijgt als werkgever geen groepsresultaten, dit om de anonimiteit van je medewerkers te garanderen. Hoe kan je dan op bedrijfsvlak actie ondernemen als je de groepsresultaten niet kent? Je kan na de invulronde je medewerkers uitnodigen om de resultaten te bespreken. Ook de feedback op je eigen persoonlijke vragenlijst kan een leidraad zijn om acties binnen je bedrijf te ondernemen.</li>
<li><strong>Telt je bedrijf meer dan 10 medewerkers?</strong><br />Elke medewerker die meedoet krijgt persoonlijke feedback met tips na het invullen. Je ontvangt als werkgever een groepsrapport zodra er bij het afsluiten 10 medewerkers of meer de vragenlijst hebben ingevuld. Dit groepsrapport kan je gebruiken als startpunt voor opvolging en actie.</li>
</ul><br>

                    </div>
                </div> <!-- end row -->

        </div>  <!-- end large-10  -->
    </div> <!-- end row -->
</div>


</body>
</html>
