@extends('layoutsWebsite.master')


@section('content')


    <div class="home-content-block website-page faq">
        <div class="row">
            <div class="large-12 columns">
                <h2>FAQ</h2>

                <div class="row">
                    <div class="medium-3 columns">
                        <ul class="no-bullet">
                            @foreach ($categories as $category)
                                @if(trim($category) != "")
                                    <li class="category-select text-center" data-category="{{$category}}">
                                        <a href="#">{{$category}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="medium-9 columns">
                        @foreach ($faq as $faqrow)


                            <div class="question hide" data-category="{{$faqrow->category_naam}}">
                                <h3>
                             <span class="icon">
                                 <i class="step fi-comment size-72"></i>
                             </span>
                                    {{ $faqrow->vraag }}
                                </h3>


                                {{ $faqrow->antwoord }}<br>

                            </div>

                        @endforeach
                    </div>
                </div>


                <br><br> <br><br> <br><br> <br><br>

            </div>  <!-- end large-10  -->
        </div> <!-- end row -->
    </div>
@stop


@section('script')
    <script>
        $(document).ready(function () {
            activeCategory($('.category-select').eq(0));
            showHideQuestions($('.category-select').eq(0).data('category'));

            $('.category-select').click(function (e) {
                e.preventDefault();
                activeCategory($(this));
                showHideQuestions($(this).data('category'))

            })
        });


        function activeCategory($el){
            $('.category-select').removeClass('active');
            $el.addClass('active');
        }

        function showHideQuestions(cat) {
            var $questions = $('.question');
            $questions.addClass('hide');
            $questions.each(function (i, el) {
                var $el = $(el);
                if ($el.data('category') == cat) {
                    $el.removeClass('hide');
                }
            });
        }
    </script>
@stop
