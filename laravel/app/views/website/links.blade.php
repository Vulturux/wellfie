@extends('layoutsWebsite.master')


@section('content')


<div class="home-content-block wat">
    <div class="row">
        <div class="large-12 columns">

            <div class="row">
                <div class=" medium-8 large-offset-2 end columns">

                    <h2>Links</h2>

                    <h4>Wetgeving</h4>
                    <ul>
                       <li>Gezondheidstoezicht:
                            <li><a href="http://www.werk.belgie.be/detailA_Z.aspx?id=1278">http://www.werk.belgie.be/detailA_Z.aspx?id=1278</a></li>
                            <li><a href="http://www.werk.belgie.be/defaultTab.aspx?id=562">http://www.werk.belgie.be/defaultTab.aspx?id=562</a></li>
                        </li>
                       <li>Psychosociaal: <a href="http://www.werk.belgie.be/defaultTab.aspx?id=557">http://www.werk.belgie.be/defaultTab.aspx?id=557</a></li>
                       <li>Sociaaljuridisch :
                            <ul>
                                <li>pensioen (<a href="http://www.rvp.be">www.rvp.be</a>)</li>
                                <li>tijdskrediet (<a href="http:www.rva.be">www.rva.be</a></li>
                                <li>Federale Overheidsdienst Werkgelegenheid, Arbeid en Sociaal Overleg (<a href="http://www.werk.belgie.be/home.aspx">http://www.werk.belgie.be/home.aspx</a>)</li>
                            </ul>
                        </li>
                    </ul>

                    <h4>Hulpmiddelen</h4>
                    <ul>
                        <li>Toolbox werkvermogen: <a href="http://www.dejuistestoel.be">www.dejuistestoel.be</a></li>
                       <li>Werkbaarheidsmonitor: <a href="http://www.werkbaarwerk.be">www.werkbaarwerk.be</a></li>
                       <li>Beeldvorming samenstelling leeftijdsgroepen in uw onderneming en toekomstvoorspelling: <a href="http://www.leeftijdsscan.be">www.leeftijdsscan.be</a></li>
                       <li>Subsidiemogelijkheden: <a href="http://www.werk.belgie.be/ervaringsfonds.aspx">http://www.werk.belgie.be/ervaringsfonds.aspx</a></li>
                       <li>Brochures psychosociaal: <br>
                                 <strong>Voor werkgevers</strong><br>
                               <ul>
                                    <li>Van verzuim tot ‘er wèl-zijn’ op het werk (<a href="pdf-uploads/Verzuimbeleid_WG_NLx_20216012012.pdf">download pdf</a>)</li>
                                   <li>Stap voor stap naar een psychosociaal welzijnsbeleid  (<a href="pdf-uploads/PsychosociaalWelzijnsbeleid.pdf">download pdf</a>)</li>
                                   <li>Hoe zet ik een alcohol- en drugpreventiebeleid op in mijn onderneming? (<a href="pdf-uploads/Alcohol_en_drugs_WG_NL_20212082009.pdf">download pdf</a>) </li>
                                   <li>Hoe zet ik een agressiepreventiebeleid op in mijn onderneming? (<a href="pdf-uploads/AgressieWGNL20184022011.pdf">download pdf</a>)</li>
                                   <li>Een stresspreventiebeleid: waarom en hoe? (<a href="pdf-uploads/Stresspreventiebeleid_WG_NL_20158012009.pdf">download pdf 1</a> / <a href="pdf-uploads/Stress_op_het_werk_WN_NL_20144012009.pdf">download pdf 2</a>)</li>
                                </ul>
                               <strong>Voor werknemers </strong><br>
                                <ul>
                                    <li>Stress op het werk</li>
                                    <li>Geweld, pesterijen en ongewenst seksueel gedrag op het werk</li>
                                    <li>Template werkgelegenheidsplan (is geen brochure)  (<a href="pdf-uploads/Model_werkgelegenheidsplan.pdf">download pdf</a>)</li>
                              </ul>
                        </li>
                       <li>Rekrutering en selectie: 
                            <ul>
                                <li><a href="http://www.vdab.be">www.vdab.be</a></li>
                                <li><a href="http://www.vacature.com">www.vacature.com</a></li>
                                <li><a href="http://www.stepstone.be">www.stepstone.be</a></li>
                                <li><a href="http://www.jobat.be">www.jobat.be</a></li>
                                <li><a href="http://www.monster.com">www.monster.com</a></li>
                                <li><a href="http://www.regiojobs.be<">www.regiojobs.be</a></li>
                                <li><a href="http://www.linkedin.com">www.linkedin.com</a></li>
                            </ul>
                       </li>
                       <li>Inspirerende literatuur:
                           <ul>
                                <li>Verbinding verbroken: <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/verbinding%20verbroken.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/verbinding%20verbroken.pdf</a></li>
                                <li>Verbindend leiderschap: <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_46_Leiderschap.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_46_Leiderschap.pdf</a></li>
                                <li>De waardenfluisteraar: <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_56_-_De_waardenfluisteraar_LR.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_56_-_De_waardenfluisteraar_LR.pdf</a></li>
                                <li>De winst van waarden : <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_63_winst%20van%20waarden%20LR.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_63_winst%20van%20waarden%20LR.pdf</a></li>
                                <li>Naar een inclusieve onderneming:  <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/BN%2060%20LR%20Inclusieve%20onderneming.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/BN%2060%20LR%20Inclusieve%20onderneming.pdf</a></li>
                                <li>Over de economische kracht van vertrouwen: <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_51_Vertrouwen_.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_51_Vertrouwen_.pdf</a></li>
                                <li>Over integriteit: <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_67_Integriteit.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_67_Integriteit.pdf</a></li>
                                <li>Hoe je het beste in mensen naar boven haalt: <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_68_Inclusieve_organisatie_WEB.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_68_Inclusieve_organisatie_WEB.pdf</a></li>
                                <li>Persoonlijke groei als bron van leiderschap: <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/BN70_NL_Persoonlijkegroei.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/BN70_NL_Persoonlijkegroei.pdf</a></li>
                                <li>Op samenwerking staat geen leeftijd: <a href="http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_72_samenwerken_def_WEB.pdf">http://www.etion.be/sites/default/files/documents/beleidsnotas/BN_72_samenwerken_def_WEB.pdf</a></li>
                           </ul>
                        </li>
                    </ul>

                    <h4>Partners</h4>
                    <ul>
                       <li>Groep IDEWE: <a href="http://www.idewe.be">www.idewe.be</a></li>
                       <li>ETION: <a href="http://www.etion.be">www.etion.be</a></li>
                       <li>Acerta: <a href="http://www.acerta.be">www.acerta.be</a></li>
                       <li>Apps Only: <a href="http://www.appsonly.be">www.appsonly.be</a></li>
                    </ul>

                </div>
            </div> <!-- end row -->
        </div>  <!-- end large-10  -->
    </div> <!-- end row -->
</div>

@stop
