@extends('layoutsWebsite.master')


@section('content')

<div class="home-content-block hoewerkthet" style="background-color: #FFF">
    <div class="row">
        <div class="large-12 columns">
            <h2>Registratie Werkgevers</h2>

            <form method="POST" action="{{{ URL::to('/users/store/') }}}" accept-charset="UTF-8" class="js-disclaimer-check">
                <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">


                <fieldset>

                    @if (Session::get('error'))
                        <div class="alert alert-error alert-danger">
                            @foreach (Session::get('error') as $error)
                               <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif

                    @if (Session::get('notice'))
                    <div class="alert">{{ Session::get('notice') }}</div>
                    @endif


                    <h3>Bedrijf</h3>

                    <div class="form-group">
                        <label for="company">Bedrijfsnaam</label>
                        <input class="form-control" placeholder="" type="text" name="companyname" id="companyname" value="{{{ Input::old('companyname') }}}">
                    </div>

                    <div class="form-group">
                        <label for="company">Ondernemingsnummer <i>(niet verplicht)</i></label>
                        <input class="form-control" placeholder="" type="text" name="ondernemingsnummer" id="ondernemingsnummer" value="{{{ Input::old('companyname') }}}">
                    </div>
                    <hr>

                    <h3>Wie kan Wellfie verantwoordelijke zijn?</h3>

                    <div class="intro">
                        De Wellfie verantwoordelijke dient de vragenlijst te beantwoorden vanuit het werkgeversperspectief en neemt ook de verdere organisatie van het project op zich. <br>De Wellfie verantwoordelijke bekleedt bij voorkeur één of meerdere van de volgende functies:
                        <ol>
                            <li>werkgever</li>
                            <li>medewerker HR- of personeelsdienst</li>
                            <li>interne preventieadviseur</li>
                        </ol>
                    </div>
                    <div class="form-group">
                        <label for="username">{{{ Lang::get('confide::confide.username') }}} <i>(Een gebruikersnaam mag alleen letters, cijfers,  _ of  -  bevatten maar geen spaties)</i></label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.username') }}}" type="text" name="username" id="username" value="{{{ Input::old('username') }}}">
                    </div>


                    <div class="form-group">
                        <label for="email">{{{ Lang::get('confide::confide.e_mail') }}}</label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
                    </div>

                    <div class="form-group">
                        <label for="email">Functie</label>
                        <select name="function">
                            <option value="CEO">Bedrijfsleider / zaakvoerder / directeur</option>
                            <option value="Secretariaatsmedewerker">Personal assistant of secretariaatsmedewerker directie</option>
                            <option value="HR">Medewerker HR- of personeelsdienst</option>
                            <option value="Preventieadviseur">Interne preventieadviseur</option>
                            <option value="Medewerker">Medewerker</option>
                            <option value="Andere">Andere</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="password">{{{ Lang::get('confide::confide.password') }}}</label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">{{{ Lang::get('confide::confide.password_confirmation') }}}</label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation">
                    </div>

                    <div class="form-group js-group-disclaimer-checkbox">
                        <input id="disclaimer-checkbox" type="checkbox">
                        <label for="checkbox">
                            Ik ga akkoord met <a href="#" data-reveal-id="myModal" data-reveal>de algemene voorwaarden</a> omtrent de vertrouwelijkheid
                        </label><br>
                    </div>


                    <div class="form-actions form-group">
                        <div class="false-button" style="display:block; height:70px;">
                            <a href="#" class="btn btn-primary btn-submit js-btn-submit-disabled" style="background-color: #EFEFEF; padding: 20px 30px; position:relative; top: 15px;">{{{ Lang::get('confide::confide.signup.submit') }}}</a>
                        </div>
                        <div class="main-button" style="display:none;">
                            <button type="submit" class="btn btn-primary btn-submit" >{{{ Lang::get('confide::confide.signup.submit') }}}</button>
                        </div>
                    </div>

                </fieldset>
            </form>


        </div>  <!-- end large-12  -->
    </div> <!-- end row -->
</div>
<div id="myModal" class="reveal-modal" data-reveal>
    <div class="diclaimer-text">
        <br>
        Alle persoonlijke gegevens die u ons verstrekt, worden nooit doorgegeven aan derden, tenzij de wetgever ons hiertoe verplicht.
        De wet op de privacy bepaalt dat u inzage hebt in uw persoonlijke gegevens die in ons bestand zouden aanwezig zijn.
        U hebt hierbij het recht op inzage en verbetering van de gegevens. De Wellfie-partners zullen deze informatie op een
        strikt confidentiële manier behandelen en zullen deze louter voor doeleinden van intern beheer, binnen het kader van
        wetenschappelijk onderzoek en eventuele verdere opvolging aanwenden. Enkel uw administratieve gegevens zullen gebruikt worden voor verdere opvolging.
        <br>
        <a class="close-reveal-modal">&#215;</a>
    </div>
</div>

@stop
