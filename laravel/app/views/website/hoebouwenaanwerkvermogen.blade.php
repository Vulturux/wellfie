@extends('layoutsWebsite.master')


@section('content')

<div class="home-content-block wat">
    <div class="row">
        <div class="large-12 columns">
            <div class="row">
                <div class=" medium-8 large-offset-2 end columns">
                    <h2>Hoe bouwen aan werkvermogen?</h2>

                    <p><a href="http://www.idewe.be">Idewe</a>, <a href="http://www.acerta.be">Acerta</a> en <a href="http://www.etion.be">ETION</a> hebben hun expertise inzake preventie en bescherming op het werk, human resources management en waardengericht ondernemen samengebracht in de tool Wellfie.  Elke verdieping van het huis van werkvermogen bevat gevalideerde vragenlijsten en denkkaders die hun merites bewezen hebben. “Een Wellfie doen” in uw bedrijf is dus een prima vertrekpunt als u aan het werkvermogen wilt bouwen. Na deelname ziet u als werkgever heel concreet sterkten en knelpunten op bedrijfsvlak; uw medewerkers ontvangen elk persoonlijke feedback om hun werkvermogen te behouden of te verbeteren. Op basis hiervan kan u samen aan de slag met als doel iedereen met goesting aan het werk te houden.
                        <br><br>
                        Hebt u voor of na deelname behoefte aan extra ondersteuning, mail dan gerust naar <a href="mailto:info@wellfie.be">info@wellfie.be</a>.<br>
                        <br>
                        Hieronder ziet u alvast, per verdieping van het huis van werkvermogen, welk type acties u kunt overwegen na deelname.
                    </p>


                    <h4>Gelijkvloers =<br> <span class="light">lichamelijke en geestelijke gezondheid</span></h4>
                    Leefstijlfactoren zoals voeding, beweging, rookgedrag, alcoholgebruik, slaapgewoonten, … spelen hierin een belangrijke rol. Stimuleer een gezonde levensstijl door bijvoorbeeld informatiesessies te organiseren, bewegingsmogelijkheden en gezonde voeding aan te bieden. Een uitgewerkt welzijnsbeleid met zowel preventieve als proactieve maatregelen is aangewezen.
                    <br><br>
                    <h4>Eerste verdieping =<br> <span class="light">competenties</span></h4>
                    Ook de kennis en vaardigheden van medewerkers moeten op peil worden gehouden.
                    Medewerkers die hun competenties kunnen ontwikkelen en de kans krijgen om hun talenten
                    te benutten, zijn meer geëngageerd en betrokken. Het is een samenspel tussen werkgever
                    en medewerker om een juist evenwicht te vinden:
                    <ul>
                        <li>De werkgever biedt kansen en middelen:
                            <ul>
                                <li>competentiemanagement, dat inzicht geeft in sterktes en zwaktes van medewerkers </li>
                                <li>het opstellen en uitvoeren van een doelgroepengericht leerbeleid. </li>
                                <li>...</li>
                            </ul>
                          </li>
                        <li>De medewerker neemt zijn werkvermogen in handen door kansen te grijpen en optimaal te benutten. </li>
                    </ul>
                    <br>
                    <h4>Tweede verdieping<br> <span class="light">waarden, houding en motivatie</span></h4>
                    Deze verdieping is het minst gemakkelijk te beïnvloeden en deze verdieping wordt verstevigd wanneer de medewerker ervaart dat zijn taken aansluiten
                    bij zijn persoonlijkheid. Een goede match tussen de persoonlijke waarden en de waarden van de onderneming draagt hiertoe bij.
                    Interventies rond leiderschapsvaardigheden, respect, vertrouwen en een evenwicht tussen werk en privé kunnen deze verdieping helpen verstevigen.<br><br>

                    <h4>Derde verdieping<br> <span class="light">werkinhoud en -eisen, werkomgeving en -organisatie, collega’s en manier van leidinggeven</span></h4>
                    Deze wordt voornamelijk bepaald door acties vanuit de onderneming. Om het werkvermogen te versterken, zijn een interessante jobinhoud en aangepaste werkeisen nodig. Maatregelen zoals jobrotatie, flexibele werktijden, keuzevrijheid wat betreft pauzes en telewerk kunnen hierbij helpen. De steun van collega’s en leidinggevenden spelen hierin ook een belangrijke rol.
                    <br>


                </div>
            </div> <!-- end row -->
        </div>  <!-- end large-10  -->
    </div> <!-- end row -->
</div>

@stop
