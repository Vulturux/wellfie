@extends('layoutsWerknemer.master')


@section('content')

    <div class="box-header clearfix">
        <h3 class="main-title">WERKNEMERS</h3>
    </div>

    @if(Session::has('message'))
        <div class="row">
            <div class="large-12 columns">
                <p class="alert-box {{ Session::get('message-type', 'success') }}">{{ Session::get('message') }}</p>
            </div>
        </div>

    @endif

    <div class="row superadmin">

        <div class="large-12 columns">

            <table class="table" style="width: 100%;">
                <thead>
                <tr>
                    <th nowrap>datum</th>
                    <th>id</th>
                    <th nowrap>username</th>
                    <th nowrap>email</th>
                    <th>bedrijf</th>
                    <th>positie</th>
                    <th>role</th>
                    <th></th>

                </tr>
                </thead>

            </table>
        </div>
    </div> <!-- end row -->


    <script src="/js/datatables/datatables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var $table = $('.table');

            var oTable = $table.DataTable({
                processing: true,
                fixedHeader: true,
                serverSide: true,
                ajax: "/superadmin/werknemers/data",

                dom: 'fBrtip',
                buttons: [
//                {extend: 'colvis', className: 'tiny secondary'},
                    {extend: 'excel', className: 'tiny secondary'},
                    {extend: 'pdf', className: 'tiny secondary'}
                ],
                columns: [
                    {data: 'created_at', name: 'created_at' },
                    {data: 'id', name: 'id' },
                    {data: 'username', name: 'username' },
                    {data: 'email', name: 'email' },
                    {data: 'company', name: 'company', orderable: false, searchable: false },
                    {data: 'latest_user_scan', name: 'latest_user_scan', orderable: false, searchable: false },
                    {data: 'permission_level_id', name: 'permission_level_id', searchable: false },
                    {data: 'extra', name: 'extra', searchable: false }


                ]

            });

        });
    </script>

@stop
