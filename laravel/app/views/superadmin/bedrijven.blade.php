@extends('layoutsSuperadmin.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">BEDRIJVEN</h3>
</div>

@if(Session::has('message'))
    <div class="row">
        <div class="large-12 columns">
            <p class="alert-box {{ Session::get('message-type', 'success') }}">{{ Session::get('message') }}</p>
        </div>
    </div>

@endif

<div class="row superadmin">

    <div class="large-12 columns">

        <table class="table" data-order='[[ 1, "asc" ]]' data-page-length='100' width="100%">
            <thead>
            <tr>
                <th nowrap>aangemaakt</th>
                <th nowrap>bedrijf</th>
                <th nowrap>voltooid</th>
                <th nowrap>email</th>
                <th>status</th>
                <th>sector</th>
                <th class="no-sort" ></th>
            </tr>
            </thead>

            <tbody>
            @foreach ($companiesArray as $company)
            <tr>
                <td>{{ $company->created_at }}</td>
                <td>{{ $company->company_name }}</td>
                <td>{{ $company->wgstatus or 'nee' }}</td>
                <td><a href="mailto:{{ $company->WGemail }}">{{ $company->WGemail }}</a></td>
                <td>{{ $company->WNstatus }}</td>
                <td title="uitgenodigd/aangemeld/voltooid">{{ $company->sector }}</td>
                <td><a href="{{ URL('superadmin/export') }}/{{ $company->id }}">Export</a>&nbsp;-&nbsp;<a
                            href="{{ URL('superadmin/bedrijven') }}/{{ $company->id }}">Detail</a>&nbsp;-&nbsp;<a
                            href="{{ URL('superadmin/bedrijven') }}/{{ $company->id }}/edit" style="color:#CCC;font-size:16px;"><i class="fi-pencil"></i></a>
                </td>
            </tr>
            @endforeach
            </tbody>

        </table>
    </div>
</div> <!-- end row -->

<script src="/js/datatables/datatables.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        var $table = $('.table');

        var oTable = $table.DataTable({
            autoWidth: false,
            processing: true,
            fixedHeader: true,
            responsive: false,
            dom: 'fBrtip',
            buttons: [
//                {extend: 'colvis', className: 'tiny secondary'},
                {extend: 'excel', className: 'tiny secondary'},
                {extend: 'pdf', className: 'tiny secondary'},
            ],
            aoColumnDefs : [ {
                bSortable : false,
                aTargets : [ "no-sort" ]
            } ]

        });

    });
</script>
@stop
