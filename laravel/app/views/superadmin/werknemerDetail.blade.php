@extends('layoutsWerknemer.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">WERKNEMER DETAIL PAGE</h3>
</div>

<div class="row superadmin">

    <h4>Detail</h4>
    <div class="large-12 columns">


        <table class="table table-striped">

            <tbody>

            <tr>
                <td>Date created</td>
                <td>{{ $user->created_at }}</td>
            </tr>
            <tr>
                <td>ID</td>
                <td>{{ $user->id }}</td>
            </tr>
            <tr>
                <td>Username</td>
                <td nowrap>{{ $user->username }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <td>Company</td>
                <td><?php if (isset($user->Company->company_name)) {
                        echo $user->Company->company_name.' ('.$user->company_id.')';
                    } else {
                        echo "no company";
                    }?>
                <td>
            </tr>
            <tr>
                <td>Permission Level</td>
                <td>{{ $user->permission_level_id }}</td>
            </tr>


            </tbody>

        </table>

    </div>
</div> <!-- end row -->


<div class="row superadmin">

    <h4>Responses Scan 1</h4>
    <div class="large-12 columns">


        <table class="table table-striped">
        @foreach ($response as $question)
            <tr>
                <td>{{ $question->questions_id }}</td>
                <td>({{ $question->question_nr }}) <strong>{{ $question->question }}</strong></td>
                <td>{{ $question->answer }}</td>
                <td>{{ $question->points }}</td>
            </tr>
        @endforeach


        </table>

    </div>
</div> <!-- end row -->



@stop
