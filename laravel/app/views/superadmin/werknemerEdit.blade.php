@extends('layoutsWerknemer.master')


@section('content')

    <div class="box-header clearfix">
        <h3 class="main-title">WERKNEMERS</h3>
    </div>

    @if(Session::has('message'))
        <div class="row">
            <div class="large-12 columns">
                <p class="alert-box {{ Session::get('message-type', 'success') }}">{{ Session::get('message') }}</p>
            </div>
        </div>

    @endif

    <div class="row superadmin">

        <div class="large-12 columns">
            <h4>Detail werknemer</h4>
            {{ Form::open(array('url' => "superadmin/werknemers/$user->id/edit", 'method' => 'post')) }}

            <table class="table table-striped">

                <tbody>

                <tr>
                    <td>Username</td>
                    <td>{{ Form::text('username', $user->username, ['disabled' => '']) }}</td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td>{{ Form::text('email', $user->email, ['disabled' => '']) }}</td>
                </tr>

                </tbody>

            </table>
            <div class="pull-left">
                {{ Form::submit('Wijzigingen opslaan', array('class' => 'button main')) }}

            </div>
            {{ Form::close() }}

            <div class="pull-left" style="margin-left: 20px;">
                {{ Form::open(array('url' => "superadmin/werknemers/$user->id/edit", 'method' => 'delete', 'class' => 'form-delete')) }}
                {{ Form::submit('Werknemer verwijderen', array('class' => 'button main btn-danger btn-delete', 'style' => 'background-color:#B20000;')) }}
                {{ Form::close() }}
            </div>


            {{ Form::close() }}

        </div>
    </div> <!-- end row -->
@stop

@section('script')
    <script>
        $('.form-delete').submit(function(e){
            return confirm('Ben je zeker dat je deze werknemer (met alle antwoorden e.d.) wilt verwijderen?');
        });
    </script>
@endsection
