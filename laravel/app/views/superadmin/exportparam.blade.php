@extends('layoutsWerknemer.master')

@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">Export</h3>
</div>

<div class="row detail-results">

    <div class="large-12 columns">
        <h1>Selecteer een filter voor export</h1>
        <br>
        {{ Form::open(array('url' => 'superadmin/exportaction', 'class'=>'exportform', 'id' => 'js-exportform')) }}
        <div class="row vraag">
            <div class="large-2 columns col-question">
                SCAN ID
            </div>
            <div class="large-10 columns col-question">
                {{ Form::select('scanId', $scanIds, 0, array('class'=>'form-control')) }}
            </div>
            @if ($errors->has('scanId'))
            <div class="large-10 large-offset-2 columns error">{{$errors->first('scanId')}}</div>
            @endif
        </div>
        <hr>
        <div class="row vraag">
            <div class="large-10 large-offset-2 columns col-question">
                <strong>Gebruik minstens één van onderstaande filters. Let op, combinaties kunnen langer duren!</strong>
            </div>
        </div>
        <div class="row vraag">
            <div class="large-2 columns col-question">
                COMPANY
            </div>
            <div class="large-10 columns col-question">
                {{ Form::select('companyId', array(null => 'Filter company') + $companyList, $companyId, array('class'=>'form-control')) }}
            </div>
            @if ($errors->has('companyId'))
                <div class="large-10 large-offset-2 columns error">{{$errors->first('companyId')}}</div>
            @endif
        </div>

        <div class="row vraag">
            <div class="large-2 columns col-question">
                PERIODE
            </div>
            <div class="large-5 columns col-question">
                <input type="text" placeholder="2014-01-01" name="periodFrom">
            </div>
            <div class="large-5 columns col-question">
                <input type="text" placeholder="2015-01-01" name="periodTo">
            </div>
            @if ($errors->has('periodFrom'))
                <div class="large-10 large-offset-2 columns error">{{$errors->first('periodFrom')}}</div>
            @endif
            @if ($errors->has('periodTo'))
                <div class="large-10 large-offset-2 columns error">{{$errors->first('periodTo')}}</div>
            @endif
        </div>
        <div class="row vraag">
            <div class="large-2 columns col-question">
                REGIO
            </div>
            <div class="large-10 columns col-question">
                {{ Form::select('regionId',  array(null => 'Filter regio')  + $regioList, 0, array('class'=>'form-control')) }}
            </div>
            @if ($errors->has('regionId'))
                <div class="large-10 large-offset-2 columns error">{{$errors->first('regionId')}}</div>
            @endif
        </div>
        <div class="row vraag">
            <div class="large-2 columns col-question">
                AANTAL WERKNEMERS
            </div>
            <div class="large-5 columns col-question">
                Min: <input type="text" placeholder="" name="employeeCountMin">
            </div>
            <div class="large-5 columns col-question">
                Max: <input type="text" placeholder="" name="employeeCountMax">
            </div>
            @if ($errors->has('employeeCountMin'))
                <div class="large-10 large-offset-2 columns error">{{$errors->first('employeeCountMin')}}</div>
            @endif
            @if ($errors->has('employeeCountMax'))
                <div class="large-10 large-offset-2 columns error">{{$errors->first('employeeCountMax')}}</div>
            @endif
        </div>
        <div class="row vraag">
            <div class="large-2 columns col-question">
                SECTOR
            </div>
            <div class="large-10 columns col-question">
                {{ Form::select('sectorId', array(null => 'Filter sector') + $sectorList, 0, array('class'=>'form-control')) }}
            </div>
            @if ($errors->has('sectorId'))
                <div class="large-10 large-offset-2 columns error">{{$errors->first('sectorId')}}</div>
            @endif
        </div>
        <div class="row vraag">
            <div class="large-2 columns col-question">
                STATUS
            </div>

            <div class="large-10 columns col-question">
                {{ Form::select('positionId', array(null => 'Filter status') + $positionList, 0, array('class'=>'form-control')) }}
            </div>
            @if ($errors->has('positionId'))
                <div class="large-10 large-offset-2 columns error">{{$errors->first('positionId')}}</div>
            @endif
        </div>

        <div class="row vraag">
            <div class="large-2 columns col-question">
                EXPORT TYPE
            </div>
            <div class="large-10 columns col-question">
                {{ Form::select('exportType', array(null => 'Antwoorden', 1 => 'Administratieve gegevens'), 0, array('class' => 'form-control')) }}
            </div>
        </div>
        <div class="button-container text-center">
            <ul class="button-group">
                <!--    <li><a href="#" class="button lessimportant"><i class="fi-stop"></i> bewaren en stoppen</a></li> -->
                <li style="float: right;">
                    {{ Form::submit('export', array('class' => 'button main')) }}
                </li>
                <li style="float: right;">
                    <a href="/superadmin/exportlist" class="button secondary">Bekijk de laatste 10 exports</a>
                </li>
            </ul>
        </div>
        {{ Form::close() }}
    </div> <!-- end large 12 columns -->

</div> <!-- end row -->

<script type="text/javascript">
    $(document).ready(function () {
        var $button = $('.button.main');

        $button.click(function(e){
            if ($(this).hasClass('throbber')) {
                e.preventDefault();
                return false;
            }
            else {
                $(this).addClass('throbber');
            }
        });

    });
</script>


@stop
