@extends('layoutsWerknemer.master')


@section('content')
<script src="//code.highcharts.com/highcharts.js"></script>
<script src="//code.highcharts.com/modules/data.js"></script>
<script src="//code.highcharts.com/modules/funnel.js"></script>
<script src="//code.highcharts.com/modules/exporting.js"></script>

<div class="box-header clearfix">
    <h3 class="main-title">DASHBOARD</h3>
</div>

<div class="row detail-results">

    <div class="large-12 columns">

    </div>

    <div class="large-6 columns">

        <h3>Resultaten voor week <?php echo $week;?> (<?php echo $year;?>)</h3>

        <div id="funnel" style="min-width: 310px; height: 600px; margin: 0 auto"></div>

    </div>

    <div class="large-6 columns">

        <h3>Totaal overzicht</h3>

        <div id="table" style="min-width: 310px; height: 300px; margin: 0 auto">
            <table>
                <tr>
                    <td>Inschrijvingen</td>
                    <td><?php echo $overal['user_count'];?></td>
                </tr>
                <tr>
                    <td>Bedrijven</td>
                    <td><?php echo $overal['company_count'];?></td>
                </tr>
                <tr>
                    <td>Verstuurde WN uitnodigingen</td>
                    <td><?php echo $overal['invited_count'];?></td>
                </tr>
                <tr>
                    <td>Voltooide werkgeversvragenlijsten</td>
                    <td><?php echo $overal['completed_wg_count'];?></td>
                </tr>
                <tr>
                    <td>Voltooide werknemersvragenlijsten</td>
                    <td><?php echo $overal['completed_wn_count'];?></td>
                </tr>
            </table>
        </div>


        <h3>Nieuwe bedrijven laatste 6 maand</h3>

        <p>Klik op een datapunt om een andere week te selecteren.</p>

        <div id="new-company-container" style="min-width: 310px; height: 300px; margin: 0 auto"></div>

    </div>
</div> <!-- end row -->

<script>
    $(document).ready(function () {

        $('#new-company-container').highcharts({
            chart: {
                type: 'spline'
            },
            title: false,

            xAxis: {
                type: 'datetime',
                title: false,
                labels: {
                  formatter: function() {
                      return Highcharts.dateFormat('%e-%b-%Y', new Date(this.value));
                  }
                },
                startOnTick: true,
                tickInterval: 1
            },
            yAxis: {
                title: {
                    text: 'Bedrijven (aantal)'
                },
                min: 0
            },
            tooltip: {
                formatter: function () {
                    return 'Week van ' + Highcharts.dateFormat('%e-%b-%Y', new Date(this.x)) + '<br>' + this.y + ' bedrijven';
                }
            },
            plotOptions: {
                spline: {
                    showInLegend: false,
                    marker: {

                    }
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                var value = this.name.toString();
                                var year = value.substr(0, 4);
                                var week = value.substring(4);
                                location.href = '/superadmin?year=' + year + '&week=' + week;
                            }
                        }
                    }
                }
            },
            series: [{
                data: [
                    <?php foreach ($companies_per_week as $companies) {
                        list($year, $month, $day) = explode('-', $companies->weekstart);
                        $month = $month - 1;
                        $values[] = "{
                            x: Date.UTC($year, $month, $day),
                            y: {$companies->count},
                            name: {$companies->week}
                        }";
                    } ?>
                    <?php echo implode(', ', $values); ?>
                ]
            }]
        });

        $('#funnel').highcharts({
            chart: {
                type: 'funnel',
                marginRight: 100
            },
            title: false,
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b> ({point.y:,.0f})',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    neckWidth: '30%',
                    neckHeight: '25%'

                    //-- Other available options
                    // height: pixels or percent
                    // width: pixels or percent
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Aantal deze week',
                data: [
                    ['Nieuwe inschrijvingen', <?php echo $this_week['user_count'];?>],
                    ['Nieuwe trajecten', <?php echo $this_week['company_count'];?>],
                    ['Werkgevers die uitnodigen', <?php echo $this_week['company_invitation_count'];?>],
                    ['Actieve trajecten', <?php echo $this_week['company_active_count'];?>],
                    ['Voltooide trajecten', <?php echo $this_week['company_completed_count'];?>]

                ]
            }]
        });



    });
</script>


@stop
