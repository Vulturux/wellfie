@extends('layoutsWerknemer.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">{{ $company->company_name }}</h3>
</div>

<div class="row superadmin">

    <div class="large-12 columns">
        <?php
            $activeUsers = 0;
            $inactiveUsers = 0;
            foreach ($invites as $invite) {
                if ($invite['invitecode'] === '(used)') {
                    $activeUsers++;
                }else{
                    $inactiveUsers++;
                }
            }
        ?>

        <table class="table table-striped">

            <tbody>

                <tr>
                    <td>Company</td>
                    <td><strong style="font-size:30px;">{{ $company->company_name }}</strong></td>
                </tr>
                <tr>
                    <td>Date created</td>
                    <td>{{ $company->created_at }}</td>
                </tr>
                <tr>
                    <td>Ondernemingsnummer</td>
                    <td>{{ $company->ondernemingsnummer }}</td>
                </tr>
                <tr>
                    <td>COMPANY ID</td>
                    <td>{{ $company->id }}</td>
                </tr>
                <tr>
                    <td>WERKGEVER</td>
                    <td>
                        <table>
                            <tr>
                                <td>email</td>
                                <td>{{ $employer->email }}</td>
                            </tr>
                            <tr>
                                <td>user_id</td>
                                <td>{{ $employer->id }}</td>
                            </tr>
                            @if(isset($companyDetail->position))
                            <tr>
                                <td>Position</td>
                                <td>{{ $companyDetail->position }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td>Active WNs</td>
                                <td>{{ $activeUsers }}</td>
                            </tr>
                            <tr>
                                <td>Inactive WNs</td>
                                <td>{{ $inactiveUsers }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>Afgerond</td>
                    <td>{{ $company->finalized ? 'Ja':'Nee' }}
                        @if ($company->finalized)
                            - <a href="/superadmin/bedrijven/detail/{{ $company->id }}/view-rapport" target="_blank">Bekijk</a>
                            - <a href="/superadmin/bedrijven/detail/{{ $company->id }}/download-rapport" target="_blank">PDF</a>
                            - <a href="/superadmin/bedrijven/detail/{{ $company->id }}/delete-rapport">Verwijder rapport</a>
                        @endif</td>
                </tr>
            </tbody>

        </table>

        <br>
            <div class="table-responsive">
                <table class="table table-users">
                    <thead>
                    <tr>
                        <th>INVITE ID</th>
                        <th>USER ID</th>
                        <th nowrap>Date</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Invite Url</th>
                        <th>Position</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($invites as $key => $invite)
                        <tr>
                            <td>{{ $invite->id }}</td>
                            <td>{{ $invite->user_id }}</td>
                            <td><strong>{{ $invite->created_at }}</strong></td>
                            <td>{{ $invite->email }}</td>
                            <td>{{ $invite->status }}</td>
                            <td>@if(is_url($invite->invitecode))

                                    <div class="row collapse invite-hash">
                                        <div class="small-9 columns">
                                            <input type="text" disabled value="{{$invite->invitecode}}" id="invite-user-{{ $key }}">
                                        </div>
                                        <div class="small-3 columns">
                                            <span class="postfix">
                                                <a href="#" class="copytoclipboard" data-target="#invite-user-{{ $key }}">
                                                <i class="fa fa-clipboard" aria-hidden="true"></i>
                                            </a>
                                            </span>
                                        </div>
                                    </div>


                                @else
                                    {{$invite->invitecode}}
                                @endif
                            </td>
                            <td>
                                <?php $lastScan = isset($invite->user, $invite->user->UserScans) ? $invite->user->UserScans->first() : null; ?>
                                @if(isset($lastScan))
                                    Scan {{ $lastScan->scan_id }}: {{ $lastScan->questionnaire_position }}/5
                                @else

                                @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>
            </div>



    </div>
</div> <!-- end row -->


@stop
