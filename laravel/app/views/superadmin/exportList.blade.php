@extends('layoutsSuperadmin.master')


@section('content')

    <div class="box-header clearfix">
        <h3 class="main-title">ExportList</h3>
    </div>

    @if(Session::has('message'))
        <div class="row">
            <div class="large-12 columns">
                <p class="alert-box {{ Session::get('message-type', 'success') }}">{{ Session::get('message') }}</p>
            </div>
        </div>

    @endif
    
    <div class="row superadmin">
        @if(count($files) === 0)
            <p>Er werden nog geen exports genomen.</p>
        @else
            <div class="large-12 columns">
                <table class="table" data-order='[[ 1, "asc" ]]' data-page-length='100' width="100%">
                    <thead>
                    <tr>
                        <th nowrap>Aanmaakdatum</th>
                        <th nowrap>filename</th>
                        <th nowrap>Download</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($files as $file)
                        <tr>
                            <td>{{$file['creationDate']}}</td>
                            <td>{{$file['path']}}</td>
                            <td><a href="export/download/{{$file['path']}}">Export File</a></td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        @endif
        <div class="button-container text-center">
            <ul class="button-group" style="float: right; margin-right: .95rem">
                <li><a href="/superadmin/export" class="button main"><i class="fi-play"></i>Maak een nieuwe export</a>
                </li>
            </ul>
        </div>
    </div> <!-- end row -->
    <script>
      var filename = '{{ $filename }}';
      if (filename) {
        setInterval(function () {
          checkExport();
        }, 5000);
        function checkExport() {
          $.ajax({
            url: '/ajax/check-export/' + filename,
            type: 'GET',
            headers: {
              contentType: "application/json; charset=utf-8",
              accept: 'application/json',
              authorization: "{{ csrf_token() }}"
            },

            success: function (data) {
              if (data.export) location.assign('/superadmin/exportlist');
            }
          });
        }
      }
    </script>
@stop
