@extends('layoutsSuperadmin.master')

@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">{{ $company->company_name }}</h3>
</div>

@if(Session::has('message'))
    <div class="row">
        <div class="large-12 columns">
            <p class="alert-box {{ Session::get('message-type', 'success') }}">{{ Session::get('message') }}</p>
        </div>
    </div>

@endif

<div class="row superadmin">

    <div class="large-12 columns">
        <?php
            $activeUsers = 0;
            $inactiveUsers = 0;
            foreach ($invites as $invite) {
                if ($invite->invitecode === '(used)') {
                    $activeUsers++;
                }else{
                    $inactiveUsers++;
                }
            }
        ?>

        <p><strong style="font-size:30px;">{{ trans('messages.heading_company_detail') }}</strong></p>


        {{ Form::open(array('url' => "superadmin/bedrijven/".$company->id."/edit", 'method' => 'post')) }}

        <table class="table table-striped">
            <tbody>
                <tr>
                    <td>{{ trans('messages.company') }}</td>
                    <td>
                        <input id="companyName" name="company_name" type="text" value="{{ $company->company_name }}" />
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('messages.company_number') }}</td>
                    <td>
                        <input id="companyNumber" name="ondernemingsnummer" type="text" value="{{ $company->ondernemingsnummer }}" />
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('messages.association_type') }}</td>
                    <td>
                        <input id="association_type" name="association_type" type="text" value="{{ $company->association_type }}" />
                    </td>
                </tr>
                <tr>
                    <td>{{ trans('messages.region') }}</td>
                    <td>
                        <input id="region" name="region" type="text" value="{{ $company->region }}" />
                    </td>
                </tr>
                {{--<tr>--}}
                    {{--<td>{{ trans('messages.email') }}</td>--}}
                    {{--<td>--}}
                        {{--<input id="companyEmail" name="companyEmail" type="email" value="{{ $companyDetail->email }}" disabled />--}}
                    {{--</td>--}}
                {{--</tr>--}}
            </tbody>
        </table>

            <div class="pull-left">
                {{ Form::submit('Wijzigingen opslaan', array('class' => 'button main')) }}

            </div>
            {{ Form::close() }}

            <div class="pull-left" style="margin-left: 20px;">
                {{ Form::open(array('url' => "superadmin/bedrijven/".$company->id."/edit", 'method' => 'delete', 'class' => 'form-delete')) }}
                {{ Form::submit('Bedrijf verwijderen', array('class' => 'button main btn-danger btn-delete', 'style' => 'background-color:#B20000;')) }}
                {{ Form::close() }}
            </div>


            {{ Form::close() }}

    </div>
</div> <!-- end row -->

@stop

@section('script')
    <script>
        $('.form-delete').submit(function(e){
           return confirm('Ben je zeker dat je dit bedrijf (met alle medewerkers, scans en antwoorden) wilt verwijderen?');
        });
    </script>
@endsection
