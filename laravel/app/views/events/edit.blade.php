@extends('layoutsSuperadmin.master')

@section('content')

    <h1>Edit Event</h1>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            plugins: ["link","code"],
            selector:'textarea',
            menu: [ ],
            toolbar: [
                "bold italic | link  | bullist numlist | list | code"
            ]
        });

    </script>

    @if ($errors->any())
         @include('events/form-errors')
    @endif

    {{ Form::model($event, array('method' => 'PATCH', 'route' => array('superadmin.events.update', $event->id), 'files' => true)) }}

    <div class="form">

        @include('events/form-partial')

        <div class="row">
             <div class="small-12 columns action-buttons">
                 {{ link_to_route('superadmin.events.index', 'Cancel',null, array('class' => 'button secondary')) }}
                 {{ Form::submit('Update', array('class' => 'button success')) }}
             </div>
         </div> <!-- end row -->
    </div>
    {{ Form::close() }}

@stop
