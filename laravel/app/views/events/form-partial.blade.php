<div class="row">
    <div class="small-3 columns">{{ Form::label('title', 'titel:') }}</div>
    <div class="small-9 columns">{{ Form::text('title') }}
        @if ($errors->has('title'))
            <small class="error"><?php echo $errors->first('title')  ?></small>
        @endif
    </div>
</div> <!-- end row -->


<div class="row">
    <div class="small-3 columns">{{ Form::label('description', 'omschrijving:') }}</div>
    <div class="small-9 columns">{{ Form::textarea('description') }}
        @if ($errors->has('description'))
            <small class="error"><?php echo $errors->first('description')  ?></small>
        @endif
        <br>
    </div>
</div> <!-- end row -->

<div class="row type-row">
    <div class="small-3 columns">{{ Form::label('datetime', 'datum &amp; tijd:') }}</div>
    <div class="small-9 columns"> {{  Form::text('datetime')}}
        @if ($errors->has('datetime'))
            <small class="error"><?php echo $errors->first('datetime')  ?></small>
        @endif
        <br>
    </div>
</div> <!-- end row -->

<script src="/js/jquery.datetimepicker.full.min.js"></script>
<script>
    $(document).ready(function () {
        $.datetimepicker.setLocale('nl');
        $('#datetime').datetimepicker({
            inline:true,
            format:'Y-m-d H:i:s'
        });
    });

</script>
