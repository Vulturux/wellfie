@extends('layoutsSuperadmin.master')


@section('content')

    <div class="box-header clearfix">
        <h3 class="main-title">Events overview</h3>
    </div>

    <div class="row superadmin">

        <div class="large-12 columns">

            <p>{{ link_to_route('superadmin.events.create', 'Add new event', array(), array('class' => 'button success tiny')) }}</p>

            @if ($events->count())
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th width="5%">id</th>
                        <th width="30%">titel</th>
                        <th>datum en tijd</th>
                        <th>link</th>
                        <th width="15%"></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($events as $event)
                        <tr>
                            <td>{{ $event->id }}</td>
                            <td>
                                <h4>{{ $event->title }}</h4>
                                <!--{{ $event->description }}-->
                            </td>
                            <td>
                                {{$event->datetime}}
                            </td>
                            <td>
                                <a href="{{$event->link}}" target="_blank">{{$event->link}}</a>
                            </td>
                            <td>
                            {{ link_to_route('superadmin.events.show', 'Edit',  array($event->id), array('class' => 'button tiny')) }}

                                {{ Form::open(array('method' => 'DELETE', 'route' => array('superadmin.events.destroy', $event->id), 'class' => 'delete')) }}
                                    {{ Form::submit('Delete', array('class'=> 'button tiny alert')) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>

                {{ $events->links() }}

            @else
                Nog geen events in de database
            @endif

        </div>
    </div> <!-- end row -->
@stop
