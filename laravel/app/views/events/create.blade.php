@extends('layoutsSuperadmin.master')


@section('content')

    <h1>Create Event</h1>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            plugins: ["link","code"],
            selector:'textarea',
            menu: [ ],
            toolbar: [
                "bold italic | link  | bullist numlist | list | code"
            ]
        });

    </script>

    @if ($errors->any())
        @include('documentation/form-errors')
    @endif


    {{ Form::open(array('route' => 'superadmin.events.store', 'files' => true)) }}

        <div class="form">

            @include('events/form-partial')

            <div class="row">
                <div class="small-12 columns action-buttons">
                     {{ Form::submit('Submit', array('class' => 'button')) }}
                </div>
            </div>
        </div>

    {{ Form::close() }}

@stop
