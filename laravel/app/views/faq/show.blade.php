@extends('layoutsSuperadmin.master')

@section('content')

<h1>Faq detail</h1>

{{ link_to_route('superadmin.faq.edit', 'Edit',  array($faq->id), array('class' => 'button tiny')) }}
{{ link_to_route('superadmin.faq.index', 'Back to overview',  array(), array('class' => 'button tiny')) }}

<table class="table-striped table-bordered">
    <tr>
        <th>vraag</th>
        <td>{{ $faq->vraag }}</td>
    </tr>
    <tr>
        <th>order</th>
        <td>{{ $faq->order }}</td>
    </tr>
    <tr>
        <th>antwoord</th>
        <td>{{ $faq->antwoord }}</td>
    </tr>
</table>


@stop
