@extends('layoutsSuperadmin.master')

@section('content')

    <div class="box-header clearfix">
        <h3 class="main-title">FAQ overview</h3>
    </div>

    <div class="row superadmin">

        <div class="large-12 columns">

            <p>{{ link_to_route('superadmin.faq.create', 'Add new question', array(), array('class' => 'button success tiny')) }}</p>

            @if ($faq->count())
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th colspan="3">vraag & antwoord</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($faq as $faqrow)
                        <tr>
                            <td>{{ $faqrow->id }}</td>
                            <td colspan="3">
                                <h4>{{ $faqrow->vraag }}</h4>
                                {{ $faqrow->antwoord }}
                            </td>
                            <td>{{ link_to_route('superadmin.faq.show', 'Show',  array($faqrow->id), array('class' => 'button tiny')) }}
                            {{ link_to_route('superadmin.faq.edit', 'Edit',  array($faqrow->id), array('class' => 'button tiny')) }}

                                {{ Form::open(array('method' => 'DELETE', 'route' => array('superadmin.faq.destroy', $faqrow->id))) }}
                                    {{ Form::submit('Delete', array('class'=> 'button tiny alert')) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>

                {{ $faq->links() }}

            @else
                Nog geen faq in db
            @endif

        </div>
    </div> <!-- end row -->

@stop
