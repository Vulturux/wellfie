@extends('layoutsSuperadmin.master')


@section('content')

    <h1>Create FAQ</h1>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            plugins: ["code","link"],
            selector:'textarea',
            menu: [ ],
            toolbar: [
                "bold italic | link  | bullist numlist | list | code"
            ]
        });

    </script>

    @if ($errors->any())
        @include('faq/form-errors')
    @endif


    {{ Form::open(array('route' => 'superadmin.faq.store')) }}

        <div class="form">

            @include('faq/form-partial')

            <div class="row">
                <div class="small-12 columns action-buttons">
                     {{ Form::submit('Submit', array('class' => 'button')) }}
                </div>
            </div>
        </div>

    {{ Form::close() }}

@stop
