@extends('layoutsSuperadmin.master')

@section('content')

    <h1>Edit FAQ</h1>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            plugins: ["code","link"],
            selector:'textarea',
            menu: [ ],
            toolbar: [
                "bold italic | link  | bullist numlist | list | code"
            ]
        });

    </script>

    @if ($errors->any())
         @include('faq/form-errors')
    @endif

    {{ Form::model($faq, array('method' => 'PATCH', 'route' => array('superadmin.faq.update', $faq->id))) }}

    <div class="form">

        @include('faq/form-partial')

        <div class="row">
             <div class="small-12 columns action-buttons">
                 {{ link_to_route('superadmin.faq.show', 'Cancel', $faq-> id, array('class' => 'button secondary')) }}
                 {{ Form::submit('Update', array('class' => 'button success')) }}
             </div>
         </div> <!-- end row -->
    </div>
    {{ Form::close() }}

@stop
