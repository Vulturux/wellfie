<div class="row">
    <div class="small-3 columns">{{ Form::label('vraag', 'vraag:') }}</div>
    <div class="small-9 columns">{{ Form::text('vraag') }}
        @if ($errors->has('vraag'))
            <small class="error"><?php echo $errors->first('vraag')  ?></small>
        @endif
    </div>
</div> <!-- end row -->


<div class="row">
    <div class="small-3 columns">{{ Form::label('order', 'order:') }}</div>
    <div class="small-9 columns">{{ Form::text('order') }}
        @if ($errors->has('order'))
            <small class="error"><?php echo $errors->first('order')  ?></small>
        @endif
    </div>
</div> <!-- end row -->

<div class="row">
    <div class="small-3 columns">{{ Form::label('category_naam', 'category_naam:') }}</div>
    <div class="small-9 columns"> {{ Form::text('category_naam', null, ['class' => 'typeahead']) }}
        @if ($errors->has('category_naam'))
            <small class="error"><?php echo $errors->first('category_naam')  ?></small>
        @endif
    </div>
</div> <!-- end row -->

<div class="row">
    <div class="small-3 columns">{{ Form::label('antwoord', 'antwoord:') }}</div>
    <div class="small-9 columns">{{ Form::textarea('antwoord') }}
        @if ($errors->has('type'))
            <small class="error"><?php echo $errors->first('type')  ?></small>
        @endif
    </div>
</div> <!-- end row -->

<script src="/js/typeahead.bundle.js"></script>
<script>
    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    var categories = {{json_encode($categories)}};

    $(document).ready(function () {
        $('.typeahead').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'categories',
                    source: substringMatcher(categories)
                });
    });

</script>

