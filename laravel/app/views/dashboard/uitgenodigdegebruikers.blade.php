@extends('layoutsWerknemer.master')


@section('content')


    <div class="box-header clearfix">
        <h3 class="main-title">Nodig uw medewerkers uit</h3>
    </div>

    <div class="row werknemers-uitnodigen">

        <div class="large-12 columns">


            <ul class="tabs tabs-top">
                @for($i=1; $i <= $scan_id; $i++)
                    <li class="tab-title @if($i === $current_scan_id) active @endif" role="presentation"><a
                                href="{{url('werkgevers/uitgenodigdewerknemers?scan='.$i)}}" role="tab" tabindex="0"
                                aria-selected="true" aria-controls="panel2-1">Scan {{$i}}</a></li>
                @endfor
            </ul>
            <div class="content @if($scan_id !== $current_scan_id) active @endif" id="panel2">
                <div class="small-12">
                    <div class="mailed-box">
                        <div id="flashmessages">
                            @if(isset($error))
                                <div data-alert class="alert-box alert">
                                    {{$error}}
                                    <a href="#" class="close">&times;</a>
                                </div>
                            @elseif(isset($success))
                                <div data-alert class="alert-box success">
                                    {{$success}}
                                    <a href="#" class="close">&times;</a>
                                </div>

                            @endif
                        </div>

                        <h4>Reeds uitnodiging ontvangen:</h4>

                        <table style="width:100%" class="table">
                            <thead>
                            <tr>
                                <th>E-mail</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($werknemers as $werknemer)
                                <tr data-userid="{{ $werknemer->id}}">
                                    <td>{{ $werknemer->email }} </td>
                                    <td class="small positie">
                                        @if($werknemer->userScan() !== null)
                                            <span class="success label">Geactiveerd</span>
                                        @else
                                            <span class="warning label">Uitgenodigd</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($scan_id === $current_scan_id)
                                            <a data-href="{{ URL('werkgevers/reinvite') }}/{{ $werknemer->id }}"
                                                    data-userid="{{ $werknemer->user_id }}"
                                                    data-email="{{ $werknemer->email }}" class="js-reinvite reinvite">Stuur
                                                nieuwe uitnodiging</a>
                                        @endif
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                        <div class="button-container text-center">
                            <ul class="button-group">
                                <li>  <a href="/werkgevers/dashboard" class="button main"><i class="fi-play"></i>  Naar het dashboard</a></li></li>
                            </ul>
                        </div>
                    </div>
                </div>  <!-- end small-9 -->
            </div> <!-- end PANEL #2 -->

        </div> <!-- end large-12columns -->

    </div>   <!-- end row -->

    <div id="resend-modal" class="reveal-modal" data-reveal>
        <div class="row ">
            <div class="small-2 columns">
                <label class="inline">Aan</label>
            </div>
            <div class="small-10 columns">
                <span class="recipient"></span>
            </div>
        </div>
        <div class="row onderwerp">
            <div class="small-2 columns">
                <label class="inline">Onderwerp</label>
            </div>
            <div class="small-10 columns">
                <input type="text" placeholder="" value="Wellfie: uitnodiging" name="resend-subject" class="required">
            </div>
        </div>

        <div class="row bericht">
            <div class="small-2 columns">
                <label class="inline">Bericht</label>
            </div>
            <div class="small-10 columns">
                <label>
                    <textarea name="resend-message" class="required" rows="15"><?php echo $defaultMailMessage ?></textarea>
                </label>
            </div>
        </div>
        <div class="row ">
            <div class="small-2 columns">
            </div>
            <div class="small-10 columns">
                <ul class="button-group" style="float:right">
                    <li>
                        <button class="button secondary js-close-resend">Annuleren</button>

                    </li>
                    <li>
                        <button class="button main js-submit-resend" type="submit">
                            Verzenden
                        </button>
                    </li>
                </ul>
            </div>
        </div>

    </div>


@stop

@section('script')
    <script>
      invite.init({
        scanId: {{$current_scan_id}}
      });
    </script>

    <script src="/js/datatables/datatables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            var $table = $('.table');

            var oTable = $table.DataTable({
                processing: true,
                fixedHeader: true,
                paging: false,
                dom: "<'right' f><r>"+
                "t"

            });

        });
    </script>
@endsection
