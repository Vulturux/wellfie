@extends('layoutsWerknemer.master')


@section('content')
    <div class="box-header clearfix">
        <h3 class="main-title">Uw dashboard</h3>
    </div>

    <div class="row">

        <div class="medium-12 columns ">
            @if(!$current_scan->scan_finished)
                <div class="large-7 columns">
                    <h3>Lopende scan</h3>

                    <p>Uw huidige scan is <strong>Scan {{$current_scan->scan_id}}</strong></p>
                    <p>U heeft <strong>{{count($invitedEmployees)}} medewerkers</strong> uitgenodigd. Hiervan hebben er
                        momenteel
                        <strong>{{count($completed_scans)}}</strong> medewerkers de vragenlijst afgewerkt.</p>
                    <a class="button default" href="/werkgevers/werknemersuitnodigen"><i class="fi-mail"></i> Werknemers
                        Uitnodiging</a>
                    <a class="button alert" href="/werkgevers/scanafsluiten"><i class="fi-stop"></i> Scan afsluiten</a>

                    <div style="color:#FF0000">
                        Let op. Je kan de vragenlijst maar één keer afsluiten.
                    </div>
                </div>
                <div class="large-5 columns">
                    <div class="panel">
                        Wanneer de bevraging bij de medewerkers afgesloten wordt, kunnen de resultaten opgehaald worden:
                        <ul>
                            <li>indien <strong>minstens 10 medewerkers</strong> de vragenlijst hebben afgewerkt kan je
                                na
                                afsluiten <strong>beleidsfeedback én een groepsrapport</strong> van de medewerkers
                                raadplegen.
                            </li>
                            <li>indien <strong>minder dan 10</strong> vragenlijsten werden ingevuld wordt na het
                                afsluiten
                                <strong>enkel de beleidsfeedback</strong> aangemaakt. Dit om de privacy van de
                                medewerkers
                                te garanderen.
                            </li>
                        </ul>


                        Werknemers die de vragenlijst invullen, krijgen altijd persoonlijke feedback.

                    </div>
                </div>

            @else
                <h3>Er zijn momenteel geen lopende scans voor uw bedrijf</h3>
                <p>Start een nieuwe scan of bekijk onderstaande tabel om de deelnemers en rapporten van eerdere scans te raadplegen. </p>
                <p><a class="button default" href="/werkgevers/startnewscan"><i class="fi-play"></i> Start een nieuwe scan</a></p>

            @endif

        </div>


    </div>

    <div class="row">
        <div class="large-12 columns">
            <h3>Alle scans voor {{ Auth::user()->company->company_name }}</h3>

            <table style="width:100%">
                <tbody>
                @foreach($companyScans as $scan)
                    <tr>
                        @if($scan->scan_finished)
                            <td><i class="fi-stop" style="color:#FF0000;"></i> Scan {{$scan->scan_id}}: deze scan is
                                afgesloten
                            </td>
                            <td style="text-align: right;">
                                <button href="#" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false"
                                        class="tiny button dropdown" style="margin-bottom:0;">
                                    Acties
                                </button>
                                <br>
                                <ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true">
                                    <li style="text-align: left;">
                                        <a href="/werkgevers/uitgenodigdewerknemers?scan={{$scan->scan_id}}"><i
                                                    class="fi-torsos-all"></i> Uitgenodigde werknemers</a>
                                    </li>
                                    <li style="text-align: left;">
                                        <a href="/werkgevers/eindresultaat/{{$scan->scan_id}}"><i
                                                    class="fi-torso-business"></i> Uw beleidsfeedback</a>
                                    </li>
                                    <li style="text-align: left;">
                                        <a href="/werkgevers/wellfierapport/{{$scan->scan_id}}"><i class="fi-book"></i> Uw Wellfie rapport</a>
                                    </li>
                                </ul>
                            </td>
                        @else
                            <td><i class="fi-play" style="color:#009000;"></i> Scan {{$scan->scan_id}}: deze scan is
                                momenteel lopende
                            </td>
                            <td style="text-align: right;">
                                <button href="#" data-dropdown="drop3" aria-controls="drop1" aria-expanded="false"
                                        class="tiny button dropdown" style="margin-bottom:0;">
                                    Acties
                                </button>
                                <br>
                                <ul id="drop3" data-dropdown-content class="f-dropdown" aria-hidden="true">
                                    <li style="text-align: left;">
                                        <a href="/werkgevers/uitgenodigdewerknemers?scan={{$scan->scan_id}}"><i
                                                    class="fi-torsos-all"></i> Uitgenodigde werknemers</a>
                                    </li>
                                    <li style="text-align: left;">
                                        <a href="/werkgevers/werknemersuitnodigen"><i class="fi-mail"></i> Werknemers
                                            uitnodigen</a>
                                    </li>
                                    <li style="text-align: left;">
                                        <a href="/werkgevers/eindresultaat/{{$scan->scan_id}}"><i
                                                    class="fi-torso-business"></i> Uw beleidsfeedback</a>
                                    </li>
                                    <li style="text-align: left;">
                                        <a href="/werkgevers/scanafsluiten"><i class="fi-stop"
                                                    style="color:#FF0000;"></i> Scan afsluiten</a>
                                    </li>
                                </ul>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop