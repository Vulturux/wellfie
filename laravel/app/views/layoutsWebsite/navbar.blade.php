@extends('layoutsWebsite.master')


@section('navbar')

    <section class="top-bar-section">
        <ul class="mainmenu right">
            <li class="@if($navPos == 1) active @endif"><a href="/werkvermogen">Wat is werkvermogen?</a></li>
            <li class="@if($navPos == 2) active @endif"><a href="/hoebouwenaanwerkvermogen">Hoe bouwen aan werkvermogen?</a></li>
            <li class="@if($navPos == 7) active @endif"><a href="/nieuws">Nieuws</a></li>
            <li class="@if($navPos == 6) active @endif"><a href="/documentatie">Documentatie</a></li>
            <li class="@if($navPos == 5) active @endif"><a href="/faq">FAQ</a></li>
            <li class="@if($navPos == 4) active @endif"><a href="/overons">Over ons</a></li>
            <li class="@if($navPos == 3) active @endif"><a href="/links">Links</a></li>
        </ul>
    </section>

@stop
