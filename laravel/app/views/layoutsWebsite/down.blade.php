<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>WELLFIE - Your Company's wellbeing in a snapshot</title>

    <link rel="stylesheet" type="text/css" href="/css/foundation.css">
    <link rel="stylesheet" type="text/css" href="/css/foundation-icons.css">
    <link rel="stylesheet" type="text/css" href="/css/style-front-design.css">
    <link rel="stylesheet" type="text/css" href="/css/style-front-responsive.css">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <script src="https://use.typekit.net/zxm7jpl.js"></script>
    <script>try {
            Typekit.load({async: true});
        } catch (e) {
        }</script>

</head>

<body class="home">

<div class="topmenu clearfix">

</div>

<div class="contain-to-grid clearfix">


    <nav class="top-bar" data-topbar role="navigation">
        <div class="section-container">
            <ul class="title-area">
                <li class="name">
                    <h1><a href="../" class="logo">Wellfie</a></h1>
                </li>
                <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>


            <!-- Right Nav Section -->


        </div>
    </nav>
</div>


<!--[if lt IE 9]>
<div class="home-content-block wat">
    <div class="row">
        <div class="panel">You need to upgrade your browser. IE8 or lower is not supported.</div>
    </div>
</div>
<![endif]-->

@yield('content')


<footer id="footer">
    <div class="main-footer">
        <div class="row ">
            <div class="large-12 columns">
                <h4>Wellfie is een inititatief van:</h4>
                <ul class="logos clearfix">
                    <li><a href="http://www.idewe.be" target="_blank"><img src="/images/common/idewe-color.png"></a></li>
                    <li><a href="http://www.acerta.be" target="_blank"><img src="/images/common/acerta-color.png"></a></li>
                    <li><a href="http://www.etion.be" target="_blank"><img src="/images/common/etion-color.png"></a></li>
                    <li class="right long"><a href="http://ec.europa.eu/esf" target="_blank"><img src="/images/common/esf-banner.gif"></a></li>
                    <li class="right"><a href="http://ec.europa.eu/esf" target="_blank"><img src="/images/common/valid-esf-and-europe.png"></a></li>
                </ul>
            </div>
        </div>
        <div class="row ">
            <div class="large-10 columns">
                <div class="accent">
                    Vraag over dit project? Stuur een mail naar <a href="mailto:info@wellfie.be">info@wellfie.be</a>.
                </div>
            </div>
        </div>
    </div>
</footer>


<script src="/js/vendor/jquery.js"></script>
<script src="/js/foundation.min.js"></script>
<script src="/js/jquery.form.min.js"></script>


<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
<script src="/js/jquery-validation-1.13.1/dist/jquery.validate.js"></script>

<script src="/js/script-all.js"></script>

<script>
    $(document).foundation();
</script>
<script>
    $(window).bind("load", function () {
        var footer = $("#footer");
        var pos = footer.position();
        var height = $(window).height();
        height = height - pos.top;
        height = height - footer.height();
        if (height > 0) {
            footer.css({
                'margin-top': height + 'px'
            });
        }
    });
</script>
@yield('script')

</body>
</html>
