        <div class="row">
            <div class="small-3 columns">{{ Form::label('question', 'question:') }}</div>
            <div class="small-9 columns">{{ Form::text('question') }}
                @if ($errors->has('question'))
                <small class="error"><?php echo  $errors->first('question')  ?></small>
                @endif
            </div>
        </div> <!-- end row -->

        <div class="row">
            <div class="small-3 columns">Title (block)</div>
            <div class="small-9 columns">
                {{ Form::select('question_titles_id', $questionTitles, $question->question_titles_id ) }}
            </div>
        </div> <!-- end row -->

        <div class="row">
            <div class="small-3 columns">{{ Form::label('question_nr', 'question nr:') }}</div>
            <div class="small-9 columns">{{ Form::text('question_nr') }}
                @if ($errors->has('question_nr'))
                <small class="error"><?php echo  $errors->first('question_nr')  ?></small>
                @endif
            </div>
        </div> <!-- end row -->

        <div class="row">
            <div class="small-3 columns">{{ Form::label('order', 'order:') }}</div>
            <div class="small-9 columns"> {{ Form::text('order') }}
                @if ($errors->has('order'))
                <small class="error"><?php echo  $errors->first('order')  ?></small>
                @endif
            </div>
        </div> <!-- end row -->

        <div class="row">
            <div class="small-3 columns">{{ Form::label('type', 'type:') }}</div>
            <div class="small-9 columns">{{ Form::text('type') }}
                @if ($errors->has('type'))
                <small class="error"><?php echo  $errors->first('type')  ?></small>
                @endif
            </div>
        </div> <!-- end row -->

