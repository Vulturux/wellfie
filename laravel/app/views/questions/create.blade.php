@extends('layoutsSuperadmin.master')


@section('content')

    <h1>Create Question</h1>

    @if ($errors->any())
        @include('questions/form-errors')
    @endif


    {{ Form::open(array('route' => 'questions.store')) }}

        <div class="form">

            @include('questions/form-partial')

            <div class="row">
                <div class="small-12 columns action-buttons">
                     {{ Form::submit('Submit', array('class' => 'button')) }}
                </div>
            </div>
        </div>

    {{ Form::close() }}

@stop
