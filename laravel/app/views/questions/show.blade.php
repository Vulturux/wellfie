@extends('layoutsSuperadmin.master')

@section('content')

<h1>All questions</h1>

{{ link_to_route('questions.edit', 'Edit',  array($question->id), array('class' => 'button tiny')) }}
{{ link_to_route('questions.index', 'Back to overview',  array(), array('class' => 'button tiny')) }}

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>question_nr</th>
        <td>{{ $question->question_nr }}</td>
    </tr>
    <tr>
        <th>question</th>
        <td>{{ $question->question }}</td>
    </tr>
    <tr>
        <th>order</th>
        <td>{{ $question->order }}</td>
    </tr>
    <tr>
        <th>type</th>
        <td>{{ $question->type }}</td>
    </tr>
    <tr>
        <th>question_connection_id</th>
        <td>{{ $question->question_connection_id }}</td>

    </tr>

    </thead>

</table>


@stop
