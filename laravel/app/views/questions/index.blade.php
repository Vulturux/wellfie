@extends('layoutsSuperadmin.master')


@section('content')

    <h1>Questions overview</h1>

    <p>{{ link_to_route('questions.create', 'Add new question', array(), array('class' => 'button success tiny')) }}</p>

    @if ($questions->count())
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>title</th>
                <th>id</th>
                <th>question_nr</th>
                <th>question</th>
                <th>order</th>
                <th>type</th>
                <th>question_connection_id</th>
                <th></th><th></th><th></th>
            </tr>
            </thead>

            <tbody>
            @foreach ($questions as $question)
                <tr>
                    <td>{{ $question->questionTitle->title }}</td>
                    <td>{{ $question->id }}</td>
                    <td>{{ $question->question_nr }}</td>
                    <td>{{ $question->question }}</td>
                    <td>{{ $question->order }}</td>
                    <td>{{ $question->type }}</td>
                    <td>{{ $question->question_connection_id }}</td>
                    <td>{{ link_to_route('questions.show', 'Show',  array($question->id), array('class' => 'button tiny')) }}</td>
                    <td>{{ link_to_route('questions.edit', 'Edit',  array($question->id), array('class' => 'button tiny')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('questions.destroy', $question->id))) }}
                            {{ Form::submit('Delete', array('class'=> 'button tiny alert')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>

        {{ $questions->links() }}

    @else
        There are no questions
    @endif

@stop
