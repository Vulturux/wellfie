@extends('layoutsSuperadmin.master')

@section('content')

    <h1>Edit Question</h1>

    @if ($errors->any())
         @include('questions/form-errors')
    @endif

    {{ Form::model($question, array('method' => 'PATCH', 'route' => array('questions.update', $question->id))) }}

    <div class="form">

        @include('questions/form-partial')

        <div class="row">
             <div class="small-12 columns action-buttons">
                 {{ link_to_route('questions.show', 'Cancel', $question-> id, array('class' => 'button secondary')) }}
                 {{ Form::submit('Update', array('class' => 'button success')) }}

             </div>
         </div> <!-- end row -->
    </div>
    {{ Form::close() }}

@stop
