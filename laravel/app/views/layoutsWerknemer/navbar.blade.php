@extends('layoutsWerknemer.master')


@section('navbar')
    <div class="contain-to-grid top">
        <nav class="top-bar" data-topbar role="navigation">
            <div class="section-container">
                <ul class="title-area">
                    <li class="name">
                        <h1 ><a href="../" class="logo">Wellfie</a></h1>
                    </li>
                    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>

                <section class="top-bar-section">
                    <!-- Right Nav Section -->
                    <ul class="right">
                        <li><a href="#"><i class="fi-database"></i> Vragenlijst</a></li>
                        <li class="has-dropdown">
                            <a href="#">
                                @if(Session::has('impersonate'))
                                    <i class="fi-torsos"></i>
                                    Admin bekijkt {{ Confide::user()->username }}
                                @else
                                    <i class="fi-torso"></i>
                                    User {{ Confide::user()->username }}
                                @endif
                            </a>
                            <ul class="dropdown">
                                <li class=""><a href="/users/logout"><i class="fi-eject"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
            </div>
        </nav>
    </div>

    <div class="">
        <div class="row">
            @if($navPos !== -1)
            <div class="nav-wrapper nav-werknemers">
                <ul class="nav-bar">
                    <li  class="@if($navPos == 0) active @endif   @if($navPos >= 0) visited @endif">
                        <a href="">
                            Demografische variabelen
                        </a>
                    </li>
                    <li class="@if($navPos == 1) active @endif   @if($navPos > 1) visited @endif">
                        <a href="">
                            Gezondheid en functionele capaciteiten
                        </a>
                    </li>
                    <li class="@if($navPos == 2) active @endif   @if($navPos > 2) visited @endif">
                        <a href="">
                            Competenties
                        </a>
                    </li>
                    <li class="@if($navPos == 3) active @endif   @if($navPos > 3) visited @endif">
                        <a href="">
                            Waarden, houding en motivatie
                        </a>
                    </li>
                    <li class="@if($navPos == 4) active @endif   @if($navPos > 4) visited @endif">
                        <a href="">
                            Werk, werkgemeenschap en leiding
                        </a>
                    </li>
                    <li class="last @if($navPos == 5) active @endif   @if($navPos > 5) visited @endif">
                        <a href="">
                            Eindresultaat
                        </a>
                    </li>
                </ul>
            </div>
                @endif
        </div>
    </div>

@stop
