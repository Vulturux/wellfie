<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WELLFIE - Werknemers vragenlijst</title>

    <link rel="stylesheet" type="text/css" href="{{Config::get('app.url')}}/css/foundation.css">
    <link rel="stylesheet" type="text/css" href="{{Config::get('app.url')}}/css/foundation-icons.css">
    <link rel="stylesheet" type="text/css" href="{{Config::get('app.url')}}/css/style-design.css">
    <link rel="stylesheet" type="text/css" href="{{Config::get('app.url')}}/css/style-responsive.css">

    <script src="https://use.typekit.net/zxm7jpl.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

</head>


<body class="werknemers">
<!--[if lt IE 9]>
<div class="browsehappy">
    <p>
        U surft op een <strong>oudere browser</strong> voor dewelke deze site niet geoptimaliseerd is, <a href="http://browsehappy.com/">gelieve te updaten naar een meer recente versie</a> voor een optimale surfervaring.
    </p>
    <span class="close">Sluiten</span>
</div>
<![endif]-->

@yield('navbar')

<section class="row main-content box">
    <div class="icon-corner TL"></div>
    <div class="icon-corner TR"></div>
    <div class="icon-corner BL"></div>
    <div class="icon-corner BR"></div>
    @yield('content')
</section>

    <footer class="footer-bottom">
        <div class="row">
            <div class="medium-8 medium-8 columns">
                <ul class="logos">
                    <li><img src="{{Config::get('app.url')}}/images/common/idewe-grey.png"></li>
                    <li><img src="{{Config::get('app.url')}}/images/common/acerta-grey.png"></li>
                    <li><img src="{{Config::get('app.url')}}/images/common/etion-grey.png"></li>
                    <li><img src="{{Config::get('app.url')}}/images/common/esf-grey.png"></li>
                    <li><img src="{{Config::get('app.url')}}/images/common/appsonly-grey.png"></li>

                </ul>
            </div>
            <div class="medium-4 medium-4 columns">
                <p class="copyright">© 2015 Wellfie</p>
            </div>
        </div>


        <script src="{{Config::get('app.url')}}/js/vendor/jquery.js"></script>
        <script src="{{Config::get('app.url')}}/js/foundation.min.js"></script>

        <script src="/js/jquery.form.min.js"></script>


        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
        <script src="{{Config::get('app.url')}}/js/jquery-validation-1.13.1/dist/jquery.validate.js"></script>

        <script src="{{Config::get('app.url')}}/js/script-all.js"></script>
        <script>
            $(document).foundation();

            $(document).ready(function() {
                $("#js-vragenform").validate();
            });

        </script>
    </footer>

</body>
</html>
