<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WELLFIE - Your Company's wellbeing in a snapshot</title>

    <link rel="stylesheet" type="text/css" href="/css/foundation.css">
    <link rel="stylesheet" type="text/css" href="/css/foundation-icons.css">
    <link rel="stylesheet" type="text/css" href="/css/style-design.css">
    <link rel="stylesheet" type="text/css" href="/css/style-responsive.css">
    <script src="/js/vendor/modernizr.js"></script>

    <script src="https://use.typekit.net/zxm7jpl.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <script src="/js/vendor/jquery.js"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-66679430-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>


<body class="werkgevers">
<!--[if lt IE 9]>
<div class="browsehappy">
    <p>
        U surft op een <strong>oudere browser</strong> voor dewelke deze site niet geoptimaliseerd is, <a href="http://browsehappy.com/">gelieve te updaten naar een meer recente versie</a> voor een optimale surfervaring.
    </p>
    <span class="close">Sluiten</span>
</div>
<![endif]-->

@yield('navbar')

<section class="row main-content box">
    <div class="icon-corner TL"></div>
    <div class="icon-corner TR"></div>
    <div class="icon-corner BL"></div>
    <div class="icon-corner BR"></div>
    @yield('content')
</section>

<footer class="footer-bottom">
    <div class="row">
        <div class="medium-8 medium-8 columns">
            <ul class="logos">
                <li>{{ HTML::image('images/common/idewe-grey.png') }}</li>
                <li>{{ HTML::image('images/common/acerta-grey.png') }}</li>
                <li>{{ HTML::image('images/common/etion-grey.png') }}</li>
                <li>{{ HTML::image('images/common/appsonly-grey.png') }}</li>
                <li>{{ HTML::image('images/common/esf-grey.png') }}</li>
            </ul>
        </div>
        <div class="medium-4 medium-4 columns">
            <p class="copyright">© 2015 Wellfie</p>
        </div>
    </div>
    </div>

    <script src="/js/foundation.min.js"></script>
    <script src="/js/foundation/foundation.tab.js"></script>
    <script src="/js/jquery.form.min.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
    <script src="{{url('js/jquery-validation-1.13.1/dist', 'jquery.validate.js') }}"></script>

    <script src="{{url('js', 'script-all.js') }}"></script>
    <script>
        $(document).foundation();
    </script>

    @yield('script')
</footer>
</body>
</html>
