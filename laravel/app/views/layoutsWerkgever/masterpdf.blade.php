<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WELLFIE - Your Company's wellbeing in a snapshot</title>

    <link rel="stylesheet" href="{{Config::get('app.url')}}/css/foundation.css">
    <link rel="stylesheet" href="{{Config::get('app.url')}}/css/foundation-icons.css">
    <link rel="stylesheet" href="{{Config::get('app.url')}}/css/style-design.css">
    <link rel="stylesheet" href="{{Config::get('app.url')}}/css/style-responsive.css">
    <style>
        table, tr, td, th, tbody, thead, tfoot, .feedback, .huis-resultaat-row {
            page-break-inside: avoid !important;
        }
        p, h2, h3 { orphans: 3; widows: 3; }
        h2, h3 { page-break-after: avoid; }
        #panel1, #panel2, #panel3, #panel4 { page-break-after: always; }
    </style>

</head>


<body class="werkgevers pdf">


<section class="row main-content box">
    <div class="icon-corner TL"></div>
    <div class="icon-corner TR"></div>
    <div class="icon-corner BL"></div>
    <div class="icon-corner BR"></div>
    @yield('content')
</section>

<footer class="footer-bottom">
    <div class="row">
        <div class="medium-8 medium-8 columns">
            <ul class="logos">
                <li><img src="{{Config::get('app.url')}}/images/common/idewe-grey.png"></li>
                <li><img src="{{Config::get('app.url')}}/images/common/acerta-grey.png"></li>
                <li><img src="{{Config::get('app.url')}}/images/common/etion-grey.png"></li>
                <li><img src="{{Config::get('app.url')}}/images/common/esf-grey.png"></li>
            </ul>
        </div>
        <div class="medium-4 medium-4 columns">
            <p class="copyright">© 2016 Wellfie</p>
        </div>
    </div>
</footer>
</body>
</html>
