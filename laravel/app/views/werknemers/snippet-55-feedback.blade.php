<strong>OPLEIDINGEN</strong><br>
Neemt u zelf initiatief als u graag opleidingen volgt en er geen opleidingscultuur
is in de onderneming? Wellicht kan u terecht bij het sectorfonds waar de werknemers
van aangesloten ondernemingen opleidingen kunnen volgen die de werkgever niet al te
veel kosten of soms ook gratis zijn.