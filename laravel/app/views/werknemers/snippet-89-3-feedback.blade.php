Langdurig beeldschermwerk uitvoeren en/of langdurig eenzelfde houding aannemen, zijn belangrijke risicofactoren voor het ontstaan van werk-gerelateerde klachten en overbelastingletsels. Beter voorkomen dan genezen!<br>
<br>
Probeer allereerst de werkomstandigheden te verbeteren:<br>
<ul>
	<li>Richt de werkplek goed in.</li>
	<li>Zorg dat de hoogte van uw stoel, tafel en beeldscherm correct is ingesteld.</li>
	<li>Zorg voor voldoende bewegingsruimte</li>
	<li>Beperk de reikafstand (sta/zit zo dicht mogelijk bij het werkvlak).</li>
	<li>Gebruik hulpmiddelen.</li>
	<li>Het kan nuttig zijn dit eens ergonomisch te bekijken en, indien nodig, aanpassingen te doen. Hiervoor kan u contact opnemen met uw leidinggevende.</li>
</ul>
Optimaliseer uw eigen handelen:<br>
<ul>
	<li>Zorg voor een goede werkhouding (hou gewrichten in de neutrale positie).</li>
	<li>Wissel houdingen af (bijvoorbeeld wissel zitten af met staan tijdens een telefoongesprek of vergadering).</li>
	<li>Neem geregeld (elk uur) een korte pauze om te bewegen.</li>
	<li>Sta eens op, loop eens rond of doe wat kleine oefeningen (bijvoorbeeld rek uw armen uit, draai uw hoofd naar links en rechts, rol uw schouders naar achter en voren, krul uw tenen, draai uw voeten, duw op uw tenen, duw op uw hielen, …)</li>
</ul>
