<strong>BURNOUT</strong><br>
Proficiat! U behoort bij de 25 % werknemers met de laagste burn-outscores.
U blijkt niet emotioneel uitgeput te geraken van uw job. Blijf op dezelfde manier met de lasten en lusten van uw job omgaan.