Een werknemer mag slechts gratis op consultatie wanneer hij gezondheidsklachten heeft die verbonden zijn aan het werk
of aan zijn werkpost, zoals bijvoorbeeld rugpijn door tillen van zware lasten of irritatie aan de luchtwegen
veroorzaakt door een product waarmee gewerkt wordt. <br>
<br>
Wenst een werknemer de arbeidsgeneesheer te consulteren, dan kan hij zich sinds kort direct en discreet een
raadpleging bij de arbeidsgeneesheer vragen. De namen en contactgegevens van de arbeidsgeneesheer
en andere preventieadviseurs moeten door de werkgever op een gemakkelijk toegankelijke plaats vermeld worden
of vindt u in het arbeidsreglement. <br>
<br>
