@extends('layoutsWerknemer.masterpdf')


@section('content')

    <div class="box-header clearfix">
        <h3 class="main-title">Uw huis van werkvermogen</h3>
    </div>

    <div class="row huis-resultaat">

        <div class="medium-12 columns ">
            <div class="tussenresultaat visualisation">
                <img src="{{Config::get('app.url')}}/images/visualisation/licht-uit.png" class="viz-image">

                <div class="viz-level ">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping1_{{ $scoresLevelOneEvaluation }}.png">
                </div>
                <div class="viz-level mannetje">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping1_mannetje_{{ $scoresLevelOneEvaluation }}.png">
                </div>

                <div class="viz-level">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping2_{{ $scoresLevelTwoEvaluation }}.png">
                </div>
                <div class="viz-level mannetje">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping2_mannetje_{{ $scoresLevelTwoEvaluation }}.png">
                </div>

                <div class="viz-level ">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping3_{{ $scoresLevelThreeEvaluation }}.png">
                </div>
                <div class="viz-level mannetje">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping3_mannetje_{{ $scoresLevelThreeEvaluation }}.png">
                </div>

                <div class="viz-level">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping4_{{ $scoresLevelFourEvaluation }}.png">
                </div>
                <div class="viz-level mannetje">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping4_mannetje_{{ $scoresLevelFourEvaluation }}.png">
                </div>

                <div class="viz-level">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping5_{{ $scoresLevelFiveEvaluation }}.png">
                </div>
                <div class="viz-level mannetje">
                    <img src="{{Config::get('app.url')}}/images/visualisation/verdieping5_mannetje_{{ $scoresLevelFiveEvaluation }}.png">
                </div>

            </div>
        </div>

    </div>


    <div class="row detail-results">

        <div class="large-8 large-offset-2 end columns">
            <h5 class="verdiep">Het dak</h5>
            <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelFiveEvaluation }}}.png')">Werkvermogen</h4>

            <?php
            if(!isset($scoresAll['29']['points'])){
                $scoresAll['29']['points']=0;
            }
            $werkvermogenScore = $scoresAll['29']['points']+$scoresAll['30']['points']+$scoresAll['31']['points']+$scoresAll['32']['points'];
            ?>

            @if($werkvermogenScore <= 9)
                <div class="feedback goed">@include('werknemers/snippet-27-3-feedback')</div>
            @elseif ($werkvermogenScore > 15)
                <div class="feedback">@include('werknemers/snippet-27-1-feedback')</div>
            @else
                <div class="feedback">@include('werknemers/snippet-27-2-feedback')</div>
            @endif

            @if($scoresAll['30']['points'] > $scoresAll['31']['points'])
                <div class="feedback">@include('werknemers/snippet-27-4-feedback')</div>
            @endif
            @if($scoresAll['31']['points'] > $scoresAll['30']['points'])
                <div class="feedback">@include('werknemers/snippet-27-5-feedback')</div>
            @endif
            @if(($scoresAll['30']['points'] == $scoresAll['31']['points']))
                <div class="feedback">@include('werknemers/snippet-27-6-feedback')</div>
            @endif


        </div>
    </div>

    <div class="row  detail-results light">

        <div class="large-8 large-offset-2 end columns">

            <h5 class="verdiep">Verdieping 4</h5>

            <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelFourEvaluation }}}.png')">Werk, werkgemeenschap en leiding</h4>

            <!-- WERK -->
            @if($scoresAll['86']['points'] > 0 || $scoresAll['86']['answer'] == 'eens per week')
                <div class="feedback slecht">@include('werknemers/snippet-86-feedback')</div>
            @endif

            @if( ($scoresAll['89']['points'] > 0 || $scoresAll['89']['answer'] == 'eens per week') && (($scoresAll['88']['points'] > 0 || $scoresAll['88']['answer'] == 'eens per week')||($scoresAll['87']['points'] > 0 || $scoresAll['87']['answer'] == 'eens per week')))
            <!-- NEW indien beeldschermwerk = een paar keer per week of dagelijks
              EN eenzelfde houding en/of
              repetitieve bewegingen = een paar keer per week of dagelijks dan volgende feedback -->

                <div class="feedback slecht">@include('werknemers/snippet-89-3-feedback')</div>

            @else
                @if($scoresAll['87']['points'] > 0 || $scoresAll['87']['answer'] == 'eens per week')
                    <div class="feedback slecht">@include('werknemers/snippet-87-feedback')</div>
                @endif

                <?php
                if(!isset($scoresAll['88']['points'])){
                    $scoresAll['88']['points']=0;
                    $scoresAll['88']['answer']='';
                } ?>

                @if($scoresAll['88']['points'] > 0 || $scoresAll['88']['answer'] == 'eens per week')
                    <div class="feedback slecht">@include('werknemers/snippet-88-feedback')</div>
                @endif

                @if($scoresAll['89']['points'] > 0 && $scoresAll['88']['points'] == 0)
                    <div class="feedback slecht">@include('werknemers/snippet-89-1-feedback')</div>
                @endif

                @if($scoresAll['89']['points'] > 0 && $scoresAll['88']['points'] > 0)
                    <div class="feedback slecht">@include('werknemers/snippet-89-2-feedback')</div>
                @endif

            @endif


            @if($scoresAll['90']['points'] > 0 || $scoresAll['90']['answer'] == 'eens per week')
                <div class="feedback slecht">@include('werknemers/snippet-90-feedback')</div>
            @endif

            @if($scoresAll['91']['points'] > 0 || $scoresAll['91']['answer'] == 'eens per week')
                <div class="feedback slecht">@include('werknemers/snippet-91-feedback')</div>
            @endif

            @if($scoresAll['92']['points'] > 0 || $scoresAll['92']['answer'] == 'eens per week')
                <div class="feedback slecht">@include('werknemers/snippet-92-feedback')</div>
            @endif

            <?php
            if(!isset($scoresAll['93']['points'])){
                $scoresAll['93']['points']=0;
                $scoresAll['93']['answer']='';
            } ?>

            @if($scoresAll['93']['points'] > 0 || $scoresAll['93']['answer'] == 'eens per week')
                <div class="feedback slecht">@include('werknemers/snippet-93-feedback')</div>
            @endif

            @if($scoresAll['94']['points'] > 0 || $scoresAll['94']['answer'] == 'eens per week')
                <div class="feedback slecht">@include('werknemers/snippet-94-feedback')</div>
            @endif

            <?php
            $ergoScore = $scoresAll['95']['points']+$scoresAll['96']['points']+$scoresAll['97']['points'];
            ?>

            @if($ergoScore > 0)
                <div class="feedback slecht">@include('werknemers/snippet-95-feedback')</div>
            @endif

            @if($scoresAll['98']['points'] > 0)
                <div class="feedback slecht">@include('werknemers/snippet-98-feedback')</div>
            @endif

        </div>
        <div class="large-6 columns">

        </div>
    </div> <!-- end row -->

    <div class="row  detail-results">

        <div class="large-8 large-offset-2 end columns">

            <h5 class="verdiep">Verdieping 3</h5>

            <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelThreeEvaluation }}}.png')">Waarden, houding en motivatie</h4>

            <!-- WAARDEN : vkw-->

            <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelThree[0]['evaluation'] }}}.png')">Waarden en normen</h5>

            @if($scoresAll['68']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-68-feedback')</div>
            @endif

            @if($scoresAll['69']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-69-feedback')</div>
            @endif

            @if($scoresAll['70']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-70-feedback')</div>
            @endif

            @if($scoresAll['71']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-71-feedback')</div>
            @endif

        <!-- WAARDEN : verbondenheid -->

            <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelThree[1]['evaluation'] }}}.png')">Persoonskenmerken</h5>

            <?php

            $verbondenheidScore = $scoresAll['72']['points']+$scoresAll['73']['points'];
            $priveScore = $scoresAll['74']['points']+$scoresAll['75']['points'];
            $tevredenheidsScore = $scoresAll['76']['points'];
            $vertrouwenmanagementScore = $scoresAll['77']['points'];
            $ontslagScore = $scoresAll['78']['points']+$scoresAll['79']['points']+$scoresAll['80']['points']+$scoresAll['81']['points'];
            $autonoomScore = $scoresAll['82']['points']+$scoresAll['83']['points']+$scoresAll['84']['points']+$scoresAll['85']['points'];
            ?>


            @if($verbondenheidScore >= 1)

                <div class="feedback slecht">@include('werknemers/snippet-72-1-feedback')</div>
            @endif
            @if(($scoresAll['72']['answer']=='eens' || $scoresAll['72']['answer']=='helemaal eens') && ($scoresAll['73']['answer']=='eens' || $scoresAll['73']['answer']=='helemaal eens'))
                <div class="feedback slecht">@include('werknemers/snippet-72-2-feedback')</div>
            @endif


            @if($priveScore >= 1)
            <!-- slecht -->
                <div class="feedback slecht">@include('werknemers/snippet-74-2-feedback')</div>
            @endif
            @if(($scoresAll['74']['answer']=='oneens' || $scoresAll['74']['answer']=='helemaal oneens') && ($scoresAll['75']['answer']=='oneens' || $scoresAll['75']['answer']=='helemaal oneens'))
            <!-- goed -->
                <div class="feedback slecht">@include('werknemers/snippet-74-1-feedback')</div>
            @endif


            @if($tevredenheidsScore >= 0.5)
                <div class="feedback slecht">@include('werknemers/snippet-76-1-feedback')</div>
            @endif
            @if($scoresAll['76']['answer']=='eens' || $scoresAll['76']['answer']=='helemaal eens')
                <div class="feedback slecht">@include('werknemers/snippet-76-2-feedback')</div>
            @endif


            @if($vertrouwenmanagementScore >= 0.5)
                <div class="feedback slecht">@include('werknemers/snippet-77-1-feedback')</div>
            @endif
            @if($scoresAll['77']['answer']=='eens' || $scoresAll['77']['answer']=='helemaal eens')
                <div class="feedback slecht">@include('werknemers/snippet-77-2-feedback')</div>
            @endif


            @if($ontslagScore < 0.5)
                <div class="feedback slecht">@include('werknemers/snippet-78-1-feedback')</div>
            @endif
            @if(($scoresAll['78']['answer']=='eens' || $scoresAll['78']['answer']=='helemaal eens') && ($scoresAll['79']['answer']=='eens' || $scoresAll['79']['answer']=='helemaal eens') && ($scoresAll['80']['answer']=='eens' || $scoresAll['80']['answer']=='helemaal eens') && ($scoresAll['81']['answer']=='oneens' || $scoresAll['81']['answer']=='helemaal oneens'))
                <div class="feedback slecht">@include('werknemers/snippet-78-2-feedback')</div>
            @endif

            @if($autonoomScore >= 0.5)
                <div class="feedback slecht">@include('werknemers/snippet-82-1-feedback')</div>
            @endif
            @if(($scoresAll['82']['answer']=='eens' || $scoresAll['82']['answer']=='helemaal eens') && ($scoresAll['83']['answer']=='eens' || $scoresAll['83']['answer']=='helemaal eens') && ($scoresAll['84']['answer']=='eens' || $scoresAll['84']['answer']=='helemaal eens') && ($scoresAll['85']['answer']=='eens' || $scoresAll['85']['answer']=='helemaal eens'))
                <div class="feedback slecht">@include('werknemers/snippet-82-2-feedback')</div>
            @endif

        </div>

    </div>

    <div class="row  detail-results light">
        <div class="large-8 large-offset-2 end columns">

            <h5 class="verdiep">Verdieping 2</h5>

            <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelTwoEvaluation }}}.png')">Competenties</h4>

            <!-- COMPETENTIES : rekrutering-->

            <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelTwo[0]['evaluation'] }}}.png')">Zichtbaarheid op arbeidsmarkt</h5>

            @if($scoresAll['46']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-46-feedback')</div>
            @endif

            @if($scoresAll['47']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-47-feedback')</div>
            @endif

            @if($scoresAll['48']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-48-feedback')</div>
            @endif

            @if($scoresAll['49']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-49-feedback')</div>
            @endif

            @if($scoresAll['50']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-50-feedback')</div>
            @endif

            @if($scoresAll['51']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-51-feedback')</div>
            @endif

        <!-- COMPETENTIES : talentmanagement-->

            <h5></h5>
            <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelTwo[1]['evaluation'] }}}.png')">Talentmanagement</h5>

            @if($scoresAll['52']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-52-feedback')</div>
            @endif

            @if($scoresAll['53']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-53-feedback')</div>
            @endif

            @if($scoresAll['54']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-54-feedback')</div>
            @endif

            @if($scoresAll['55']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-55-feedback')</div>
            @endif

            @if($scoresAll['56']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-56-feedback')</div>
            @endif

            @if($scoresAll['57']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-57-feedback')</div>
            @endif

            @if($scoresAll['58']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-58-feedback')</div>
            @endif

            @if($scoresAll['59']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-59-feedback')</div>
            @endif

            @if($scoresAll['60']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-60-feedback')</div>
            @endif

            @if($scoresAll['61']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-61-feedback')</div>
            @endif

            @if($scoresAll['62']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-62-feedback')</div>
            @endif

            @if($scoresAll['63']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-63-feedback')</div>
            @endif
        </div>

    </div>

    <div class="row detail-results last">

        <div class="large-8 large-offset-2 end columns">

            <h5 class="verdiep">Verdieping 1</h5>

            <h4 class="title-block" style="background-image:url('{{Config::get('app.url')}}/images/icons/face{{{ $scoresLevelOneEvaluation }}}.png')">Gezondheid en functionele capaciteiten</h4>

            <!-- 2. GEZONDHEID -->
            <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelOne[0]['evaluation'] }}}.png')">Medische vragen</h5>


            @if($scoresAll['12']['answer'] == 'ja' || $scoresAll['12']['answer'] == 'nee')
                <div class="feedback">@include('werknemers/snippet-12-feedback')</div>
            @endif

            @if($scoresAll['13']['answer'] == 'ja' || $scoresAll['13']['answer'] == 'nee')
                <div class="feedback">@include('werknemers/snippet-13-feedback')</div>
            @endif

            @if($scoresAll['14']['answer'] == 'ja')
                <div class="feedback">@include('werknemers/snippet-14-feedback')</div>
            @endif

        <!-- leefstijlziekte -->
            <?php  if(!isset($scoresAll['16']['points'])){
                $scoresAll['16']['points']=0;
            } ?>
            @if($scoresAll['16']['points'] > 0 || $scoresAll['17']['points'] > 0 || $scoresAll['18']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-15-feedback')</div>
            @endif

            @if($scoresAll['20']['answer'] == 'ja')
            <!-- depressie -->
                <div class="feedback">@include('werknemers/snippet-18-feedback')</div>
            @endif

            @if($scoresAll['21']['answer'] == 'ja')
            <!-- andere ziekte -->
                <div class="feedback">@include('werknemers/snippet-19-feedback')</div>
            @endif

        <!-- 2. GEZONDHEID LEEFSTIJL -->
            <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelOne[2]['evaluation'] }}}.png')">Leefstijl</h5>

            <?php
            $levenstijl="ok";
            if ($scoresAll['23']['points']+$scoresAll['24']['points']+$scoresAll['25']['points']+$scoresAll['26']['points']+$scoresAll['28']['points'] > 0) {
                $levenstijl="nok";
            }
            ?>

        <!--  als je vindt dat je het zelf goed doet (leefgewoonten vr id22 : ja)  -->
            @if($levenstijl == 'ok' && $scoresAll['22']['answer'] == 'ja')
                <div class="feedback">@include('werknemers/snippet-20-feedback')</div>
            @endif

        <!--  als je vindt dat je het zelf NIET goed doet (leefgewoonten vr id22 : nee )  -->
            @if($levenstijl == 'nok' && $scoresAll['22']['answer'] == 'ja')
                <div class="feedback">
                    U vindt dat u gezonde leefgewoonten heeft, toch
                    <ul>
                        @if($scoresAll['23']['answer'] == 'ja')
                            <li>rookt u</li>
                        @endif

                        @if($scoresAll['24']['answer'] == 'nee')
                            <li>beweegt u te weinig</li>
                        @endif

                        @if($scoresAll['25']['answer'] == 'nee' || $scoresAll['26']['answer'] == 'nee')
                            <li>eet u te weinig groenten en fruit</li>
                        @endif

                        @if($scoresAll['27']['answer'] == 'ja')
                            <li>drinkt u te veel alcohol</li>
                        @endif
                    </ul>
                </div>
            @endif

        <!-- individuele feedback op roken etc... -->
            @if($scoresAll['23']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-21-feedback')</div>
            @endif

            @if($scoresAll['24']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-22-feedback')</div>
            @endif

            @if($scoresAll['25']['points'] > 0 || $scoresAll['26']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-23-feedback')</div>
            @endif

            @if($scoresAll['27']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-25-feedback')</div>
            @endif

        <!-- samenvatting / eindparagraaf -->
            @if($levenstijl == 'nok')
                <div class="feedback">@include('werknemers/snippet-20-2-feedback')</div>
            @endif

            @if($levenstijl == 'ok' && $scoresAll['22']['answer'] == 'nee') 20-1
            <div class="feedback">@include('werknemers/snippet-20-1-feedback')</div>
            @endif

            <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelOne[2]['evaluation'] }}}.png')">Verzuim</h5>

            <!-- dagen & keer samen -->
            @if($scoresAll['33']['points'] > 0 || $scoresAll['34']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-31-feedback')</div>
            @endif
        <!-- keer wegens redenen die op het werk..-->
            @if($scoresAll['35']['points'] > 0)
                <div class="feedback">@include('werknemers/snippet-33-feedback')</div>
            @endif
        <!-- keer toch gaan werken-->
            @if($scoresAll['36']['answer'] !== 'geen enkele keer')
                <div class="feedback">@include('werknemers/snippet-34-feedback')</div>
            @endif

            <h5 class="smiley"  style="background-image:url('{{Config::get('app.url')}}/images/icons/face-small{{{ $scoresLevelOne[3]['evaluation'] }}}.png')">Burnout</h5>

            <!-- GEZONDHEID : burnout-->
            <?php
            $burnoutScore = $scoresAll['37']['points']+$scoresAll['38']['points'];
            ?>
            @if($burnoutScore <= 6)
                <div class="feedback">@include('werknemers/snippet-35-1-feedback')</div>
            @elseif ($burnoutScore > 13)
                <div class="feedback">@include('werknemers/snippet-35-2-feedback')</div>
            @else
            <!-- no fb -->
            @endif

        <!-- GEZONDHEID : bevlogenheid-->

            <?php
            $bevlogenheidsScore = $scoresAll['39']['points']+$scoresAll['40']['points']+ $scoresAll['41']['points']+$scoresAll['42']['points'];
            ?>

            @if($bevlogenheidsScore <= 19)
                <div class="feedback slecht">@include('werknemers/snippet-37-1-feedback')</div>
            @elseif ($bevlogenheidsScore > 25)
                <div class="feedback goed">@include('werknemers/snippet-37-2-feedback')</div>
            @else
            <!-- no fb -->
            @endif

        </div>
    </div>

    <div class="row intro end-eindresultaat">
        <div class="large-12 columns" style="margin-top: 50px;">

            <div class="image"><img src="{{Config::get('app.url')}}/images/icons/wellfie-scan-128.png"></div>
            <strong>Bedankt om mee te werken aan deze enquête!</strong> Hopelijk helpt het u bij uw keuzes op het werk.
        </div>
    </div>

@stop
