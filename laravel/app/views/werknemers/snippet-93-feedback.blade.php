U rapporteert problemen met de ventilatie van uw werkplaats. Dit is niet alleen onaangenaam maar heeft ook een nefaste invloed op uw concentratie en vermoeidheid. <br>
<br>
Enkele tips voor een goed verluchte werkplaats:<br>
<ul>
	<li>Zet regelmatig (bijvoorbeeld. als u even weg bent of pauze neemt) ramen, deuren, poorten, … open.</li>
	<li>Gebruik ademhalingsbescherming indien nodig.</li>
	<li>Een plantje fleurt uw bureau op en helpt tevens de lucht te zuiveren van CO2 en andere onzuiverheden.</li>
	<li>Meld aanhoudende problemen aan uw leidinggevende.</li>
</ul>