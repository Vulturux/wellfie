<strong>INWERKTRAJECT</strong><br>
Stel uw werkgever voor om per team, dienst, afdeling of functie (afhankelijk van wat meest relevant is) een
inwerktraject voor nieuwe medewerkers uit te tekenen. Dit kan u met een paar collega's samen doen of
afdelings- en functie-overschrijdend met een werkgroepje.
