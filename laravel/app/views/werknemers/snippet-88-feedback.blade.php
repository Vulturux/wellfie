Langdurig eenzelfde houding aannemen, is een belangrijke risicofactor voor het ontstaan van overbelastingletsels ter hoogte van spieren, pezen, zenuwen en gewrichten. Beter voorkomen dan genezen!<br>
<br>
Probeer allereerst de werkomstandigheden te verbeteren:<br>
<ul>
	<li>Richt de werkplek goed in.</li>
	<li>Werk op een goede hoogte: een werkvlak op lage hoogte (heuphoogte) is geschikt bij zware arbeid (taken die kracht vergen), terwijl precisiewerk een hoger werkvlak (ellebooghoogte) vereist.</li>
	<li>Zorg voor voldoende bewegingsruimte.</li>
	<li>Verklein de reikafstand (sta/zit zo dicht mogelijk bij het werkvlak).</li>
	<li>Het kan nuttig zijn dit eens ergonomisch te bekijken en, indien nodig, aanpassingen te doen. Hiervoor kan u contact opnemen met uw leidinggevende.</li>
</ul>
Optimaliseer uw eigen handelen:<br>
<ul>
	<li>Zorg voor een goede werkhouding (hou gewrichten in de neutrale positie).</li>
	<li>Neem geregeld (elk uur) een korte pauze.</li>
	<li>Sta eens op, loop eens rond of doe wat kleine oefeningen (bijvoorbeeld rek uw armen uit, draai uw hoofd naar links en rechts, rol uw schouders naar achter en voren, krul uw tenen, draai uw voeten, duw op uw tenen, duw op uw hielen, …).</li>
</ul>
Deze principes kunnen aangeleerd worden tijdens een opleiding rond het beheersen van fysieke belasting.<br>
<br>