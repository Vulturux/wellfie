<strong>GESPREKSCULTUUR</strong><br>
Indien u ervaart dat er weinig oog is voor een lerende en open gesprekscultuur, kan u
misschien overwegen om dit aan te kaarten bij uw leidinggevende. Dergelijk gesprek is
uiteraard delicaat. Doet u het, dan is het belangrijk om vooraf een aantal objectieve
voorbeelden te verzamelen en dit onderwerp op een constructieve manier aan te kaarten.