<strong>BEVLOGENHEID</strong><br>
U behoort helaas bij de 25 % werknemers met de laagste bevlogenheidsscores. U lijkt weinig enthousiast te zijn over
de inhoud van uw job en er weinig voldoening en energie uit te putten. Het kan een goed idee zijn dit te bespreken
binnen de onderneming, bijvoorbeeld met uw leidinggevende gedurende een functioneringsgesprek. Mogelijk kan uw
jobinhoud gewijzigd worden zodat deze beter aansluit bij uw interesses of talenten, of kan er een systeem van
jobrotatie worden opgezet waardoor u meer variatie krijgt in uw job.<br>
Indien nodig kan u steeds een gesprek aanvragen bij de interne vertrouwenspersoon of bij de arbeidsgeneesheer of
preventieadviseur psychosociale aspecten van uw externe dienst voor preventie en bescherming op het werk.<br>
De namen en contactgegevens van deze personen moeten door de werkgever op een gemakkelijk toegankelijke plaats
vermeld worden of vindt u in het arbeidsreglement