U rapporteert problemen met de verlichting van uw werkplek. Voor uw ogen is het belangrijk dat u uw werk in een goed verlichte ruimte kunt uitoefenen.<br>
<br>
Enkele tips voor een goed verlichte werkplaats:<br>
<br>
<ul>
	<li>De werkplek is, bij voorkeur, verlicht door inval van voldoende daglicht.</li>
	<li>Pak de oorzaak aan. U kan bijvoorbeeld uw werktafel van plaats veranderen zodat er geen storende spiegelingen van kunstlicht, daglicht of reflecterende wanden invallen op het werkvlak of computerscherm.</li>
	<li>Meld aanhoudende problemen aan uw leidinggevende. Vraag, indien nodig, een bijkomende lamp, zonnewering, … </li>
</ul>