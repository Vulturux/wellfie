Je hebt niet het gevoel dat je op het werk je talenten kan benutten en jezelf verder ontwikkelen.<br>
<ul>
	<li>Ken jezelf: weet wat je eigen sterktes zijn, en ook je beperkingen? Verschillende (gratis) online
        instrumenten helpen je daarbij. Een voorbeeld is <a href="http://www.verkenjezelf.be">www.verkenjezelf.be</a>. Vraag ook aan de mensen in je
        omgeving hoe zij jou zien.</li>
	<li>Laat je kennen: deel je wensen en verwachtingen met je chef. Anders kan je moeilijk verwachten dat hij of zij er rekening mee houdt.</li>
	<li>Zoek zelf mogelijke uitdagingen in het bedrijf. Stel jezelf kandidaat voor een speciale opdracht of formuleer een creatief voorstel.</li>
</ul>
	
	
