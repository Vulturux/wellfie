Langdurig uitvoeren van repetitieve (herhaaldelijk dezelfde) bewegingen is een belangrijke 
risicofactor voor het ontstaan van overbelastingletsels. Beter voorkomen dan genezen!<br>
<br>
Probeer allereerst de werkomstandigheden te verbeteren:
<ul>
<li>Werk op een goede hoogte: een werkvlak op lage hoogte (heuphoogte) is geschikt 
	bij zware arbeid (taken die kracht vergen), terwijl precisiewerk een hoger werkvlak
	 (ellebooghoogte) vereist.</li>
<li>Richt de werkplek goed in.</li>
<li>Zorg voor voldoende bewegingsruimte.</li>
<li>Verklein de reikafstand (sta/zit zo dicht mogelijk bij het werkvlak).</li>
<li>Gebruik goed materiaal en gereedschap.</li>
<li>Indien u moet tillen, probeer de last lichter te maken (1 doos in plaats van meerdere dozen tegelijk).</li>
</ul>
<br>
Optimaliseer daarna uw eigen handelen:
<ul>
<li>Hou gewrichten in de neutrale positie.</li>
<li>Werk met de handen dicht bij de romp.</li>
<li>Wissel af met andere, minder belastende taken/houdingen.</li>
<li>Neem, indien mogelijk, meerdere korte pauzes in plaats van 1 lange.</li>
<li>Beweeg en doe enkele oefeningen (als u lang moet zitten: sta eens recht en zet enkele stappen, 
	doe wat arm- en beenoefeningen, ...) </li>
</ul>
<br>
Deze principes kunnen aangeleerd worden tijdens een opleiding rond het beheersen van fysieke belasting.
