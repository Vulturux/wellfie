<strong>BEVLOGENHEID</strong><br>
Proficiat! U behoort bij de 25 % werknemers met de hoogste bevlogenheidsscores. U blijkt veel energie en voldoening te putten uit uw job.
Wees u er echter van bewust dat omwille van die zeer positieve werkbeleving signalen van stress en burn-out minder snel opvallen.
Daardoor gaan sterk bevlogen personen niet zo snel op de rem staan en kan burn-out zich langzaam ontwikkelen. Burn-out is
namelijk een klassiek gevolg van chronische stress door positieve motivatie.