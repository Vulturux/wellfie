U moet geregeld zware lasten tillen, dragen, trekken of duwen. Werknemers die frequent zware lasten tillen, trekken of duwen, hebben aanzienlijk meer kans op letsels (voornamelijk ter hoogte van armen, schouders, nek en lage rug). Het is dus belangrijk dat u op de juiste manier tilt, draagt, trekt of duwt om deze problemen te voorkomen.<br>
<br>
Probeer allereerst de werkomstandigheden te verbeteren:<br>
<ul>
	<li>Vermijd tillen (waar mogelijk: schuif of duw).</li>
	<li>Gebruik hulpmiddelen.</li>
	<li>Vraag hulp aan een collega.</li>
	<li>Zorg voor voldoende bewegingsruimte.</li>
	<li>Verklein de reikafstand (sta zo dicht mogelijk bij de last of breng de last eerst dichterbij).</li>
	<li>Gebruik goed materiaal en gereedschap.</li>
	<li>Maak de last lichter (1 doos in plaats van meerdere dozen tegelijk).</li>
	<li>Beperk de loopafstand met de last.</li>
	<li>Wissel houdingen/activiteiten af en beweeg.</li>
	<li>Draag aangepaste kledij en schoenen.</li>
</ul>
Optimaliseer daarna uw eigen handelen:<br>
<ul>
	<li>Sta in evenwicht.</li>
	<li>Hou uw rug in een neutrale positie (= niet te hol, maar ook niet te bol).</li>
	<li>Werk met de handen dicht bij de romp.</li>
	<li>Gebruik de kracht van eigen lichaamsgewicht en van de beenspieren.</li>
	<li>Zet de beweging rustig in.</li>
	<li>Beweeg handen en voeten in dezelfde richting.</li>
</ul>
Deze principes kunnen aangeleerd worden tijdens een opleiding rond het beheersen van fysieke belasting.<br>
<br>