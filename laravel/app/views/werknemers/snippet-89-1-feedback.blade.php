Langdurig beeldschermwerk uitvoeren, is een belangrijke risicofactor voor het ontstaan van werk-gerelateerde klachten ter hoogte van polsen, ellebogen, schouders, nek en rug. Beter voorkomen dan genezen!<br>
<br>
Probeer allereerst de werkomstandigheden te verbeteren:<br>
<ul>
	<li>Richt de werkplek goed in.</li>
	<li>Zorg dat de hoogte van uw stoel, tafel en beeldscherm correct is ingesteld.</li>
	<li>Gebruik hulpmiddelen zoals een laptophouder en een extern toetsenbord.</li>
	<li>Het kan nuttig zijn dit eens ergonomisch te bekijken en, indien nodig, aanpassingen te doen. Hiervoor kan u contact opnemen met uw leidinggevende.</li>
</ul>
Optimaliseer uw eigen handelen:<br>
<ul>
	<li>Zorg voor een goede werkhouding (nek, rug, schouders en polsen).</li>
	<li>Wissel houdingen af (bijvoorbeeld wissel zitten af met staan tijdens een telefoongesprek of vergadering).</li>
	<li>Neem geregeld (elk uur) een korte pauze.</li>
	<li>Sta eens op, loop eens rond of doe wat kleine oefeningen bijvoorbeeld rek uw armen uit, draai uw hoofd naar links en rechts, rol uw schouders naar achter en voren, …).</li>
</ul>
Deze principes kunnen aangeleerd worden tijdens een opleiding rond werken met beeldschermen.