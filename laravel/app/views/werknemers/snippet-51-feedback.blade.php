Enkele tips voor een interview bij een selectieprocedure: <br>
Een goede voorbereiding is heel belangrijk. Denk na over uw motivatie.
Wat moet er voor u zeker in een job terug te vinden zijn als u solliciteert? Wat maakt dat u elke dag uit uw
bed wil komen voor deze job? De kans dat u gevraagd wordt om enkele sterke punten en minpunten te benoemen met
een verduidelijkend voorbeeld is groot.  Daarnaast zal de interviewer via concrete voorbeelden proberen te
achterhalen welke vaardigheden u in huis hebt. Online zijn tal van oefentests terug te vinden.
Als u er enkele op voorhand uitvoert, raakt u vertrouwd met deze context.<br>
<br>