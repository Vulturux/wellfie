Algemeen wordt aan volwassenen aanbevolen om dagelijks 30 minuten lichamelijk actief te zijn aan een middelmatige intensiteit. Deze 30 minuten mogen verspreid over de dag plaatsvinden, wel in blokken van minimum 10 minuten.<br>
Een andere mogelijkheid is om zware fysieke inspanningen 3 x per week uit te voeren aan minstens 20 minuten continu.<br>
<br>
Kies die activiteiten die passen in uw dagelijks leven en aansluiten bij uw interesse. In onderstaande tabel kan u activiteiten van matige en hoge intensiteit terugvinden.<br>
<br>
<table>
	<tr>
		<td>Matige intensiteit</td>
		<td>Hoge intensiteit</td>
	</tr>
	<tr>
		<td valign="top">
			<ul>
				<li>in de tuin werken</li>
				<li>wandelen aan
				5 à 6 km/uur (flink doorstappen)</li>
				<li>trap aflopen</li>
				<li>fietsen aan 10 km/uur</li>
				<li>gymnastiek</li>
				<li>kanoën</li>
				<li>kajakken</li>
				<li>snorkelen</li>
				<li>tafeltennis</li>
				<li>tai-chi</li>
				<li>volleybal</li>
				<li>waterskiën</li>
				<li>worstelen</li>
				<li>zwemmen</li>
			</ul>
		</td>
		<td>
			<ul>
				<li>lopen vanaf 9 km/uur</li>
				<li>fietsen vanaf 15 km/uur</li>
				<li>gevechtssporten</li>
				<li>handbal</li>
				<li>hockey</li>
				<li>klimmen</li>
				<li>basketbal</li>
				<li>boxen</li>
				<li>schaatsen</li>
				<li>skateboarden</li>
				<li>skiën</li>
				<li>snelwandelen</li>
				<li>squash</li>
				<li>tennis</li>
				<li>trap oplopen (80 treden/min)</li>
				<li>voetbal</li>
				<li>wielrennen</li>
				<li>competitie zwemmen</li>
				</li>
			</ul>
		</td>
	</tr>
</table>