<strong>BURNOUT</strong><br>

U behoort helaas bij de 25 % werknemers met de hoogste burn-outscores. Uw job vormt voor u een zware belasting waardoor uw werkvermogen afneemt.<br>
	Tips om dit terug op te krikken zijn:<br>
	<ul>
		<li>Organiseer: delegeer taken, maak een planning op, hou ruimte in uw agenda 	voor onverwachte zaken, zorg voor realistische deadlines, leer ‘neen’ zeggen.</li>
		<li>Zorg voor een goede slaap: vast slaapritueel, slaapkamer enkel gebruiken om te ontspannen, vermijd dutjes.</li>
		<li>Probeer meer te genieten: verwen uzelf, zoek de natuur op, zorg voor voldoende daglicht, reserveer kwaliteitstijd voor uzelf, lach.</li>
		<li>Probeer gezond te eten: neem voldoende tijd, sla geen lunchpauzes over, eet voldoende groenten en fruit, drink veel water.</li>
		<li>Onderhoud sociale contacten: sla een babbeltje met collega’s, deel stresservaringen in plaats van ze op te kroppen, vraag steun en advies aan anderen, roep op tijd hulp in!</li>
		<li>Probeer voldoende te bewegen: maak een wandeling in plaats van tv te kijken, kies voor de trap en niet de lift, stretch regelmatig achter uw bureau.</li>
		<li>Probeer ook gezond te denken: stop met generaliseren of doemdenken, probeer problemen te relativeren, pak alleen aan waar u een invloed op hebt, accepteer uw eigen onmogelijkheden, stop met piekeren, breng uw geest tot rust.</li>
	</ul>
		Wanneer u graag eens persoonlijk met iemand spreekt over de problemen die u ervaart, aarzel dan niet om iemand te contacteren.
    Spreken met iemand uit uw persoonlijk netwerk kan reeds aanvoelen als een opluchting, maar u kan eveneens terecht bij uw huisarts,
    de interne vertrouwenspersoon, bij uw arbeidsgeneesheer of preventieadviseur psychosociale aspecten van uw externe dienst voor
    preventie en bescherming op het werk. De namen en contactgegevens van deze personen moeten door de werkgever op een
    gemakkelijk toegankelijke plaats vermeld worden of vindt u in het arbeidsreglement?