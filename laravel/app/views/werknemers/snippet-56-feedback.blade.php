<strong>FEEDBACKGESPREKKEN</strong><br>
U hoeft niet te wachten op feedback om te weten waar uw sterktes en zwaktes zitten.
Wie zelf actief om feedback vraagt, groeit sneller in zijn job. Vraag naar beide kanten
van de balans: Wat doe ik goed? Waar kan ik me nog in verbeteren en op welke manier? Stel
vooral open vragen om meer te weten te komen (de W vragen: wie wat wanneer waarom welke waarop .
én hoe). Spreek uw leidinggevende, of een meer ervaren collega die u vertrouwt, aan op
een rustig moment om dit te bevragen en vraag door: wat precies vinden ze ok, als ze
zeggen dat u goed bezig bent. Waardoor is iets wel of niet ok: hoe concreter hun uitleg,
hoe meer u eraan heeft.