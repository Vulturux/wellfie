U bleek het afgelopen jaar een groot aantal dagen of herhaaldelijk afwezig geweest te zijn.
Denkt u daarbij dat aanpassingen op uw werk, zoals een andere werkorganisatie of meer aandacht
voor ergonomie en gezondheid, ervoor zouden kunnen zorgen dat u zich minder dagen ziek moet melden?
Dan zou het goed zijn om dit probleem bespreekbaar te maken met bijvoorbeeld uw leidinggevende of personeelsverantwoordelijke