Hoge bloeddruk, hart- en vaatziekten en suikerziekte zijn aandoeningen waar leefstijl een belangrijke invloed op kan hebben. Gezond eten, bewegen, niet roken, … is sneller gezegd dan gedaan. Als u hierover advies wilt of u ondervindt problemen met uw werk, dan kan u hiervoor terecht bij uw arbeidsgeneesheer (die u eventueel kan doorverwijzen).<br>
<br>
