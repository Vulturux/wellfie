@extends('layoutsWerknemer.master')


@section('content')

    <div class="box-header clearfix">
        <h3 class="main-title">{{ $title }}</h3>
    </div>

    <div class="row">

        <div class="large-12 columns vragen-content-container">
            U hebt alle enquetes van uw bedrijf reeds ingevuld. U zal een mail ontvangen wanneer er een nieuwe Wellfie voor u beschikbaar is.
        </div> <!-- end large 12 columns -->

    </div> <!-- end row -->

@stop