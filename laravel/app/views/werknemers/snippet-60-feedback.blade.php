<strong>KENNISMANAGEMENT</strong><br>
Pols eens bij uw werkgever of hij hierrond actie wenst te ondernemen. Misschien wil hij u samen met enkele collega’s betrekken
en kan u een lijst opstellen van kennis die u of uw collega’s missen en daarnaast vermelden hoe deze kennis op een efficiënte
manier gedeeld kan worden. Dit kan dan de aanzet vormen van een project interne kennisdeling.