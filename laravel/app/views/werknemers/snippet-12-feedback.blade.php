Werknemers die blootgesteld zijn aan een beroepsrisico, moeten, wanneer ze 4 weken of meer
afwezig geweest zijn door ziekte of ongeval, bij werkhervatting langsgaan bij de
arbeidsgeneesheer. Ter gelegenheid van deze consultatie kan samen met de
arbeidsgeneesheer bekeken worden of de recente gezondheidsproblemen
beïnvloed worden door of effect hebben op het werk. De arbeidsgeneesheer kan
eventueel een werkaanpassing voorstellen.<br>
<br>
Hiernaast heeft iedere werknemer, blootgesteld aan een beroepsrisico of  niet,
recht op een bezoek bij de arbeidsgeneesheer voorafgaand aan de werkhervatting
wanneer hij al minstens 4 weken arbeidsongeschikt is en het werk maar kan
hervatten na aanpassingen van de werkpost of de arbeidsomstandigheden.<br>
<br>
De werkgever vraagt deze onderzoeken aan.<br>