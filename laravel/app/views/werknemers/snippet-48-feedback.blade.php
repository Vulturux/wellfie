Linkedin is een professioneel netwerkkanaal. Steeds meer werkgevers melden hun vacatures
via Linkedin en rekruteringsbureaus gaan op basis van sleutelwoorden op zoek naar geschikte
profielen voor hun vacatures. Zorg ervoor dat ook u aanwezig bent op Linkedin en dat u een
up-to-date profiel heeft. Heeft u geen ervaring met social media, klop dan aan bij een
fervente gebruiker uit uw netwerk of zoek een handleiding op internet.