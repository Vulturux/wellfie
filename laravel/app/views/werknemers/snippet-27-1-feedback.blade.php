Uw werkvermogen lijkt zeer laag te zijn. Acties om dit terug op te krikken zijn dringend nodig.<br>
Onderstaande adviezen kunnen u op weg helpen. Daarnaast kan u ook even bij uzelf bedenken waaraan dit te wijten kan zijn.
U kan dit bespreken met uw leidinggevende of met uw arbeidsgeneesheer. Dit kan bij een gepland onderzoek of u kan
hiervoor een afspraak maken. Ook bij de preventieadviseur psychosociale aspecten kan u hiervoor terecht.