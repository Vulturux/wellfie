Zodra een man meer dan 21 of een vrouw meer dan 14 glazen alcohol per week drinkt, spreken we van overmatig alcoholgebruik. We raden dan ook aan om dit maximum niet te overschrijden<br>
<br>
Het is niet gemakkelijk om leefgewoonten te veranderen. Alle hulp hierbij is welkom. Indien u de kans krijgt om een gezondheidscoach of begeleider te raadplegen, eventueel via uw werkgever, doen!<br>
<br>
