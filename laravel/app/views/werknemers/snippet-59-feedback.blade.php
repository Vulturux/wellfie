<strong>LOOPBAANCHEQUES</strong><br>
Met loopbaancheques kan u tegen een kleine vergoeding tot 8 uur loopbaanbegeleiding volgen bij een erkend centrum.
Loopbaanbegeleiding helpt u een antwoord te vinden op loopbaanvragen als: ‘Hoe vind ik weer plezier in mijn job?
Zou ik gelukkiger zijn in een andere job? Welke richting kan en wil ik binnen mijn huidige onderneming zelf uitgaan?’ ...
Meer info vindt u op de site van de VDAB. <br>
<a href="http://www.vdab.be/loopbaanbegeleiding">http://www.vdab.be/loopbaanbegeleiding</a>