U rapporteert dat uw onderneming onvoldoende ergonomische aanpassingen doet (met betrekking tot werkplekinrichting, instelbaar meubilair en/of hulpmiddelen). Dit kan u best met uw leidinggevende bespreken. De preventieadviseur kan langs komen om uw werkplek te evalueren en na te gaan of (bijkomende) ergonomische aanpassingen aangewezen zijn. <br>
<br>
Enkele tips om zelf aan de slag te gaan:<br>
<ul>
	<li>Wijzig, indien mogelijk, de inrichting van uw werkplek. </li>
	<li>Zit/sta voldoende kort bij het werkvlak.</li>
	<li>Zorg voor voldoende bewegingsruimte.</li>
	<li>Stel de hoogte van uw werkvlak goed in zodat te diep voorover buigen of te ver reiken vermeden worden. Een werkvlak op lage hoogte (heuphoogte) is geschikt bij zware arbeid (taken die kracht vragen), terwijl precisiewerk een hoger (ellebooghoogte) werkvlak vereist.</li>
	<li>Stel uw werkstoel goed in. Bij een goede en ontspannen zithouding is de hoek tussen boven- en onderbeen iets groter dan 90°. Ook de hoek tussen romp en bovenbeen is 90° of iets meer. De ellebogen vormen een hoek van 90°. Gebruik de voorziene hendels en knopjes om de rugsteun, zitting en armsteunen in hoogte en diepte goed in te stellen.</li>
	<li>Ergonomische hulpmiddelen zoals een karretje of tillift (bij het tillen van lasten), een laptophouder, een extern toetsenbord en een externe muis (bij werken met laptop) dragen bij tot uw werkcomfort en verkleinen de kans op werk-gerelateerde klachten en overbelastingletsels. </li>
	<li>Vraag aan uw leidinggevende, indien nodig, of er een aanspreekpunt/verantwoordelijke ergonomie aanwezig is in uw onderneming voor bijkomend advies.</li>
</ul>