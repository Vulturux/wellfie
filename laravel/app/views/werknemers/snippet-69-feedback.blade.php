Je ziet onvoldoende de betekenis van jouw bijdrage aan het gemeenschappelijk resultaat.<br>
<ul>
	<li>Weet je concreet welk zichtbaar resultaat van jou wordt verwacht? Als dit niet zo is, klaar dit uit met je opdrachtgever.</li>
	<li>Kijk zelf tussentijds of je op de goede weg bent en vraag hierbij ook anderen hun mening.</li>
	<li>Vertel het collega’s als ze iets goed doen en geniet van elk compliment dat je zelf krijgt.</li>
</ul>

	
