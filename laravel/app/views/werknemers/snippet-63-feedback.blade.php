<strong>DOELSTELLINGEN</strong><br>
Als het niet duidelijk is welke taken bij uw functie horen, vraag dan
uw functieprofiel op bij personeelszaken. Kijk samen met uw leidinggevende
hoe u dit nog verder zou kunnen aanvullen en of er per jaar ook uitdagende
doelstellingen kunnen vastgelegd worden, zodat duidelijk is voor alle partijen
wat van wie verwacht wordt tegen wanneer en op welke manier u daarbij ondersteuning kan krijgen.