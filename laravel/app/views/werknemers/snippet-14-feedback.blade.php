Wanneer u last heeft van rug, hals, ledematen of gewrichten bij het uitvoeren van  bepaalde taken, kan u hierover uw interne preventieadviseur of arbeidsgeneesheer raadplegen. Zij kunnen u, indien nodig, in contact brengen met een ergonoom. Het aanpassen van een werkpost, hulpmiddelen, … kunnen helpen om uw taken op een goede manier uit te voeren.<br>
<br>
