U rapporteert dat u geregeld moet werken in een lawaaierige omgeving. Langdurige en herhaalde blootstelling aan overmatig geluid leidt tot achteruitgang van het gehoor. Verder is lawaai een extra mentale belasting.<br>
<br>
Meld aanhoudende geluidsproblemen aan uw leidinggevende. Als oplossing op korte termijn, en in afwachting van de invoering van technische en organisatorische maatregelen, kan u gebruik maken van gehoorbescherming zoals oordoppen en oorkappen.<br>
<br>