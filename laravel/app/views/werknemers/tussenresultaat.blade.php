@extends('layoutsWerknemer.master')


@section('content')

<div class="box-header clearfix">
    <h3 class="main-title">Uw {{{ $title }}}</h3>
</div>

<div class="row intro">
    <div class="medium-12 columns ">
        @if($level == 1)
            Doorloop nog de drie verdiepingen “competenties”, ”Waarden, houding en motivatie” en “Werk, werkgemeenschap en leiding”.
            Op het einde ontvangt u tips om uw werkvermogen te behouden of te bevorderen.
        @endif
        @if($level == 2)
            Doorloop nog de twee verdiepingen ”Waarden, houding en motivatie” en “Werk,
            werkgemeenschap en leiding”. Nadien ontvangt u tips om uw werkvermogen te behouden of te bevorderen.
        @endif
        @if($level == 3)
            U bent er bijna! Doorloop nog de laatste verdieping “Werk, werkgemeenschap en leiding”.
            Weldra kan u aan de slag met de tips om uw werkvermogen te behouden of te bevorderen.
        @endif
    </div>
</div>
<div class="row huis-resultaat visualisation-container">

    <div class="medium-12 columns {{ $levelClass }}">
        <div class="tussenresultaat visualisation">
            <img src="{{ URL::asset('images/visualisation/licht-uit.png'); }}" class="viz-image">
            <div class="viz-level verdiep {{ $levelClass }}">
                <img src="/images/visualisation/verdieping{{ $level }}_{{ $catevaluation }}.png">
            </div>
            <div class="viz-level selectie {{ $levelClass }}">
                <img src="/images/visualisation/verdieping{{ $level }}_selectie.png">
            </div>
            <div class="viz-level mannetje {{ $levelClass }}">
                <img src="/images/visualisation/verdieping{{ $level }}_mannetje_{{ $catevaluation }}.png">
            </div>

        </div>
    </div>


    <div class="viz-sideinfo  {{ $levelClass }} arrow_box">

            <div class="viz-sideinfo-block first">
                <div class="smiley-container"><img src="/images/icons/face{{ $catevaluation }}.png" class="smiley"></div>
                <div class="title big">TOTAAL</div>
            </div>
            <div class="viz-sideinfo-block sign">
                <div class="signchar">=</div>
            </div>

    <?php $count=0; ?>
        @foreach ($scores as $score)
                <?php
                    if ($count > 0) {
                        echo '<div class="viz-sideinfo-block sign"><div class="signchar">+</div></div>';
                    }
                    $count++;
                ?>

                <div class="viz-sideinfo-block">
                    <div class="smiley-container"><img src="/images/icons/face{{ $score['evaluation'] }}.png" class="smiley"></div>
                    <div class="title small">{{ $score['title'] }}</div>
                </div>
        @endforeach
    </div>


</div>
<div class="row" style="margin-top: 20px;">

    <div class="large-12 columns">
        <div class="button-container text-center">
            <ul class="button-group">
                <li><a href="/werknemers" class="button main"><i class="fi-play"></i> Ga verder</a></li>
            </ul>
        </div>
    </div> <!-- end large 12 columns -->

</div> <!-- end row -->



@stop
