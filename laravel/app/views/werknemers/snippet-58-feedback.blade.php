<strong>LOOPBAANEVOLUTIE</strong><br>
Als u wil doorgroeien in de onderneming, horizontaal of verticaal, breng uw leidinggevende dan zeker
tijdig op de hoogte. Samen kunnen jullie dan nagaan wat u nog nodig heeft om door te kunnen groeien
naar een volgende functie en hoe uw leidinggevende of een opleiding u hierbij kunnen helpen.<br>

