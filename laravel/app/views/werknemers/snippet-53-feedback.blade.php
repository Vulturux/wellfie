<strong>OPLEIDINGEN</strong><br>
Als u een opleiding wil volgen, kan u gebruik maken van opleidingscheques via de VDAB.
Op de website van de VDAB vindt u meer info hierover en ook of u hiervoor in aanmerking komt:
<a href="https://www.vdab.be/opleidingscheques/werknemers.shtml">https://www.vdab.be/opleidingscheques/werknemers.shtml</a>.
Wellicht heeft uw werkgever ook recht op subsidies voor opleidingen via de KMO portefeuille.
Vraag dit na op de
personeelsdienst. <a href="http://www.agentschapondernemen.be/artikel/wat-de-kmo-portefeuille">http://www.agentschapondernemen.be/artikel/wat-de-kmo-portefeuille</a>.