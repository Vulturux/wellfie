Jouw persoonlijke waarden sluiten onvoldoende aan bij deze van het bedrijf.
<br>

<ul>
    <li>Noteer je persoonlijke waarden-top-10, zodat je voor jezelf weet wat echt telt in je leven. Bekijk dit lijstje elke morgen even.</li>
    <li>Kom op voor je waarden, durf erover spreken. Je zult merken dat het mensen dichterbij brengt.</li>
    <li>Zorg dat je weet welke waarden je bedrijf vooropstelt. Bespreek wat ze concreet betekenen in jouw werk.</li>
</ul>

	
