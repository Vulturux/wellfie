<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>WELLFIE ADMIN</title>

    <link rel="stylesheet" href="{{url('css', 'foundation.css') }}">
    <link rel="stylesheet" href="{{url('css', 'foundation-icons.css') }}">
    <link rel="stylesheet" href="{{url('css', 'style-design.css') }}">
    <link rel="stylesheet" href="{{url('js/datatables', 'datatables.min.css') }}">
    <script src="/js/vendor/modernizr.js"></script>

    <style>
        body {
            background-color: #AAA;
        }
    </style>
    <script src="https://use.typekit.net/zxm7jpl.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <script src="/js/vendor/jquery.js"></script>

</head>

<body class="werknemers">

    @yield('navbar')

    <section class="row main-content box">
        <div class="icon-corner TL"></div>
        <div class="icon-corner TR"></div>
        <div class="icon-corner BL"></div>
        <div class="icon-corner BR"></div>
        @yield('content')
    </section>

    <footer class="footer-bottom">

    </footer>

    <script src="/js/foundation.min.js"></script>
    <script src="/js/foundation/foundation.tab.js"></script>
    <script src="/js/jquery.form.min.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
    <script src="{{url('js/jquery-validation-1.13.1/dist', 'jquery.validate.js') }}"></script>

    <script src="{{url('js', 'script-all.js') }}"></script>

    <script>
        $(document).foundation();


    </script>

    @yield('script')

</body>

</html>
