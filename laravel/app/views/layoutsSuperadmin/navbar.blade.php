@extends('layoutsSuperadmin.master')


@section('navbar')
    <div class="contain-to-grid top">
        <nav class="top-bar" data-topbar role="navigation">
            <div class="section-container">
                <ul class="title-area">
                    <li class="name">
                        <h1 ><a href="../" class="logo">Wellfie</a></h1>
                    </li>
                    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>

                <section class="top-bar-section">
                    <!-- Right Nav Section -->
                    <ul class="right">
                        <li><a href="/superadmin/">Index</a></li>
                        <li><a href="/superadmin/bedrijven">Bedrijven</a></li>
                        <li><a href="/superadmin/werknemers">Gebruikers</a></li>
                        <li><a href="/superadmin/faq">FAQ</a></li>
                        <li><a href="/superadmin/documentation">Documentatie</a></li>
                        <li><a href="/superadmin/events">Events</a></li>
                        <li><a href="/superadmin/export">Export</a></li>
                    </ul>
                </section>
            </div>
        </nav>
    </div>
@stop
