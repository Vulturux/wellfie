<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function ($request) {
    //
});


App::after(function ($request, $response) {
    //
});

/*
|--------------------------------------------------------------------------
| Custom filters
|--------------------------------------------------------------------------
|
| The following filters are used to regulate useractions.
|
*/
//permissionchecks
Route::filter('permission.WN', function($route){
    //wn
   if(!Auth::user()->permission_level_id === 0){
       return Redirect::to('/');
   }
});

Route::filter('permission.WG', function($route){

    //wg
    if(!(Auth::user()->permission_level_id >= 6 && Auth::user()->permission_level_id <9)){
        return Redirect::to('/');
    }
});

Route::filter('permission.admin', function($route){
    //admin
    if(!Auth::user()->permission_level_id >= 9){
        return Redirect::to('/');
    }
});

//employer
Route::filter('check.position', function ($route) {
    $userScanService = new \App\Services\UserScanService(new \App\Services\CompanyScanService());
    $userScan = $userScanService->getLatestUserScan(Auth::user()->id);

    if ($userScan->questionnaire_position >= 5) {
        return Redirect::to('werkgevers/dashboard');
    }

    if ($route->getParameter('position') > $userScan->questionnaire_position) {
        return Redirect::to('werkgevers/stage/' . $userScan->questionnaire_position);
    }
});

Route::filter('isScanFinished', function ($route) {
    $companyScanService = new \App\Services\CompanyScanService();
    $companyScan = $companyScanService->getLatestByCompanyId(Auth::user()->company_id);

    $userScanService = new \App\Services\UserScanService($companyScanService);
    $userScan = $userScanService->getLatestUserScan(Auth::user()->id);

    if (isset($companyScan) && !$companyScan->scan_finished) {
        return
            $userScan->questionnaire_position >= 5
                ? Redirect::to('werkgevers/dashboard')
                : Redirect::to('werkgevers/stage/' . $userScan->questionnaire_position);
    }
});


Route::filter('canDoInvites', function ($route) {
    $companyScanService = new \App\Services\CompanyScanService();
    $companyScan = $companyScanService->getLatestByCompanyId(Auth::user()->company_id);

    $userScanService = new \App\Services\UserScanService($companyScanService);
    $userScan = $userScanService->getLatestUserScan(Auth::user()->id);

    if (!isset($companyScan) || (isset($companyScan) && $companyScan->scan_finished)) {
        return
            $userScan->questionnaire_position >= 5
                ? Redirect::to('werkgevers/dashboard')
                : Redirect::to('werkgevers/stage/' . $userScan->questionnaire_position);
    }
});

//employee
Route::filter('check.employee.position', function ($route) {
    $companyScanService = new \App\Services\CompanyScanService();
    $userScanService = new \App\Services\UserScanService($companyScanService);
    $userScan = $userScanService->getLatestUserScan(Auth::user()->id);
    $companyScan = $companyScanService->getLatestByCompanyId(Auth::user()->company_id);

    if(!isset($userScan) || $companyScan->scan_id !== $userScan->scan_id){
        return Redirect::to('werknemers/geenActieveScan');
    }

    if ($userScan->questionnaire_position >= 5 && $route->uri() !== "werknemers/eindresultaat") {
        return Redirect::to('werknemers/eindresultaat');
    }

    if ($route->getParameter('position') > $userScan->questionnaire_position) {
        return Redirect::to('werknemers/stage/' . $userScan->questionnaire_position);
    }
});

Route::filter('intermediateresult.position', function ($route) {
    $userScanService = new \App\Services\UserScanService(new \App\Services\CompanyScanService());
    $userScan = $userScanService->getLatestUserScan(Auth::user()->id);

    if (isset($userScan) && $userScan->questionnaire_position >= 5) {
        return Redirect::to('werknemers/eindresultaat');
    }

    if (isset($userScan) && ($route->getParameter('position') > $userScan->questionnaire_position)) {
        return Redirect::to('werknemers/tussenresultaat/' . $userScan->questionnaire_position);
    }
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function () {
    if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function () {
    return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function () {
    if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function () {
    if (Session::token() != Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});
