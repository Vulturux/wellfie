<?php namespace App\Queue;

use App\Services\ExportService;

class ExportJob {

    protected $exportService;

    public function __construct() {
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire($job, $data)
    {
        $exportService = new ExportService();
        $exportService->getWellfieDataWithFilters($data);

        $job->delete();
    }

}
