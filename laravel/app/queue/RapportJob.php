<?php namespace App\Queue;

use App\Services\RapportService;
use App\Services\UserScanService;

class RapportJob {

    protected $rapportService;

    public function __construct() {
    }

    /**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire($job, $data)
	{
	    $userScanService = new UserScanService();
        $rapportService = new RapportService($userScanService);
        $rapportService->generate($data);

        $job->delete();
	}

}
