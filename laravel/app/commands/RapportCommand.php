<?php

use App\Models\Company;
use App\Models\CompanyRapport;
use App\Models\User;
use App\Models\UserPosition;
use Illuminate\Console\Command;
use App\Queue\RapportJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class RapportCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'rapport:generate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $companies = Company::all();
        foreach ($companies as $company) {
            $owner = User::where('company_id', $company->id)->where('permission_level_id', 6)->first();
            if ($owner) {
                $userPosition = UserPosition::where('user_id', $owner->id)->where('scan_id', 1)->first();
                if ($userPosition && $userPosition->position > 8)
                {
                    Log::info('Questionnaire closed for company ' .$company->company_name . ' (' . $company->id.')');

                    if (isset($company->company_name)) $this->info($company->company_name);

                    if ($companyScanRapport = $company->scan(1))
                    {
                        $companyScanRapport->finalized = true;
                        $companyScanRapport->save();
                    } else {
                        $companyScanRapport = new CompanyRapport();
                        $companyScanRapport->finalized = true;
                        $companyScanRapport->scan_id = 1;
                        $companyScanRapport->company_id = $company->id;
                        $companyScanRapport->save();
                    }

                    $data = [$owner->id, 1];
                    try {

                        Queue::push(function($job) use ($data)
                        {
                            $rapportJob = new RapportJob();
                            $rapportJob->fire($job, $data);
                        });

                    } catch (\Exception $e)
                    {
                        Log::warning("Export queue: " . $e->getMessage());
                    }
                }
            }
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

		);
	}

}
