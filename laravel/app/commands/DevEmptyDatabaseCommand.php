<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DevEmptyDatabaseCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dev:empty-database';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Empty companies, company-rapports, company-scans, invites, responses, user_positions, user_scans, users tables.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$tables = ['companies', 'company_rapports', 'company_scans', 'invites', 'responses', 'user_positions', 'user_scans', 'users'];

		if(App::environment() === 'local'){
            foreach ($tables as $table){
                \DB::statement("TRUNCATE TABLE $table CASCADE");
            }

            $this->info('Done');
        }else{
		    $this->error('Only allowed locally, stupid!');
        }

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
