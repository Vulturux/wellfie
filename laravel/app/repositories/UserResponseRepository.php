<?php

namespace App\Repositories;

use App\Models\UserResponse;

/**
 * class UserResponseRepository
 *
 * @todo Create contract/interface
 */
class UserResponseRepository
{
	/**
	 * @param array $attributes
	 * @param array $select
	 * @return App\Models\UserResponse
	 */
    public function getFirstWhere(array $attributes, array $select = ['*'])
    {
		return UserResponse::where($attributes)->first($select);
    }

	/**
	 * @param array $attributes
	 * @param array $select
	 * @return Illuminate\Database\Eloquent\Collection
	 */
    public function getAllWhere(array $attributes, array $select = ['*'])
    {
		return UserResponse::where($attributes)->get($select);
    }

	/**
	 * $relationAttributes must be keyed by the relation ORM.
	 *
	 * @param string $relationAttributes
	 * @param array $attributes
	 * @param array $select
	 * @return Illuminate\Database\Eloquent\Collection
	 */
    public function getAllWhereHas(array $relationAttributes, array $attributes = [], array $select = ['*'])
    {
		$query = UserResponse::whereHas(
			key($relationAttributes), function ($query) use ($relationAttributes) {
				$query->where(reset($relationAttributes));
			}
		);

		if (!empty($attributes)) $query->where($attributes);

		return $query->get($select);
    }

    /**
	 * @param array $data
     * @return bool
     */
    public function create(array $data)
    {
        return UserResponse::create($data);
    }

}
