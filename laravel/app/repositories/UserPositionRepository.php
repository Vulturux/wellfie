<?php

namespace App\Repositories;

use App\Models\UserPosition;

/**
 * class UserPositionRepository
 *
 * @todo Create contract/interface
 */
class UserPositionRepository
{
	/**
	 * @param array $attributes
	 * @param array $select
	 * @return App\Models\UserPosition
	 */
    public function getFirstWhere(array $attributes, array $select = ['*'])
    {
		return UserPosition::where($attributes)->first($select);
    }

    /**
	 * @param array $attributes
	 * @param array $data
     * @return bool
     */
    public function update(array $attributes, array $data)
    {
    	return UserPosition::where($attributes)->update($data);
    }

}