<?php

namespace App\Repositories;

use App\Models\Position;

/**
 * class PositionRepository
 */
class PositionRepository
{
	/**
	 * @param array $attributes
	 * @param array $select
	 * @return App\Models\Position
	 */
    public function getFirstWhere(array $attributes, array $select = ['*'])
    {
		return Position::where($attributes)->first($select);
    }

	/**
	 * @param array $select
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getAllHavingStageCategory(array $select = ['*'])
	{
		return Position::stageCategories()->get($select);
	}

}