<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddLevelsToSectiontotals extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sectiontotals', function(Blueprint $table)
		{
            $table->integer('maxgood');
            $table->integer('maxmodarate');

        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sectiontotals', function(Blueprint $table)
		{
            $table->dropColumn('maxgood');
            $table->dropColumn('maxmodarate');

        });
	}

}
