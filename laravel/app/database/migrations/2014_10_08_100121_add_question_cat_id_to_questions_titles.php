<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddQuestionCatIdToQuestionsTitles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('questions_titles', function(Blueprint $table)
		{
            $table->integer('question_cat_id');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('questions_titles', function(Blueprint $table)
		{
            $table->dropColumn('question_cat_id');

        });
	}

}
