<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CleanupDuplicateRecords extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		// Find all records having same position/scan_id/user_id remove all but one
		$query = DB::table('user_positions')
			->select('user_id', 'scan_id', 'position', DB::raw('COUNT(*) as c'), 'id')
			->groupBy('user_id', 'scan_id', 'position')
			->having('c', '>', 1);

		$duplicates = $query->lists('id');

		while(count($duplicates) > 0) {
			$query = DB::table('user_positions')
				->select('user_id', 'scan_id', 'position', DB::raw('COUNT(*) as c'), 'id')
				->groupBy('user_id', 'scan_id', 'position')
				->having('c', '>', 1);

			$duplicates = $query->lists('id');
			DB::table('user_positions')->whereIn('id', $duplicates)->delete();
		}


		// Find all records having same scan_id/user_id with different positions. Keep highest position.
		$query2 = DB::table('user_positions')
			->select('user_id', 'scan_id', DB::raw('COUNT(*) as c'), 'id')
			->groupBy('user_id', 'scan_id')
			->having('c', '>', 1);

		$duplicateMultiplePositions = $query2->lists('user_id');

		foreach($duplicateMultiplePositions as $duplicate) {
			$q = DB::table('user_positions')
				->where('user_id', $duplicate)
				->get();

			if(count($q) > 1) {
				if ($q[0]->position > $q[1]->position) {
					DB::table('user_positions')->where('id', $q[1]->id)->delete();
				} elseif ($q[0]->position < $q[1]->position) {
					DB::table('user_positions')->where('id', $q[0]->id)->delete();
				}
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
