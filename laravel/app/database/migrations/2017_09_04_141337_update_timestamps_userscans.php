<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\UserPosition;
use App\Models\UserScan;

class UpdateTimestampsUserscans extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (UserPosition::all() as $userPosition) {
            $userScan = UserScan::where('user_id', '=', $userPosition->user_id)
                ->where('scan_id', '=', $userPosition->scan_id)->get();
            echo '----------------------------------------------------------------------------' . PHP_EOL;
            if (count($userScan) > 1) echo '!!!! multiple userscans found for ' . $userPosition->user_id .PHP_EOL;
            if (count($userScan) === 0) echo '!!!! no userscans found for ' . $userPosition->user_id . PHP_EOL;

            if(count($userScan) === 1) {
                $us = $userScan[0];
                $us->created_at = $userPosition->created_at;
                $us->updated_at = $userPosition->updated_at;
                $us->save();
                echo 'Updated timestamps for userscan with id ' . $userPosition->user_id . PHP_EOL;
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
