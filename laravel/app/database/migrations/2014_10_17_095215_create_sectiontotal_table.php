<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSectiontotalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sectiontotal', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('sectiontitle');
            $table->integer('questions_cat_id');
            $table->integer('total');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sectiontotal');
	}

}
