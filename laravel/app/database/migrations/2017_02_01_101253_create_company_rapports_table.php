<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyRapportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company_rapports', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('scan_id');
            $table->boolean('finalized')->default(false)->after('remark_question');
            $table->string('rapport')->nullable()->after('finalized');

			$table->timestamps();
		});

        Schema::table('company_rapports', function(Blueprint$table)
        {
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company_rapports');
	}

}
