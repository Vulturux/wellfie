<?php

use App\Models\User;
use App\Models\UserPosition;
use \Event as Ev;
use App\Models\Company;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;

class CreateRapportVerification extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        $companies = Company::where('finalized', 0)->get();
        foreach ($companies as $company) {
            $owner = User::where('company_id', $company->id)->where('permission_level_id', 6)->first();
            if ($owner) {
                $userPosition = UserPosition::where('user_id', $owner->id)->first();
                if ($userPosition->position > 8)
                {
                    Log::info('Questionnaire closed for company ' .$company->company_name . ' (' . $company->id.')');
                    Company::where('id', $company->id)->update(array('finalized' => true));
                    Ev::fire('questionnaire.closed', [$owner->id]);
                }
            }
        }

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}

}
