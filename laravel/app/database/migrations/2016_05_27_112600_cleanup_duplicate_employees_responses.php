<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CleanupDuplicateEmployeesResponses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		// Remove all responses / user positions for users no longer in the DB
		DB::table('responses')->where('user_id','<',192)->delete();
		DB::table('user_positions')->where('user_id', '<', 192)->delete();

		// Find all records having same position/scan_id/anwser_id/user_id remove all but one
		$query = DB::table('responses')
			->select('user_id', 'scan_id', 'questions_id', 'answer_id', DB::raw('COUNT(*) as c'), 'id', 'type')
			->where('open_answer', null)
			->groupBy('user_id', 'scan_id', 'questions_id', 'answer_id')
			->having('c', '>', 1);

		$duplicates = $query->lists('id');

		while(count($duplicates) > 0) {
			$query = DB::table('responses')
				->select('user_id', 'scan_id', 'questions_id', 'answer_id', DB::raw('COUNT(*) as c'), 'id', 'type')
				->where('open_answer', null)
				->groupBy('user_id', 'scan_id', 'questions_id', 'answer_id')
				->having('c', '>', 1);

			$duplicates = $query->lists('id');
			DB::table('responses')->whereIn('id', $duplicates)->delete();
		}

		// Find all records having same scan_id/user_id with different positions. Keep highest position.
		$query2 = DB::table('responses')
			->select('user_id', 'questions_id', 'scan_id', DB::raw('COUNT(*) as c'), 'id','answer_id', 'open_answer')
			->where('type', '<>', 3)
			->groupBy('user_id', 'questions_id', 'scan_id')
			->having('c', '>', 1)->get();

		foreach ($query2 as $duplicate) {

			$result = DB::table('responses')
				->select('questions_id', 'answer_id', 'scan_id', 'open_answer', 'id', 'user_id')
				->where('user_id', $duplicate->user_id)
				->where('questions_id', $duplicate->questions_id)
				->where('scan_id', $duplicate->scan_id)
				->get();

			while (count($result) > 2) {

				$result = DB::table('responses')
					->select('questions_id', 'answer_id', 'scan_id', 'open_answer', 'id', 'user_id')
					->where('user_id', $duplicate->user_id)
					->where('questions_id', $duplicate->questions_id)
					->where('scan_id', $duplicate->scan_id)
					->get();

				DB::table('responses')->where('id', (int)$result[0]->id)->delete();
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
