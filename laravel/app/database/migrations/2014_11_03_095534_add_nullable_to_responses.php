<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNullableToResponses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('responses', function(Blueprint $table)
		{
            DB::statement('ALTER TABLE responses
                                MODIFY COLUMN open_answer VARCHAR(255) DEFAULT NULL,
                                MODIFY COLUMN answer_id VARCHAR(255) DEFAULT NULL,
                                MODIFY COLUMN scan_id VARCHAR(255) DEFAULT NULL;
                                ');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('responses', function(Blueprint $table)
		{
			
		});
	}

}
