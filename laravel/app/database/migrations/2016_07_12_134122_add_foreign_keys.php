<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddForeignKeys extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//DB::statement("alter table invites add unique(invitecode)");
		//DB::statement("alter table invites add unique(email)");
		DB::statement("alter table invites modify user_id int(11) unsigned null");
		DB::statement("update invites set user_id = null where user_id = 0");
		DB::statement("alter table invites modify company_id int(11) unsigned null");
		DB::statement("alter table users modify company_id int(11) unsigned null");

		DB::statement("delete from invites where user_id < 191");

		DB::statement("alter table invites add foreign key (user_id) references users(id) on delete cascade");
		DB::statement("alter table users add foreign key (company_id) references companies(id) on delete cascade");


	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}

}
