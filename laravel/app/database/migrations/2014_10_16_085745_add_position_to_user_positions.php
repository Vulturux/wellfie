<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPositionToUserPositions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_positions', function(Blueprint $table)
		{
            $table->integer('position');
            $table->integer('scan_id');
            $table->integer('user_id')->unique();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_positions', function(Blueprint $table)
		{
            $table->dropColumn('position');
            $table->dropColumn('scan_id');
            $table->dropColumn('user_id');

        });
	}

}
