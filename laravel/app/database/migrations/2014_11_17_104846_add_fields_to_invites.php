<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFieldsToInvites extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invites', function(Blueprint $table)
		{
            $table->string('surname')->nullable;
            $table->string('forename')->nullable;
            $table->string('username')->nullable;
            $table->string('email')->nullable;
            $table->string('invitecode')->nullable;
            $table->integer('company_id')->nullable;
            $table->integer('user_id')->default(0);

        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invites', function(Blueprint $table)
		{
			
		});
	}

}
