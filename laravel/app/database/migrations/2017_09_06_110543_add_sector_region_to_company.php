<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Company;
use App\Models\User;


class AddSectorRegionToCompany extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('region');
        });
        Schema::table('companies', function (Blueprint $table) {
            $table->integer('sector')->nullable();
            $table->integer('region')->nullable();
        });

        echo 'added sector to company' . PHP_EOL;

        $responses = DB::table('responses')
            ->join('users', 'users.id', '=', 'responses.user_id')
            ->where('questions_id', '=', 102)
            ->orWhere('questions_id', '=', 104)
            ->get();

        forEach ($responses as $response) {
            $company = Company::find($response->company_id);
            if ($response->questions_id === 102) {
                $company->region = $response->answer_id;
                echo 'filled in region for ' . $company->id . PHP_EOL;
            }
            if ($response->questions_id === 104) {
                $company->sector = $response->answer_id;
                echo 'filled in sector for ' . $company->id . PHP_EOL;
            }
            $company->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
