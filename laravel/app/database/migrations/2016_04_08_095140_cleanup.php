<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class Cleanup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$query = DB::table('users')
			->where('users.email' , 'REGEXP' , 'idewe|ibeve|etion|vkw|acerta|appsonly|test')
			->orWhere('users.username', 'REGEXP', 'test')
			->orWhere(DB::raw("BINARY users.email"), 'REGEXP', 'OLD')
			->orWhere(DB::raw("BINARY users.username"), 'REGEXP', 'OLD')
			->where('username', '<>', 'superadmin')
		;
		$testusers = $query->lists('email', 'id');

		$query2 = DB::table('companies')
			->where('company_name', 'REGEXP', 'test')
			->orWhere('company_name', '=', '')
			->orWhereIn('company_name', [
				'Apps Only',
				'bigdata',
				'bigdataee2',
				'bigdataee23',
				'IDEWE 1',
				'Acerta Consult',
				'Acerta',
				'abc',
				'ygi',
				'Acerta',
				'Softie',
				'PPT',
				'* Urawaza',
				'dsfsdf--',
				'ABC',
				'* IDEWE',
				'Acerta',
				'IDEWE',
				'IDEWE',
				'IDEWE HASSELT',
				'IDEWE',
				'Acerta Consult',
				'Vandenbroeck consulting',
				'IK',
				'IDEWE 123',
				'IDEWESteffie',
				'KC Duurzame Inzetbaarheid',
				'KCDuIn',
				'De Krikker',
				'Steegmans',
				'Boost de werkgoesting',
				'droomhuis',
				'AZERTY',
				'urawazates',
				'Calibrate',
				'Degeest NV',
				'Involved NV',
				'Anita'
			])
		;
		$companies = $query2->lists('id');

		$query3 = DB::table('users')
			->whereIn('company_id', $companies)
			->Where('username', '<>', 'superadmin')
		;
		$companyusers = $query3->lists('email', 'id');

		$userIds = array_unique(array_merge(array_keys($companyusers), array_keys($testusers)));
		$userEmails = array_unique(array_merge(array_values($companyusers), array_values($testusers)));
		
		// Delete all companies.
		DB::table('companies')->whereIn('id', $companies)->delete();

		// Delete all users.
		DB::table('users')->whereIn('id', $userIds)->delete();

		// Delete all invitations.
		DB::table('invites')->whereIn('user_id', $userIds)->delete();

		// Delete responses for all these users.
		DB::table('responses')->whereIn('user_id', $userIds)->delete();

		// Delete positions for all these users.
		DB::table('user_positions')->whereIn('user_id', $userIds)->delete();

		// Delete password_reminders for all these users.
		DB::table('password_reminders')->whereIn('email', $userEmails)->delete();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
