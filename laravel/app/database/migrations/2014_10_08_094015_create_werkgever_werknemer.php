<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWerkgeverWerknemer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('werkgever_werknemer', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('werkgever_werknemer');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('werkgever_werknemer');
	}

}
