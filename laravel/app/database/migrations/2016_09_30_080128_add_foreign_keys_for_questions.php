<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysForQuestions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    Schema::table('questions_titles', function(Blueprint $table){
           $table->foreign('question_cat_id')->references('id')->on('questions_categories');
        });

        Schema::table('questions', function(Blueprint $table){
            $table->foreign('question_titles_id')->references('id')->on('questions_titles');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('questions', function(Blueprint $table){
           $table->dropForeign('questions_question_titles_id_foreign');
        });

        Schema::table('questions_titles', function(Blueprint $table){
            $table->dropForeign('questions_titles_question_cat_id_foreign');
        });
	}

}
