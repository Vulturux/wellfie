<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\CompanyRapport;
use App\Models\CompanyScan;

class MigrateDataToCompanyScans extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        foreach(CompanyRapport::all() as $rapport){
            echo 'Adding company scan for company '. $rapport->company_id . ' - scan ' . $rapport->scan_id . PHP_EOL;

            CompanyScan::create([
                'company_id' => $rapport->company_id,
                'scan_id' => $rapport->scan_id,
                'scan_finished' => $rapport->finalized,
                'rapport_generated' => $rapport->finalized,
                'rapport_filename' => $rapport->rapport
            ]);
        }

        $totalDone = 0;

        $query = DB::select(DB::raw('SELECT DISTINCT user_id, scan_id FROM responses'));

        $total = count($query);

        foreach ($query as $userResponse) {
            if (isset($userResponse->user_id, $userResponse->scan_id)) {

                $user = \App\Models\User::find($userResponse->user_id);
                if(isset($user->company_id)){
                    $hasCompanyScan = CompanyScan::where('company_id', $user->company_id)
                        ->where('scan_id', $userResponse->scan_id)->exists();

                    if (!$hasCompanyScan) {

                        echo 'Adding company scan for company '. $user->company_id . ' - scan ' . $userResponse->scan_id . PHP_EOL;
                        CompanyScan::create([
                            'company_id' => $user->company_id,
                            'scan_id' => $userResponse->scan_id,
                            'scan_finished' => false,
                            'rapport_generated' => false,
                            'rapport_filename' => null,
                        ]);
                    }
                }


            }

            $totalDone += 1;
//            echo 'Done ' . $totalDone . '/' . $total .  '(' .($totalDone/$total*100). '%)' . PHP_EOL;
        }




	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
