<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNullableorderToAnswers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('answers', function(Blueprint $table)
		{
            DB::statement("ALTER TABLE companies
                                MODIFY COLUMN company_name VARCHAR(255) DEFAULT NULL,
                                MODIFY COLUMN association_type VARCHAR(255) DEFAULT NULL,
                                MODIFY COLUMN region INT(11) DEFAULT 0,
                                MODIFY COLUMN remark_question INT(11) DEFAULT 0;
                                ");

        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('answers', function(Blueprint $table)
		{
			
		});
	}

}
