<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class RemoveInvitesWithoutCompany extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$query = DB::table("companies");
		$companyIds = $query->lists('id');

		DB::table("invites")
			->whereNotIn('company_id', $companyIds)
			->groupBy('company_id')
			->delete();

		DB::statement("alter table invites add foreign key (company_id) references companies(id) on delete cascade");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}

}
