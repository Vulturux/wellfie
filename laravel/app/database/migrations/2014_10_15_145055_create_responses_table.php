<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResponsesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::drop('questions_responses');

        Schema::create('responses', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('questions_id');
            $table->integer('answer_id');
            $table->string('open_answer');
            $table->integer('user_id');
            $table->integer('scan_id');
            $table->timestamps();

        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        Schema::drop('responses');

    }

}
