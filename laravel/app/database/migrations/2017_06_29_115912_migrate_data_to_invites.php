<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrateDataToInvites extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $totalDone = 0;

        $query = DB::select(DB::raw('SELECT DISTINCT user_id, scan_id FROM responses'));

        $total = count($query);

        foreach ($query as $userResponse) {
            if (isset($userResponse->user_id, $userResponse->scan_id)) {

                $user = \App\Models\User::find($userResponse->user_id);
                if($user && $user->permission_level_id === 0 && isset($user->company_id)){
                    $hasInvite = \App\Models\Invite::where('company_id', $user->company_id)
                        ->where('scan_id', $userResponse->scan_id)->where('user_id', $userResponse->user_id)->exists();

                    if (!$hasInvite) {

                        echo 'Adding invite for user '. $user->id . ' - scan ' . $userResponse->scan_id . PHP_EOL;
                        \App\Models\Invite::create([
                            'email' => $user->email,
                            'invitecode' => uniqid(),
                            'company_id' => $user->company_id,
                            'user_id' => $user->id,
                            'scan_id' => $userResponse->scan_id,
                            'type' => 'werknemer',

                        ]);
                    }
                }


            }

            $totalDone += 1;
//            echo 'Done ' . $totalDone . '/' . $total .  '(' .($totalDone/$total*100). '%)' . PHP_EOL;
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
