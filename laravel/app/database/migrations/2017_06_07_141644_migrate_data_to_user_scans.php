<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\UserPosition;
use App\Models\UserScan;
use App\Models\User;


class MigrateDataToUserScans extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (UserPosition::all() as $userPosition) {
            $user = User::find($userPosition->user_id);
            if (isset($user)) {
                echo 'Adding user scan for user ' . $userPosition->user_id . ' - scan ' . $userPosition->scan_id . PHP_EOL;

                $userScan = UserScan::create([
                    'user_id' => $userPosition->user_id,
                    'scan_id' => $userPosition->scan_id,
                    'questionnaire_position' => $userPosition->position > 5 ? 5 : $userPosition->position
                ]);
            }
        }

        $totalDone = 0;

        $query = DB::select(DB::raw('SELECT DISTINCT user_id, scan_id FROM responses'));

        $total = count($query);

        foreach ($query as $userResponse) {
            if (isset($userResponse->user_id, $userResponse->scan_id)) {

                $user = \App\Models\User::find($userResponse->user_id);
                if ($user) {
                    $hasUserScan = UserScan::where('user_id', $user->id)
                        ->where('scan_id', $userResponse->scan_id)->exists();

                    if (!$hasUserScan) {

                        echo 'Adding user scan for user ' . $userResponse->user_id . ' - scan ' . $userResponse->scan_id . PHP_EOL;
                        UserScan::create([
                            'user_id' => $userResponse->user_id,
                            'scan_id' => $userResponse->scan_id,
                            'questionnaire_position' => 5,
                        ]);
                    }
                }


            }

            $totalDone += 1;
//            echo 'Done ' . $totalDone . '/' . $total .  '(' .($totalDone/$total*100). '%)' . PHP_EOL;
        }

        $rejectedUsers = [];

        UserScan::chunk(200, function ($userScans) use (&$rejectedUsers) {
            foreach ($userScans as $userScan) {
                $maxCatId = DB::select(DB::raw('
  SELECT MAX(question_cat_id) from responses
INNER JOIN questions on questions.id = responses.questions_id
INNER JOIN questions_titles on questions.question_titles_id = questions_titles.id
WHERE ' . $userScan->user_id . '= responses.user_id AND ' . $userScan->scan_id . '= responses.scan_id'));

                echo '----------------------------------------------------------------------------' . PHP_EOL;
                $user = User::find($userScan->user_id);
                if (isset($user)) {
                    if(isset($maxCatId[0]->max)) {
                        $isEmployer = $user->isEmployer();
                        $oldpos = $userScan->questionnaire_position;

                        if ($isEmployer && $userScan->questionnaire_position > $maxCatId[0]->max) {
                            $userScan->questionnaire_position = $maxCatId[0]->max;
                            $userScan->save();
                        } else if (!$isEmployer && $userScan->questionnaire_position > $maxCatId[0]->max - 5) {
                            $userScan->questionnaire_position = $maxCatId[0]->max - 5;
                            $userScan->save();
                        }

                        $newPos = $userScan->questionnaire_position;
                        echo 'max cat id: ' . $maxCatId[0]->max . PHP_EOL;
                        echo 'user_id: ' . $userScan->user_id . ', scan_id: ' . $userScan->scan_id . PHP_EOL;
                        echo 'oldPos: ' . $oldpos . ', newPos: ' . $newPos . PHP_EOL;
                    } else {
                        $userScan->questionnaire_position = 0;
                        $userScan->save();

                        array_push($rejectedUsers, [
                            'maxcatId' => $maxCatId[0]->max,
                            'user_id' => $user->id
                        ]);
                    }
                }
            }
        });




        echo '------------------------------------------------------------------'. PHP_EOL;
        echo '----------------------------REJECTED------------------------------' . PHP_EOL;
        echo '------------------------------------------------------------------' . PHP_EOL;
        echo '------------------------------------------------------------------' . PHP_EOL;
        if(isset($rejectedUsers) && count($rejectedUsers) > 0) {
            echo 'rejectedUsers count: ' . count($rejectedUsers) . PHP_EOL;
            foreach ($rejectedUsers as $ru) {
                if(isset($ru, $ru->maxcatid, $ru->user_id)){
                    echo 'maxcatId: ' . $ru->maxcatId . ' user_id: ' . $ru->user_id . PHP_EOL;
                }else {
                    echo 'rejecteduser not set' . PHP_EOL;
                }
            }
        }else {
            echo 'No rejected Users' . PHP_EOL;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
