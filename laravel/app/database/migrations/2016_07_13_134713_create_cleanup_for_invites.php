<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateCleanupForInvites extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		// ------ Remove duplicates email entries from invites
		$query = DB::select("select email, count(id) as cnt from invites GROUP BY email HAVING cnt > 1");

		$emails = [];
		foreach ($query as $q) {
			$query2 = DB::table('invites')
				->where('email', $q->email)
				->orderBy('updated_at')
				->get(['email', 'user_id', 'updated_at']);
			$emails[] = $query2;
		}

		foreach ($emails as $objects) {
			$registered = array_filter(
				$objects,
				function ($e) {
					return $e->user_id != null;
				}
			);

			// Delete all the ones that are not linked
			if ($registered) {
				DB::table('invites')
					->where('email', $objects[0]->email)
					->where('user_id', null)
					->delete();
			} else {
				// No registration found for current user, delete oldest invite
				$inv = DB::table('invites')
					->where('email', $objects[0]->email)
					->orderBy('updated_at', 'desc')->get(['id']);

				// Keep newest invite for email
				unset($inv[0]);

				// Delete all leftovers for email
				foreach($inv as $invited) {
					DB::table('invites')
						->where('id', $invited->id)
						->delete();
				}
			}
		}

		// ------ Remove duplicate invitecodes, only when one of those is already registered (invitecode is not needed anymore)

		// Select all duplicate invitecodes
		$query = DB::select('select invitecode, count(id) as cnt from invites GROUP BY invitecode HAVING cnt > 1');

		$code = [];
		foreach ($query as $q) {
			$code[] = $q->invitecode;
		}

		foreach ($code as $c) {
			$query2 = DB::table('invites')
				->where('invitecode', $c)
				->get();

			foreach($query2 as $q) {
				if ($q->user_id != null) {
					// Remove invitecode because user is registered.
					DB::statement("update invites set invitecode = null where id =".$q->id);
				}
			}
		}

		$duplicates = array(7790, 12628, 9271, 9561, 8830, 12909, 12260, 12586, 12574, 13091, 10119, 10712, 10115, 10686, 11904, 12841, 8836, 11335, 9625, 12907, 8766, 12812, 12039, 13048, 8865, 13012, 9425, 13069, 9522, 11349, 8756, 13086, 10734, 12610);
		$query3 = DB::table('invites')
			->whereIn($duplicates)
			->delete();

		// Make column invitecode unique
		DB::statement("ALTER TABLE invites ADD UNIQUE (invitecode)");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}

}
