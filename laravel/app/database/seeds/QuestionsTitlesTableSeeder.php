<?php

class QuestionsTitlesTableSeeder extends Seeder {

	public function run()
	{

        $data  = [
            ['title' => 'regio, werknemers,... (1-4)', 'question_cat_id' => 1, 'type' => 'empty'],
            ['title' => 'Zijn er in... (5-9)', 'question_cat_id' => 2, 'type' => 'empty'],
            ['title' => 'Duid voor volgende procedures het gepaste antwoord aan.', 'question_cat_id' => 2, 'type' => ''],
            ['title' => 'Krijgt iedereen (13-14)', 'question_cat_id' => 2, 'type' => 'empty'],
            ['title' => 'Voor de onderneming (15-17)', 'question_cat_id' => 2, 'type' => 'empty'],
            ['title' => 'Werknemers melden ...  (18-19)', 'question_cat_id' => 2, 'type' => 'empty'],
            ['title' => 'Duid voor volgende domeinen aan of dit belangrijk is, of er een beleid aanwezig is en of er acties ondernomen worden.', 'question_cat_id' => 2, 'type' => ''],
            ['title' => 'Werden in uw onderneming de laatste vijf jaar risicoanalyses uitgevoerd rond:', 'question_cat_id' => 2, 'type' => ''],
            ['title' => 'Stempt u (37-55)', 'question_cat_id' => 3, 'type' => 'empty'],
            ['title' => 'In welke mate gaat u akkoord met onderstaande stellingen? Duid het gepaste antwoord aan tussen 0 (helemaal niet akkoord) en 10 (helemaal akkoord).', 'question_cat_id' => 4, 'type' => 'onetoten'],
            ['title' => 'Mijn werknemers... (60-66)', 'question_cat_id' => 4, 'type' => 'empty'],
            ['title' => 'Moeten in uw onderneming werknemers', 'question_cat_id' => 5, 'type' => ''],
            ['title' => 'Wordt bij het ontwerp van een nieuwe werkpost of aankoop van nieuw materiaal', 'question_cat_id' => 5, 'type' => ''],
            ['title' => 'Bij werknemers met lichamelijke klachten', 'question_cat_id' => 5, 'type' => ''],
            ['title' => 'Is er bereidheid (81 - 89)', 'question_cat_id' => 5, 'type' => 'empty'],
            ['title' => 'geslacht, regio, werknemers, ... (1-11)', 'question_cat_id' => 6, 'type' => 'empty'],
            ['title' => 'Wanneer u door ziekte  (12-13)', 'question_cat_id' => 7, 'type' => 'empty'],
            ['title' => 'Heeft u aandoeningen of (14-26)', 'question_cat_id' => 7, 'type' => 'empty'],
            ['title' => 'Hoe vindt u het huidige niveau van uw werkvermogen (27-30)', 'question_cat_id' => 7, 'type' => 'empty'],
            ['title' => 'Hoeveel dagen .. (31)', 'question_cat_id' => 7, 'type' => 'empty'],
            ['title' => 'Hoeveel keer bleef... (32-34)', 'question_cat_id' => 7, 'type' => 'empty'],
            ['title' => 'Ik voel mij opgebrand door mijn werk ... (35-40)', 'question_cat_id' => 7, 'type' => 'empty'],
            ['title' => 'Vindt u het belangrijk ... (41-43)', 'question_cat_id' => 7, 'type' => 'empty'],
            ['title' => 'Kent u de kanalen... (46-63)', 'question_cat_id' => 8, 'type' => 'empty'],
            ['title' => 'In welke mate gaat u akkoord met onderstaande stellingen? Duid het gepaste antwoord aan tussen 0 (helemaal niet akkoord) en 10 (helemaal akkoord).', 'question_cat_id' => 9, 'type' => 'onetoten'],
            ['title' => 'Ik kan met anderen... (72-81)', 'question_cat_id' => 9, 'type' => 'empty'],
            ['title' => 'Ik doe moeite voor mijn job...', 'question_cat_id' => 9, 'type' => 'onetoten'],
            ['title' => 'Moet u tijdens uw werk', 'question_cat_id' => 10, 'type' => ''],
            ['title' => 'Vindt u dat uw onderneming voldoende ergonomische aanpassingen doet om uw werkbelasting te beperken? ', 'question_cat_id' => 10, 'type' => ''],
            ['title' => '', 'question_cat_id' => 10, 'type' => 'empty'],

        ];

        DB::table('questions_titles')->truncate();
        QuestionsTitle::insert($data);


    }

}