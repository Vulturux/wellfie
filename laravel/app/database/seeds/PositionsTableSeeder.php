<?php

use App\Models\Position;

class PositionsTableSeeder extends Seeder {

	protected $attributes = ['title', 'pos_id', 'cat_id', 'path'];

 	protected $positions = [
        [
            'title' => 'Introductie',
            'pos_id' => -1,
        ],
        [
            'title' => 'Demografische variabelen',
            'path' => 'demografische-variabelen',
            'pos_id' => 0,
            'cat_id' => 1,
        ],
        [
            'title' => 'Gezondheid en functionele capaciteiten',
            'path' => 'gezondheid-functionele-capaciteiten',
            'pos_id' => 1,
            'cat_id' => 2,
        ],
        [
            'title' => 'Competenties',
            'path' => 'competenties',
            'pos_id' => 2,
            'cat_id' => 3,
        ],
        [
            'title' => 'Waarden, houding en motivatie',
            'path' => 'waarden-houding-motivatie',
            'pos_id' => 3,
            'cat_id' => 4,
        ],
        [
            'title' => 'Werk, werkgemeenschap en leiding',
            'path' => 'werk-werkgemeenschap-leiding',
            'pos_id' => 4,
            'cat_id' => 5,
        ],
        [
            'title' => 'Eindresultaat',
            'pos_id' => 5,
        ],
        [
            // 'title' => 'Communiceren – organiseren – actie',
            'title' => 'Hoe uitnodigen?',
            'pos_id' => 6,
        ],
        [
            'title' => 'Werknemers uitnodigen',
            'pos_id' => 7,
        ],
        [
            'title' => 'Opvolging',
            'pos_id' => 8,
        ],
        [
            'title' => 'Hoe resultaten interpreteren?',
            'pos_id' => 9,
        ],
        [
            'title' => 'Totaalresultaat',
            'pos_id' => 10,
        ]
    ];

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('positions')->truncate();

		foreach ($this->positions as $pos) {
			
			$newPosition = [];

			foreach ($this->attributes as $attr) {

				$newPosition[$attr] = isset($pos[$attr]) ? $pos[$attr] : null;
			}
			
			Position::create($newPosition);
		}

	}

}