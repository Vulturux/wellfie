<?php


class WerkgeverWerknemerTableSeeder extends Seeder {

	public function run()
	{

        $data  = [
            ['werkgever_werknemer' => 'werkgever'],
            ['werkgever_werknemer' => 'werknemer'],
        ];

        DB::table('werkgever_werknemer')->truncate();
        WerkgeverWerknemer::insert($data);


	}

}