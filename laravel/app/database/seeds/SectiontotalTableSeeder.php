<?php

class SectiontotalTableSeeder extends Seeder {


    public function run()
    {
        $data  = [

            ['section_title' =>	'Medische vragen' ,'questions_cat_id' =>2,'total' => 19, 'maxgood' => 0, 'maxmodarate' => 1],
            ['section_title' =>	'Levensstijl' ,'questions_cat_id' =>2,'total' => 25, 'maxgood' => 1, 'maxmodarate' => 3],
            ['section_title' =>	'Werkvermogen' ,'questions_cat_id' =>6,'total' => 20, 'maxgood' => 9, 'maxmodarate' => 15],
            ['section_title' =>	'Verzuim' ,'questions_cat_id' =>2,'total' => 5, 'maxgood' => 1, 'maxmodarate' => 3],
            ['section_title' =>	'Burnout en Bevlogenheid ' ,'questions_cat_id' =>2,'total' => 21, 'maxgood' => 6, 'maxmodarate' => 13],

            ['section_title' =>	'Zichtbaarheid op arbeidsmarkt' ,'questions_cat_id' =>3,'total' => 6, 'maxgood' => 1, 'maxmodarate' => 3],
            ['section_title' =>	'Talentmanagement' ,'questions_cat_id' =>3,'total' => 12, 'maxgood' => 3, 'maxmodarate' => 7],

            ['section_title' =>	'Waarden en normen' ,'questions_cat_id' =>4,'total' => 4, 'maxgood' => 0, 'maxmodarate' => 2],
            ['section_title' =>	'Persoonskenmerken' ,'questions_cat_id' =>4,'total' => 5, 'maxgood' => 1, 'maxmodarate' => 3],

            ['section_title' =>	'Werk' ,'questions_cat_id' =>5,'total' => 13, 'maxgood' => 3, 'maxmodarate' => 8],


        ];

        DB::table('sectiontotals')->truncate();
        Sectiontotal::insert($data);
    }

}