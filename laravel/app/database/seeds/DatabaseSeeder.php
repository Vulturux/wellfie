<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->call('WerkgeverWerknemerTableSeeder');
        $this->command->info('1 WerkgeverWerknemer  : table seeded!');

        $this->call('QuestionsCategoriesTableSeeder');
        $this->command->info('2 QuestionsCategories  : table seeded!');

        $this->call('QuestionsTitlesTableSeeder');
        $this->command->info('3 QuestionsTitles  : table seeded!');

        $this->call('QuestionsTableSeeder');
        $this->command->info('4 Questions  : questions table seeded!');

        $this->call('AnswersTableSeeder');
        $this->command->info('5 Answers  : answers table seeded!');

        $this->call('SectiontotalTableSeeder');
        $this->command->info('6 total sections  : table seeded!');



    }

}
