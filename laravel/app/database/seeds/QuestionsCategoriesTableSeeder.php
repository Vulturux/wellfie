<?php

class QuestionsCategoriesTableSeeder extends Seeder {

    public function run()
    {

        $data  = [
            ['category' => 'Demografische variabelen (WG)', 'werkgever_werknemer' => 1],
            ['category' => 'Gezondheid en functionele capaciteiten (WG)', 'werkgever_werknemer' => 1],
            ['category' => 'Competenties (WG)', 'werkgever_werknemer' => 1],
            ['category' => 'Waarden, houding en motivatie (WG)', 'werkgever_werknemer' => 1],
            ['category' => 'Werk, werkgemeenschap en leiding (WG)', 'werkgever_werknemer' => 1],
            ['category' => 'Demografische variabelen (WN)', 'werkgever_werknemer' => 2],
            ['category' => 'Gezondheid en functionele capaciteiten (WN)', 'werkgever_werknemer' => 2],
            ['category' => 'Competenties (WN)', 'werkgever_werknemer' => 2],
            ['category' => 'Waarden, houding en motivatie (WN)', 'werkgever_werknemer' => 2],
            ['category' => 'Werk, werkgemeenschap en leiding (WN)', 'werkgever_werknemer' => 2],

        ];

        DB::table('questions_categories')->truncate();
        QuestionsCategory::insert($data);


    }


}