<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'WebsiteController@index');
Route::get('falseinvitation/', 'WebsiteController@falseinvitation');
Route::get('werkvermogen/', 'WebsiteController@werkvermogen');
Route::get('hoebouwenaanwerkvermogen/', 'WebsiteController@hoebouwenaanwerkvermogen');
Route::get('links/', 'WebsiteController@links');
Route::get('overons/', 'WebsiteController@overons');
Route::get('faq/', 'WebsiteController@faq');

Route::get('documentatie/file/{file}', 'WebsiteController@documentationFile');
Route::get('documentatie/{type?}', 'WebsiteController@documentation');

Route::get('nieuws/{id?}', 'WebsiteController@events');

Route::get('pdftest/', 'WebsiteController@pdftest');
Route::get('pdftestwithassets/', 'WebsiteController@pdftestwithassets');
Route::get('pdftestwithoutassets/', 'WebsiteController@pdftestwithoutassets');
Route::get('pdftestwithassetsview/', 'WebsiteController@pdftestwithassetsview');
Route::get('pdftestwithoutassetsview/', 'WebsiteController@pdftestwithoutassetsview');
// WERKNEMERS
Route::group(array('before' => ['auth','permission.WN']), function()
{
    Route::get('werknemers', ['uses' => 'WerknemersController@stage', 'before' => 'check.employee.position']);
    Route::get('werknemers/stage/{stage?}', [ 'as' => 'wn.stage', 'uses' => 'WerknemersController@stage', 'before'=> 'check.employee.position']);
    Route::post('werknemers/postanswers', 'WerknemersController@postanswers');
    Route::get('werknemers/tussenresultaat/{pos?}',   array('uses'=>'WerknemersController@tussenresultaat', 'before'=>'intermediateresult.position'));
    Route::get('werknemers/eindresultaat', [ 'as' => 'wn.eindresultaat', 'uses' => 'WerknemersController@eindresultaat', 'before'=> 'check.employee.position']);
    Route::get('werknemers/pdfexport', 'WerknemersController@pdfexport');
    Route::get('werknemers/score', 'WerknemersController@score');
    Route::get('werknemers/geenActieveScan', 'WerknemersController@geenActieveScan');
});

// WERKGEVERS
Route::group(array('before' => ['auth','permission.WG']), function()
{
    Route::get('werkgevers', 'WerkgeversController@wellfieprocess');
    Route::get('werkgevers/wellfieprocess', 'WerkgeversController@wellfieprocess');

    Route::get('werkgevers/intro', [ 'as'   => 'wg.intro', 'uses' => 'WerkgeversController@intro', 'before' => 'isScanFinished' ]);
    Route::get('werkgevers/stage/{position?}', [ 'as' => 'wg.stage', 'uses' => 'WerkgeversController@stage', 'before' => 'check.position']);
    Route::get('werkgevers/pdfexport/{scanid?}', 'WerkgeversController@pdfexport');
    Route::get('werkgevers/eindresultaat/{scanid}',  array('uses'=>'WerkgeversController@eindresultaat'));
    Route::get('werkgevers/wellfierapport/{scanid?}/{nonav?}', ['as' => 'wg.welffierapport', 'uses' => 'WerkgeversController@wellfierapport']);
    Route::get('werkgevers/wellfiePdfRapport/{scanid?}/{userid?}/{companyid?}', 'WerkgeversController@wellfiePdfRapport');
    Route::post('werkgevers/postanswers', 'WerkgeversController@postanswers');
    Route::get('werkgevers/scanafsluiten', [ 'as' => 'wg.hoeresultateninterpreteren', 'uses' => 'WerkgeversController@finalizeScan']);


    //dashboard
    Route::get('werkgevers/dashboard', 'DashboardController@index');

    Route::get('werkgevers/werknemersuitnodigen', [ 'as' => 'wg.werknemersuitnodigen', 'uses' => 'DashboardController@werknemersuitnodigen', 'before' => 'canDoInvites']);
    Route::post('werkgevers/postWerknemersuitnodigen', ['uses' => 'DashboardController@postWerknemersuitnodigen', 'before' => 'canDoInvites']);
    Route::post('werkgevers/postinvite', ['uses' => 'DashboardController@postinvite', 'before' => 'canDoInvites']);

    Route::get('werkgevers/uitgenodigdewerknemers', 'DashboardController@uitgenodigdewerknemers');
    Route::post('werkgevers/reinvite/{userid?}',   array('uses'=>'DashboardController@reinvite'));
    Route::get('werkgevers/startnewscan', ['uses' => 'DashboardController@startnewscan', 'before' => 'isScanFinished']);

    // AJAX
    Route::get('ajax/check-rapport/{scanId}', 'WerkgeversController@checkRapport');
    Route::get('werkgevers/ajax/previous-scan-invite-emails', 'WerkgeversController@getPreviousScanInviteEmails');
});

Route::get('/ajax/check-export/{filename}', ['uses' => 'SuperadminController@checkForExport', 'before'=> 'permission.admin']);


// SUPERADMIN
Route::group(array('before' => ['auth','permission.admin']), function()
{
    Route::get('superadmin', 'SuperadminController@index');
    Route::get('superadmin/werknemers', 'SuperadminController@werknemers');
    Route::get('superadmin/werknemers/data', 'SuperadminController@werknemersData');
	Route::get('superadmin/werknemers/{id}/responses',   array('uses'=>'SuperadminController@werknemerResponses'));
	Route::get('superadmin/werknemers/{id}/edit',   array('uses'=>'SuperadminController@werknemerEdit'));
	Route::post('superadmin/werknemers/{id}/edit',   array('uses'=>'SuperadminController@werknemerUpdate'));
	Route::delete('superadmin/werknemers/{id}/edit',   array('uses'=>'SuperadminController@werknemerDelete'));



	Route::get('superadmin/export/{id?}', 'SuperadminController@export');
	Route::get('superadmin/exportlist', 'SuperadminController@exportlist');
	Route::get('superadmin/export/download/{path}', 'SuperadminController@exportDownload');
    Route::post('superadmin/exportaction', 'SuperadminController@exportaction');


	Route::get('superadmin/bedrijven', 'SuperadminController@bedrijven');

	Route::get('superadmin/bedrijven/{id?}/view-rapport', 'SuperadminController@viewCompanyRapport');
    Route::get('superadmin/bedrijven/{id?}/download-rapport', 'SuperadminController@downloadCompanyRapport');
    Route::get('superadmin/bedrijven/{id?}/delete-rapport', 'SuperadminController@deleteCompanyRapport');

	Route::get('superadmin/bedrijven/{id}', 'SuperadminController@bedrijvenDetail');
	Route::get('superadmin/bedrijven/{id}/edit', 'SuperadminController@bedrijvenEdit');
	Route::post('superadmin/bedrijven/{id}/edit', 'SuperadminController@bedrijvenUpdate');
	Route::delete('superadmin/bedrijven/{id}/edit', 'SuperadminController@bedrijvenDelete');

	Route::get('superadmin/impersonate/{id}', 'SuperadminController@impersonate');

    Route::resource('superadmin/questions', 'QuestionsController');
    Route::resource('superadmin/answers', 'AnswersController');
    Route::resource('superadmin/faq', 'FaqController');
    Route::resource('superadmin/documentation', 'DocumentationController');
    Route::resource('superadmin/events', 'EventController');

});

App::missing(function($exception)
{
    return Redirect::to('/');
});


// DUMP -> remove
Route::get('survey/werknemers', 'SurveyController@werknemers');

// Custom Confide routes
Route::get('auth', 'AuthController@index');
Route::get('auth/login', 'AuthController@login');
Route::get('auth/signup', 'AuthController@signup');
Route::get('auth/forgotpassword', 'AuthController@forgotpassword');
// Confide routes

Route::get('users/create', 'UsersController@create');
Route::get('users/createfrominvite/{invitecode?}',   array('uses'=>'UsersController@createfrominvite'));

Route::post('users/store', 'UsersController@store');
Route::post('users/storefrominvite', 'UsersController@storefrominvite');

Route::get('login', 'UsersController@login');
Route::get('users/', 'UsersController@login');
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
