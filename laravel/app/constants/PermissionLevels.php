<?php

namespace App\Constants;

/**
 * Class PermissionLevels
 *
 * @package \App\Constants
 */
class PermissionLevels {

	const SUPERADMIN = 9;
	const EMPLOYER = 6;
	const USER = 0;
}
