<?php namespace App\Handlers\Events;

use App\Queue\ExportJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class GenerateWellfieExportHandler{

    public function handle($data)
    {
        try {

            Queue::push(function($job) use ($data)
            {
                $exportJob = new ExportJob();
                $exportJob->fire($job, $data);
            });
        } catch (\Exception $e)
        {
            Log::warning("Wellfie-Export queue: " . $e->getMessage());
        }
    }
}