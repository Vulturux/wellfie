<?php namespace App\Handlers\Events;

use App\Queue\RapportJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class GenerateCompanyRapportHandler{

    public function handle($data)
    {
        try {

            Queue::push(function($job) use ($data)
            {
                $rapportJob = new RapportJob();
                $rapportJob->fire($job, $data);
            });
        } catch (\Exception $e)
        {
            Log::warning("Export queue: " . $e->getMessage());
        }
    }
}