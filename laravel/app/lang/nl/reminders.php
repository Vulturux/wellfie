<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/
	
	"password" => "Een paswoord op z'n minst zes karaters bevatten en overeenkomen met de paswoord bevestiging.",

	"user" => "We kunnen geen gebruiker vinden met dit e-mail adres.",

	"token" => "Je paswoord reset token is ongeldig.",

	"sent" => "Paswoord herrinering is verstuurd !",


);
