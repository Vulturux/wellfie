<?php

return array(

    'heading_company_detail' => 'Detail bedrijf',
    'company' => 'Bedrijf',
    'company_number' => 'Ondernemingsnummer',
    'email' => 'Email',
    'position' => 'Positie',
    'association_type' => 'Ondernemingsvorm',
    'region' => 'Regio',
    'defaultInviteMailMessage' => "Beste,
        
Met onze onderneming willen we het leeftijdsbewust welzijns- en personeelsbeleid verder ontplooien. Dergelijk beleid mikt op een optimale ontwikkeling, benutting en behoud van kennis en capaciteiten van medewerkers in alle leeftijdsgroepen.
        
Daarom nemen we deel aan het ‘Wellfie’-project (Wellfie staat voor: een selfie van het welbevinden in onze onderneming). Uw stem als medewerker van deze onderneming is van groot belang. Wij vragen u daarom in te loggen en de vragenlijst in te vullen. Deze vragen gaan over aspecten van uw gezondheid, vaardigheden, waarden en werk.
        
Alvast bedankt voor uw medewerking!
        
Bedrijfsleider en ‘Wellfie’-promotor
",

);
