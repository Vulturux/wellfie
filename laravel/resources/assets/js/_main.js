$(document).ready(function () {

    $(document).foundation();

    $(function () {

        // PUBLIC  -----------------------------
        // -----------------------------

        $("#disclaimer-checkbox").change(function () {
            if (this.checked) {
                //Do stuff
                //$('.btn-submit').prop("disabled", false); // Element(s) are now enabled.
                $('.false-button').hide();
                $('.main-button').show();
            } else {

                $('.false-button').show();
                $('.main-button').hide();

            }
        });

        $('.js-btn-submit-disabled').on("click", function (e) {
            e.preventDefault();
            $(".js-group-disclaimer-checkbox").fadeIn(300).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
        });


        // VRAGENLIJST
        $('.js-questions-show-info').on("click", function () {

            $(this).parent().parent().find(".info").slideDown("slow", function () {
                // Animation complete.
            });
            $(this).hide();
        });

        $('.js-answers-show-info').on("click", function () {

           // console.log('answers')
            $(this).parent().parent().find(".answers-info").slideDown("slow", function () {
                // Animation complete.
            });
            $(this).hide();

            //because this is inside the label undo the action
            checkparent = $(this).parent().parent().find('input');
            if (checkparent.prop('checked') == true) {
                $(checkparent).prop("checked", false);
            } else if (checkparent.prop('checked') == false) {
                $(checkparent).prop("checked", true);
            }


        });

    });


//copy to clipboard
    $('.copytoclipboard').click(function (e) {
        e.preventDefault();
        var $this = $(this),
            $target = $($this.data('target'));

        if (copyToClipboard($target[0])) {
            $this.find('i').toggleClass('fa-clipboard', false).toggleClass('fa-check', true).toggleClass('fa-times', false);
        } else {
            $this.find('i').toggleClass('fa-clipboard', false).toggleClass('fa-check', false).toggleClass('fa-times', true);
        }

        setTimeout(function () {
            $this.find('i').toggleClass('fa-clipboard', true).toggleClass('fa-check', false).toggleClass('fa-times', false);

        }, 1000)
    });

});

//http://jsfiddle.net/88Ms2/378/
var videoEmbed = {
    invoke: function ($el) {

        $el.html(function (i, html) {
            return videoEmbed.convertMedia(html);
        });

    },
    convertMedia: function (html) {
        html = $.trim(html);
        var pattern1 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
        var pattern2 = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g;

        var replacement;


        if (pattern1.test(html)) {
            replacement = '<iframe width="420" height="345" src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            html = html.replace(pattern1, replacement);
        }else if (pattern2.test(html)) {
            replacement = '<iframe width="420" height="345" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>';
            html = html.replace(pattern2, replacement);
        }else if (html.indexOf('.jpg') > -1 || html.indexOf('.png') > -1 || html.indexOf('.gif') > -1) {
            html = '<a href="' + html + '" target="_blank"><img class="sml" src="' + html + '" /></a><br />';
        } else {
            html = '<a href="' + html + '" target="_blank">' + html + '<br />';

        }

        return html;
    }
};

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
