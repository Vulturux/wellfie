# Fagrant, madewithlove' development box

## How to use for your project

Download the Vagrantfile and move those to the directory where you are starting your project.

### Vagrantfile

This vagrant file uses [Vaprobash](https://github.com/fideloper/Vaprobash) to provision the box.

If you want to add extra stuff, take a look at the original [Vaprobash Vagrantfile](https://github.com/fideloper/Vaprobash/blob/master/Vagrantfile). You can simply add one of those lines and it'll work.


## Setting up

You need to change the following things on a project basis:

### IP address and hostname

On line `17` you need to set the IP you want to use, make it unique across other VM's. Make sure to check [here](https://madewithlove.atlassian.net/wiki/display/DT/Vagrant+boxes+hostname+list) wich IP is the next free one available.

	server_ip = "192.168.56.10x"

On line `68` you can set the hostname.

	 config.vm.hostname = "fagrant.dev"

Please also add your combination of IP and hostname to [the confluence page](https://madewithlove.atlassian.net/wiki/display/DT/Vagrant+boxes+hostname+list).

Don't forget to edit your hostsfile to match the combination of IP and hostname.

	sudo nano /etc/hosts

Add this:

	192.168.56.10x fagrant.dev


###Boot it up

That should be it. To start your Vagrant machine, type:

    vagrant up
	
	
### Once it's running

If the provisioning is done, and you see the default apache page it means you need to remove the default virtual host.

	sudo rm /etc/apache2/sites-available/000-default.conf
	sudo rm /etc/apache2/sites-enabled/000-default.conf
	sudo service apache2 restart
	
### Mailcatcher

MailCatcher runs a super simple SMTP server which catches any message sent to it to display in a web interface. Send your mails via 127.0.0.1:1025 on the machine. Then check out http://hostname.dev:1080 to see the mail that's arrived so far.

## What's in the box
This Vagrant box includes the following packages:

- Oh-My-Zsh
- PHP 5.5
- Apache
- MySQL (user: root, pass: root)
- SQLite
- MongoDB
- ElasticSearch
- ElasticHQ
- Supervisord
- Memcache
- Composer
- NodeJS
- npm
- Grunt
- Ruby
- bundle
- bower
- Compass
- Sass
- Mailcatcher
