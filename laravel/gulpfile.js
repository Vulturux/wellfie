process.env.DISABLE_NOTIFIER = true;

var elixir = require('laravel-elixir');

elixir(function (mix) {
    mix.copy('resources/assets/images', 'public/images');
    mix.copy('resources/assets/vendor/js', 'public/js');
    mix.copy('resources/assets/sass/foundation-icons/fonts', 'public/css/fonts');
    mix.copy('resources/assets/vendor/css/font-awesome-4.7.0/fonts', 'public/css/fonts');


    mix.scriptsIn('resources/assets/js', 'public/js/script-all.js');

    mix.sass('foundation-icons/foundation-icons.css');
    mix.sass('foundation/foundation.scss', 'public/css/foundation.css');

    mix.sass("style-front-design.scss");
    mix.sass("style-front-responsive.scss");
    mix.sass("style-design.scss");
    mix.sass("style-responsive.scss");
});

