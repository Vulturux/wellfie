<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\CompanyRapport
 *
 * @property integer $id 
 * @property integer $company_id 
 * @property integer $scan_id 
 * @property boolean $finalized 
 * @property string $rapport 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read \App\Models\Company $company 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyRapport whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyRapport whereCompanyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyRapport whereScanId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyRapport whereFinalized($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyRapport whereRapport($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyRapport whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyRapport whereUpdatedAt($value)
 */
	class CompanyRapport {}
}

namespace App\Models{
/**
 * App\Models\Company
 *
 * @property integer $id 
 * @property string $company_name 
 * @property string $ondernemingsnummer 
 * @property string $association_type 
 * @property string $region 
 * @property string $remark_question 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property boolean $finalized 
 * @property string $rapport 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyRapport[] $rapports 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompanyScan[] $companyScans 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereCompanyName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereOndernemingsnummer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereAssociationType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereRemarkQuestion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereFinalized($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Company whereRapport($value)
 */
	class Company {}
}

namespace App\Models{
/**
 * App\Models\UserPosition
 *
 * @property integer $id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $position 
 * @property integer $scan_id 
 * @property integer $user_id 
 * @property integer $position_max 
 * @property-read \App\Models\User $user 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserPosition whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserPosition whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserPosition whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserPosition wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserPosition whereScanId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserPosition whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserPosition wherePositionMax($value)
 * @method static \App\Models\UserPosition companyUserPositions($companyId)
 */
	class UserPosition {}
}

namespace App\Models{
/**
 * App\Models\QuestionsTitle
 *
 * @property integer $id 
 * @property string $title 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $question_cat_id 
 * @property string $type 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Question[] $question 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsTitle whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsTitle whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsTitle whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsTitle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsTitle whereQuestionCatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsTitle whereType($value)
 */
	class QuestionsTitle {}
}

namespace App\Models{
/**
 * App\Models\EventModel
 *
 * @property integer $id 
 * @property string $title 
 * @property string $description 
 * @property \Carbon\Carbon $datetime 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventModel whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventModel whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventModel whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventModel whereDatetime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventModel whereUpdatedAt($value)
 */
	class EventModel {}
}

namespace App\Models{
/**
 * App\Models\WerkgeverWerknemer
 *
 * @property integer $id 
 * @property string $werkgever_werknemer 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WerkgeverWerknemer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WerkgeverWerknemer whereWerkgeverWerknemer($value)
 */
	class WerkgeverWerknemer {}
}

namespace App\Models{
/**
 * App\Models\UserScan
 *
 * @property integer $id 
 * @property integer $user_id 
 * @property integer $scan_id 
 * @property integer $questionnaire_position 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read User $user 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserScan whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserScan whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserScan whereScanId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserScan whereQuestionnairePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserScan whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserScan whereUpdatedAt($value)
 */
	class UserScan {}
}

namespace App\Models{
/**
 * App\Models\Invite
 *
 * @property integer $id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $email 
 * @property string $invitecode 
 * @property integer $company_id 
 * @property integer $user_id 
 * @property integer $scan_id 
 * @property string $type 
 * @property-read User $user 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereInvitecode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereCompanyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereScanId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invite whereType($value)
 */
	class Invite {}
}

namespace App\Models{
/**
 * App\Models\UserResponse
 *
 * @property integer $id 
 * @property integer $questions_id 
 * @property integer $answer_id 
 * @property string $open_answer 
 * @property integer $user_id 
 * @property integer $scan_id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $type 
 * @property-read \App\Models\Question $question 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResponse whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResponse whereQuestionsId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResponse whereAnswerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResponse whereOpenAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResponse whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResponse whereScanId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResponse whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResponse whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\UserResponse whereType($value)
 */
	class UserResponse {}
}

namespace App\Models{
/**
 * App\Models\CompanyScan
 *
 * @property integer $id 
 * @property integer $company_id 
 * @property integer $scan_id 
 * @property boolean $scan_finished 
 * @property boolean $rapport_generated 
 * @property string $rapport_filename 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyScan whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyScan whereCompanyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyScan whereScanId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyScan whereScanFinished($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyScan whereRapportGenerated($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyScan whereRapportFilename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyScan whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompanyScan whereUpdatedAt($value)
 */
	class CompanyScan {}
}

namespace App\Models{
/**
 * App\Models\QuestionType
 *
 */
	class QuestionType {}
}

namespace App\Models{
/**
 * App\Models\Response
 *
 * @property integer $id 
 * @property integer $questions_id 
 * @property integer $answer_id 
 * @property string $open_answer 
 * @property integer $user_id 
 * @property integer $scan_id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $type 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Response whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Response whereQuestionsId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Response whereAnswerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Response whereOpenAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Response whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Response whereScanId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Response whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Response whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Response whereType($value)
 */
	class Response {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property integer $id 
 * @property string $username 
 * @property string $email 
 * @property string $password 
 * @property string $confirmation_code 
 * @property integer $company_id 
 * @property integer $permission_level_id 
 * @property string $remember_token 
 * @property integer $confirmed 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $function 
 * @property-read \App\Models\Company $company 
 * @property-read \App\Models\UserPosition $UserPosition 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserScan[] $UserScans 
 * @property-read \Illuminate\Database\Eloquent\Collection|Invite[] $invites 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereConfirmationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCompanyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePermissionLevelId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereConfirmed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereFunction($value)
 */
	class User {}
}

namespace App\Models{
/**
 * App\Models\QuestionsCategory
 *
 * @property integer $id 
 * @property string $category 
 * @property integer $werkgever_werknemer 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsCategory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsCategory whereCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsCategory whereWerkgeverWerknemer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\QuestionsCategory whereUpdatedAt($value)
 */
	class QuestionsCategory {}
}

namespace App\Models{
/**
 * App\Models\Question
 *
 * @property integer $id 
 * @property string $question 
 * @property integer $question_nr 
 * @property string $order 
 * @property string $more_info 
 * @property integer $type 
 * @property string $layout_type_class 
 * @property integer $question_connection_id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $question_titles_id 
 * @property integer $totalsection_id 
 * @property integer $status 
 * @property-read \App\Models\QuestionsTitle $questionTitle 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereQuestion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereQuestionNr($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereMoreInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereLayoutTypeClass($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereQuestionConnectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereQuestionTitlesId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereTotalsectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Question whereStatus($value)
 */
	class Question {}
}

namespace App\Models{
/**
 * App\Models\Answer
 *
 * @property integer $id 
 * @property integer $questions_id 
 * @property string $answer 
 * @property float $points 
 * @property integer $order 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $more_info 
 * @property integer $status 
 * @property-read \App\Models\Question $question 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereQuestionsId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer wherePoints($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereMoreInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Answer whereStatus($value)
 */
	class Answer {}
}

namespace App\Models{
/**
 * App\Models\Documentation
 *
 * @property integer $id 
 * @property string $title 
 * @property string $description 
 * @property string $type 
 * @property string $link 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Documentation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Documentation whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Documentation whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Documentation whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Documentation whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Documentation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Documentation whereUpdatedAt($value)
 */
	class Documentation {}
}

namespace App\Models{
/**
 * App\Models\QuestionsConnection
 *
 */
	class QuestionsConnection {}
}

namespace App\Models{
/**
 * App\Models\Faq
 *
 * @property integer $id 
 * @property string $vraag 
 * @property string $category_naam 
 * @property string $image 
 * @property string $antwoord 
 * @property integer $order 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereVraag($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereCategoryNaam($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereAntwoord($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Faq whereUpdatedAt($value)
 */
	class Faq {}
}

namespace App\Models{
/**
 * App\Models\Position
 *
 * @property integer $id 
 * @property string $title 
 * @property integer $pos_id 
 * @property integer $cat_id 
 * @property string $path 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Position whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Position whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Position wherePosId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Position whereCatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Position wherePath($value)
 * @method static \App\Models\Position stageCategories()
 */
	class Position {}
}

namespace App\Models{
/**
 * App\Models\Sectiontotal
 *
 * @property integer $id 
 * @property string $section_title 
 * @property integer $questions_cat_id 
 * @property integer $total 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property integer $maxgood 
 * @property integer $maxmodarate 
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sectiontotal whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sectiontotal whereSectionTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sectiontotal whereQuestionsCatId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sectiontotal whereTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sectiontotal whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sectiontotal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sectiontotal whereMaxgood($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Sectiontotal whereMaxmodarate($value)
 */
	class Sectiontotal {}
}

